<?php

//Mostramos errores
ini_set("display_errors", 1);

$project_name = "/projects/mosquiflex/";
//Constantes
define("empresa", 'MT');
define("root", $_SERVER['DOCUMENT_ROOT'] . $project_name);
define("path_web", $project_name . "web/");
define("path_css", $project_name . "css/");
define("path_js", $project_name . "js/");
define("path_web_mods", $project_name . "web/mods/");
define("path_admin", $project_name . "admin/");
define("path_image", $project_name . "images/");
define("path_image_colors", $project_name . "images/images_colors/");
define("path_image_croquis", $project_name . "images/croquis_product");
define("path_image_components", $project_name . "images/images_components");
define("path_files_get", $project_name . "as400/get/");
define("path_files_put", root . "as400/put/");
define("path_as400", root . "as400/");
define("path_class", root . "class/");
define("path_excels", root . "scripts/excels/");
define("path_external", root . 'external_libreries/');
define("path_cache", root . 'cache/');
define("path_uploads_files", root . 'uploads_files/');
define("path_pdfs", root . 'pdfs_presupuestos_pedidos/');
define("url_web", "http://localhost:8888/");

//Idioma
include_once 'controller_lang.php';

//Clases
require 'class/functions.php';
require 'class/conf.php';
require 'class/db.php';
require 'class/lang.php';
require 'class/familia.php';
require 'class/customers.php';
require 'class/provincias.php';
require 'class/presupuesto.php';
require 'class/Cache.php';
require 'class/conn_odbc.php';
require 'class/odbc.php';
require 'class/phpmailer/SMTP.php';
require 'class/phpmailer/Exception.php';
require 'class/phpmailer/PHPMailer.php';
require 'class/redsys/apiRedsys.php';

//constante dispositivo
define("dispositivo", comprobar_dispositivo());

//login_time();
?>