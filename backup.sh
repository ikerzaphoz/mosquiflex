#!/bin/bash
# crea una copia de seguridad de una base de datos
########################
##### variables a editar
#
DB_USER=sql_mos17
DB_PASS=t5u8KoB4
DB_NAME=mosquiflex
BACKUP_DIR=//home/mosquiflex/www/backup/
#
##### fin de variables a editar
########################
BACKUP_FILE=${BACKUP_DIR}$(date +%Y%m%d)-${DB_NAME}.sql
# usamos mysqldump para hacer la copia de seguridad que se guarda en BACKUP_DIR
mysqldump --opt -u ${DB_USER} -p${DB_PASS} ${DB_NAME} > ${BACKUP_FILE}
# usamos bzip2 para comprimir el sql
bzip2 ${BACKUP_FILE}