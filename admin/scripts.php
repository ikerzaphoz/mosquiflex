<?php

include '../config.php';
$page = "scripts";
$path = "";
if(isset($_GET['path'])): $path = $_GET['path']; endif;
include 'mod_head/index.php';
include 'mod_nav/index.php';
if($path == "scripts"):
    include 'mod_scripts/index.php';
endif;
include 'mod_footer/index.php';