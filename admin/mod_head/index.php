<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Administraci&oacute;n - Mosquiflex &#174;</title>
    <meta name="viewport" content="width=device-width,height=device-height initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?=path_css?>bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=path_css?>font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=path_css?>bootstrap-colorpicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?=path_css?>toastr.min.css">
    <link rel="stylesheet/less" type="text/css" href="<?=path_css?>style_admin.less?v=<?=microtime();?>">
    <script type="application/javascript" src="<?=path_js?>jquery.js"></script>
    <script type="application/javascript" src="<?=path_js?>less.min.js"></script>
</head>
<body>