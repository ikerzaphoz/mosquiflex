<?php

$lang = new Lang();
$array_lang = $lang->get_array_lang();
$visible = "";
$text_info = "";
if(empty($array_lang)):
    $visible = "hidden";
    $text_info = "No se encuentran datos";
endif;

?>
<div class="container">
    <h3 class="text-center">IDIOMAS</h3>
    <button class="btn-warning btn btn_actualizar_lang" lang_update="es">Actualizar Idioma ES</button>
    <button class="btn-warning btn btn_actualizar_lang" lang_update="pt">Actualizar Idioma PT</button>
    <table class="table table-responsive table-striped <?=$visible?>">
        <thead>
            <th class="text-center">#</th>
            <th class="text-center">Variable</th>
            <th class="text-center">ES</th>
            <th class="text-center">PT</th>
            <th class="text-center">Opciones</th>
        </thead>
        <tbody>
            <?php

            foreach($array_lang as $item_array_lang): ?>
                <tr>
                    <form class="form_edit_lang" name="form_edit_lang" method="post">
                        <td><?=$item_array_lang['id']?></td>
                        <td><?=$item_array_lang['var_name']?></td>
                        <td><input name="lang_es" type="text" class="form-control" value="<?=$item_array_lang['lang_es']?>"/></td>
                        <td><input name="lang_pt" type="text" class="form-control" value="<?=$item_array_lang['lang_pt']?>"/></td>
                        <td class="text-center">
                            <span class="btn btn-success btn_save_lang glyphicon glyphicon-floppy-disk"></span>
                        </td>
                        <input type="hidden" name="id_lang" value="<?=$item_array_lang['id']?>">
                    </form>
                </tr>

            <?php endforeach; ?>
        </tbody>
    </table>

    <?php

        if(!empty($text_info)):?>

            <h5><?=$text_info?></h5>

        <?php endif;

    ?>
</div>