<?php

require '../../../config.php';
$lang = new Lang();
$array_lang = $lang->get_array_lang($_POST['lang_update']);

$nombre_archivo = root."lang/lang_".$_POST['lang_update']."_2.php";

if(file_exists($nombre_archivo))
{
    unlink($nombre_archivo);
}


if($archivo = fopen($nombre_archivo, "a"))
{
    fwrite($archivo, "<?php");
    fwrite($archivo, " ");
    foreach($array_lang as $item => $valor){
        $text = "define('".$valor['var_name']."', '".$valor['lang_'.$_POST['lang_update']]."'); ";
        fwrite($archivo, $text);
    }
    fwrite($archivo, "?>");
    fclose($archivo);
    $array_error = array('error' => '0', 'message' => 'Archivo creado correctamente');
}else{
    $array_error = array('error' => '1', 'message' => 'Error al crear archivo');
}

echo json_encode($array_error);


?>