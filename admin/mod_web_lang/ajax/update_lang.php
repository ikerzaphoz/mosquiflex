<?php

require '../../../config.php';
$lang = new Lang();
$id_lang = "";
$lang_es = "";
$lang_pt = "";
$affected = "";
if(isset($_POST['id_lang'])) $id_lang = $_POST['id_lang'];
if(isset($_POST['lang_es'])) $lang_es = $_POST['lang_es'];
if(isset($_POST['lang_pt'])) $lang_pt = $_POST['lang_pt'];
$affected = $lang->update_var_name($id_lang, $lang_es, $lang_pt);
if($affected == 1):
    $array_error = array('error' => '0', 'message' => 'Datos actualizados correctamente');
elseif($affected == 0):
    $array_error = array('error' => '1', 'message' => 'Cambios no encontrados');
else:
    $array_error = array('error' => '2', 'message' => 'Error al modificar datos');
endif;

echo json_encode($array_error);