<?php

$familia = new Familia();
$array_familia = $familia->getFamilia();
foreach($array_familia as $item):
    ${'array_familia_colores_'.$item['fafami']} = array();
    array_push(${'array_familia_colores_'.$item['fafami']},$familia->getColoresByFamilia($item['fafami']));
endforeach;


?>

<div class="container">
    Editar productos - colores

    <?php foreach($array_familia as $item):?>

        <div>
            <h5>Colores familia - <?=$item['fadesc']?></h5>

            <table class="table table-striped">
                <thead>
                <th>#</th>
                <th>Descripción</th>
                <th>Color</th>
                <th>Opciones</th>
                </thead>
                <tbody class="contentTable">

                <?php

                foreach (${'array_familia_colores_'.$item['fafami']} as $index => $item_col):
                    foreach($item_col as $index_aux):
                        ?>
                        <tr>
                            <form class="form_color">
                                <td><?=$index_aux['cofami']?>-<?=$index_aux['cosubf']?>-<?=$index_aux['cocolo']?></td>
                                <td>
                                    <input name="codesc" type="text" class="form-control edit_desc_color" value="<?=$index_aux['codesc']?>">
                                </td>
                                <td>
                                    <input style="background-color: <?=$index_aux['html']?>" name="html" type="text" class="input-color form-control edit_desc_html" value="<?=$index_aux['html']?>">
                                </td>
                                <td>
                        <span class="btn btn-success edit_save_color fa fa-save">
                                </td>
                                <input type="hidden" value="<?=$index_aux['cofami']?>" name="cofami">
                                <input type="hidden" value="<?=$index_aux['cosubf']?>" name="cosubf">
                                <input type="hidden" value="<?=$index_aux['cocolo']?>" name="cocolo">
                            </form>
                        </tr>
                        <?php
                    endforeach;
                endforeach;

                ?>
                </tbody>
            </table>
        </div>

    <?php endforeach; ?>

</div>