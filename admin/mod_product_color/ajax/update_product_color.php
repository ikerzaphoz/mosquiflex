<?php

//codesc=BLANCO&html=FFFFFF&cofami=2731&cosubf=1&cocolo=2
require '../../../config.php';
$familia = new Familia();

$codesc = "";
$html = "";
$cofami = "";
$cosubf = "";
$cocolo = "";
if(isset($_POST['codesc'])) $codesc = $_POST['codesc'];
if(isset($_POST['html'])) $html = $_POST['html'];
if(isset($_POST['cofami'])) $cofami = $_POST['cofami'];
if(isset($_POST['cosubf'])) $cosubf = $_POST['cosubf'];
if(isset($_POST['cocolo'])) $cocolo = $_POST['cocolo'];

$affected = $familia->update_familia_color($codesc, $html, $cofami, $cosubf, $cocolo);
if($affected == 1):
    $array_error = array('error' => '0', 'message' => 'Datos actualizados correctamente');
elseif($affected == 0):
    $array_error = array('error' => '1', 'message' => 'Cambios no encontrados');
else:
    $array_error = array('error' => '2', 'message' => 'Error al modificar datos');
endif;

echo json_encode($array_error);