<?php

include '../config.php';
$page = "productos";
$path = "";
if(isset($_GET['path'])): $path = $_GET['path']; endif;
include 'mod_head/index.php';
include 'mod_nav/index.php';
if($path == "producto" OR empty($path)):
    include 'mod_product/index.php';
elseif($path == "producto_status"):
    include 'mod_product/status.php';
elseif($path == "color"):
    include 'mod_product_color/index.php';
endif;
include 'mod_footer/index.php';
