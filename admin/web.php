<?php

include '../config.php';
$page = "web";
$path = "";
if(isset($_GET['path'])): $path = $_GET['path']; endif;
include 'mod_head/index.php';
include 'mod_nav/index.php';
if($path == "idiomas" OR empty($path)):
    include 'mod_web_lang/index.php';
endif;
include 'mod_footer/index.php';