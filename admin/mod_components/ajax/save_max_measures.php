<?php

require '../../../config.php';
$familia = new Familia();

$measure = "";
$arfami = "";
$arsubf = "";
$ararti = "";

if(isset($_POST['measure'])) $measure = $_POST['measure'];
if(isset($_POST['arfami'])) $arfami = $_POST['arfami'];
if(isset($_POST['arsubf'])) $arsubf = $_POST['arsubf'];
if(isset($_POST['ararti'])) $ararti = $_POST['ararti'];

$affected = $familia->update_max_measures($arfami, $arsubf, $ararti, $measure);
if($affected == 1):
    $array_error = array('error' => '0', 'message' => 'Datos actualizados correctamente');
elseif($affected == 0):
    $array_error = array('error' => '1', 'message' => 'Cambios no encontrados');
else:
    $array_error = array('error' => '2', 'message' => 'Error al modificar datos');
endif;

echo json_encode($array_error);