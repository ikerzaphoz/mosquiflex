<?php

include '../config.php';
$page = "componentes";
$path = "";
if(isset($_GET['path'])): $path = $_GET['path']; endif;
include 'mod_head/index.php';
include 'mod_nav/index.php';
if($path == "medidas" OR empty($path)):
    include 'mod_components/index.php';
endif;
include 'mod_footer/index.php';
