<?php

require '../../config.php';
require path_class.'phpexcel/PHPExcel.php';
$empresa = "MT";
if(isset($_GET['empresa']) && !empty($_GET['empresa'])):
    $empresa = $_GET['empresa'];
endif;
$archivo = path_excels.$empresa."/aplazamiento.xlsx";

$bd = new Db();

$inputFileType = PHPExcel_IOFactory::identify($archivo);
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$objPHPExcel = $objReader->load($archivo);
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$total_insert = 0;
$total = 0;
$sql = "INSERT INTO aplazamiento VALUES ";
for ($row = 2; $row <= $highestRow; $row++){

    $total++;
    $campo1 = replaceCharacteresSql($sheet->getCell("A".$row)->getValue());
    $campo2 = replaceCharacteresSql($sheet->getCell("B".$row)->getValue());
    $campo3 = replaceCharacteresSql($sheet->getCell("C".$row)->getValue());
    $campo4 = replaceCharacteresSql($sheet->getCell("D".$row)->getValue());
    $campo5 = replaceCharacteresSql($sheet->getCell("E".$row)->getValue());
    $campo6 = replaceCharacteresSql($sheet->getCell("F".$row)->getValue());
    $campo7 = replaceCharacteresSql($sheet->getCell("G".$row)->getValue());
    $campo8 = replaceCharacteresSql($sheet->getCell("H".$row)->getValue());
    $campo9 = replaceCharacteresSql($sheet->getCell("I".$row)->getValue());
    $campo10 = replaceCharacteresSql($sheet->getCell("J".$row)->getValue());

    $sql.= "('$campo1', '$campo2', '$campo3', '$campo4', '$campo5', '$campo6', '$campo7', '$campo8', '$campo9', '$campo10', '".$empresa."'),";

}

$sql = substr($sql, 0, -1);
$total_insert =  $bd->ejecutarReturnAffected($sql);
echo $total_insert . " FILAS NUEVAS DE " . $total . " FILAS ENCONTRADAS EN EL EXCEL aplazamiento DE LA EMPRESA ".$empresa;