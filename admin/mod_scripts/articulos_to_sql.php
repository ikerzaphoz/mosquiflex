<?php

require '../../config.php';
require path_class.'phpexcel/PHPExcel.php';
$empresa = "MT";
if(isset($_GET['empresa']) && !empty($_GET['empresa'])):
    $empresa = $_GET['empresa'];
endif;
$archivo = path_excels.$empresa."/articulos.xlsx";

$bd = new Db();

$inputFileType = PHPExcel_IOFactory::identify($archivo);
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$objPHPExcel = $objReader->load($archivo);
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$total_insert = 0;
$total = 0;
$sql = "INSERT INTO articulos VALUES";
for ($row = 2; $row <= $highestRow; $row++){

    $total++;
    $campo1 = replaceCharacteresSql($sheet->getCell("A".$row)->getValue());
    $campo2 = replaceCharacteresSql($sheet->getCell("B".$row)->getValue());
    $campo3 = replaceCharacteresSql($sheet->getCell("C".$row)->getValue());
    $campo4 = replaceCharacteresSql($sheet->getCell("D".$row)->getValue());
    $campo5 = replaceCharacteresSql($sheet->getCell("E".$row)->getValue());
    $campo6 = replaceCharacteresSql($sheet->getCell("F".$row)->getValue());
    $campo7 = replaceCharacteresSql($sheet->getCell("G".$row)->getValue());
    $campo8 = replaceCharacteresSql($sheet->getCell("H".$row)->getValue());
    $campo9 = replaceCharacteresSql($sheet->getCell("I".$row)->getValue());
    $campo10 = replaceCharacteresSql($sheet->getCell("J".$row)->getValue());
    $campo11 = replaceCharacteresSql($sheet->getCell("K".$row)->getValue());
    $campo12 = replaceCharacteresSql($sheet->getCell("L".$row)->getValue());
    $campo13 = replaceCharacteresSql($sheet->getCell("M".$row)->getValue());
    $campo14 = replaceCharacteresSql($sheet->getCell("N".$row)->getValue());
    $campo15 = replaceCharacteresSql($sheet->getCell("O".$row)->getValue());
    $campo16 = replaceCharacteresSql($sheet->getCell("P".$row)->getValue());
    $campo17 = replaceCharacteresSql($sheet->getCell("Q".$row)->getValue());
    $campo18 = replaceCharacteresSql($sheet->getCell("R".$row)->getValue());
    $campo19 = replaceCharacteresSql($sheet->getCell("S".$row)->getValue());
    $campo20 = replaceCharacteresSql($sheet->getCell("T".$row)->getValue());
    $campo21 = replaceCharacteresSql($sheet->getCell("U".$row)->getValue());
    $campo22 = replaceCharacteresSql($sheet->getCell("V".$row)->getValue());
    $campo23 = replaceCharacteresSql($sheet->getCell("W".$row)->getValue());
    $campo24 = replaceCharacteresSql($sheet->getCell("X".$row)->getValue());
    $campo25 = replaceCharacteresSql($sheet->getCell("Y".$row)->getValue());
    $campo26 = replaceCharacteresSql($sheet->getCell("Z".$row)->getValue());

    $campo27 = replaceCharacteresSql($sheet->getCell("AA".$row)->getValue());
    $campo28 = replaceCharacteresSql($sheet->getCell("AB".$row)->getValue());
    $campo29 = replaceCharacteresSql($sheet->getCell("AC".$row)->getValue());
    $campo30 = replaceCharacteresSql($sheet->getCell("AD".$row)->getValue());
    $campo31 = replaceCharacteresSql($sheet->getCell("AE".$row)->getValue());
    $campo32 = replaceCharacteresSql($sheet->getCell("AF".$row)->getValue());
    $campo33 = replaceCharacteresSql($sheet->getCell("AG".$row)->getValue());
    $campo34 = replaceCharacteresSql($sheet->getCell("AH".$row)->getValue());
    $campo35 = replaceCharacteresSql($sheet->getCell("AI".$row)->getValue());
    $campo36 = replaceCharacteresSql($sheet->getCell("AJ".$row)->getValue());
    $campo37 = replaceCharacteresSql($sheet->getCell("AK".$row)->getValue());
    $campo38 = replaceCharacteresSql($sheet->getCell("AL".$row)->getValue());
    $campo39 = replaceCharacteresSql($sheet->getCell("AM".$row)->getValue());
    $campo40 = replaceCharacteresSql($sheet->getCell("AN".$row)->getValue());
    $campo41 = replaceCharacteresSql($sheet->getCell("AO".$row)->getValue());
    $campo42 = replaceCharacteresSql($sheet->getCell("AP".$row)->getValue());
    $campo43 = replaceCharacteresSql($sheet->getCell("AQ".$row)->getValue());
    $campo44 = replaceCharacteresSql($sheet->getCell("AR".$row)->getValue());
    $campo45 = replaceCharacteresSql($sheet->getCell("AS".$row)->getValue());

    //Desde el campo 45 al campo empresa hay 3 campos de estado que por defecto toman el valor 1.

    $sql.= " ('$campo1', '$campo2', '$campo3', '$campo4', '$campo5', '$campo6', '$campo7', '$campo8', '$campo9', '$campo10', '$campo11', '$campo12', '$campo13','$campo14', '$campo15', '$campo16', '$campo17', '$campo18', '$campo19', '$campo20', '$campo21', '$campo22','$campo23', '$campo24', '$campo25', '$campo26', '$campo27', '$campo28', '$campo29', '$campo30', '$campo31','$campo32', '$campo33', '$campo34', '$campo35', '$campo36', '$campo37', '$campo38', '$campo39', '$campo40','$campo41', '$campo42', '$campo43', '$campo44', '$campo45', '1', '1', '1', '".$empresa."'),";

}
$sql_aux = substr($sql, 0, -1);
$check_update =  $bd->ejecutarReturnAffected($sql_aux);
echo $total_insert . " FILAS NUEVAS DE " . $total . " FILAS ENCONTRADAS EN EL EXCEL articulos DE LA EMPRESA ".$empresa;