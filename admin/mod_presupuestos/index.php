<?php

$presupuesto = new Presupuesto();
$array_presupuesto = $presupuesto->obtener_todos_presupuestos();

?>

<div class="container">
    <h4>Ver Presupuestos</h4>
    <table class="table table-striped">
        <thead>
        <th>#</th>
        <th>Presupuesto</th>
        <th>Cliente</th>
        <th>Fecha</th>
        <th>Total</th>
        <th>Opciones</th>
        </thead>
        <tbody>
            <?php
                foreach($array_presupuesto as $item):?>
                    <tr>
                        <td><?=$item['id']?></td>
                        <td><?=$item['num_pre']?></td>
                        <td><?=$item['id_cliente']?></td>
                        <td><?=$item['fecha']?></td>
                        <td><?=round2decimals($item['total'])?>&euro;</td>
                        <td>
                            <a target="_blank" href="<?=path_web_mods?>mod_pdf/index.php?pres=<?=$item['id']?>" <span class="btn-sm btn_pdf_pedido btn btn-danger fa fa-file-pdf-o">&nbsp;Ver</span></a>
                            <button class="btn-sm btn btn-default">Mostrar albaranes</button>
                        </td>
                    </tr>
                    <?php

                    $array_albaranes = $presupuesto->get_albaranes($item['id']);
                    foreach($array_albaranes as $albaran):?>
                        <tr>
                            <td><?=round2decimals($albaran['albpre'])?>&euro;</td>
                        </tr>
                    <?php endforeach; ?>
                <?php endforeach;?>
        </tbody>
    </table>
</div>