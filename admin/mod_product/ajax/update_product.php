<?php

require '../../../config.php';
$familia = new Familia();

$fadesc = "";
$status = "";
$fafami = "";
if(isset($_POST['fadesc'])) $fadesc = $_POST['fadesc'];
if(isset($_POST['status'])) $status = $_POST['status'];
if(isset($_POST['fafami'])) $fafami = $_POST['fafami'];
if(empty($status)):
    $status = "0";
else:
    $status = "1";
endif;

$affected = $familia->update_familia($fadesc, $status, $fafami);
if($affected == 1):
    $array_error = array('error' => '0', 'message' => 'Datos actualizados correctamente');
elseif($affected == 0):
    $array_error = array('error' => '1', 'message' => 'Cambios no encontrados');
else:
    $array_error = array('error' => '2', 'message' => 'Error al modificar datos');
endif;

echo json_encode($array_error);