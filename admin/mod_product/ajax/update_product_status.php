<?php

$status_1 = "";
if (isset($_POST['status_1'])): $status_1 = $_POST['status_1'];
endif;
$status_2 = "";
if (isset($_POST['status_2'])): $status_2 = $_POST['status_2'];
endif;
$status_3 = "";
if (isset($_POST['status_3'])): $status_3 = $_POST['status_3'];
endif;
$arfami = "";
if (isset($_POST['arfami'])): $arfami = $_POST['arfami'];
endif;
$arsubf = "";
if (isset($_POST['arsubf'])): $arsubf = $_POST['arsubf'];
endif;
$ararti = "";
if (isset($_POST['ararti'])): $ararti = $_POST['ararti'];
endif;

require '../../../config.php';
$familia = new Familia();

if (empty($status_1)):
    $status_1 = "0";
else:
    $status_1 = "1";
endif;

if (empty($status_2)):
    $status_2 = "0";
else:
    $status_2 = "1";
endif;

if (empty($status_3)):
    $status_3 = "0";
else:
    $status_3 = "1";
endif;

$affected = $familia->update_status_product($arfami, $arsubf, $ararti, $status_1, $status_2, $status_3);

if ($affected == 1):
    $array_error = array('error' => '0', 'message' => 'Datos actualizados correctamente');
elseif ($affected == 0):
    $array_error = array('error' => '1', 'message' => 'Cambios no encontrados');
else:
    $array_error = array('error' => '2', 'message' => 'Error al modificar datos');
endif;

echo json_encode($array_error);
