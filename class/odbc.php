<?php

/*
 * Funciones para las consultas a las librerías odbc 
 */

Class Odbc {

    public function ejecutar($conexion, $sql) {
        try {
            $rs = odbc_exec($conexion, $sql);
                    } catch (Exception $error) {
            echo "Error al ejecutar." . $error->getMessage();
        }
        return $rs;
    }

    public function ejecutar_new($conexion, $sql) {
        try {
            $rs = odbc_exec($conexion, $sql);
        } catch (Exception $error) {
            $rs = $error;
        }
        return $rs;
    }
    
    public function numero_filas_afectadas($result){
        $rs = odbc_num_rows($result);
        if(empty($rs)):
            $rs = 0;
        endif;
        return $rs;
    }

    public function ejecutar_array($conexion, $sql, $index = NULL) {
        $array = [];
        $rs = $this->ejecutar($conexion, $sql);
        while ($fila = odbc_fetch_array($rs)) {
            array_push($array, $fila);
        }
        return $array;
    }

}
