<?php

Class Familia {

    private $fafami;
    private $fadesc;
    private $famaxf;
    private $status;
    private $table = "familias";

    public function __construct($fafami = NULL, $fadesc = NULL, $famaxf = NULL, $status = NULL) {
        $this->fafami = $fafami;
        $this->fadesc = $fadesc;
        $this->famaxf = $famaxf;
        $this->status = $status;
    }

    public function getFafami() {
        return $this->fafami;
    }

    public function setFafami($fafami) {
        $this->fafami = $fafami;
    }

    public function getFadesc() {
        return $this->fadesc;
    }

    public function setFadesc($fadesc) {
        $this->fadesc = $fadesc;
    }

    public function getFamaxf() {
        return $this->famaxf;
    }

    public function setFamaxf($famaxf) {
        $this->famaxf = $famaxf;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    /*
      public function getFamilia($index = NULL, $status = NULL)
      {
      $bd = new Db();
      $sql = "SELECT fafami, fadesc, status, min_ran_anc, min_ran_alt FROM " . $this->table;
      if (!isset($status)):
      $sql .= " WHERE status = '1'";
      endif;
      $sql .= " ORDER BY fafami";
      $result = $bd->obtener_consultas($sql, $index);
      return $result;
      }
     */

    public function getFamilia($index = NULL, $status = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM " . $this->table;
        if (!isset($status)):
            $sql .= " WHERE status = '1' AND empresa = '".empresa."'";
        else:
            $sql .= " WHERE empresa = '".empresa."'";
        endif;
        $sql .= " ORDER BY fafami";
        //$result = Cache::get(path_cache.'familias'.$index);
        //if (empty($result)) {
        $result = $bd->obtener_consultas($sql, $index);
        //Cache::put(path_cache.'familias'.$index, $result);
        //}
        return $result;
    }

    public function getGrupoColoresFamilia($id_familia) {
        $bd = new Db();
        $sql = "SELECT cofami, cosubf, empresa FROM colores WHERE cofami = '$id_familia' AND cosubf <='9' AND empresa = '".empresa."' GROUP BY cosubf";
        //$result = Cache::get(path_cache.'colores'.$id_familia);
        //if (empty($result)) {
        $result = $bd->obtener_consultas($sql);
        //Cache::put(path_cache.'colores'.$id_familia, $result);
        //}
        return $result;
    }

    public function getColoresByFamilia($id_familia, $id_subfamilia = NULL) {
        $bd = new Db();
        $sql = "SELECT cofami, cosubf, cocolo, codesc, html, empresa FROM colores WHERE cofami = '$id_familia' AND cosubf <='9' AND empresa = '".empresa."'";
        if (!empty($id_subfamilia)):
            $sql .= " AND cosubf = '$id_subfamilia'";
        endif;
        //$result = Cache::get(path_cache.'colores'.$id_familia.$id_subfamilia);
        //if (empty($result)) {
        $result = $bd->obtener_consultas($sql);
        //Cache::put(path_cache.'colores'.$id_familia.$id_subfamilia, $result);
        //}
        return $result;
    }

    public function getColoressByCocolo($id_familia, $cocolo) {
        $bd = new Db();
        $sql = "SELECT cofami, cosubf, cocolo, codesc, html, empresa FROM colores WHERE cofami = '$id_familia' AND cosubf <='9' AND empresa = '".empresa."'";
        if (!empty($cocolo)):
            $sql .= " AND cocolo = '$cocolo'";
        endif;
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function getPriceOfRango($id_familia, $code_sub, $rango) {
        $bd = new Db();
        $sql = "SELECT sipvp1 FROM sizes_products WHERE sifami = '$id_familia' AND sisubf = '$code_sub' AND siarti = '$rango' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['sipvp1'];
    }

    public function getAnchoMin($id_familia, $code_sub) {
        $bd = new Db();
        $sql = "SELECT sidanc FROM sizes_products WHERE sifami = '$id_familia' AND sisubf = '$code_sub' AND empresa = '".empresa."' ORDER BY sidanc asc LIMIT 1";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['sidanc'];
    }

    public function getAnchoMax($id_familia, $code_sub) {
        $bd = new Db();
        $sql = "SELECT sihanc FROM sizes_products WHERE sifami = '$id_familia' AND sisubf = '$code_sub' AND empresa = '".empresa."' ORDER BY sihanc desc LIMIT 1";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['sihanc'];
    }

    public function getAltoMin($id_familia, $code_sub) {
        $bd = new Db();
        $sql = "SELECT sidalt FROM sizes_products WHERE sifami = '$id_familia' AND sisubf = '$code_sub' AND empresa = '".empresa."' ORDER BY sidalt asc LIMIT 1";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['sidalt'];
    }

    public function getAltoMax($id_familia, $code_sub) {
        $bd = new Db();
        $sql = "SELECT sihalt FROM sizes_products WHERE sifami = '$id_familia' AND sisubf = '$code_sub' AND empresa = '".empresa."' ORDER BY sihalt desc LIMIT 1";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['sihalt'];
    }

    public function update_familia($fadesc, $status, $fafami) {
        $bd = new Db();
        $sql = "UPDATE " . $this->table . " SET fadesc='$fadesc', status='$status' WHERE fafami = '$fafami'";
        return $bd->ejecutarReturnAffected($sql);
    }

    public function update_familia_color($codesc, $html, $cofami, $cosubf, $cocolo) {
        $bd = new Db();
        $sql = "UPDATE colores SET codesc='$codesc', html='$html' WHERE cofami = '$cofami' AND cosubf = '$cosubf' AND cocolo = '$cocolo' AND empresa = '".empresa."'";
        return $bd->ejecutarReturnAffected($sql);
    }

    public function getOptionsProduct($arfami, $subfami) {
        $bd = new Db();
        $sql = "SELECT arfami, arsubf, arpvp1, ardesc, ararti, status_2 FROM articulos WHERE arfami = '$arfami' AND arsubf = '$subfami' AND status_2 = '1' AND empresa = '".empresa."' ORDER BY ardesc ASC, arpvp1 ASC";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function getComponentsByProduct($id_component) {
        $bd = new Db();
        $sql = "SELECT arpvta, ardesc FROM articulos WHERE arfami = '$id_component' AND empresa = ''".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    //Ejemplo cache

    public function getComponentsBySubfamily($id_component, $status = NULL, $status2 = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM articulos WHERE arfami = '$id_component' AND empresa = '".empresa."'";
        if(isset($status)):
            $sql .= " AND ".$status. " = '1'";
        endif;
        if(isset($status2)):
            $sql .= " AND ".$status2. " = '1'";
        endif;
        //$result = Cache::get(path_cache.'articulos'.$id_component);
        //if (empty($result)) {
        $result = $bd->obtener_consultas($sql);
        //Cache::put(path_cache.'articulos'.$id_component, $result);
        //}
        return $result;
    }

//    public function getComponentsBySubfamily($id_component)
//    {
//        $bd = new Db();
//        $sql = "SELECT * FROM articulos WHERE arfami = '$id_component'";
//        $result = $bd->obtener_consultas($sql);
//        return $result;
//    }

    public function getComponentsGroupsColor($arfami, $arsubf) {
        $bd = new Db();
        $sql = "SELECT *, substring(ararti,1,2) as code_arti FROM articulos WHERE arfami = '$arfami' AND arsubf = '$arsubf' AND empresa = '".empresa."' GROUP BY code_arti ORDER BY ararti";
        //$result = Cache::get(path_cache.'componentsGroupsColor'.$arfami.$arsubf);
        //if (empty($result)) {
        $result = $bd->obtener_consultas($sql);
        //Cache::put(path_cache.'componentsGroupsColor'.$arfami.$arsubf, $result);
        //}
        return $result;
    }

    public function getTitleProduct($arfami, $arsubf) {
        $bd = new Db();
        $sql = "SELECT ardesc FROM articulos WHERE arfami = '$arfami' AND arsubf = '$arsubf' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['ardesc'];
    }

    public function getTitleComponent($arfami, $arsubf, $id_component) {
        $bd = new Db();
        $sql = "SELECT ardesc FROM articulos WHERE arfami = '$arfami' AND arsubf = '$arsubf' AND ararti = '$id_component' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['ardesc'];
    }

    public function getColorsRal($title_tal = NULL, $familia = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM colores_ral WHERE coralti = '$title_tal' AND is_in_colors = '0' AND empresa = '".empresa."'";
        if (isset($familia)):
            $sql .= " AND familia_ral = '$familia'";
        endif;
        $result = $bd->obtener_consultas($sql);
        if (!empty($result)):
            return $result[0];
        else:
            return 0;
        endif;
    }

    public function getIsLacado($title_ral = NULL, $familia = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM colores_ral WHERE coralti = '$title_ral' AND is_in_colors = '0' AND empresa = '".empresa."'";
        if (isset($familia)):
            $sql .= " AND familia_ral = '$familia'";
        endif;
        $result = $bd->obtener_consultas($sql);
        if (empty($result)):
            return 0;
        else:
            return 1;
        endif;
    }

    public function getColorsRalLineaPedido($title_tal = NULL, $familia = NULL) {
        $bd = new Db();
        $sql = "SELECT coralht as html, coraldes as codesc, empresa FROM colores_ral WHERE coralti = '$title_tal' AND empresa = '".empresa."'";
        if (isset($familia)):
            $sql .= " AND familia_ral = '$familia'";
        endif;
        $result = $bd->obtener_consultas($sql);
        if (!empty($result)):
            return $result[0];
        else:
            return 0;
        endif;
    }

    public function getInfoComponent($cafami = NULL, $casub = NULL, $caarti = NULL, $arprpr = NULL) {
        $bd = new Db();
        $sql = "SELECT arpvta, arpval, ardesc FROM articulos WHERE arfami = '$cafami' AND arsubf = '$casub' AND ararti = '$caarti' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result[0];
    }

    public function getEmbalaje($cafami = NULL, $casub = NULL, $caarti = NULL, $arprpr = NULL) {
        $bd = new Db();
        $sql = "SELECT caemba FROM aux_articulos WHERE cafami = '$cafami' AND casub = '$casub' AND caarti = '$caarti' AND caprov = '$arprpr' AND empresa = '".empresa."' LIMIT 1";
        //$result = Cache::get(path_cache.'embalaje'.$cafami.$casub.$caarti.$arprpr);
        //if (empty($result)) {
        $result = $bd->obtener_consultas($sql);
        //Cache::put(path_cache.'embalaje'.$cafami.$casub.$caarti.$arprpr, $result);
        //}
        return $result[0]['caemba'];
    }

    public function get_tipoMedida_tarifa($cafami = NULL, $casub = NULL, $caarti = NULL) {
        $bd = new Db();
        $sql = "SELECT arunit FROM articulos  WHERE arfami = '$cafami' AND arsubf = '$casub' AND ararti = '$caarti' AND empresa = '".empresa."'";
        //$result = Cache::get(path_cache.'medida'.$cafami.$casub.$caarti);
        //if (empty($result)) {
        $result = $bd->obtener_consultas($sql);
        //Cache::put(path_cache.'medida'.$cafami.$casub.$caarti, $result);
        //}
        return $result[0]['arunit'];
    }

    public function searchComponent($id_component = NULL, $text = NULL, $type_search = NULL) {
        $bd = new Db();
        $sql = "SELECT arsubf, ararti, arprpr, ardesc, arpvta, arpval, arfami, substring(ararti,1,2) as code_arti FROM articulos WHERE arfami = '$id_component' AND empresa = '".empresa."'";
        if ($type_search == "descripcion"):
            $sql .= "AND UPPER(ardesc) LIKE UPPER('%$text%')";
        endif;
        //$sql .= " GROUP BY arsubf ORDER BY ararti";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function searchComponentByCode($id_component = NULL, $arsubf = NULL, $ararti = NULL) {
        $bd = new Db();
        $sql = "SELECT arpvp1, arsubf, ararti, arprpr, ardesc, arpvta, arpval, arfami FROM articulos WHERE arfami = '$id_component' AND arsubf = $arsubf AND ararti = '$ararti' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function getMaxMeasure($cafami = NULL) {
        $bd = new Db();
        $sql = "SELECT max_measure, arfami, arsubf, ararti, ardesc, articulos.empresa FROM articulos INNER JOIN aux_articulos ON articulos.arfami = aux_articulos.cafami AND articulos.arsubf = aux_articulos.casub AND articulos.ararti = aux_articulos.caarti AND arfami = '$cafami' AND articulos.empresa = '".empresa."'";
        //$result = Cache::get(path_cache.'max_measure'.$cafami);
        //if (empty($result)) {
        $result = $bd->obtener_consultas($sql);
        //Cache::put(path_cache.'max_measure'.$cafami, $result);
        //}
        return $result;
    }

    public function update_max_measures($fafami = NULL, $fasubf = NULL, $farti = NULL, $measure = NULL) {
        $bd = new Db();
        $sql = "UPDATE aux_articulos SET max_measure = '$measure' WHERE cafami = '$fafami' AND casub = '$fasubf' AND caarti = '$farti' AND empresa = '".empresa."'";
        return $bd->ejecutarReturnAffected($sql);
    }

    public function get_status_articulos($arfami = NULL) {
        $bd = new Db();
        $sql = "SELECT arfami, arsubf, ararti, ardesc, status_1, status_2, status_3 FROM articulos";
        if (!empty($arfami)):
            $sql .= " WHERE arfami = '$arfami' AND empresa = '".empresa."'";
        else:
            $sql .= "WHERE empresa = '".empresa."'";
        endif;
        $sql .= " ORDER BY arfami, arsubf, ararti";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function update_status_product($arfami = NULL, $arsubf = NULL, $ararti = NULL, $status_1 = NULL, $status_2 = NULL, $status_3 = NULL) {
        $bd = new Db();
        $sql = "UPDATE articulos SET status_1 = '$status_1', status_2 = '$status_2', status_3 = '$status_3' WHERE arfami = '$arfami' AND arsubf = '$arsubf' AND ararti = '$ararti' AND empresa = '".empresa."'";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }

    public function select_familia() {
        $bd = new Db();
        $sql = "SELECT DISTINCT (arfami) FROM articulos WHERE arfami > '1790' AND arfami < '2791' AND arfami < '2711' OR arfami > '2725' AND arfami < '2791' OR arfami = '2799' AND empresa = '".empresa."' ORDER BY arfami";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

}
