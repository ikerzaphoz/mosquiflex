<?php

function is_valid_email($str) {
    return (filter_var($str, FILTER_VALIDATE_EMAIL)) ? 1 : 0;
}

//Funcion para obtener el valor del precio del producto segun medidas (tabla tarifa)
function calc_precio_enrollable_ventana($number, $min_value = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    if (substr($number, 3, 1) <= 5):
        $number = $number + '0.05';
    endif;
    $number = round($number, 1, PHP_ROUND_HALF_EVEN);
    $number = str_replace(array(",", "."), '', $number);
    if ($number == "1" OR $number == "10000"):
        $number = "10";
    elseif ($number == "2"):
        $number = "20";
    else:
        if ($number % 2 != 0):
            $number++;
        endif;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

//Funcion para obtener el valor del precio del producto segun medidas (tabla tarifa)
function calc_precio_ancho_enrollable_puerta($number, $min_value = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if ($number <= "0800"):
        $number = 8;
    elseif ($number > "0800" AND $number <= "1000"):
        $number = 10;
    elseif ($number > "1000" AND $number <= "1200"):
        $number = 12;
    elseif ($number > "1200" AND $number <= "1400"):
        $number = 14;
    elseif ($number > "1400" AND $number <= "1600"):
        $number = 16;
    elseif ($number > "1600" AND $number <= "2000"):
        $number = 20;
    elseif ($number > "2000" AND $number <= "2400"):
        $number = 24;
    elseif ($number > "2400" AND $number <= "2800"):
        $number = 28;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

//Funcion para obtener el valor del precio del producto segun medidas (tabla tarifa)
function calc_precio_alto_enrollable_puerta($number, $min_value = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if ($number <= "1750"):
        $number = 175;
    elseif ($number > "1750" AND $number <= "2000"):
        $number = 200;
    elseif ($number > "2000" AND $number <= "2250"):
        $number = 225;
    elseif ($number > "2250" AND $number <= "2500"):
        $number = 250;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

//Funcion para obtener el valor del precio del producto segun medidas (tabla tarifa)
function calc_precio_ancho_plisada($number, $min_value = NULL, $subf = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if (empty($subf)):
        if ($number <= "900"):
            $number = 9;
        elseif ($number > "900" AND $number <= "1100"):
            $number = 11;
        elseif ($number > "1100" AND $number <= "1300"):
            $number = 13;
        elseif ($number > "1300" AND $number <= "1600"):
            $number = 16;
        elseif ($number > "1600" AND $number <= "1900"):
            $number = 19;
        elseif ($number > "1900" AND $number <= "2300"):
            $number = 23;
        elseif ($number > "2300" AND $number <= "2600"):
            $number = 26;
        elseif ($number > "2600" AND $number <= "2900"):
            $number = 29;
        elseif ($number > "2900" AND $number <= "3200"):
            $number = 32;
        elseif ($number > "3200" AND $number <= "3500"):
            $number = 35;
        elseif ($number > "3500" AND $number <= "3800"):
            $number = 38;
        endif;
    else:
        if ($number <= "1800"):
            $number = 18;
        elseif ($number > "1800" AND $number <= "2200"):
            $number = 22;
        elseif ($number > "2200" AND $number <= "2600"):
            $number = 26;
        elseif ($number > "2600" AND $number <= "3200"):
            $number = 32;
        elseif ($number > "3200" AND $number <= "3800"):
            $number = 38;
        endif;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

//Funcion para obtener el valor del precio del producto segun medidas (tabla tarifa)
function calc_precio_alto_plisada($number, $min_value = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if ($number <= "1750"):
        $number = 175;
    elseif ($number > "1750" AND $number <= "2000"):
        $number = 200;
    elseif ($number > "2000" AND $number <= "2250"):
        $number = 225;
    elseif ($number > "2250" AND $number <= "2500"):
        $number = 250;
    elseif ($number > "2500" AND $number <= "2700"):
        $number = 270;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

function calc_precio_ancho_abatible($number, $min_value = NULL, $subf = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if (empty($subf)):
        if ($number <= "600"):
            $number = 6;
        elseif ($number > "600" AND $number <= "800"):
            $number = 8;
        elseif ($number > "800" AND $number <= "1000"):
            $number = 10;
        elseif ($number > "1000" AND $number <= "1200"):
            $number = 12;
        endif;
    else:
        if ($number <= "1200"):
            $number = 12;
        elseif ($number > "1200" AND $number <= "1600"):
            $number = 16;
        elseif ($number > "1600" AND $number <= "2000"):
            $number = 20;
        elseif ($number > "2000" AND $number <= "2400"):
            $number = 24;
        endif;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

function calc_precio_alto_abatible($number, $min_value = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if ($number <= "1800"):
        $number = 18;
    elseif ($number > "1800" AND $number <= "2000"):
        $number = 20;
    elseif ($number > "2000" AND $number <= "2200"):
        $number = 22;
    elseif ($number > "2200" AND $number <= "2400"):
        $number = 24;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

function calc_precio_ancho_fija($number, $min_value = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if ($number <= "0800"):
        $number = 8;
    elseif ($number > "0800" AND $number <= "1000"):
        $number = 10;
    elseif ($number > "1000" AND $number <= "1200"):
        $number = 12;
    elseif ($number > "1200" AND $number <= "1400"):
        $number = 14;
    elseif ($number > "1400" AND $number <= "1600"):
        $number = 16;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

function calc_precio_alto_fija($number, $min_value = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if ($number <= "1000"):
        $number = 10;
    elseif ($number > "1000" AND $number <= "1200"):
        $number = 12;
    elseif ($number > "1200" AND $number <= "1400"):
        $number = 14;
    elseif ($number > "1400" AND $number <= "1600"):
        $number = 16;
    elseif ($number > "1600" AND $number <= "1800"):
        $number = 18;
    elseif ($number > "1800" AND $number <= "2000"):
        $number = 20;
    elseif ($number > "2000" AND $number <= "2200"):
        $number = 22;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

function calc_precio_ancho_marco_corredera($number, $min_value = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if ($number <= "1200"):
        $number = 12;
    elseif ($number > "1200" AND $number <= "1600"):
        $number = 16;
    elseif ($number > "1600" AND $number <= "2000"):
        $number = 20;
    elseif ($number > "2000" AND $number <= "2400"):
        $number = 24;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

function calc_precio_alto_marco_corredera($number, $min_value = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if ($number <= "1000"):
        $number = 10;
    elseif ($number > "1000" AND $number <= "1200"):
        $number = 12;
    elseif ($number > "1200" AND $number <= "1400"):
        $number = 14;
    elseif ($number > "1400" AND $number <= "1600"):
        $number = 16;
    elseif ($number > "1600" AND $number <= "1800"):
        $number = 18;
    elseif ($number > "1800" AND $number <= "2000"):
        $number = 20;
    elseif ($number > "2000" AND $number <= "2200"):
        $number = 22;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

function calc_precio_ancho_hoja_corredera($number, $min_value = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if ($number <= "600"):
        $number = 6;
    elseif ($number > "600" AND $number <= "800"):
        $number = 8;
    elseif ($number > "800" AND $number <= "1000"):
        $number = 10;
    elseif ($number > "1000" AND $number <= "1200"):
        $number = 12;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

function calc_precio_alto_hoja_corredera($number, $min_value = NULL) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if ($number <= "1000"):
        $number = 10;
    elseif ($number > "1000" AND $number <= "1200"):
        $number = 12;
    elseif ($number > "1200" AND $number <= "1400"):
        $number = 14;
    elseif ($number > "1400" AND $number <= "1600"):
        $number = 16;
    elseif ($number > "1600" AND $number <= "1800"):
        $number = 18;
    elseif ($number > "1800" AND $number <= "2000"):
        $number = 20;
    elseif ($number > "2000" AND $number <= "2200"):
        $number = 22;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

function calc_precio_hoja_corredera($number) {
    $number = str_pad($number, 5, "0", STR_PAD_RIGHT);
    $number = str_replace(array(",", "."), '', $number);
    if ($number <= "1200"):
        $number = 6;
    elseif ($number > "1200" AND $number <= "1600"):
        $number = 8;
    elseif ($number > "1600" AND $number <= "2000"):
        $number = 10;
    elseif ($number > "2000" AND $number <= "2400"):
        $number = 12;
    endif;
    return $number;
}

//Función para comprobar si el usuario está logueado y sino redireccionar
function check_login() {
    if (empty($_SESSION['ini_log'])):
        header("Location:" . path_web . "?error=ini_log");
    endif;
}

//Devuelve el texto con tamaño del cajon segun el valor obtenido
function getCajon($value = NULL) {
    if (empty($value)):
        return "C.33";
    elseif ($value == 1):
        return "C.35";
    else:
        return "C.42";
    endif;
}

//Función para redonder a dos decimales y eliminar el punto.
function round2decimals($number) {
    $number = (float) $number;
    $number = round($number, 2);
    $number = number_format($number, 2, ".", '');
    return $number;
}

//Función para redonder a tres decimales y eliminar el punto.
function round3decimals($number) {
    $number = (float) $number;
    $number = round($number, 3);
    $number = number_format($number, 3, ".", '');
    return $number;
}

//Devuelve el select de paises segun el valor que recibe
function selectPaises($array_paises) {
    foreach ($array_paises as $item):
        ?>
        <option value="<?= $item['papais'] ?>"><?= $item['panomb'] ?></option>
        <?php
    endforeach;
}

//Devuelve el select de direcciones segun el valor que recibe
function selectDirecciones($array_direcciones) {
    foreach ($array_direcciones as $item):
        $is_fija = "0";
        if(isset($item['is_fija'])):
            $is_fija = "1";
        endif;
        ?>
        <option is_fija="<?=$is_fija?>" value="<?= $item['id'] ?>"><?= $item['nombre'] ?></option>
        <?php
    endforeach;
}

//Devuelve el select de provincias segun el valor que recibe
function selectProvincias($array_provincias) {
    foreach ($array_provincias as $item):
        ?>
        <option value="<?= $item['poprov'] ?>"><?= $item['ponomb'] ?></option>
        <?php
    endforeach;
}

//Devuelve el select, con la opción seleccionado, segun el valor que recibe
function selectProvincias_id($array_provincias, $id) {
    foreach ($array_provincias as $item):

        $selected = "";
        if ($id == $item['poprov']):
            $selected = "selected";
        endif;
        ?>
        <option <?= $selected ?> value="<?= $item['poprov'] ?>"><?= $item['ponomb'] ?></option>
        <?php
    endforeach;
}

//Devuelve el select de poblaciones segun el valor que recibe
function selectPoblacion($array_poblaciones) {
    foreach ($array_poblaciones as $item):
        ?>
        <option value="<?= $item['cppobl'] ?>"><?= $item['cpnomb'] ?></option>
        <?php
    endforeach;
}

//Reemplaza caracteres especiales
function replaceCharacteresSql($text) {
    $text = htmlspecialchars($text);
    $text = htmlentities($text, ENT_QUOTES);
    $text = str_replace("[", "", $text);
    $text = str_replace("]", "", $text);
    return $text;
}

//Convierte los caracteres especiales en codigo html
function characteresSqlToHtml($text) {
    $text = htmlspecialchars_decode($text);
    $text = html_entity_decode($text);
    return $text;
}

//Oculta los textos numero de presupuesto, etc que se obtienen de la base de datos para ocultarlos en el pdf
function hideNumPre($text) {
    $text = str_replace('Ppto. MT. CL ', '', $text);
    $text = str_replace($_SESSION['clproc'] . $_SESSION['clcodi'] . '-', '', $text);
    $text = ltrim($text, '0');
    $text = "Ppto. " . $text;
    return $text;
}

//Oculta los textos numero de pedido, etc que se obtienen de la base de datos para ocultarlos en el pdf
function hideNumPed($text) {
    $text = str_replace('Ped. MT. CL ', '', $text);
    $text = str_replace($_SESSION['clproc'] . $_SESSION['clcodi'] . '-', '', $text);
    $text = ltrim($text, '0');
    $text = "Ped. " . $text;
    return $text;
}

//Oculta el texto ppto
function hidePpto($text) {
    $text = str_replace('Ppto.', '', $text);
    return $text;
}

//Oculta el texto ped
function hidePed($text) {
    $text = str_replace('Ped.', '', $text);
    return $text;
}

//Devuelve el precio al aplicar un descuento al precio recibido
function calc_dto($price, $dto) {
    $ahorro = ($price * $dto) / 100;
    $precio = $price - $ahorro;
    return round2decimals($precio);
}

//Ocultar los textos 'brut' 'blanco' del valor obtenido de la base de datos en los títulos de los artículos.
function hideColorListComponent($text) {
    $text = str_replace('BRUT', '', $text);
    $text = str_replace('BLANCO', '', $text);
    return $text;
}

//Ocultar los textos 'RAL'
function hideRal($text) {
    $text = str_replace('RAL ', '', $text);
    return $text;
}

//Creamos la sesión con los colores ral especiales 'lacados'
function define_array_ral() {
    if (!isset($_SESSION['array_colores_ral']))
        $_SESSION['array_colores_ral'] = array();
}

//Imprimimos el array de colores especiales 'lacados'
function show_array_ral() {
    print_r($_SESSION['array_colores_ral']);
}

//Define el array de lacados especiales
function set_array_ral($title_ral, $unidades) {
    if (!isset($_SESSION['array_colores_ral'][$title_ral]))
        $_SESSION['array_colores_ral'][$title_ral]['unidades'] = "0";
    $_SESSION['array_colores_ral'][$title_ral]['unidades'] = $unidades;
}

//Elimina del array de lacados especiales
function down_units_ral($title_ral, $unidades) {
    if (!isset($_SESSION['array_colores_ral'][$title_ral]))
        $_SESSION['array_colores_ral'][$title_ral]['unidades'] = "0";
    $_SESSION['array_colores_ral'][$title_ral]['unidades'] = $_SESSION['array_colores_ral'][$title_ral]['unidades'] - $unidades;
}

//Añade al array de lacados especiales
function up_units_ral($title_ral, $unidades) {
    if (!isset($_SESSION['array_colores_ral'][$title_ral]))
        $_SESSION['array_colores_ral'][$title_ral]['unidades'] = "0";
    $_SESSION['array_colores_ral'][$title_ral]['unidades'] = $_SESSION['array_colores_ral'][$title_ral]['unidades'] + $unidades;
}

//Obtiene el texto del tipo de medidda segun el valor obtenido
function getTipoMedida($type_medida) {
    $text_medida = "";
    if ($type_medida == "1"):
        $text_medida = "m<sup>2</sup>";
    elseif ($type_medida == "2"):
        $text_medida = lang_title_metros;
    else:
        $text_medida = lang_title_unidad;
    endif;
    return $text_medida;
}

//Normaliza caracteres de la cadena recibida.
function normaliza($cadena) {
    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞ
ßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuy
bsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $cadena = utf8_decode($cadena);
    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
    $cadena = strtolower($cadena);
    return utf8_encode($cadena);
}

//Funcion para obtener el nombre del expositor correctamente
function obtener_titulo_expositor($ararti) {
    $titulo = "Muestra ";
    switch ($ararti):
        case 1:
            $titulo .= "enrollable ventana";
            break;
        case 2:
            $titulo .= "enrollable ventana con muelle retenci&oacute;n";
            break;
        case 3:
            $titulo .= "enrollable puerta";
            break;
        case 4:
            $titulo .= "plisadas";
            break;
        case 5:
            $titulo .= "abatibles";
            break;
        case 6:
            $titulo .= "fijas y correderas";
            break;
        case 101:
            $titulo .= "selector de colores";
            break;
        case 201:
            $titulo .= "expositor personalizable";
            break;
    endswitch;
    return $titulo;
}

function login_time() {
    if (!isset($_SESSION)):
        session_start();
    endif;
    if (!isset($_SESSION['tiempo'])) {
        $_SESSION['tiempo'] = time();
    }
    if (time() - $_SESSION['tiempo'] > 6000) {
        session_destroy();
        header("Location:" . path_web . "index.php");
        die();
    }
}

// Elimina los parámetros suministrador mediante la array $keys de la URL $url
function remove_url_query_args($url, $keys = array()) {
    $url_parts = parse_url($url);
    if (empty($url_parts['query']))
        return $url;

    parse_str($url_parts['query'], $result_array);
    foreach ($keys as $key) {
        unset($result_array[$key]);
    }
    $url_parts['query'] = http_build_query($result_array);
    $url = (isset($url_parts["scheme"]) ? $url_parts["scheme"] . "://" : "") .
            (isset($url_parts["user"]) ? $url_parts["user"] . ":" : "") .
            (isset($url_parts["pass"]) ? $url_parts["pass"] . "@" : "") .
            (isset($url_parts["host"]) ? $url_parts["host"] : "") .
            (isset($url_parts["port"]) ? ":" . $url_parts["port"] : "") .
            (isset($url_parts["path"]) ? $url_parts["path"] : "") .
            (isset($url_parts["query"]) ? "?" . $url_parts["query"] : "") .
            (isset($url_parts["fragment"]) ? "#" . $url_parts["fragment"] : "");
    return $url;
}

//devuelve desktop, tablet o mobile
function comprobar_dispositivo() {

    $dispositivo = "desktop";

    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $dispositivo = "tablet";
    }

    if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $dispositivo = "mobile";
    }

    if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ( (isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
        $dispositivo = "mobile";
    }

    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
    $mobile_agents = array(
        'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
        'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
        'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
        'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
        'newt', 'noki', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
        'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
        'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
        'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
        'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-');

    if (in_array($mobile_ua, $mobile_agents)) {
        $dispositivo = "mobile";
        ;
    }

    if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'opera mini') > 0) {
        $dispositivo = "mobile";
        //Check for tablets on opera mini alternative headers
        $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) ? $_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : (isset($_SERVER['HTTP_DEVICE_STOCK_UA']) ? $_SERVER['HTTP_DEVICE_STOCK_UA'] : ''));
        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
            $dispositivo = "tablet";
        }
    }

    return $dispositivo;
}

function existe_id_pedido($valor, $nombre_tabla, $nombre_campo, $nombre_campo_empresa) {
    $conn_odbc = new Conn_odbc();
    $conexion = $conn_odbc->connect();
    $odbc = new Odbc();
    $existe_id_pedido = $odbc->ejecutar_array($conexion, "SELECT * FROM " . $nombre_tabla . " WHERE " . $nombre_campo . "='" . $valor . "' AND " . $nombre_campo_empresa . " = 'MT'");

    return $existe_id_pedido;
}

function saveInfoCabeceraCurl($array, $text_plain = NULL, $mensaje = NULL) {
    if (isset($text_plain) AND $text_plain == "1"):

        $array_cabecera = trim($array);

        $conn_odbc = new Conn_odbc();
        $conexion = $conn_odbc->connect();
        $odbc = new Odbc();
        $array_tabla = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWB100");
        $wbnro = count($array_tabla) + 1;
        $wbnro = str_pad($wbnro, 7, "0", STR_PAD_LEFT);
        $wempr = "MT";
        $wbalma = substr($array_cabecera, 0, 2);
        $wbtipa = substr($array_cabecera, 2, 1);
        $wbtipp = substr($array_cabecera, 3, 1);
        $wbfcha = substr($array_cabecera, 4, 8);
        $wbproc = substr($array_cabecera, 12, 2);
        $wbcodi = substr($array_cabecera, 14, 5);
        $wbdirv = substr($array_cabecera, 19, 1);
        $wbdire = "000"; //Siempre 000
        $wbaget = substr($array_cabecera, 23, 3);
        $wbsped = substr($array_cabecera, 26, 10);
        $wbfcsa = substr($array_cabecera, 36, 8);
        $wbimpr = substr($array_cabecera, 44, 9);
        if (empty($wbimpr) OR $wbimpr == '         '):
            $wbimpr = "000000000";
        endif;
        $wbid = substr($array_cabecera, 53, 7);
        $wbtecn = "220";
        $wbnped = "00"; //Campo cantidad (No sabemos porque está aqui en la tabla)
        $wbfche = date("Ymd");
        $wbhora = date("His");
        $wbctr1 = "";
        $wbctr2 = "";
        $wbctr3 = "";
        $wbctr4 = "";
        $wbctr5 = "";
        $wbctr6 = "";
        $wbctr7 = "";
        $wbctr8 = "";
        $wbctr9 = "";
        $wbct10 = "";

        $existe_id_pedido = existe_id_pedido($wbid, "GCWB100", "WBID");

        if (empty($existe_id_pedido)):
            $consulta = "INSERT INTO GCWB100 (WBNRO, WBEMPR, WBALMA, WBTIPA, WBTIPP, WBFCHA, WBPROC, WBCODI, WBDIRV, WBDIRE, WBAGET, WBSPED, WBFCSA, WBIMPR, WBID, WBTECN, WBNPED, WBFCHE, WBHORA, WBCTR1, WBCTR2, WBCTR3, WBCTR4, WBCTR5, WBCTR6, WBCTR7, WBCTR8, WBCTR9, WBCT10) VALUES (" . $wbnro . ", '" . $wempr . "', " . $wbalma . ", " . $wbtipa . ", '" . $wbtipp . "', " . $wbfcha . ", " . $wbproc . ", " . $wbcodi . ", '" . $wbdirv . "', '" . $wbdire . "', '" . $wbaget . "', '" . $wbsped . "', " . $wbfcsa . ", " . $wbimpr . ", " . $wbid . ", " . $wbtecn . ", " . $wbnped . ", " . $wbfche . ", " . $wbhora . ", '" . $wbctr1 . "', '" . $wbctr2 . "', '" . $wbctr3 . "', '" . $wbctr4 . "', '" . $wbctr5 . "', '" . $wbctr6 . "', '" . $wbctr7 . "', '" . $wbctr8 . "', '" . $wbctr9 . "', '" . $wbct10 . "')";

        else:
            $hora_actual = date("Y-m-d H:i:s");
            if (!empty($mensaje)):

                echo "<br><span class='mensaje_inicial'>" . $hora_actual . " Existe pedido ID ->" . $wbid . "</span><br>";

                $array_tabla_log = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWX100");
                $wbnro_log = count($array_tabla_log) + 1;

                $mensaje_error = "Existe pedido ID ->" . $wbid;

                $consulta_log = "INSERT INTO GCWX100 (WXNRO, WXID, WXTECN, WXFCHE, WXHORA, WXTXT) VALUES (" . $wbnro_log . ", " . $wbid . ", " . $wbtecn . ", " . $wbfche . ", " . $wbhora . ", '" . $mensaje_error . "')";

                $odbc->ejecutar($conexion, $consulta_log);
            endif;
            $consulta = "<br><span class='mensaje_error'>" . $hora_actual . " Error. Existe id de pedido</span>";
        endif;

        return $consulta;

    else:

        $abalma = "20";
        $abtipa = "1";
        $abtipp = "D";
        $abfcha = $array[0]['date'];
        $abproc = "03";
        $abcodi = "00717";
        $abdriv = "V";
        $abdire = "   ";
        $abaget = "SEU";
        $absped = $array[0]['id'];
        $abfcsa = "00000000";
        $abimpr = "REEMBOLSO";
        $id = $array[0]['id'];
        $abtecn = "220";

        $array_cabecera = ['abalma' => $abalma, 'abtipa' => $abtipa, 'abtipp' => $abtipp, 'abfcha' => $abfcha, 'abproc' => $abproc, 'abcodi' => $abcodi, 'abdriv' => $abdriv, 'abdire' => $abdire, 'abaget' => $abaget, 'absped' => $absped, 'abfcsa' => $abfcsa, 'abimpr' => $abimpr, 'id' => $id, 'abtecn' => $abtecn];

    endif;


    return $array_cabecera;
}

function saveInfoDetalleCurl($array, $text_plain = NULL) {
    if (isset($text_plain) AND $text_plain == "1"):
        $array_detalle = $array;

        $conn_odbc = new Conn_odbc();
        $conexion = $conn_odbc->connect();
        $odbc = new Odbc();
        $array_tabla = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWE100");
        $wenro = count($array_tabla) + 1;
        $weempr = "MT";
        $wealma = substr($array_detalle, 1, 2);
        $wenrr = substr($array_detalle, 3, 3);
        $weord = substr($array_detalle, 6, 4);
        $wetipl = substr($array_detalle, 10, 1);
        if ($wetipl == "3"):
            $wefami = '0';
            $wesubf = '0';
            $wecolo = '0';
            $wearti = '0';
        else:

            $wefami = substr($array_detalle, 11, 4);
            $wesubf = substr($array_detalle, 15, 2);
            $wecolo = substr($array_detalle, 17, 6);
            $wearti = substr($array_detalle, 23, 6);


        endif;

        if (empty($wecolo) OR $wecolo == " " OR $wecolo == "  " OR $wecolo == "   " OR $wecolo == "     " OR $wecolo == "     " OR $wecolo == "      "):
            $wecolo = "000000";
        endif;

        $wedesc = substr($array_detalle, 29, 30);
        $wecapr = substr($array_detalle, 59, 5);
        $weanch = substr($array_detalle, 64, 5);
        $weanch = substr($weanch, 0, 2) . "." . substr($weanch, 2, 5);
        $wealto = substr($array_detalle, 69, 5);
        $wealto = substr($wealto, 0, 2) . "." . substr($wealto, 2, 5);
        $wemand = substr($array_detalle, 74, 2);
        $werepl = substr($array_detalle, 76, 3);
        if (empty($werepl) OR $werepl == '   '):
            $werepl = "000";
        endif;
        //$werepl = str_pad($werepl, 3, "0");
        $wecc = substr($array_detalle, 79, 5);
        $wecc = substr($wecc, 0, 2) . "." . substr($wecc, 2, 5);
        $wetotm = substr($array_detalle, 84, 6);
        $wetotm = substr($wetotm, 0, 3) . "." . substr($wetotm, 3, 6);
        $wevert = substr($array_detalle, 90, 1);
        if (empty($wevert) OR $wevert == ' '):
            $wevert = "0";
        endif;
        $weincl = substr($array_detalle, 91, 1);
        if (empty($weincl) OR $weincl == ' '):
            $weincl = "0";
        endif;
        $weproc = substr($array_detalle, 92, 2);
        $wecodi = substr($array_detalle, 94, 5);
        $wesped = substr($array_detalle, 99, 10);
        $weid = substr($array_detalle, 109, 7);
        $wetecn = "220";
        $wefche = date("Ymd");
        $wehora = date("His");
        $wectr1 = "";
        $wectr2 = "";
        $wectr3 = "";
        $wectr4 = "";
        $wectr5 = "";
        $wectr6 = "";
        $wectr7 = "";
        $wectr8 = "";
        $wectr9 = "";
        $wect10 = "";


        $consulta = "INSERT INTO GCWE100 (WENRO, WEEMPR, WEALMA, WENRR, WENORD, WETIPL, WEFAMI, WESUBF, WECOLO, WEARTI, WEDESC, WEANCH, WEALTO, WEMAND, WEREPL, WECC, WETOTM, WEVERT, WEINCL, WEPROC, WECODI, WESPED, WEID, WETECN, WEFCHE, WEHORA, WECTR1, WECTR2, WECTR3, WECTR4, WECTR5, WECTR6, WECTR7, WECTR8, WECTR9, WECT10) VALUES (" . $wenro . ", '" . $weempr . "', " . $wealma . ", " . $wenrr . ", " . $weord . ", " . $wetipl . ", " . $wefami . ", " . $wesubf . ", " . $wecolo . ", " . $wearti . ", '" . $wedesc . "', " . $weanch . ", " . $wealto . ", " . $wemand . ", '" . $werepl . "', " . $wecc . ", " . $wetotm . ", " . $wevert . ", '" . $weincl . "', " . $weproc . ", " . $wecodi . ", " . $wesped . ", " . $weid . ", " . $wetecn . ", " . $wefche . ", " . $wehora . ", '" . $wectr1 . "', '" . $wectr2 . "', '" . $wectr3 . "', '" . $wectr4 . "', '" . $wectr5 . "', '" . $wectr6 . "', '" . $wectr7 . "', '" . $wectr8 . "', '" . $wectr9 . "', '" . $wect10 . "')";

        return $consulta;

    else:
        $array = $array[0]['lines'];

        $aealma = "20";
        $aenrr = "111";
        $aenord = "1111";
        $aetipl = "1";
        $aefami = "1111";
        $aesubf = "11";
        $aecolo = "111111";
        $aearti = "111111";
        $aedesc = "";
        $aecapr = "11111";
        $aeancho = "111111";
        $aealto = "111111";
        $aemand = "11";
        $aerepl = "111";
        $aecc = "111111";
        $aetotm = "111111";
        $aevert = "1";
        $aeincl = "1";
        $aeproc = "03";
        $aecodi = "00717";
        $absped = "1111111111";
        $id = "1111111";
        $abtecn = "220";

        $array_detalle = ['aealma' => $aealma, 'aenrr' => $aenrr, 'aenord' => $aenord, 'aetipl' => $aetipl, 'aefami' => $aefami, 'aesubf' => $aesubf, 'aecolo' => $aecolo, 'aearti' => $aearti, 'aedesc' => $aedesc, 'aecapr' => $aecapr, 'aeancho' => $aeancho, 'aealto' => $aealto, 'aemand' => $aemand, 'aerepl' => $aerepl, 'aecc' => $aecc, 'aetotm' => $aetotm, 'aevert' => $aevert, 'aeincl' => $aeincl, 'aeproc' => $aeproc, 'aecodi' => $aecodi, 'absped' => $absped, 'id' => $id, 'abtecn' => $abtecn];

    endif;

    return $array_detalle;
}

function saveInfoDireccionCurl($array, $text_plain = NULL) {
    if (isset($text_plain) AND $text_plain == "1"):
        $array_direccion = $array;

        $conn_odbc = new Conn_odbc();
        $conexion = $conn_odbc->connect();
        $odbc = new Odbc();
        $array_tabla = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWG100");
        $wgnro = count($array_tabla) + 1;
        $wgempr = "MT";
        $wgalma = substr($array_direccion, 1, 2);
        $wgdest = substr($array_direccion, 3, 40);
        $wgdir1 = substr($array_direccion, 43, 30);
        $wgdir2 = substr($array_direccion, 73, 30);
        $wgpais = substr($array_direccion, 103, 2);

        $wgppro = substr($array_direccion, 105, 4);
        if (empty($wgppro) OR $wgppro == '    '):
            $wgppro = '0000';
        endif;
        $wgprov = substr($array_direccion, 109, 4);
        $wgplob = substr($array_direccion, 113, 3);
        //$wgcont = substr($array_direccion, 116,2);
        $wgcont = '  ';
        $wgobse = substr($array_direccion, 118, 30);
        $wgsped = substr($array_direccion, 148, 10);
        $wgnopb = substr($array_direccion, 158, 30);
        $wgnopr = substr($array_direccion, 188, 30);
        $wgid = substr($array_direccion, 218, 7);
        $wgtecn = substr($array_direccion, 225, 3);
        $wgfche = date("Ymd");
        $wghora = date("His");
        $wgctr1 = "";
        $wgctr2 = "";
        $wgctr3 = "";
        $wgctr4 = "";
        $wgctr5 = "";
        $wgctr6 = "";
        $wgctr7 = "";
        $wgctr8 = "";
        $wgctr9 = "";
        $wgct10 = "";

        $consulta = "INSERT INTO GCWG100 (WGNRO, WGEMPR, WGALMA, WGDEST, WGDIR1, WGDIR2, WGPAIS, WGPPRO, WGPROV, WGPOBL, WGCONT, WGOBSE, WGSPED, WGNOPB, WGNOPR, WGID, WGTECN, WGFCHE, WGHORA, WGCTR1, WGCTR2, WGCTR3, WGCTR4, WGCTR5, WGCTR6, WGCTR7, WGCTR8, WGCTR9, WGCT10) VALUES (" . $wgnro . ", '" . $wgempr . "', " . $wgalma . ", '" . $wgdest . "', '" . $wgdir1 . "', '" . $wgdir2 . "', " . $wgpais . ", " . $wgppro . ", " . $wgprov . ", " . $wgplob . ", '" . $wgcont . "', '" . $wgobse . "', " . $wgsped . ", '" . $wgnopb . "', '" . $wgnopr . "', '" . $wgid . "', " . $wgtecn . "," . $wgfche . ", " . $wghora . ", '" . $wgctr1 . "', '" . $wgctr2 . "', '" . $wgctr3 . "', '" . $wgctr4 . "', '" . $wgctr5 . "', '" . $wgctr6 . "', '" . $wgctr7 . "', '" . $wgctr8 . "', '" . $wgctr9 . "', '" . $wgct10 . "')";

        return $consulta;

    else:
        $array = $array[0]['address'];

        $agalma = "";
        $agdest = "";
        $agdir1 = "";
        $agdir2 = "";
        $agpais = "";
        $agppro = "";
        $agprov = "";
        $agpobl = "";
        $agcont = "";
        $agobse = "";
        $absped = "";
        $nopb = "";
        $nopr = "";
        $id = "";
        $abtecn = "220";

        $array_direccion = ['agalma' => $agalma, 'agdest' => $agdest, 'agdir1' => $agdir1, 'agdir2' => $agdir2, 'agpais' => $agpais, 'agppro' => $agppro, 'agprov' => $agprov, 'agpobl' => $agpobl, 'agcont' => $agcont, 'agobse' => $agobse, 'absped' => $absped, 'nopb' => $nopb, 'nopr' => $nopr, 'id' => $id, 'abtecn' => $abtecn];

    endif;

    return $array_direccion;
}

function pingWeb($host) {
    if ($socket = @ fsockopen($host, 80, $errno, $errstr, 30)) {
        $estado = 'online';
        fclose($socket);
    } else {
        $estado = 'offline';
    }

    return $estado;
}

function locationToIndex() {
    header("Location:" . path_web);
}

//Pendiente subida a real
function ordernar_array_albaranes_impresion_old($array) {
    $total_reg = count($array);
    $count_reg_nuevo = 0;
    $count_lacados = 0;
    $count_acabados = 0;
    $count_incrementos = 0;
    $array_ordenado = array();
    foreach ($array as $index => $item):
        if ($item['albobs'] == "lacado"):
            $pos = $count_lacados + $count_acabados + $count_incrementos;
            $array_ordenado[$pos] = $item;
            $count_lacados++;
            $count_reg_nuevo++;
            $color_lacado = $item['albco'];
//            echo "Añadida linea de lacado con id de linea: ".$item['id']."<br>";
            foreach ($array as $index_aux => $item_aux):
                if ($item_aux['albfami'] == "2731" || $item_aux['albfami'] == "2732" || $item_aux['albfami'] == "2733" || $item_aux['albfami'] == "2734" || $item_aux['albfami'] == "2735" || $item_aux['albfami' == "2736"]):
                    if ($color_lacado == $item_aux['albco'] && $item_aux['albobs'] != "lacado"):
                        $pos = $count_lacados + $count_acabados + $count_incrementos;
                        $array_ordenado[$pos] = $item_aux;
                        $count_acabados++;
                        $count_reg_nuevo++;
//                        echo "Añadida linea de producto acabado con id de linea: ".$item_aux['id']."<br>";
                    endif;
                endif;
            endforeach;
        endif;
    endforeach;
    return $array_ordenado;
}

//Pendiente subida a real
function ordernar_array_albaranes_impresion($array) {
    $total_reg = count($array);
    $count_reg_nuevo = 0;
    $count_lacados = 0;
    $count_acabados = 0;
    $count_incrementos = 0;
    $array_ordenado = array();
    foreach ($array as $index => $item):
        if (strpos($item['albobs'], "is_increment") !== false):
            $pos = $count_lacados + $count_acabados + $count_incrementos;
            $array_ordenado[$pos] = $item;
            $count_incrementos++;
            $count_reg_nuevo++;
        endif;
    endforeach;
    foreach ($array as $index_aux => $item_aux):
        if ($item['albobs'] == "lacado"):
            $pos = $count_lacados + $count_acabados + $count_incrementos;
            $array_ordenado[$pos] = $item_aux;
            $count_lacados++;
            $count_reg_nuevo++;
            $color_lacado = $item_aux['albco'];
        endif;
    endforeach;
    foreach ($array as $index => $item):
        if (empty($item['albobs']) && ($item['albfami'] == "2731" || $item['albfami'] == "2732" || $item['albfami'] == "2733" || $item['albfami'] == "2734" || $item['albfami'] == "2735" || $item['albfami' == "2736"])):
            $pos = $count_lacados + $count_acabados + $count_incrementos;
            $array_ordenado[$pos] = $item;
            $count_acabados++;
            $count_reg_nuevo++;
        endif;
    endforeach;

    return $array_ordenado;
}
?>