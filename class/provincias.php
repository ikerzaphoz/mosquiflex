<?php

Class Provincias{
    private $id_pais;
    private $id_prov;
    private $nom_prov;

    public function __construct($id_pais = NULL, $id_prov = NULL, $nom_prov = NULL)
    {
        $this->id_pais = $id_pais;
        $this->id_prov = $id_prov;
        $this->nom_prov = $nom_prov;
    }

    public function getIdPais()
    {
        return $this->id_pais;
    }

    public function setIdPais($id_pais)
    {
        $this->id_pais = $id_pais;
    }

    public function getIdProv()
    {
        return $this->id_prov;
    }

    public function setIdProv($id_prov)
    {
        $this->id_prov = $id_prov;
    }

    public function getNomProv()
    {
        return $this->nom_prov;
    }

    public function setNomProv($nom_prov)
    {
        $this->nom_prov = $nom_prov;
    }

    public function getListadoNombres(){
//        $bd = new Db();
//        $sql = "SELECT * FROM provincias WHERE popais = '1'";
//        $result = $bd->obtener_consultas($sql);
//        return $result;
        $bd = new Db();
        $sql = "SELECT * FROM provincias WHERE popais = '1' AND empresa = '".empresa."'";
        //$result = Cache::get(path_cache.'provincias');
        //if (empty($result)) {
            $result = $bd->obtener_consultas($sql);
            //Cache::put(path_cache.'provincias', $result);
        //}
        return $result;

    }


}