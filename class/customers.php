<?php

Class Customers {

    private $cif;
    private $pass;

    public function __construct($cif = NULL, $pass = NULL) {
        $this->cif = $cif;
        $this->pass = $pass;
    }

    public function getCif() {
        return $this->cif;
    }

    public function setCif($cif) {
        $this->cif = $cif;
    }

    public function getPass() {
        return $this->pass;
    }

    public function setPass($pass) {
        $this->pass = $pass;
    }

    public function ini_log($cif, $pass) {
        $bd = new Db();
        $sql = "SELECT * FROM clientes WHERE (cldnic = '$cif' OR email = '$cif') AND pass = '$pass' AND empresa = '".empresa."'";
        if ($bd->ejecutarReturnAffected($sql) == 1):
            return $bd->obtener_consultas($sql);
        else:
            return 0;
        endif;
    }

    public function get_cliente($clproc = NULL, $clcodi = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM clientes WHERE clproc = '$clproc' AND clcodi = '$clcodi' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function get_direccion_envio($clproc = NULL, $clcodi = NULL) {
        $bd = new Db();
        $sql = "SELECT clproc, clcodi, cldnic, cldir1, cldir2, clpais, clppro, clprov, clpobl, cltlf1, cltlf2, clfax, empresa FROM clientes WHERE clproc = '$clproc' AND clcodi = '$clcodi' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function get_direcciones_envio($clproc = NULL, $clcodi = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM direcciones_envio_pedidos WHERE clproc = '$clproc' AND clcodi = '$clcodi' AND status = '1' AND is_temporal = '0' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }
    
    public function get_direcciones_fijas($clproc = NULL, $clcodi = NULL) {
        $bd = new Db();
        $sql = "SELECT dedest as nombre, deproc as clproc, decodi as clcodi, deveng as id, dedest as destinatario, dedir1 as cldir1, dedir2 as cldir2, depais as clpais, deprov as clprov, depobl as clpobl, empresa, is_fija FROM direcciones_clientes_fijas WHERE deproc = '$clproc' AND decodi = '$clcodi' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function getPais($id) {
        $bd = new Db();
        $sql = "SELECT panomb FROM paises WHERE papais = '$id' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['panomb'];
    }

    public function getProvincia($id_pais, $id_pro) {
        $bd = new Db();
        $sql = "SELECT ponomb FROM provincias WHERE popais = '$id_pais' AND poprov = '$id_pro' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['ponomb'];
    }

    public function getPoblacion($id_pais, $id_pro, $id_pob) {
        $bd = new Db();
        $sql = "SELECT cpnomb FROM poblaciones WHERE cppais = '$id_pais' AND cpprov = '$id_pro' AND cppobl = '$id_pob' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['cpnomb'];
    }
    
    public function getCodigosPostales($id_pais, $id_pro, $nomb_pobl){
        $bd = new Db();
        $sql = "SELECT * FROM poblaciones WHERE cppais = '$id_pais' AND cpprov = '$id_pro' AND cpnomb = '$nomb_pobl' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function add_dire_envio($cldir1 = NULL, $destinatario = NULL, $cldir2 = NULL, $pais = NULL, $provincia = NULL, $poblacion = NULL, $cltlf1 = NULL, $cltlf2 = NULL, $nombre = NULL, $clproc = NULL, $clcodi = NULL, $is_temporal = NULL) {
        $bd = new Db();
        $sql = "INSERT INTO direcciones_envio_pedidos (nombre, destinatario, clproc, clcodi, cldir1, cldir2, clpais, clppro, clprov, clpobl, cltlf1, cltlf2, status, is_temporal, empresa) VALUES ('$nombre', '$destinatario' ,'$clproc', '$clcodi', '$cldir1', '$cldir2', '$pais', '$provincia', '$provincia', '$poblacion', '$cltlf1', '$cltlf2', '1', '$is_temporal', '".empresa."')";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }

    public function getSelectPaises() {
        $bd = new Db();
        $sql = "SELECT * FROM paises WHERE empresa = '".empresa."' GROUP BY panomb ORDER BY papais";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function getSelectProvincias($id_pais) {
        $bd = new Db();
        $sql = "SELECT * FROM provincias WHERE popais = '$id_pais' AND empresa = '".empresa."' GROUP BY ponomb";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function getSelectPoblaciones($id_pais, $id_prov) {
        $bd = new Db();
        $sql = "SELECT * FROM poblaciones WHERE cppais = '$id_pais' AND cpprov = '$id_prov' AND empresa = '".empresa."' GROUP BY cpnomb";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function get_direccion_envio_id($id) {
        $bd = new Db();
        $sql = "SELECT * FROM direcciones_envio_pedidos WHERE id = '$id' AND status = '1'  AND is_temporal = '0' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }
    
    public function get_direccion_envio_is_fija($clproc = NULL, $clcodi = NULL, $deveng = NULL) {
        $bd = new Db();
        $sql = "SELECT dedest as nombre, deproc as clproc, decodi as clcodi, deveng as id, dedest as destinatario, dedir1 as cldir1, dedir2 as cldir2, depais as clpais, deprov as clprov, depobl as clpobl, empresa, is_fija FROM direcciones_clientes_fijas WHERE deproc = '$clproc' AND decodi = '$clcodi' AND deveng = '$deveng' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function get_direccion_envio_id_temporal($id) {
        $bd = new Db();
        $sql = "SELECT * FROM direcciones_envio_pedidos WHERE id = '$id' AND status = '1'  AND is_temporal = '1' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function deleteDire($clproc = NULL, $clcodi = NULL, $id_dire = "NULL") {
        $bd = new Db();
        $sql = "UPDATE direcciones_envio_pedidos SET status = 0 WHERE clproc = '$clproc' AND clcodi = '$clcodi' AND id = '$id_dire' AND empresa = '".empresa."'";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }

    public function get_direccion_envio_pdf($clproc = NULL, $clcodi = NULL) {
        $bd = new Db();
        $sql = "SELECT cldir1, cldir2, clpais, clppro, clprov, clpobl, cltlf1, cltlf2, clfax, empresa FROM clientes WHERE clproc = '$clproc' AND clcodi = '$clcodi' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function get_direccion_envio_id_pdf($id) {
        $bd = new Db();
        $sql = "SELECT * FROM direcciones_envio_pedidos WHERE id = '$id' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function get_last_id_dire($clproc = NULL, $clcodi = NULL) {
        $bd = new Db();
        $sql = "SELECT id FROM direcciones_envio_pedidos WHERE clproc = '$clproc' AND empresa = '".empresa."' AND clcodi = '$clcodi' ORDER BY id DESC LIMIT 1";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function get_direccion_recogida() {
        $bd = new Db();
        $sql = "SELECT * FROM direcciones_envio_pedidos WHERE id = '0' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function getEmail($clcif = NULL) {
        $bd = new Db();
        $sql = "SELECT email FROM clientes WHERE cldnic = '$clcif' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['email'];
    }

    public function setEmail($clcif = NULL, $email = NULL) {
        $bd = new Db();
        $sql = "UPDATE clientes SET email = '$email' WHERE cldnic = '$clcif' AND empresa = '".empresa."'";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }

    public function getInfoPass($clcif = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM recuperar_pass WHERE cldnic = '$clcif' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function setToken($clcif = NULL, $token = NULL, $fecha_creacion = NULL, $fecha_caduca = NULL) {
        $bd = new Db();
        $sql = "UPDATE recuperar_pass SET token = '$token', fecha_creacion = '$fecha_creacion', fecha_caduca = '$fecha_caduca' WHERE cldnic = '$clcif' AND empresa = '".empresa."'";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }

    public function getToken($token = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM recuperar_pass WHERE token = '$token' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        if (isset($result[0])):
            return $result[0];
        else:
            return 0;
        endif;
    }

    public function setTokenInsert($clcif = NULL, $token = NULL, $fecha_creacion = NULL, $fecha_caduca = NULL) {
        $bd = new Db();
        $sql = "INSERT INTO recuperar_pass (cldnic, token, fecha_creacion, fecha_caduca, empresa) VALUES('$clcif', '$token', '$fecha_creacion', '$fecha_caduca', '".empresa."')";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }

    public function setPassword($clcif = NULL, $pass = NULL) {
        $bd = new Db();
        $sql = "UPDATE clientes SET pass = '$pass' WHERE cldnic = '$clcif' AND empresa = '".empresa."'";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }

    public function empty_token($clcif = NULL) {
        $bd = new Db();
        $sql = "DELETE FROM recuperar_pass WHERE cldnic = '$clcif' AND empresa = '".empresa."'";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }

    public function setEmailByCodigo($email = NULL, $clproc = NULL, $clcodi = NULL) {
        $bd = new Db();
        $sql = "UPDATE clientes SET email = '$email' WHERE clproc = '$clproc' AND clcodi = '$clcodi' AND empresa = '".empresa."'";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }
    
    public function numero_cliente_final() {
        $bd = new Db();
        $sql = "SELECT count(*) as total_clientes_final FROM clientes WHERE clproc = '99' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['total_clientes_final'];
    }
    
    public function comprobar_existe_email($email = NULL) {
        $bd = new Db();
        $sql = "SELECT count(*) as total_email FROM clientes WHERE email = '$email' AND empresa = '".empresa."'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['total_email'];
    }

    public function registro_cliente_final($clproc = NULL, $clcodi = NULL, $nombre = NULL, $direccion = NULL, $pais = NULL, $provincia = NULL, $poblacion = NULL, $dni = NULL, $telefono = NULL, $contrasenna = NULL, $email = NULL) {
        $bd = new Db();
        $sql = "INSERT INTO clientes (clproc, clcodi, clnomb, cldir1, clpais, clppro, clprov, clpobl, cldnic, cltlf1, pass, email, is_cliente_final, empresa) VALUES ('$clproc', '$clcodi', '$nombre', '$direccion', '$pais', '$provincia', '$provincia', '$poblacion', '$dni', '$telefono', '$contrasenna', '$email', '1', '".empresa."')";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }
    
}
