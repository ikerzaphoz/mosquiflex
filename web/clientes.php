<?php

include '../config.php';
$page = "clientes";

//Cargo contenido para flexol.es
if(empresa == "EN"):
    include '../flexol_mosquiteras/index_clientes.php';
    exit;
endif;

$error = "0";
$cliente = new Customers();

if (!isset($_GET) OR ($_GET['opt'] != "remember_pass" && $_GET['opt'] != "customer_registration")):
    check_login();
endif;

if (isset($_GET['error'])): $error = $_GET['error']; endif;
if (isset($_GET['pres_error'])):

    ?>
    <script>
        window.onload = function () {
            toastr.options = {
                "positionClass": "toast-top-full-width"
            }
            toastr.error("Presupuestos", "Presupuesto no encontrado");
        };
    </script>
    <?php

endif;

include root . 'web/mods/mod_head/index.php';
//Menu
if (dispositivo == "desktop"):
    include root . 'web/mods/mod_nav/index.php';
else:
    include root . 'web/mods/mod_nav/index_mobile.php';
endif;
if ($error == -1):
    include root . 'web/mods/mod_customers/ficha.php';
endif;
if (isset($_GET['opt'])):
    include root . 'web/mods/mod_customers/' . $_GET['opt'] . '.php';
endif;
include root . 'web/mods/mod_footer/index.php';
include root . 'web/mods/mod_modal/modal_ini_log.php';
include root . 'web/mods/mod_modal/modal_add_dire_envio.php';
include root . 'web/mods/mod_modal/modal_edit_notas.php';
include root . 'web/mods/mod_modal/modal_view_conceptos.php';
include root . 'web/mods/mod_modal/modal_help.php';
include root . 'web/mods/mod_modal/modal_upload_file.php';