<div class="effect7 col-md-12 col-xs-12 text-left line_product_expositor">
    <div class="row col-md-12 col-xs-12">
        <h4 class="title_product"><?=lang_title_expositores_punto_venta?></h4>
    </div>
    <div class="row col-md-12 col-xs-12 text-center">
        <div class="col-md-4 col-xs-4 title_line_product">Producto</div>
        <div class="col-md-4 col-xs-4 title_line_product">Unidades</div>
        <div class="col-md-3 col-xs-3 title_line_product">Precio (€)</div>
        <div class="col-md-1 col-xs-1 title_line_product"></div>
    </div>

    <div class="col-md-12 col-xs-12 row text-center">
        <?php

        foreach ($_POST as $producto => $unidades):
            $unidades_producto = "";
            $titulo_producto = "";
            $precio = "";
            $arfami = "1999";
            $arsubf = "52";
            $ararti = "";
            $alto = "0.740";
            $ancho = "0.530";
            if (isset($unidades) AND $unidades > 0):
                switch ($producto):
                    case 'enr_ventana':
                        $titulo_producto = "enrollable ventana";
                        $precio_unitario = 75;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000001";
                        break;
                    case 'enr_ventana_muelle':
                        $titulo_producto = "enrollable ventana con muelle retenci&oacute;n";
                        $precio_unitario = 80;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000002";
                        break;
                    case 'enr_puerta':
                        $titulo_producto = "enrollable puerta";
                        $precio_unitario = 150;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000003";
                        break;
                    case 'exp_plisada':
                        $titulo_producto = "plisada";
                        $precio_unitario = 150;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000004";
                        break;
                    case 'exp_abatible':
                        $titulo_producto = "abatible";
                        $precio_unitario = 150;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000005";
                        break;
                    case 'exp_fija_corredera':
                        $titulo_producto = "fijas y correderas";
                        $precio_unitario = 75;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000006";
                        break;
                    case 'exp_selector_color':
                        $titulo_producto = "selector de colores";
                        $precio_unitario = 10;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000101";
                        break;
                    case 'exp_fija_expositor':
                        $titulo_producto = "expositor personalizable";
                        $precio_unitario = 500;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000201";
                        $alto = "2.100";
                        $ancho = "0.620";
                        break;
                endswitch;
                $unidades_producto = $unidades;
                $total_presupuesto = $total_presupuesto + $precio;

            endif;

            if (!empty($unidades) AND !empty($titulo_producto)):

                ?>
                <div class="col-md-12 col-xs-12 line_expositor line_product">
                    <div class="col-md-4 col-xs-4 text-left">Muestra <?= strtolower($titulo_producto) ?></div>
                    <div class="col-md-4 col-xs-4 text-center">
                        <input type="number" min="0" value="<?= $unidades_producto ?>"
                               class="border_units_components change_units_resumen_expositor">
                    </div>
                    <div class="col-md-3 col-xs-3 product_price text-center"><?= round2decimals($precio) ?></div>
                    <div class="col-md-1 col-xs-1"><span><i
                                class='fa fa-times-circle btn_delete_line_component' aria-hidden='true'></i></span>
                    </div>
                    <input type="hidden" class="price_expositor_hidden" value="<?= $precio_unitario ?>">
                    <input type="hidden" class="arfami input_id_product" span_value="<?=$arfami?>" value="<?= $arfami ?>">
                    <input type="hidden" class="arsubf input_subfamily_product" value="<?= $arsubf ?>">
                    <input type="hidden" class="ararti" value="<?= $ararti ?>">
                    <input type="hidden" class="fami_color input_color_product" span_value="0" value="0">
                    <input type="hidden" class="input_group_color" span_value="0">
                    <input type="hidden" class="ancho_expositor_hidden input_width_product" value="<?= $ancho ?>">
                    <input type="hidden" class="alto_expositor_hidden input_height_product" value="<?= $alto ?>">
                    <input type="hidden" class="precio_unidades_medidas" value="<?= $precio_unitario ?>">
                    <input type="hidden" class="is_almacen" value="0">
                    <input type="hidden" class="observaciones_product" value="<?=$producto?>">
                </div>
                <?php
            endif;
        endforeach;

        ?>
    </div>
</div>
