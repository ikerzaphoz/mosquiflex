<div id="content_product" class="row">
    <form class="form_product" name="form_product" method="post" action="<?=path_web?>productos.php">
        <div class="panel-group">

            <div class="panel panel-default form-inline col-md-12 col-xs-12">
                <div class="panel-heading selected">
                    <h4 class="panel-title">
                        <div class="title_panel_title"><?=lang_title_product?></div>
                    </h4>
                </div>

                <div class="panel-collapse text-center nopadding">
                    <div id_product="<?=$id_pro?>" class="radio col-md-8 col-xs-6">
                        <img class="icon_product" src="<?=path_image?>icons_products/<?=$id_pro?>.jpg"><label><span class="title_label selected"><?=$title_product?></span></label>
                    </div>
                    <div class="radio col-md-4 col-xs-6">
                        <ul class="text-left list-unstyled">
                            <li><?=lang_title_ficha_producto?>&nbsp;<i class="fa fa-file-pdf-o" aria-hidden="true"></i></li>
                            <li><?=lang_title_ficha_prestaciones?> (CE)&nbsp;<i class="fa fa-file-o" aria-hidden="true"></i></li>
                            <li class="hidden"><?=lang_title_hoja_pedido?>&nbsp;<i class="fa fa-file-excel-o" aria-hidden="true"></i></li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="type-product panel panel-default form-inline col-md-12 col-xs-12">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="title_panel_title"><?=lang_title_tipo_producto?></div>
                    </h4>
                </div>

                <div class="panel-collapse text-center nopadding">
                    <div class="col-md-4 col-xs-6 text-right align_left_mobile content_radio_product">
                        <input type="radio" name="change_components" class="change_components change_components_acabado" value="2"><label class="font-light check_product_component">&nbsp;&nbsp;&nbsp;<?=lang_title_producto_acabado?></label>
                    </div>
                    <div class="col-md-4 col-xs-6 text-center">
                        <input type="radio" name="change_components" class="<?=$hidden_element?> change_components" value="1"><label class="<?=$hidden_element?> font-light check_product_component">&nbsp;&nbsp;&nbsp;<?=lang_title_componente?></label>
                    </div>
                    <div class="col-md-4 col-xs-6 text-left">
                        <input type="radio" name="change_components" class="<?=$hidden_element?> change_components" value="3"><label class="<?=$hidden_element?> font-light check_product_component">&nbsp;&nbsp;&nbsp;<?=lang_title_venta_industrial?></label>
                    </div>
                </div>
            </div>

            <div class="info_insert"></div>

            <div class="hidden panel panel-default form-inline col-md-12 col-xs-12 div_color_acabado nopadding">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="title_panel_title"><?=lang_title_colores?></div>
                    </h4>
                </div>
                <div class="panel-collapse col-md-12 col-xs-12 nopadding">
                    <div class="row">
                        <div class="div_colors col-md-8 col-xs-6">
                            <h5><?=lang_title_carta_colores?></h5>
                            <?php

                            foreach($array_grupos as $item): ?>

                                <div group_color="<?=$item['cosubf']?>" class="col-md-12 col-xs-12 group_color_mobile">
                                    <h6>GRUPO <?=$item['cosubf']?></h6>
                                    <?php
                                    $array_colores = $familia->getColoresByFamilia($id_pro, $item['cosubf']);

                                    foreach($array_colores as $item_color): ?>
                                        <div color_product="<?=$item_color['cocolo']?>" class="group_colors col-md-6">
                                            <?php


                                            if($item['cosubf'] == 4 OR $item['cosubf'] == 3): ?>

                                                <?php

                                                $url_imagen = $id_pro.$item['cosubf'].$item_color['cocolo'].".jpg";

                                                ?>
                                                <div class="color_product change_color_product">
                                                    <img class="img_color_product" src="<?=path_image_colors.$url_imagen?>">
                                                </div>

                                            <?php else:?>

                                                <div style="background-color: <?=$item_color['html']?>" class="color_product change_color_product"></div>


                                            <?php endif; ?>
                                            <span class="default_color title_color_product"><?=$item_color['codesc']?></span>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endforeach;

                            ?>
                            <div group_color="lacados" class="col-md-12 col-xs-12">
                                <h6><?=lang_lacados_carta_ral?></h6>
                                <div color_product="lacado" class="group_colors col-md-12 col-xs-12 div_group_lacado_mobile">
                                    <div class="color_product change_color_product">
                                        <div style="background-color: #FFFFFF" class="color_product change_color_product_ral"></div>
                                    </div>

                                    <span class="default_color title_color_product text_color_ral"></span>
                                    <input class="form-control change_color_ral" size="4" maxlength="4" min="4" type="text" name="change_color_ral" placeholder="Ej. 8012">
                                </div>
                            </div>
                        </div>
                        <div class="div_preview_img col-md-2 col-xs-6">
                            <h5><?=lang_title_vista_previa?></h5>
                            <div class="text-center div_content_preview_img">
                                <div style="background-image: url(<?=path_image?>images_product/<?=$id_pro?>/2.png)" class="row preview_img">
                                    <span class="text_no_image_preview"></span>
                                    <div class="content_lupa_vista_previa">
                                        <a class="image-popup-no-margins lupa_div_content_preview_img icono-lupa fa fa-search" href="<?=path_image?>images_product/<?=$id_pro?>/2.png">
                                            <img class="hidden" src="<?=path_image?>images_product/<?=$id_pro?>/2.png">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span class="message_color text-warning"><span class="fa fa-warning"></span>&nbsp;<?=lang_text_diferir_color?></span>
                </div>
            </div>

            <div class="form-inline col-md-12 col-xs-12 div_submit_product padding8 hidden">
                    <div class="panel-collapse text-center padding8">
                    <div class="form-group">
                        <button type="button" class="btn-sm btn_modal_ini_pro btn bg-border-info" name="type_button" value="presupuesto"><?=lang_title_continuar_pres?></button>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn-sm btn_modal_ini_pro btn bg-border-warning" name="type_button" value="pedido"><?=lang_title_continuar_ped?></button>
                    </div>
                </div>
            </div>

        </div>

        <input value="<?=$id_pro?>" span_value="<?=$id_pro?>" class="hidden input_id_product" name="id_product"/>
        <input value="" class="hidden input_color_product" name="color_product"/>
        <input value="" class="hidden input_group_color" name="group_color"/>
        <input value="" class="hidden input_type_button" name="type_button"/>

    </form>
</div>