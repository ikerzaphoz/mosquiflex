<?php

require '../../../../config.php';

$familia = new Familia();
$array_familia = $familia->getFamilia('fafami');

$id_product = "";
if (isset($_POST['id_product'])): $id_product = $_POST['id_product']; endif;
$color_product = "";
if (isset($_POST['color_product'])): $color_product = $_POST['color_product']; endif;
$group_color = "";
if (isset($_POST['group_color'])): $group_color = $_POST['group_color']; endif;
$subfamily_product = "";
if (isset($_POST['subfamily_product'])): $subfamily_product = $_POST['subfamily_product']; endif;
$input_unit_product = "";
if (isset($_POST['unit_product']))
    $input_unit_product = $_POST['unit_product'];
$input_width_product = "";
if (isset($_POST['width_product']))
    $input_width_product = $_POST['width_product'];
$input_height_product = "";
if (isset($_POST['height_product']))
    $input_height_product = $_POST['height_product'];
$input_color_product = "";
if (isset($_POST['input_color_product']))
    $input_color_product = $_POST['input_color_product'];
$total_lacado = "";
if (isset($_POST['total_lacado']))
    $total_lacado = $_POST['total_lacado'];

if ($id_product == 2732):
    $subfamily_product = "";
    if ($input_width_product <= "1.40"):
        $subfamily_product = "";
    endif;
    if ($input_width_product > "1.40"):
        $subfamily_product = "2";
    endif;
endif;
$art_sub = $subfamily_product . $group_color;

$ancho_minimo = $familia->getAnchoMin($id_product, $art_sub);
$ancho_maximo = $familia->getAnchoMax($id_product, $art_sub);
$alto_minimo = $familia->getAltoMin($id_product, $art_sub);
$alto_maximo = $familia->getAltoMax($id_product, $art_sub);

if ($input_width_product < $ancho_minimo || $input_width_product > $ancho_maximo):
    echo "<span class='text-danger'>Revisar medidas mínimas y máximas</span>";
    exit;
endif;

if ($input_height_product < $alto_minimo OR $input_height_product > $alto_maximo):
    echo "<span class='text-danger'>Revisar medidas mínimas y máximas</span>";
    exit;
endif;

$rango = "";

if ($id_product == 2731):
    $input_width_product_rango = calc_precio_enrollable_ventana($input_width_product, $array_familia[$id_product]['min_ran_anc']);
    $input_height_product_rango = calc_precio_enrollable_ventana($input_height_product, $array_familia[$id_product]['min_ran_alt']);
endif;

if ($id_product == 2732):
    $input_width_product_rango = calc_precio_ancho_enrollable_puerta($input_width_product, $array_familia[$id_product]['min_ran_anc']);
    $input_height_product_rango = calc_precio_alto_enrollable_puerta($input_height_product, $array_familia[$id_product]['min_ran_alt']);
endif;

if ($id_product == 2733):
    $input_width_product_rango = calc_precio_ancho_plisada($input_width_product, $array_familia[$id_product]['min_ran_anc'], $subfamily_product);
    $input_height_product_rango = calc_precio_alto_plisada($input_height_product, $array_familia[$id_product]['min_ran_alt']);
endif;

if ($id_product == 2734):
    $input_width_product_rango = calc_precio_ancho_fija($input_width_product, $array_familia[$id_product]['min_ran_anc']);
    $input_height_product_rango = calc_precio_alto_fija($input_height_product, $array_familia[$id_product]['min_ran_alt']);
endif;

if ($id_product == 2735):
//    $input_width_product_rango = calc_precio_ancho_corredera($input_width_product, $array_familia[$id_product]['min_ran_anc']);
//    $input_height_product_rango = calc_precio_alto_corredera($input_height_product, $array_familia[$id_product]['min_ran_alt']);
//    $input_width_hoja_product_rango = calc_precio_hoja_corredera($input_width_product);
endif;


if ($id_product == 2736):
    $input_width_product_rango = calc_precio_ancho_abatible($input_width_product, $array_familia[$id_product]['min_ran_anc'], $subfamily_product);
    $input_height_product_rango = calc_precio_alto_abatible($input_height_product, $array_familia[$id_product]['min_ran_alt']);
endif;

if ($id_product == 2735):

//    $rango_hoja = $input_width_hoja_product_rango.$input_height_product_rango;
//    $precio_rango_hoja = $familia->getPriceOfRango($id_product, "1".$art_sub, $rango_hoja);
//    $precio_rango_hoja = $precio_rango_hoja * 2 * $input_unit_product;
//    $precio_rango_hoja = number_format($precio_rango_hoja, 2);
//    if (strpos($precio_rango_hoja, ",")):
//        $precio_rango_hoja = str_replace(",", "", $precio_rango_hoja);
//    endif;
//
//    $rango = $input_width_product_rango . $input_height_product_rango;
//    $precio_rango = $familia->getPriceOfRango($id_product, $art_sub, $rango);
//    $precio_rango = $precio_rango * $input_unit_product;
//    $precio_rango = number_format($precio_rango, 2);
//    if (strpos($precio_rango, ",")):
//        $precio_rango = str_replace(",", "", $precio_rango);
//    endif;
//
//    echo $precio_rango+$precio_rango_hoja;
//    exit;

else:

    $rango = $input_width_product_rango . $input_height_product_rango;

    $precio_rango = $familia->getPriceOfRango($id_product, $art_sub, $rango);
    $precio_rango = $precio_rango * $input_unit_product;
    $precio_rango = number_format($precio_rango, 2);
    if (strpos($precio_rango, ",")):
        $precio_rango = str_replace(",", "", $precio_rango);
    endif;
    echo $precio_rango;
    exit;
endif;