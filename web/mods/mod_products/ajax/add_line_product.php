<?php

require '../../../../config.php';
$familia = new Familia();

$input_unit_product = "";
if(isset($_POST['input_unit_product'])) $input_unit_product = $_POST['input_unit_product'];

$input_width_product = "";
if(isset($_POST['input_width_product'])) $input_width_product = $_POST['input_width_product'];

$input_height_product = "";
if(isset($_POST['input_height_product'])) $input_height_product = $_POST['input_height_product'];

$id_product = "";
if(isset($_POST['id_product'])) $id_product = $_POST['id_product'];

$color_product = "";
if(isset($_POST['color_product'])) $color_product = $_POST['color_product'];

$group_color = "";
if(isset($_POST['group_color'])) $group_color = $_POST['group_color'];

$subfamily_product = "";
if(isset($_POST['subfamily_product'])) $subfamily_product = $_POST['subfamily_product'];

$min_anc = "";
if(isset($_POST['input_danch'])) $min_anc = $_POST['input_danch'];

$max_anc = "";
if(isset($_POST['input_hanch'])) $max_anc = $_POST['input_hanch'];

$min_alt = "";
if(isset($_POST['input_dalto'])) $min_alt = $_POST['input_dalto'];

$max_alt = "";
if(isset($_POST['input_halto'])) $max_alt = $_POST['input_halto'];

if($id_product == 2732):
    if($input_width_product <= "1.40"):
        $subfamily_product = "";
    endif;
endif;

$art_sub = $subfamily_product.$group_color;

$array_familia = $familia->getFamilia('fafami');
$array_color = $familia->getColoressByCocolo($id_product, $color_product);

$rango = "";

if($id_product == 2731):
    $input_width_product_rango = calc_precio_enrollable_ventana($input_width_product, $array_familia[$id_product]['min_ran_anc']);
    $input_height_product_rango = calc_precio_enrollable_ventana($input_height_product, $array_familia[$id_product]['min_ran_alt']);
endif;

if($id_product == 2732):
    $input_width_product_rango = calc_precio_ancho_enrollable_puerta($input_width_product, $array_familia[$id_product]['min_ran_anc']);
    $input_height_product_rango = calc_precio_alto_enrollable_puerta($input_height_product, $array_familia[$id_product]['min_ran_alt']);
endif;

if($id_product == 2733):
    $input_width_product_rango = calc_precio_ancho_plisada($input_width_product, $array_familia[$id_product]['min_ran_anc'], $subfamily_product);
    $input_height_product_rango = calc_precio_alto_plisada($input_height_product, $array_familia[$id_product]['min_ran_alt']);
endif;

if($id_product == 2734):
    $input_width_product_rango = calc_precio_ancho_fija($input_width_product, $array_familia[$id_product]['min_ran_anc']);
    $input_height_product_rango = calc_precio_alto_fija($input_height_product, $array_familia[$id_product]['min_ran_alt']);
endif;

if($id_product == 2735):
//    $input_width_product_rango = calc_precio_ancho_corredera($input_width_product, $array_familia[$id_product]['min_ran_anc']);
//    $input_height_product_rango = calc_precio_alto_corredera($input_height_product, $array_familia[$id_product]['min_ran_alt']);
//    $input_width_hoja_product_rango = calc_precio_hoja_corredera($input_width_product);
endif;

if($id_product == 2736):
    $input_width_product_rango = calc_precio_ancho_abatible($input_width_product, $array_familia[$id_product]['min_ran_anc'], $subfamily_product);
    $input_height_product_rango = calc_precio_alto_abatible($input_height_product, $array_familia[$id_product]['min_ran_alt']);
endif;


if ($id_product == 2735):

//    $rango_hoja = $input_width_hoja_product_rango.$input_height_product_rango;
//    $precio_rango_hoja = $familia->getPriceOfRango($id_product, "1".$art_sub, $rango_hoja);
//    $precio_rango_hoja = $precio_rango_hoja * 2 * $input_unit_product;
//    $precio_rango_hoja = number_format($precio_rango_hoja, 2);
//    if (strpos($precio_rango_hoja, ",")):
//        $precio_rango_hoja = str_replace(",", "", $precio_rango_hoja);
//    endif;
//
//    $rango = $input_width_product_rango.$input_height_product_rango;
//    $precio_rango = $familia->getPriceOfRango($id_product,$art_sub,$rango);
//    $precio_rango = $precio_rango + $precio_rango_hoja;

else:

    $rango = $input_width_product_rango.$input_height_product_rango;
    $precio_rango = $familia->getPriceOfRango($id_product,$art_sub,$rango);

endif;

?>

    <tr>

        <td>

            <select class="form-control" title="" name="id_product_line">

            <?php

            foreach($array_familia as $index => $item): ?>
                <option <?php if($id_product == $index):echo 'selected'; endif;?> value="<?=$index?>"><?=$item['fadesc']?></option>
            <?php endforeach; ?>
            </select>

        </td>
        <td>
            <input title="" value="<?=$input_unit_product?>" min="1" type="number" name="input_unit_product" class="input_resume input_unit_product form-cxontrol"/>
        </td>
        <td>
            <input title="" value="<?=$input_width_product?>" type="text" name="input_width_product" class="input_resume input_width_product form-control"/>
        </td>
        <td>
            <input title="" value="<?=$input_height_product?>" type="text" name="input_height_product" class="input_resume input_height_product form-control"/>
        </td>
        <td><?=$array_color[0]['codesc'];?></td>
        <td><?=$precio_rango?> €</td>
        <td>
            <textarea class="border_gris noresize" placeholder="Observaciones"></textarea>
        </td>
        <td>
            <span class="glyphicon glyphicon-duplicate text-warning btn_copy_line"></span>
            <span class="glyphicon glyphicon-remove-circle text-danger btn_delete_line"></span>
        </td>

    </tr>