<?php

if (!isset($_SESSION)):
    session_start();
endif;

require '../../../../config.php';

ini_set("display_errors", 0);

$array_error = array('error' => '-1', 'message' => 'Error desconocido');

$error = 1;

$presupuesto = new Presupuesto();
$id_cliente = $_SESSION['clproc'] . $_SESSION['clcodi'];
$fecha = date('Y/m/d H:i:s');
$total = $_POST['price'];
$referenca = $_POST['referencia'];
$is_pedido = $_POST['is_pedido'];
$agencia = $_POST['agencia'];
$value_agencia = "";
$adjuntos_incremento = 0;
$cont_product = 1;

$is_dire_fija = "0";
if (isset($_POST['is_dire_fija']))
    $is_dire_fija = $_POST['is_dire_fija'];

$pedido = "1";
if ($is_pedido == "presupuesto"):
    $pedido = "0";
    $cont_ped = $presupuesto->count() + 1;
    $num_pre = "Ppto. MT. CL " . $_SESSION['clproc'] . $_SESSION['clcodi'] . "-" . str_pad($cont_ped, 8, "0", STR_PAD_LEFT);
else:
    $cont_pre = $presupuesto->count_ped() + 1;
    $num_pre = "Ped. MT. CL " . $_SESSION['clproc'] . $_SESSION['clcodi'] . "-" . str_pad($cont_pre, 8, "0", STR_PAD_LEFT);
endif;

$fecha_condiciones = date("Ym", strtotime($fecha));
//$ceprov, $cecodi
$array_condiciones = $presupuesto->getCondicionesEspeciales($_SESSION['clprov'], $_SESSION['clcodi'], '', '', '', '', '', '');
$alb_condiciones_cedtop = "";
$alb_condiciones_cedtoe = "";
$cond_dto = "";

if (isset($array_condiciones[0][0]['cedtop'])):
    $alb_condiciones_cedtop = round2decimals($array_condiciones[0][0]['cedtop']);
    $cond_dto = "1";    
endif;

if (isset($array_condiciones[0][1]['cedtop'])):
    $alb_condiciones_cedtop = round2decimals($array_condiciones[0][1]['cedtop']);
    $cond_dto = "1";
endif;

if (!empty($array_condiciones[0][0]['cedtoe'])):
    $alb_condiciones_cedtoe = round2decimals($array_condiciones[0][0]['cedtoe']);
    $cond_dto = "1";
endif;

if (!empty($array_condiciones[0][1]['cedtoe'])):
    $alb_condiciones_cedtoe = round2decimals($array_condiciones[0][1]['cedtoe']);
    $cond_dto = "1";
endif;

$id_pres = $presupuesto->save_presupuesto($num_pre, $id_cliente, $fecha, $total, $referenca, $pedido, '', $alb_condiciones_cedtop, $alb_condiciones_cedtoe);

if ($total < '50' && !empty($id_pres) && $agencia == "seleccionar_dire"):
    $presupuesto->insertar_concepto($id_pres, "GASTOS DE ENVÍO", '0', '0', '9,00');
endif;

$data = $_POST['data'];
unset($_SESSION['products']);
unset($_SESSION['array_colores_ral']);
foreach ($data as $item):

    $is_almacen = "0";
    if (isset($item['is_almacen'])): $is_almacen = $item['is_almacen'];
    endif;
    
    if ($item['id_product'] == '1999' AND ! empty($item['group_color'])):
        $precio = $item['product_price'] / $item['unit_product'];
        $alb = $presupuesto->save_albaran($id_pres, $item['id_product'], $item['subfamily_product'], '', $item['unit_product'], $item['width_product'], $item['height_product'], $precio, '', '0', $item['group_color'], '', '', '', $item['art_desc'], $item['col_desc'], $cont_product);
        $is_linea_3 = "0";

    elseif (isset($item['width_product']) AND $item['id_product'] != '1999'):
        $item['width_product'] = str_replace(",", ".", $item['width_product']);
        $item['height_product'] = str_replace(",", ".", $item['height_product']);
        $cod_color = "";
        if (!empty($item['group_color'])):
            $cod_color = $item['group_color'];
        endif;
        if ($item['id_product'] == '2736'):
            $item['observaciones_product'] = $item['tipologia'];
        endif;

        $fecha_condiciones = date("Ym", strtotime($fecha));
        //$ceprov, $cecodi, $cefami, $cesubf, $cearti, $cecolo, $is_almacen, $fecha_condiciones
        $array_condiciones = $presupuesto->getCondicionesEspeciales($_SESSION['clprov'], $_SESSION['clcodi'], $item['id_product'], $item['subfamily_product'], '', $item['color_product'], $is_almacen, $fecha_condiciones);
        $alb_condiciones = "";
        $cond_dto = "";

        if (isset($array_condiciones[0][0]['cedtoa'])):
            $alb_condiciones = $array_condiciones[0][0]['cedtoa'];
            $cond_dto = "1";
        endif;

        if (!empty($array_condiciones[0][0]['cepres'])):
            $alb_condiciones = round2decimals($array_condiciones[0][0]['cepres']);
            $cond_dto = "0";
        endif;

        $alb = $presupuesto->save_albaran($id_pres, $item['id_product'], $item['subfamily_product'] . $cod_color, $item['color_product'], $item['unit_product'], $item['width_product'], $item['height_product'], $item['product_price'], $item['observaciones_product'], $is_almacen, "", '', $alb_condiciones, $cond_dto, trim($item['art_desc']), $item['col_desc'], $cont_product);
        $cont_product++;

        $is_linea_3 = "0";

    elseif (isset($item['is_lacado'])):
        $albfami = "2799";
        $albsubf = "90";
        $albarti = "";
        if ($item['product_price_lacado'] == "75"):
            $albarti = "000101";
        endif;
        if ($item['product_price_lacado'] == "125"):
            $albarti = "000102";
        endif;
        if ($item['product_price_lacado'] == "200"):
            $albarti = "000103";
        endif;
        if ($item['product_price_lacado'] == "275"):
            $albarti = "000104";
        endif;
        if ($item['product_price_lacado'] == "350"):
            $albarti = "000105";
        endif;
        if ($item['product_price_lacado'] == "475"):
            $albarti = "000106";
        endif;

        $fecha_condiciones = date("Ym", strtotime($fecha));
        //$ceprov, $cecodi, $cefami, $cesubf, $cearti, $cecolo, $is_almacen, $fecha_condiciones
        $array_condiciones = $presupuesto->getCondicionesEspeciales($_SESSION['clprov'], $_SESSION['clcodi'], $albfami, '', '', $item['color_lacado'], $is_almacen, $fecha_condiciones);

        $alb_condiciones = "";
        $cond_dto = "";

        if (isset($array_condiciones[0][0]['cedtoa'])):
            $alb_condiciones = $array_condiciones[0][0]['cedtoa'];
            $cond_dto = "1";
        endif;

        if (!empty($array_condiciones[0][0]['cepres'])):
            $alb_condiciones = round2decimals($array_condiciones[0][0]['cepres']);
            $cond_dto = "0";
        endif;

        $alb = $presupuesto->save_albaran($id_pres, $albfami, $albsubf, $item['color_lacado'], $item['unit_lacado'], '', '', $item['product_price_lacado'], "lacado", $is_almacen, $albarti, '', $alb_condiciones, $cond_dto, $item['art_desc'], $item['col_desc'], $cont_product);
        $is_linea_3 = "0";

    elseif (isset($item['input_unit_product_component'])):

        $fecha_condiciones = date("Ym", strtotime($fecha));
        //$ceprov, $cecodi, $cefami, $cesubf, $cearti, $cecolo, $is_almacen, $fecha_condiciones
        $array_condiciones = $presupuesto->getCondicionesEspeciales($_SESSION['clprov'], $_SESSION['clcodi'], $item['id_product'], $item['id_component'], '', '', $is_almacen, $fecha_condiciones);
        $dto = "";

        $alb_condiciones = "";
        $cond_dto = "";

        if (isset($array_condiciones[0][0]['cedtoa'])):
            $alb_condiciones = $array_condiciones[0][0]['cedtoa'];
            $cond_dto = "1";
        endif;

        if (!empty($array_condiciones[0][0]['cepres'])):
            $alb_condiciones = round2decimals($array_condiciones[0][0]['cepres']);
            $cond_dto = "0";
        endif;

        $alb = $presupuesto->save_albaran($id_pres, $item['id_product'], $item['id_component'], '', $item['input_unit_product_component'], '', '', $item['product_price_component'], 'is_increment' . $item['id_component'], $is_almacen, "", $item['adjunto'], $alb_condiciones, $cond_dto, $item['art_desc'], $item['col_desc'], $cont_product);
        $is_linea_3 = "0";
        if (!empty($item['adjunto'])):
            $adjuntos_incremento++;
            $is_linea_3 = "1";
        endif;
//    elseif(isset($item['is_expositor'])):
//        $alb = $presupuesto->save_albaran($id_pres, $item['id_product'],$item['subfamily_product'],'',$item['unit_product'],$item['ancho'],$item['alto'],$item['product_price'],'', '0', $item['ararti_product']);

    elseif (isset($item['observaciones_product']) == "Linea3" && $is_linea_3 == "0" && empty($item['id_product'])):
        $alb = $presupuesto->save_albaran($id_pres, '', '', '', '1', '', '', '', 'Linea3', '', '', '', '', '', '', '', $cont_product);
        $is_linea_3 = "1";
    else:
        $presupuesto->setDireccionEnvio($id_pres, $item['id_dire'], $is_dire_fija);
        if ($agencia == "seleccionar_dire"):
            $value_agencia = $item['id_dire'];
            if (is_numeric($value_agencia)):
                $value_agencia = "";
            endif;
        elseif ($agencia == "recoge_agencia_cliente"):
            $value_agencia = "rsa";
        elseif ($agencia == "recoge_cliente"):
            $value_agencia = "rec";
        endif;
        $presupuesto->setAgencia($id_pres, $value_agencia);
        $is_linea_3 = "0";
    endif;

    if ($alb == 1):

        $array_error = array('error' => '0', 'message' => 'Guardando datos', 'id_pres' => $id_pres, 'is_pedido' => $is_pedido);

        $error = 0;

    else:
        $array_error = array('error' => '1', 'message' => 'Error al guardar datos', 'is_pedido' => $is_pedido);

        $error = 1;

    endif;

endforeach;

if ($error == 0 AND $project_name != "/projects/mosquiflex/") {
    $tipo_email = "pedido_presupuesto";
    //include '../../mod_pdf/send_email/send_pdf_files.php';
    include '../../mod_pdf/send_email/send_email_cliente.php';
    include '../../mod_pdf/send_email/send_email_ventas.php';
}


echo json_encode($array_error);

exit;
