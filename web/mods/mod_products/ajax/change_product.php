<?php

require '../../../../config.php';
$familia = new Familia();

$id_pro = $_POST['id_product'];

if($id_pro == "exp"):

    include 'informacion_expositor_modal.php';

else:

$array_familia = $familia->getGrupoColoresFamilia($id_pro);

foreach($array_familia as $item): ?>

    <div group_color="<?=$item['cosubf']?>" class="col-md-12 col-xs-12 group_color_mobile">
        <h6><?=lang_title_grupo?> <?=$item['cosubf']?></h6>
        <?php
        $array_colores = $familia->getColoresByFamilia($item['cofami'], $item['cosubf']);

        foreach($array_colores as $item_color): ?>
            <div color_product="<?=$item_color['cocolo']?>" class="group_colors col-md-6">
                <?php


                if($item['cosubf'] == 4 OR $item['cosubf'] == 3): ?>

                    <?php

                    $url_imagen = $id_pro.$item['cosubf'].$item_color['cocolo'].".jpg";

                    ?>
                    <div class="color_product change_color_product">
                        <img class="img_color_product" src="<?=path_image_colors.$url_imagen?>">
                    </div>

                <?php else:?>

                    <div style="background-color: <?=$item_color['html']?>" class="color_product change_color_product"></div>


                <?php endif; ?>
                <span class="default_color title_color_product"><?=$item_color['codesc']?></span>
            </div>
        <?php endforeach; ?>
    </div>
<?php endforeach;
?>
<div group_color="lacados" class="col-md-12 col-xs-12">
    <h6><?=lang_lacados_carta_ral?></h6>
    <div color_product="lacado" class="group_colors col-md-12 col-xs-12 div_group_lacado_mobile">
        <div class="color_product change_color_product">
            <div style="background-color: #FFFFFF" class="color_product change_color_product_ral"></div>
        </div>

        <span class="default_color title_color_product text_color_ral"></span>
        <input class="form-control change_color_ral" size="4" maxlength="4" min="4" type="text" name="change_color_ral" placeholder="Ej. 8012">
    </div>
</div>

<?php endif; ?>