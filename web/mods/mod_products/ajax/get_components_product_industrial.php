<?php

require '../../../../config.php';

$id_pro = "";
if (isset($_POST['id_product'])): $id_pro = $_POST['id_product']; endif;

switch($id_pro):
    case '2731':
        $id_component = "1791";
        break;
    case '2732':
        $id_component = "1792";
        break;
    case '2733':
        $id_component = "1793";
        break;
    case '2734':
        $id_component = "1794";
        break;
    case '2735':
        $id_component = "1794";
        break;
    case '2736':
        $id_component = "1795";
        break;
endswitch;

$familia = new Familia();
$array_components = $familia->getComponentsBySubfamily($id_component, "status_3");
$is_industrial = "1";
?>

<div class="panel panel-default form-inline col-md-12 col-xs-12 nopadding">
    <div class="panel-heading selected">
        <h4 class="panel-title">
            <div class="title_panel_title title_versiones"><?=lang_title_venta_industrial?></div>
        </h4>
    </div>

    <div class="col-md-12 col-xs-12">
        <form class="search_components" name="search_components">
            <div class="form-group has-feedback div_content_search_components">
                <div class="panel-collapse nopadding">
                    <div class="col-md-6 col-xs-6 text-left mt-15">
                        <div class="col-md-6 col-xs-6">
                            <input checked type="radio" name="type_search_components" class="type_search_components" value="descripcion"><label class="font-light check_search_components">&nbsp;&nbsp;&nbsp;<?=lang_title_buscar_descripcion?></label>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <input type="radio" name="type_search_components" class="type_search_components" value="codigo"><label class="font-light check_search_components">&nbsp;&nbsp;&nbsp;<?=lang_title_buscar_codigo?></label>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div>
                            <label class="hidden" for="">1791-01-000001</label>
                            <input type="search" class="mt-10 mb-10 form-control btn_search_component" name="search_components" placeholder="<?=lang_products_introduce_busqueda?>">
                        </div>
                        <i class="glyphicon glyphicon-search icono_lupa_search_components form-control-feedback"></i>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="col-md-1 col-xs-1 pull-right row">
        <div class="boton_componentes_flotante text-center hidden">
            <button type="button" value="presupuesto" name="type_button"
                    class="btn-flotante btn_flotante_generar btn bg-border-info"><span><?=lang_title_continuar_pres?></span>
            </button>
            <br>
            <button type="button" value="pedido" class="btn-flotante btn_flotante_generar btn bg-border-warning">
                <?=lang_title_continuar_ped?>
            </button>
        </div>
        <!--        <div class="boton_componentes_flotante_add text-center hidden">-->
        <!--            <button type="button" value="presupuesto" name="type_button" class="btn-flotante btn_flotante_add btn bg-border-info"><span>Añadir al <span class="type_document"></span></span></button><br>-->
        <!--        </div>-->
    </div>

    <div class="col-md-12 col-xs-12 row text-center head_components">
        <div class="col-md-1 col-xs-1"><?=lang_title_unidades?></div>
        <div class="col-md-1 col-xs-1"><?=lang_title_medidas?></div>
        <div class="col-md-2 col-xs-2"><?=lang_title_articulo?></div>
        <div class="col-md-2 col-xs-2"><?=lang_title_referencia?></div>
        <div class="col-md-3 col-xs-3"><?=lang_title_descripcion?></div>
        <div class="col-md-1"><?=lang_title_embalaje?> <br>estándar Ud./m<sup>2.</sup></div>
        <div class="col-md-1"><?=lang_title_precio?> (€) <br>Ud./m<sup>2</sup>.</div>
        <div class="col-md-1"><?=lang_title_precio_upper?> <?=lang_title_total?> (€)</div>
    </div>

    <div class="body_components">

        <?php

        $group_color = 0;

        foreach ($array_components as $item):

            $arsubf = str_pad($item['arsubf'], 2, 0, STR_PAD_LEFT);
            $ararti = str_pad($item['ararti'], 6, 0, STR_PAD_LEFT);

            $embalaje = $familia->getEmbalaje($id_component, $arsubf, $ararti, $item['arprpr']);

            $precio_almacen = round2decimals($item['arpval']);

            $referencia = $id_component . "-" . $arsubf . "-" . $ararti;

            $referencia_img = $id_component.$arsubf.$ararti;

            if ($id_pro == "2735"):
                $imagen = path_image_components . "/2734/" . $referencia_img . ".jpg";
            else:
                $imagen = path_image_components . "/" . $id_pro . "/" . $referencia_img . ".jpg";
            endif;
            //Agrupamos por color / subfamilia dependiendo del componente
            if(isset($id_pro)):
                include root.'web/mods/mod_products/code_product/components_color_industrial_'.$id_pro.'.php';
            endif;

        endforeach; ?>

    </div>

</div>

<input type="hidden" name="id_component_product[]" class="id_component_product" value="<?= $id_component ?>">
