<?php

//Archivo que muestra la información del producto, si existe, al seleccionar la opcion 'producto acabado'

require '../../../../config.php';
$id_pro = "";
if(isset($_POST['id_product'])): $id_pro = $_POST['id_product']; endif;

if(file_exists(root.'web/mods/mod_products/code_product/'.$id_pro.'.php')):
    include root.'web/mods/mod_products/code_product/'.$id_pro.'.php';
endif;

?>