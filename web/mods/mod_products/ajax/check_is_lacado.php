<?php

require '../../../../config.php';
$familia = new Familia();

$color_product = "";
if (isset($_POST['color_product']))
    $color_product = $_POST['color_product'];
$id_product = "";
if (isset($_POST['id_product']))
    $id_product = $_POST['id_product'];

$is_lacado = "";
$is_lacado = $familia->getIsLacado($color_product, $id_product);

echo $is_lacado;
