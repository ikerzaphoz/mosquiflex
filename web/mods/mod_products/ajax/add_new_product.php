<?php
require '../../../../config.php';
$familia = new Familia();

$id_product = "";
if (isset($_POST['id_product'])): $id_product = $_POST['id_product'];
endif;
$color_product = "";
if (isset($_POST['color_product'])): $color_product = $_POST['color_product'];
endif;
$group_color = "";
if (isset($_POST['group_color'])): $group_color = $_POST['group_color'];
endif;
$subfamily_product = "";
if (isset($_POST['subfamily_product'])): $subfamily_product = $_POST['subfamily_product'];
endif;
$exist_line_lacado = "";
if (isset($_POST['exist_line_lacado'])): $exist_line_lacado = $_POST['exist_line_lacado'];
endif;
$tipologia = "";
if (isset($_POST['input_tipologia'])): $tipologia = $_POST['input_tipologia'];
endif;

if ($id_product == "exp"):
    ?>

    <div class="col-md-12 col-xs-12 row text-center">
        <?php
        foreach ($_POST as $producto => $unidades):
            $unidades_producto = "";
            $titulo_producto = "";
            $precio = "";
            $arfami = "1999";
            $arsubf = "52";
            $ararti = "";
            $alto = "0.740";
            $ancho = "0.530";
            if (isset($unidades) AND $unidades > 0 AND ( $producto == "enr_ventana" OR $producto == "enr_ventana_muelle" OR $producto == "enr_puerta" OR $producto == "exp_plisada" OR $producto == "exp_abatible" OR $producto == "exp_fija_corredera" OR $producto == "exp_selector_color" OR $producto == "exp_fija_expositor")):
                switch ($producto):
                    case 'enr_ventana':
                        $titulo_producto = "enrollable ventana";
                        $precio_unitario = 75;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000001";
                        break;
                    case 'enr_ventana_muelle':
                        $titulo_producto = "enrollable ventana con muelle retenci&oacute;n";
                        $precio_unitario = 80;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000002";
                        break;
                    case 'enr_puerta':
                        $titulo_producto = "enrollable puerta";
                        $precio_unitario = 150;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000003";
                        break;
                    case 'exp_plisada':
                        $titulo_producto = "plisada";
                        $precio_unitario = 150;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000004";
                        break;
                    case 'exp_abatible':
                        $titulo_producto = "abatible";
                        $precio_unitario = 150;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000005";
                        break;
                    case 'exp_fija_corredera':
                        $titulo_producto = "fijas y correderas";
                        $precio_unitario = 75;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000006";
                        break;
                    case 'exp_selector_color':
                        $titulo_producto = "selector de colores";
                        $precio_unitario = 10;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000101";
                        break;
                    case 'exp_fija_expositor':
                        $titulo_producto = "expositor personalizable";
                        $precio_unitario = 500;
                        $precio = $precio_unitario * $unidades;
                        $ararti = "000201";
                        $alto = "2.100";
                        $ancho = "0.620";
                        break;
                endswitch;
                $unidades_producto = $unidades;
            endif;

            if (!empty($unidades) AND ! empty($titulo_producto)):
                ?>
                <div class="col-md-12 col-xs-12 line_expositor line_product">
                    <div class="col-md-4 col-xs-4 text-left">Muestra <?= strtolower($titulo_producto) ?></div>
                    <div class="col-md-4 col-xs-4 text-center">
                        <input type="number" min="0" value="<?= $unidades_producto ?>"
                               class="border_units_components change_units_resumen_expositor">
                    </div>
                    <div class="col-md-3 col-xs-3 product_price text-center"><?= round2decimals($precio) ?></div>
                    <div class="col-md-1 col-xs-1"><span><i
                                class='fa fa-times-circle btn_delete_line_component' aria-hidden='true'></i></span>
                    </div>
                    <input type="hidden" class="price_expositor_hidden" value="<?= $precio_unitario ?>">
                    <input type="hidden" class="arfami input_id_product" span_value="<?= $arfami ?>" value="<?= $arfami ?>">
                    <input type="hidden" class="arsubf input_subfamily_product" value="<?= $arsubf ?>">
                    <input type="hidden" class="ararti" value="<?= $ararti ?>">
                    <input type="hidden" class="fami_color input_color_product" span_value="0" value="0">
                    <input type="hidden" class="input_group_color" span_value="0">
                    <input type="hidden" class="ancho_expositor_hidden input_width_product" value="<?= $ancho ?>">
                    <input type="hidden" class="alto_expositor_hidden input_height_product" value="<?= $alto ?>">
                    <input type="hidden" class="precio_unidades_medidas" value="<?= $precio_unitario ?>">
                    <input type="hidden" class="is_almacen" value="0">
                    <input type="hidden" class="observaciones_product"
                           value="<?= $producto ?>">
                </div>
            <?php
        endif;
    endforeach;
    ?>
    </div>

<?php
else:

    $is_lacado = "";
    $is_lacado = $familia->getIsLacado($color_product, $id_product);
    if ($is_lacado == "1"):
        $group_color = 2;
        set_array_ral($color_product, 1);
        if($id_product == "2734" || $id_product == "2735"):
            $group_color = 1;
        endif;
    endif;

    $code_sub = $subfamily_product . $group_color;
    $ancho_minimo = $familia->getAnchoMin($id_product, $code_sub);
    $ancho_maximo = $familia->getAnchoMax($id_product, $code_sub);
    $alto_minimo = $familia->getAltoMin($id_product, $code_sub);
    $alto_maximo = $familia->getAltoMax($id_product, $code_sub);

    $array_familia = $familia->getFamilia('fafami');
    if ($is_lacado == "1"):
        $array_color = $familia->getColorsRalLineaPedido($color_product, $id_product);
    else:
        $array_color = $familia->getColoressByCocolo($id_product, $color_product)[0];
    endif;
    ?>
    <div class="line_product effect7 col-md-12 col-xs-12">
        <div class="row col-md-12 col-xs-12">
            <h4 class="title_product"><?= $array_familia[$id_product]['fadesc'] ?>&nbsp;&nbsp;&nbsp;<span
                    data-tooltip="tooltip" data-placement="top" title="<?= lang_text_ver_mas_info ?>"
                    class="title_info_product title-warning fa fa-info-circle image-popup-no-margins icono-lupa fa fa-search"
                    href="<?= path_image ?>images_product/<?= $id_product ?>/medidas.png"><img class="hidden"
                                                                                           src="<?= path_image ?>images_product/<?= $id_pro ?>/medidas.png">
                </span></h4>
        </div>
        
        <?php if ($id_product == "2735"):
            include '../linea_pedido_presupuesto_corredera.php';
            else:
                
        ?>
        
        <div class="row col-md-12 col-xs-12 text-center">
            <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_version ?></div>
            <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_color ?></div>
            <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_unidades ?></div>
            <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_ancho ?></div>
            <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_alto ?></div>
            <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_precio ?> (€)</div>
        </div>
        <div class="col-md-12 col-xs-12 row text-center">
            <div class="col-md-2 col-xs-2">
                <?php
                $version = "";
                switch ($id_product):
                    case '2731':
                        ?>
                        <select class="form-control change-subfamily input_subfamily_product">
<!--                            <option <?php /*if (empty($subfamily_product)): echo "selected";
                        endif; */?> value="">C.33
                            </option>-->
                            <option <?php if ($subfamily_product == 1): echo "selected";
                        endif; ?> value="1">C.35
                            </option>
                            <option <?php if ($subfamily_product == 2): echo "selected";
            endif; ?> value="2">C.42
                            </option>
                        </select>
            <?php
            break;
        case '2732':
            ?>
                        <select class="form-control change-subfamily input_subfamily_product">
                            <option <?php if ($subfamily_product == 0): echo "selected";
            endif; ?> value="">P.Única
                            </option>
                            <option <?php if ($subfamily_product == 2): echo "selected";
            endif; ?> value="2">P.Doble
                            </option>
                        </select>
                        <input type="hidden" name="input_tipologia" value="<?= $tipologia ?>" class="input_tipologia">
            <?php
            break;
        case '2733':
        case '2736':
            ?>
                        <select class="form-control change-subfamily input_subfamily_product">
                            <option <?php if ($subfamily_product == 0): echo "selected";
            endif; ?> value="">P.Única
                            </option>
                            <option <?php if ($subfamily_product == 1): echo "selected";
            endif; ?> value="1">P.Doble
                            </option>
                        </select>
                        <input type="hidden" name="input_tipologia" value="<?= $tipologia ?>" class="input_tipologia">
            <?php
            break;
    endswitch;
    ?>
            </div>
            <div class="col-md-2 col-xs-2 content_color_pedido">
    <?php
    if ($group_color == 3 OR $group_color == 4):
        $url_imagen = $id_product . $group_color . $color_product . ".jpg";
        ?>
                    <div class="color_product_line">
                        <img class="img_color_product" src="<?= path_image_colors . $url_imagen ?>">
                    </div>
    <?php else: ?>
                    <div color_ral="<?= hideRal($array_color['codesc']) ?>"
                         style="background-color: <?= $array_color['html'] ?>" class="color_product_line">
                        <img class="img_color_product hidden" src="">
                    </div>
    <?php endif;
    ?>
                <span class="title_color"><?= $array_color['codesc'] ?></span>
            </div>
            <div class="col-md-2 col-xs-2">
                <input required placeholder="1" title="" value="1" min="1" type="number" name="input_unit_product"
                       class="input_unit_product form-control w75"/>
            </div>
            <div class="col-md-2 col-xs-2">
                <input pattern="[0-9]+([\.,][0-9]+)"
                       title="Valor mínimo: <?= $ancho_minimo ?> y máximo: <?= $ancho_maximo ?>. Ejemplo formato: 1.010"
                       required placeholder="<?= $ancho_minimo ?>" type="text" name="input_width_product"
                       class="input_width_product form-control check_size w100 border-warning" placeholder="0.000"
                       value=""/>
            </div>
            <div class="col-md-2 col-xs-2">
                <input pattern="[0-9]+([\.,][0-9]+)" required placeholder="<?= $alto_minimo ?>"
                       title="Valor mínimo: <?= $alto_minimo ?> y máximo: <?= $alto_maximo ?>. Ejemplo formato: 1.010"
                       type="text" name="input_height_product"
                       class="input_height_product form-control check_size w100 border-warning" placeholder="0.000"
                       value=""/>
            </div>
            <div class="col-md-2 col-xs-2">
                <span class="product_price"><span class="text-danger"><?= lang_text_revisar_medidas ?></span></span>
            </div>
        </div>

        <div class="segunda_linea_producto col-md-12 col-xs-12 row">
            <div class="col-md-2 col-xs-2 text-left">
    <?php if ($id_product == "2731" OR $id_product == "2734"): ?>
                    <button data-toggle="modal" data-target="#modal_option_product"
                            class="btn btn-sm btn-grey btn_option_product"><?= lang_text_anadir_incrementos ?>
                    </button>
    <?php endif; ?>
            </div>
            <div class="hidden col-md-6 col-xs-6">
                <textarea name="observaciones_product" class="observaciones_product"
                          placeholder="Observaciones..."></textarea>
            </div>
            <div class="col-md-10 col-xs-10 increments text-right"></div>
        </div>


        <div class="hidden info_product_line">
            <span class="hidden input_id_product" span_value="<?= $id_product ?>" span_name="id_product"></span>
            <span class="hidden input_color_product" span_value="<?= $color_product ?>"
                  span_name="color_product"></span>
            <span class="hidden input_group_color" span_value="<?= $group_color ?>" span_name="group_color"></span>
            <span class="hidden input_danch" span_value="<?= str_pad($ancho_minimo, 5, "0") ?>"
                  span_name="input_danch"></span>
            <span class="hidden input_hanch" span_value="<?= str_pad($ancho_maximo, 5, "0") ?>"
                  span_name="input_hanch"></span>
            <span class="hidden input_dalto" span_value="<?= str_pad($alto_minimo, 5, "0") ?>"
                  span_name="input_dalto"></span>
            <span class="hidden input_halto" span_value="<?= str_pad($alto_maximo, 5, "0") ?>"
                  span_name="input_halto"></span>
        </div>
        <div class="segunda_linea_producto row col-md-12 col-xs-12 text-right">
            <span data-tooltip="tooltip" data-placement="top" title="<?= lang_text_copiar_linea_pedido ?>"
                  class="fa fa-2x fa-copy btn_copy_line text-warning btn-grey btn-option-line"></span>
            <span data-tooltip="tooltip" data-placement="top" title="<?= lang_text_eliminar_linea_pedido ?>"
                  class="fa fa-2x fa-close btn_delete_line text-danger btn-grey btn-option-line"></span>
            <span data-tooltip="tooltip" data-toggle="modal" data-target="#modal_add_product" data-placement="top"
                  title="<?= lang_text_añadir_producto ?>"
                  class="fa fa-2x fa-plus hidden btn_add_product text-success btn-grey btn-option-line"></span>
        </div>
        <?php endif; ?>
    </div>

    <?php if (empty($exist_line_lacado)): ?>
        <div class="content-lacados">
            <div class="line_lacados effect7 col-md-12 col-xs-12">
                <div class="row col-md-12 col-xs-12">
                    <h4 class="title_product text-left">Incrementos lacados&nbsp;&nbsp;&nbsp;<span
                            data-tooltip="tooltip" data-placement="top" title="Ver más información lacados"
                            class="title_info_product title-warning fa fa-info-circle image-popup-no-margins icono-lupa fa fa-search"
                            href="<?= path_image ?>images_product/<?= $id_product ?>/lacados.jpg"><img
                                class="hidden" src="<?= path_image ?>images_product/<?= $id_pro ?>/lacados.jpg"></span>
                    </h4>

                    <div class="row col-md-12 col-xs-12 text-center">
                        <div class="col-md-3 col-xs-3 title_line_product"></div>
                        <div class="col-md-3 col-xs-3 title_line_product">Color</div>
                        <div class="col-md-3 col-xs-3 title_line_product">Unidades</div>
                        <div class="col-md-3 col-xs-3 title_line_product">Precio (€)</div>
                    </div>

                    <div class="content_lacado_ral row col-md-12 col-xs-12 text-center">
        <?php
        if ($is_lacado == 1):

            foreach ($_SESSION['array_colores_ral'] as $color_session => $item):
                ?>
                                <div class="col-md-12 col-xs-12 line_lacado_ral">
                                    <div class="col-md-3 col-xs-3 title_line_product">
                                        <div class="preview_color_lacado"
                                             style="background-color: <?= $array_color['html'] ?>;"></div>
                                    </div>
                                    <div class="col-md-3 col-xs-3 color_lacado"><?= $color_session ?></div>
                                    <div class="col-md-3 col-xs-3 unit_lacado"><?= $item['unidades'] ?></div>
                                    <div class="col-md-3 col-xs-3 product_price_lacado">75.00</div>
                                </div>
            <?php
            endforeach;
            $total_presupuesto = $total_presupuesto + 75.00;
        endif;
        ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

<?php endif; ?>
