<div class="panel-body text-center nopadding">
    <div class="col-md-12 col-xs-12 nopadding">
        <input type="hidden" name="id_product" value="exp">
        <div class="radio col-md-6 col-xs-6">
            <div class="col-md-6 col-xs-6 pd1">
                <div class="col-md-12 col-xs-12 contenido_expositor div_title_expositores expositores_ventana">
                    <div class="col-md-8 col-xs-8">
                        <span class="text">Enrollable ventana</span>
                        <span class="price">75€</span>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="75"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="enr_ventana" value="">
                    </div>
                </div>
                <div class="col-md- col-xs-12 div_title_expositores contenido_expositor expositores_ventana">
                    <div class="col-md-8 col-xs-8">
                        <span class="text">Con muelle retención</span>
                        <span class="price">80€</span>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="80"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="enr_ventana_muelle" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">
                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                            <img class="hidden"
                                 src="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                        </a>
                        <span class="text_lupa">Ver imagen</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-6 contenido_expositor pd1">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <span class="text">Enrollable puerta</span>
                        <span class="price">150€</span>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="150"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="enr_puerta" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">
                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                            <img class="hidden"
                                 src="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                        </a>
                        <span class="text_lupa">Ver imagen</span>
                    </div> 
                </div>
            </div>
            <div class="col-md-6 col-xs-6 mt-3 contenido_expositor pd1">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <span class="text">Plisadas</span>
                        <span class="price">150€</span>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="150"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="exp_plisada" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">
                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/plisada.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/plisada.png">
                            <img class="hidden" src="<?= path_image ?>images_product/expositores/plisada.png">
                        </a>
                        <span class="text_lupa">Ver imagen</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-6 mt-3 contenido_expositor pd1">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <span class="text">Abatibles</span>
                        <span class="price">150€</span>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="150"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="exp_abatible" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">

                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/abatible.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/abatible.png">
                            <img class="hidden" src="<?= path_image ?>images_product/expositores/abatible.png">
                        </a>
                        <span class="text_lupa">Ver imagen</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-6 mt-3 contenido_expositor pd1">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <span class="text">Fijas y Correderas</span>
                        <span class="price">75€</span>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="75"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="exp_fija_corredera" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">

                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/fija_corredera.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/fija_corredera.png">
                            <img class="hidden"
                                 src="<?= path_image ?>images_product/expositores/fija_corredera.png">
                        </a>
                        <span class="text_lupa">Ver imagen</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-6 mt-3 contenido_expositor pd1">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <span class="text">Selector de Colores</span>
                        <span class="price">10€</span>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="10"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="exp_selector_color" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">

                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/selector.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/selector.png">
                            <img class="hidden"
                                 src="<?= path_image ?>images_product/expositores/selector.png">
                        </a>
                        <span class="text_lupa">Ver imagen</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="radio col-md-6 col-xs-6 div_imagen_expositor contenido_expositor pd1">
            <div class="col-md-12 col-xs-12 div_title_expositores">
                <div class='col-md-8 col-xs-8'>
                    <span class="text">Expositor personalizable</span>
                    <span class="price">500€</span>
                </div>
                <div class="col-md-4 col-xs-4 div_unidades_expositores">
                    <input class="unidades_expositores_modal" type="number" value="0" min="0">
                    <span class="text_uds">Uds.</span>
                    <input type="hidden" class="precio_expo hidden" value="500"/>
                    <input type="hidden" class="total_line">
                    <input class="info_submit" type="hidden" name="exp_fija_expositor" value="">
                </div>
            </div>
            <div class="col-md-12 col-xs-12 contenido_expositor_border">

                <div class="col-md-12 col-xs-12 div_img_expositores">
                    <img src="<?= path_image ?>images_product/expositores/expositor.png">
                </div>
                <div>
                    <a class="image-popup-no-margins icono-lupa fa fa-search"
                       href="<?= path_image ?>images_product/expositores/expositor.png">
                        <img class="hidden" src="<?= path_image ?>images_product/expositores/expositor.png">
                    </a>
                    <span class="text_lupa">Ver imagen</span>
                </div>
            </div>
        </div>
    </div>
</div>