<?php

require '../../../../config.php';
$familia = new Familia();

$unit_components = "";
if(isset($_POST['unit_components'])) $unit_components = $_POST['unit_components'];
$id_component = "";
if(isset($_POST['id_component'])) $id_component = $_POST['id_component'];
$arsubf = "";
if(isset($_POST['arsubf'])) $arsubf = $_POST['arsubf'];
$ararti = "";
if(isset($_POST['ararti'])) $ararti = $_POST['ararti'];
$arprpr = "";
if(isset($_POST['arprpr'])) $arprpr = $_POST['arprpr'];

$embalaje = $familia->getEmbalaje($id_component, $arsubf, $ararti, $arprpr);
$array_price_articulo = $familia->getInfoComponent($id_component, $arsubf, $ararti, $arprpr);
$precio_almacen = $array_price_articulo['arpval'];
$precio_unitario = $array_price_articulo['arpvta'];
$precio_total = 0;
$total_unit_almacen = 0;
$total_unit_unitario = 0;
//Unidades a precio embalaje
if($unit_components > 0 AND $unit_components >= $embalaje AND $unit_components % $embalaje == 0){
    $precio_total = $unit_components * $precio_almacen;
    $total_unit_almacen = $unit_components;
}else{
    //Si el numero de unidades es mayor que componentes pero no es multiplo restar las unidades y calcular de forma individual.
    if($unit_components < $embalaje):
        $precio_total = $unit_components * $precio_unitario;
        $total_unit_unitario = $unit_components;
    endif;
    if($unit_components > $embalaje):

        $cociente = floor($unit_components / $embalaje);
        $resto = $unit_components % $embalaje;
        $total_unit_unitario = $resto;
        $total_unit_almacen = $cociente * $embalaje;
        $precio_total = $total_unit_unitario*$precio_unitario;
        $precio_total += $total_unit_almacen*$precio_almacen;
    endif;
}

$array_precio_componente = array("Precio_total" => $precio_total, "Precio_almacen" => $precio_almacen, "Precio_unitario" => $precio_unitario, "Unidades_almacen" => $total_unit_almacen, "Unidades_unitario" => $total_unit_unitario);

echo json_encode($array_precio_componente);