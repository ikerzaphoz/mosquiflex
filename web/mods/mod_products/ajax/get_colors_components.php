<?php

require '../../../../config.php';
$id_pro = "";
if (isset($_POST['id_product']))
    $id_pro = $_POST['id_product'];
$no_brut = "";
if (isset($_POST['no_brut']))
    $no_brut = $_POST['no_brut'];

$familia = new Familia();

$array_grupos = $familia->getGrupoColoresFamilia($id_pro);

?>

<div class="panel-collapse col-md-12 col-xs-12">
    <div class="row">
        <div class="div_colors col-md-12 col-xs-12">

            <?php if (empty($no_brut)): ?>
                <div group_color="brut" class="col-md-12 col-xs-12">
                    <div color_product="bruto" class="group_colors_components group_colors_modal_components col-md-6">
                        <div style="background-color: #F3F3F3" class="color_components change_color_components"></div>

                        <span class="default_color title_color_product title_color_product_modal">Bruto</span>
                    </div>
                </div>
            <?php endif; ?>

            <?php

            foreach ($array_grupos as $item):

                $class_hidden = "";
                if($id_pro == "2732" AND $item['cosubf'] == "2")
                    $class_hidden = "hidden";

                ?>

                <div group_color="<?= $item['cosubf'] ?>" class="col-md-12 col-xs-12 <?=$class_hidden?>">
                    <h6>GRUPO <?= $item['cosubf'] ?></h6>
                    <?php
                    $array_colores = $familia->getColoresByFamilia($id_pro, $item['cosubf']);

                    foreach ($array_colores as $item_color): ?>
                        <div color_product="<?= $item_color['cocolo'] ?>"
                             class="group_colors_components group_colors_modal_components col-md-6">
                            <?php


                            if ($item['cosubf'] == 4 OR $item['cosubf'] == 3): ?>

                                <?php

                                $url_imagen = $id_pro . $item['cosubf'] . $item_color['cocolo'] . ".jpg";

                                ?>
                                <div class="color_components change_color_components is_image">
                                    <img class="img_color_product" src="<?= path_image_colors . $url_imagen ?>">
                                </div>

                            <?php else: ?>

                                <div style="background-color: <?= $item_color['html'] ?>"
                                     class="color_components change_color_components"></div>


                            <?php endif; ?>
                            <span
                                class="default_color title_color_product title_color_product_modal"><?= $item_color['codesc'] ?></span>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
            <div group_color="lacados" class="col-md-12 col-xs-12">
                <h6>LACADOS - Carta RAL</h6>
                <div color_product="lacado" class="group_colors_components group_colors_modal_components col-md-6">

                    <div class="color_components">
                        <div style="background-color: #FFFFFF"
                             class="change_color_product_ral color_components change_color_components_modal change_color_components_ral_modal"></div>
                    </div>
                    <span class="default_color text_color_ral title_color_product title_color_product_modal_ral"></span>
                    <input class="form-control change_color_ral" size="4" maxlength="4" min="4" type="text"
                           name="change_color_ral" placeholder="Ej. 8012">
                </div>
            </div>
        </div>
    </div>
</div>
