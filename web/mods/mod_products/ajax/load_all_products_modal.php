<?php

require '../../../../config.php';

$familia = new Familia();
$array_familia = $familia->getFamilia();

?>

<div id="content_product" class="div_all_product div_all_product_modal row nopadding">
    <form class="form_product_add_product" name="form_product_add_product">
        <div class="panel-group">

            <div class="panel panel-default form-inline col-md-12 col-xs-12 nopadding">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="title_panel_title nopadding">Productos</div>
                    </h4>
                </div>
                <div class="panel-collapse col-md-12 col-xs-12 nopadding">
                    <?php

                    /*foreach ($array_familia as $item):
                        $id_pro = $item['fafami'];
                        $title_product = $item['fadesc'];

                        ?>

                        <div id_product="<?= $id_pro ?>" class="radio radio_product col-md-4 col-xs-4">
                            <img class="icon_product"
                                 src="<?= path_image ?>icons_products/<?= $id_pro ?>.jpg"><label><span
                                    class="title_label"><?= $title_product ?></span></label>
                        </div>

                    <?php endforeach;*/ ?>
                        <div id_product="2731" class="radio radio_product col-md-4 col-xs-6">
                                <img class="icon_product" src="<?= path_image ?>icons_products/2731.jpg">
                                <label><span class="title_label">Enrollables Ventana</span></label>
                        </div>
                        <div id_product="2732" class="radio radio_product col-md-4 col-xs-6">
                                <img class="icon_product" src="<?= path_image ?>icons_products/2732.jpg">
                                <label><span class="title_label">Enrollable Puerta</span></label>
                        </div>
                        <div id_product="plisadas" class="radio radio_product col-md-4 col-xs-6">
                                <img class="icon_product" src="<?= path_image ?>icons_products/2733.jpg">
                                <label><span class="title_label">Plisadas</span></label>
                        </div>
                        <div id_product="fijas_correderas" class="radio radio_product col-md-4 col-xs-6">
                                <img class="icon_product" src="<?= path_image ?>icons_products/2734.jpg">
                                <label><span class="title_label">Fijas y Correderas</span></label>
                        </div>
                        <div id_product="2736" class="radio radio_product col-md-4 col-xs-6">
                                <img class="icon_product" src="<?= path_image ?>icons_products/2736.jpg">
                                <label><span class="title_label">Abatibles</span></label>
                        </div>
                                   
                    <div id_product="exp" class="button_expositor radio radio_product col-md-4 col-xs-4">
                        <img class="icon_product"
                             src="<?= path_image ?>icons_products/<?= $id_pro ?>.jpg"><label><span
                                class="title_label">Expositores</span></label>
                    </div>
                </div>
            </div>

            <div class="hidden type_product_group_fijas_correderas panel panel-default form-inline col-md-12 col-xs-12">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="title_panel_title">Versión del producto</div>
                    </h4>
                </div>
                <div class="panel-collapse text-center nopadding">
                    <div id_product="2734" class="radio radio_product col-md-4 col-xs-6">
                        <img class="icon_product" src="<?= path_image ?>icons_products/2734.jpg">
                        <label><span class="title_label">Fijas</span></label>
                    </div>
                    <div id_product="2735" class="radio radio_product col-md-4 col-xs-6">
                        <img class="icon_product" src="<?= path_image ?>icons_products/2735.jpg">
                        <label><span class="title_label">Correderas</span></label>
                    </div>
                </div>
            </div>
            <div class="hidden type_product_group_plisadas panel panel-default form-inline col-md-12 col-xs-12">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="title_panel_title">Versión del producto</div>
                    </h4>
                </div>
                <div class="panel-collapse text-center nopadding">
                    AQUI CARGAR CONTENIDO PLISADAS
                </div>
            </div>
            <div class="hidden type-product panel panel-default form-inline col-md-12 col-xs-12">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="title_panel_title"><?=lang_title_tipo_producto?></div>
                    </h4>
                </div>

                <div class="panel-collapse text-center nopadding">
                    <div class="col-md-4 col-xs-4 text-right div_button_acabado">
                        <input type="radio" name="change_components" class="change_components" value="2"><label
                            class="font-light check_product_component">&nbsp;&nbsp;&nbsp;<?=lang_title_producto_acabado?></label>
                    </div>
                    <div class="col-md-4 col-xs-4 text-center div_button_componente">
                        <input type="radio" name="change_components" class="change_components" value="1"><label
                            class="font-light check_product_component">&nbsp;&nbsp;&nbsp;<?=lang_title_componente?></label>
                    </div>
                    <div class="col-md-4 col-xs-4 text-left div_button_industrial">
                        <input type="radio" name="change_components" class="change_components" value="3"><label
                            class="font-light check_product_component">&nbsp;&nbsp;&nbsp;<?=lang_title_venta_industrial?></label>
                    </div>
                </div>
                <input value="1" class="hidden is_type_product_modal" name="is_type_product_modal"/>
            </div>

            <div class="info_insert"></div>

            <div class="panel panel-default form-inline col-md-12 col-xs-12 div_color_acabado nopadding">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="title_panel_title nopadding titulo_colores_modal_productos"><?=lang_title_colores?></div>
                    </h4>
                </div>
                <div class="panel-collapse col-md-12 col-xs-12 nopadding">
                    <div class="row">
                        <div class="div_colors col-md-8 col-xs-8">
                            <h5><?=lang_title_carta_colores?></h5>
                            <h6><i class="fa fa-warning text-warning"></i><?=lang_title_seleccione_producto_colores?></h6>
                        </div>
                        <div class="div_preview_img col-md-2 col-xs-2">
                            <h5><?=lang_title_vista_previa?></h5>
                            <div class="text-center div_content_preview_img">
                                <div style="background-image: url(<?= path_image ?>images_product/<?= $id_pro ?>/2.png)"
                                     class="row preview_img modal_preview_img">
                                    <span class="text_no_image_preview"></span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-inline col-md-12 col-xs-12 div_submit_product hidden nopadding">
                <div class="panel-collapse text-center padding8">
                    <div class="form-group">
                        <button type="submit" class="btn btn-sm bg-border-info" name="type_button" value="presupuesto">
                            <?=lang_title_add_al?> <span class="hidden type_doc_modal"></span></button>
                    </div>
                </div>
            </div>


        </div>

        <input value="<?= $id_pro ?>" class="hidden input_id_product" name="id_product"/>
        <input value="" class="hidden input_color_product" name="color_product"/>
        <input value="" class="hidden input_group_color" name="group_color"/>

    </form>
</div>