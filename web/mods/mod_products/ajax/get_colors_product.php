<?php

require '../../../../config.php';
$id_pro = "";
if(isset($_POST['id_product'])) $id_pro = $_POST['id_product'];

$class_product = "";
if($id_pro == "2735"):
    $class_product = "is_corredera";
endif;

$familia = new Familia();

$array_grupos = $familia->getGrupoColoresFamilia($id_pro);

?>

<div class="panel-collapse col-md-12 col-xs-12">
    <div class="row">
        <div class="div_colors col-md-12 col-xs-12">

<?php

foreach($array_grupos as $item): ?>

    <div group_color="<?=$item['cosubf']?>" class="col-md-12 col-xs-12">
        <h6><?=lang_title_grupo?> <?=$item['cosubf']?></h6>
        <?php
        $array_colores = $familia->getColoresByFamilia($id_pro, $item['cosubf']);

        foreach($array_colores as $item_color): ?>
            <div color_product="<?=$item_color['cocolo']?>" class="group_colors group_colors_modal col-md-6">
                <?php


                if($item['cosubf'] == 4 OR $item['cosubf'] == 3): ?>

                    <?php

                    $url_imagen = $id_pro.$item['cosubf'].$item_color['cocolo'].".jpg";

                    ?>
                    <div class="color_product change_color_product is_image <?=$class_product?>">
                        <img class="img_color_product" src="<?=path_image_colors.$url_imagen?>">
                    </div>

                <?php else:?>

                    <div style="background-color: <?=$item_color['html']?>" class="color_product change_color_product <?=$class_product?>"></div>


                <?php endif; ?>
                <span class="default_color title_color_product title_color_product_modal"><?=$item_color['codesc']?></span>
            </div>
        <?php endforeach; ?>
    </div>
<?php endforeach; ?>
            <div group_color="lacados" class="col-md-12 col-xs-12">
                <h6><?=lang_lacados_carta_ral?></h6>
                <div color_product="lacado" class="group_colors col-md-12 col-xs-12 group_colors_modal">

                    <div class="color_product">
                        <div style="background-color: #FFFFFF" class="color_product change_color_product_ral change_color_product_ral_modal <?=$class_product?>"></div>
                    </div>
                    <span class="default_color text_color_ral title_color_product title_color_product_modal_ral"></span>
                    <input class="form-control change_color_ral" size="4" maxlength="4" min="4" type="text" name="change_color_ral" placeholder="Ej. 8012">
                </div>
            </div>
        </div>
    </div>
</div>
