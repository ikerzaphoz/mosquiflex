<?php

require '../../../../config.php';
$familia = new Familia();

/*
[cantidad] => 1
    [ancho] => 1.200
    [alto] => 1.
    [group_color] => 1
    [color] => 3
    [tipo_producto] => corredera_completa
*/

$cantidad = "";
if (isset($_POST['cantidad'])): $cantidad = $_POST['cantidad'];
endif;
$ancho = "";
if (isset($_POST['ancho'])): $ancho = $_POST['ancho'];
endif;
$alto = "";
if (isset($_POST['alto'])): $alto = $_POST['alto'];
endif;
$group_color = "";
if (isset($_POST['group_color'])): $group_color = $_POST['group_color'];
endif;
$color = "";
if (isset($_POST['color'])): $color = $_POST['color'];
endif;
$una_hoja = "";
if(isset($_POST['una_hoja'])): $una_hoja = $_POST['una_hoja'];
endif;
$tipo_producto = "";
if(isset($_POST['tipo_producto'])): $tipo_producto = $_POST['tipo_producto'];
endif;

switch ($tipo_producto):
    case 'corredera_completa':
        
        $ancho_marco_corredera = calc_precio_ancho_marco_corredera($ancho, "12");
        $alto_marco_corredera = calc_precio_alto_marco_corredera($alto, "10");
        
        $rango_marco = $ancho_marco_corredera . $alto_marco_corredera;
        
        $precio_rango_marco = $familia->getPriceOfRango("2735", $group_color, $rango_marco);
        
        $precio_marco = $precio_rango_marco * $cantidad;
                      
        $ancho_hoja = round2decimals(round($ancho/2,3));
                            
        $ancho_hoja_corredera = calc_precio_ancho_hoja_corredera($ancho_hoja, "6");
        $alto_hoja_corredera = calc_precio_alto_hoja_corredera($alto, "10");
        
        $rango_hoja = $ancho_hoja_corredera . $alto_hoja_corredera;
                     
        $precio_rango_hoja = $familia->getPriceOfRango("2735", "1" . $group_color, $rango_hoja);
        
        $cantidad_hojas = "1";
        
        if(empty($una_hoja)):
            $precio_hoja = $precio_rango_hoja * ($cantidad * 2);
            $cantidad_hojas = "2";
            $ancho_hoja_info = $ancho/2;
        else: 
            $precio_hoja = $precio_rango_hoja * $cantidad;
            $ancho_hoja_info = $ancho;
        endif;
        
        $precio_total_corredera = $precio_marco + $precio_hoja;
        
        $array_mensaje = array('precio_hoja' => $precio_hoja, 'precio_marco' => $precio_marco, 'precio_total' => $precio_total_corredera, 'cantidad_hojas' => $cantidad_hojas, "ancho_hojas" => $ancho_hoja);
        
        echo json_encode($array_mensaje);
      
        break;
        
    case 'corredera_marco':
        
        $ancho_marco_corredera = calc_precio_ancho_marco_corredera($ancho, "12");
        $alto_marco_corredera = calc_precio_alto_marco_corredera($alto, "10");
        
        $rango_marco = $ancho_marco_corredera . $alto_marco_corredera;
        
        $precio_rango_marco = $familia->getPriceOfRango("2735", $group_color, $rango_marco);
        
        $precio_marco = $precio_rango_marco * $cantidad;
       
        $array_mensaje = array('precio_total' => $precio_marco);
        
        echo json_encode($array_mensaje);
      
        break;
    
    case 'corredera_hoja':
        
        $ancho_hoja_corredera = calc_precio_ancho_hoja_corredera($ancho, "6");
        $alto_hoja_corredera = calc_precio_alto_hoja_corredera($alto, "10");
        
        $rango_hoja = $ancho_hoja_corredera . $alto_hoja_corredera;
               
        $precio_rango_hoja = $familia->getPriceOfRango("2735", "1" . $group_color, $rango_hoja);
        
        $precio_hoja = $precio_rango_hoja * $cantidad;
        
        $array_mensaje = array('precio_total' => $precio_hoja);
        
        echo json_encode($array_mensaje);
        
        break;
endswitch;

exit;

$ancho_marco_corredera = calc_precio_ancho_marco_corredera($ancho_marco_corredera, "12");
$alto_marco_corredera = calc_precio_alto_marco_corredera($alto_marco_corredera, "10");

$rango_marco = $ancho_marco_corredera . $alto_marco_corredera;

$precio_rango_marco = $familia->getPriceOfRango("2735", $group_color_corredera, $rango_marco);
$precio_marco = $precio_rango_marco * $cantidad_marco_corredera;

$ancho_hoja_corredera = calc_precio_ancho_hoja_corredera($ancho_hoja_corredera, "6");
$alto_hoja_corredera = calc_precio_alto_hoja_corredera($alto_hoja_corredera, "10");

$rango_hoja = $ancho_hoja_corredera . $alto_hoja_corredera;

$precio_rango_hoja = $familia->getPriceOfRango("2735", "1" . $group_color_corredera, $rango_hoja);
$precio_hoja = $precio_rango_hoja * $cantidad_hoja_corredera;

$precio_total_corredera = $precio_marco + $precio_hoja;

$array_mensaje = array('precio_hoja' => $precio_hoja, 'precio_marco' => $precio_marco, 'precio_total' => $precio_total_corredera);

echo json_encode($array_mensaje);
