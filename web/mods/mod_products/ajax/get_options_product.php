<?php

require '../../../../config.php';
$familia = new Familia();

$array_opciones = $familia->getOptionsProduct("2799","90");
$hidden_image = "";

$array_components = "";
if(isset($_POST['array_components'])) $array_components = $_POST['array_components'];

$version = 0;
if(isset($_POST['version'])) $version = $_POST['version'];

if($_POST['id_product'] == "2731"):

if($version == 0 OR $version == 1):
    $array_hidden = array("","1","2","3","4","5","6","7","14","12", "101", "102", "103", "104", "105", "106", "15");
endif;
if($version == 2):
    $array_hidden = array("","1","2","3","4", "5", "6", "7", "13", "12", "101", "102", "103", "104", "105", "106", "15");
endif;

else:
    if($_POST['id_product'] == "2735"):
        $hidden_image = "hidden";
        $array_hidden = array("","1","3","4","5","6","7", "8", "9", "10", "11", "13","14","12", "101", "102", "103", "104", "105", "106", "15", '100002', '100001');
    else:
        $hidden_image = "hidden";
        $array_hidden = array("","1","6","7", "8", "9", "10", "11", "13","14","12", "101", "102", "103", "104", "105", "106", "15", '100002', '100001');
    endif;
endif;

?>

<div class="row col-md-12">
<div class="col-md-1"></div>
<div class="title_line_product col-md-3 <?=$hidden_image?>"><?=lang_text_croquis?></div>
<div class="title_line_product col-md-5"><?=lang_title_descripcion?></div>
<div class="title_line_product col-md-2 text-center">Incremento<br>€/Ud. Mosquitera</div>
</div>

<?php foreach($array_opciones as $item):
       
        $hidden = "";
    if(array_search($item['ararti'],$array_hidden)):
        $hidden = "hidden";
    endif;

    $id_component = "";
    if(isset($item['arfami']) AND isset($item['arsubf']) AND isset($item['ararti'])) $id_component = $item['arfami'].$item['arsubf'].$item['ararti'];
    $ardesc = "";
    if(isset($item['ardesc'])) $ardesc = $item['ardesc'];
    $arpvp1 = "";
    if(isset($item['arpvp1'])) $arpvp1 = number_format($item['arpvp1'],2);
    $arimg = "";
    if(isset($id_component)) $arimg = $id_component.'.jpg';

    $selected = "";
    if(!empty($array_components)):
        foreach($array_components as $item_com):
            if($item_com['id_component'] == $id_component):
                $selected = "checked";
            endif;
        endforeach;
    endif;

    ?>

    <div class="<?=$hidden?> row mt-10 col-md-12">
        <div class="col-md-1">
            <input type="checkbox" <?=$selected?> class="checkbox_options">
        </div>
        <div class="col-md-3 img <?=$hidden_image?>">
            <div class="image_croquis <?php if($arimg == "279990000013.jpg"): echo "image_muelle"; endif;?>" style="background-image: url(<?=path_image_croquis?>/<?=$item['arfami']?>/<?=$arimg?>)"></div>
        </div>
        <div class="col-md-5 desc"><?=$ardesc?></div>
        <div class="col-md-2 price"><?=number_format($item['arpvp1'],2)?></div>
        <input type="hidden" class="id_component" value="<?=$id_component?>">
    </div>

<?php endforeach; ?>