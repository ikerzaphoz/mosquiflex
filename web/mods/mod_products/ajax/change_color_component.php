<?php

require '../../../../config.php';

$familia = new Familia();

$value_type_product = "";
if(isset($_POST['value_type_product'])) $value_type_product = $_POST['value_type_product'];
$group_color = "";
if(isset($_POST['group_color'])) $group_color = $_POST['group_color'];
$arfami = "";
if(isset($_POST['arfami'])) $arfami = $_POST['arfami'];
$arsubf = "";
if(isset($_POST['arsubf'])) $arsubf = $_POST['arsubf'];
$ararti = "";
if(isset($_POST['ararti'])) $ararti = $_POST['ararti'];
$arprpr = "";
if(isset($_POST['arprpr'])) $arprpr = $_POST['arprpr'];

if($arfami == "1791"):
    //Artículos que no cargan el precio correcto
    //2 primeros digitos de articulo = 01
    //$arsubf 6 = guia 22X40
    //$arsubf 12 = CAJON-35-20-1700
    //$arsubf 13 = CAJON-42-25-1700
    //$arsubf 21 = GUIA FELPUDO B
    $control_price_product = "0";
    if(substr($ararti, 0, 2) == "01" AND ($arsubf == "6" OR $arsubf == "12" OR $arsubf == "13" OR $arsubf == "21")):
        $control_price_product = "1";
        if($group_color == "brut"):
            $ararti = "010001";
        elseif($group_color == "1"):
            $ararti = "010002";
        elseif($group_color == "2" OR $group_color == "lacado"):
            $ararti = "010003";
        elseif($group_color == "3"):
            $ararti = "010006";
        else:
            $ararti = "010007";
        endif;
    endif;
    //2 primeros dígitos de Ararti  = 02
    //$arsubf 6 = guia doble
    //$arsubf 13 = CAJON-42-25-2300
    //$arsubf 21 = GUIA FELPUDO C
    if(substr($ararti, 0, 2) == "02" AND ($arsubf == "6" OR $arsubf == "13" OR $arsubf == "21")):
        $control_price_product = "1";
        if($group_color == "brut"):
            $ararti = "020001";
        elseif($group_color == "1"):
            $ararti = "020002";
        elseif($group_color == "2" OR $group_color == "lacado"):
            $ararti = "020003";
        elseif($group_color == "3"):
            $ararti = "020006";
        else:
            $ararti = "020007";
        endif;
    endif;
    //2 primeros dígitos de Ararti  = 10
    //$arsubf 21 = GUIA DOBLE FEL.-A-
    if(substr($ararti, 0, 2) == "10" AND ($arsubf == "21")):
        $control_price_product = "1";
        if($group_color == "brut"):
            $ararti = "020001";
        elseif($group_color == "1"):
            $ararti = "100002";
        elseif($group_color == "2" OR $group_color == "lacado"):
            $ararti = "100003";
        elseif($group_color == "3"):
            $ararti = "100004";
        else:
            $ararti = "100007";
        endif;
    endif;
    //2 primeros dígitos de Ararti  = 11
    //$arsubf 21 = GUIA DOBLE FEL.-B-
    if(substr($ararti, 0, 2) == "11" AND ($arsubf == "21")):
        $control_price_product = "1";
        if($group_color == "brut"):
            $ararti = "020001";
        elseif($group_color == "1"):
            $ararti = "110002";
        elseif($group_color == "2" OR $group_color == "lacado"):
            $ararti = "110003";
        elseif($group_color == "3"):
            $ararti = "110004";
        else:
            $ararti = "110007";
        endif;
    endif;
    //2 primeros dígitos de Ararti  = 12
    //$arsubf 21 = GUIA DOBLE FEL.-C-
    if(substr($ararti, 0, 2) == "12" AND ($arsubf == "21")):
        $control_price_product = "1";
        if($group_color == "brut"):
            $ararti = "020001";
        elseif($group_color == "1"):
            $ararti = "120002";
        elseif($group_color == "2" OR $group_color == "lacado"):
            $ararti = "120003";
        elseif($group_color == "3"):
            $ararti = "120004";
        else:
            $ararti = "120007";
        endif;
    endif;
    //Esto funciona para algunos artítulos, añadido código para controlar alguños de ellos
    if(empty($control_price_product)):
        if($group_color == "brut"):
                $ararti = "1";
            elseif($group_color == "1"):
                $ararti = "2";
            elseif($group_color == "2" OR $group_color == "lacado"):
                $ararti = "3";
            elseif($group_color == "3"):
                $ararti = "6";
            else:
                $ararti = "7";
        endif;
    endif;
endif;

$array_precios = $familia->getInfoComponent($arfami,$arsubf,$ararti,$arprpr);

$precio = 0;
if($value_type_product == 1):
    $precio = $array_precios['arpvta'];
else:
    $precio = $array_precios['arpval'];
endif;

echo $precio;