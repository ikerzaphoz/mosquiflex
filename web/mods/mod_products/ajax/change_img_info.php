<?php

require '../../../../config.php';
$subfamily = "";
if(isset($_POST['subfamily'])) $subfamily = $_POST['subfamily'];
$id_product = "";
if(isset($_POST['id_product'])) $id_product = $_POST['id_product'];
$id_group_color = "";
if(isset($_POST['id_group_color'])) $id_group_color = $_POST['id_group_color'];

$code_sub = $subfamily.$id_group_color;
$familia = new Familia();
$ancho_minimo = $familia->getAnchoMin($id_product, $code_sub);
$ancho_maximo = $familia->getAnchoMax($id_product, $code_sub);
$alto_minimo = $familia->getAltoMin($id_product, $code_sub);
$alto_maximo = $familia->getAltoMax($id_product, $code_sub);

$array_medidas = array('ancho_minimo' => $ancho_minimo, 'ancho_maximo' => $ancho_maximo, 'alto_minimo' => $alto_minimo, 'alto_maximo' => $alto_maximo);

echo json_encode($array_medidas);