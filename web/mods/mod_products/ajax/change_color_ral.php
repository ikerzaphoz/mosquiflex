<?php

require '../../../../config.php';
$familia = new Familia();

$title_ral = "";
if(isset($_POST['title_ral'])) $title_ral = $_POST['title_ral'];
$id_product = "";
if(isset($_POST['id_product'])) $id_product = $_POST['id_product'];

$array_color = $familia->getColorsRal($title_ral, $id_product);
$check_group_color = $familia->getColoressByCocolo($id_product, $title_ral);
if(!empty($check_group_color)){
    $array_error = array('error' => '-2', 'message' => 'Ral existente en nuestro catálogo');
    echo json_encode($array_error);
    exit;
}

$array_error = array('error' => '-1', 'message' => 'Error desconocido');
if(!empty($array_color)):
    $array_error = array('error' => '0', 'coralti' => $array_color['coralti'], 'coraldes' => $array_color['coraldes'], 'coralht' => $array_color['coralht']);
else:
    $array_error = array('error' => '1', 'coraldes' => "No existe RAL", 'coralht' => "#FFFFFF");
endif;
echo json_encode($array_error);