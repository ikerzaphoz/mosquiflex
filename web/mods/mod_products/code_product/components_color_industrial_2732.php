<?php

switch ($arsubf):
    case("01"):
        if ($group_color == 0):
            include 'components_group_color.php';
        endif;
        break;
    case ("02"):
        if ($group_color == 1):
            include 'components_group_color.php';
        endif;
        break;
    case ("03"):
        if ($group_color == 2):
            include 'components_group_color.php';
        endif;
        break;
    case ("04"):
        if ($group_color == 3):
            include 'components_group_color.php';
        endif;
        break;
    default:

        ?>

        <div class="fila_components_products col-md-12 col-xs-12 row text-center">
            <div class="border_components row">
                <div class="col-md-1 padding-top-20 padding-left-0 padding-right-0">
                    <input name="input_unit_components[]" step="<?= $embalaje ?>" type="number" value="0"
                           min="0" class="nopadding form-control input_unit_components">
                </div>
                <div class="col-md-1 col-xs-1 padding-top-20 padding-left-0 padding-right-0">
                    Unidades
                </div>
                <div class="col-md-2">
                    <img class="img_components" src="<?= $imagen ?>">
                    <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= $imagen ?>">
                        <img class="hidden" src="<?= $imagen ?>">
                    </a>
                </div>
                <div class="col-md-2 padding-top-20"><?= $referencia ?></div>
                <div
                    class="col-md-3 padding-top-20 text-left"><?= hideColorListComponent($item['ardesc']) ?></div>
                <div class="col-md-1 padding-top-20"><?= $embalaje ?></div>
                <div class="col-md-1 col-xs-1 padding-top-20"><?= $precio_almacen ?></div>
                <div class="col-md-1 padding-top-20"><span class="total_component"></span></div>
                <input type="hidden" class="id_component" name="id_component[]" value="<?= $id_component ?>">
                <input type="hidden" class="precio_almacen" value="<?= $precio_almacen ?>">
                <input type="hidden" class="arsubf" name="arsubf[]" value="<?= $arsubf ?>">
                <input type="hidden" class="ararti" name="ararti[]" value="<?= $ararti ?>">
                <input type="hidden" class="arprpr" name="arprpr[]" value="<?= $item['arprpr'] ?>">
                <input type="hidden" class="type_medida_component" name="type_medida_component[]" value="3">
                <input type="hidden" class="descripcion" name="descripcion[]" value="<?= $item['ardesc'] ?>">
                <input type="hidden" class="total_component_hidden" name="precio[]" value="">
                <input type="hidden" class="type_button_hidden" name="button_component" value="">
                <input type="hidden" class="embalaje" name="embalaje" value="<?= $embalaje ?>">
                <input type="hidden" class="ararti_first" name="ararti_first[]"
                       value="<?= substr($ararti, 0, 2) ?>">
                <input type="hidden" name="components_color[]" class="components_color" value="-">
                <input type="hidden" name="is_almacen[]" class="is_almacen" value="1">
            </div>
        </div>

        <?php

endswitch;