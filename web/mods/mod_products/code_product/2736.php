<div class="panel panel-default form-inline col-md-12 col-xs-12 nopadding">
    <div class="panel-heading">
        <h4 class="panel-title">
            <div class="title_panel_title title_versiones"><?= lang_title_versiones ?></div>
        </h4>
    </div>

    <div class="panel-collapse text-center producto_acabado nopadding">
        <div class="row col-xs-12 col-md-12">
            <div sub_familia="0" class="radio radio_subfamilia radio_abatible col-md-6 col-xs-6"><?= lang_title_puerta_unica ?></div>
            <div sub_familia="1" class="radio radio_subfamilia radio_abatible col-md-6 col-xs-6"><?= lang_title_puerta_doble ?></div>
        </div>
    </div>

    <input value="" class="hidden input_subfamily_product" name="subfamily_product"/>

</div>

<div class="panel panel-default form-inline col-md-12 col-xs-12 nopadding div_tipologia_abatible">
    <div class="panel-heading">
        <h4 class="panel-title">
            <div class="title_panel_title title_versiones"><?= lang_title_tipologia ?>&nbsp;&nbsp;&nbsp;<span
                    data-tooltip="tooltip" data-placement="top" title="<?=lang_text_ver_mas_info?>"
                    class="title_info_product title-warning fa fa-info-circle image-popup-no-margins icono-lupa fa fa-search"
                    href="<?= path_image ?>images_product/2736/tipologia.jpg"><img class="hidden"
                                                                                                   src="<?= path_image ?>images_product/2736/tipologia.jpg">
</span></div>
        </h4>
    </div>

    <div class="panel-collapse text-center producto_acabado nopadding">
        <div class="row col-xs-12 col-md-12 mt-10 mb-10">
            <div class="col-xs-6 col-md-6">
                <span class="text-select-version"><?=lang_text_seleccione_version?></span>
            </div>
            <div class="col-xs-6 col-md-6">
                <select class="form-control hidden tipologia_0 select_tipologia">
                    <option disabled selected>Puerta única</option>
                    <option value="a1">A1</option>
                    <option value="a2">A2</option>
                </select>
                <select class="form-control hidden tipologia_1 select_tipologia">
                    <option disabled selected>Puerta doble</option>
                    <option value="a3">A3</option>
                    <option value="a4">A4</option>
                    <option value="a5">A5</option>
                    <option value="a6">A6</option>
                </select>
            </div>
        </div>
    </div>

    <input value="" class="hidden input_tipologia" name="input_tipologia"/>

</div>