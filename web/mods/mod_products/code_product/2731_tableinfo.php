<table class="table_info_medidas">
    <tr>
        <td id_sub="0">
            <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?=path_image?>images_product/<?=$id_product?>/cajon33.png">
                <img class="hidden" src="<?=path_image?>images_product/<?=$id_pro?>/cajon33.png">
            </a>
            <img class="icon_subfamilia img_table_info" src="<?=path_image?>images_product/<?=$id_product?>/cajon33.png">

        </td>
        <td id_sub="1">
            <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?=path_image?>images_product/<?=$id_product?>/cajon35.png">
                <img class="hidden" src="<?=path_image?>images_product/<?=$id_pro?>/cajon35.png">
            </a>
            <img class="icon_subfamilia img_table_info" src="<?=path_image?>images_product/<?=$id_product?>/cajon35.png">
        </td>
        <td id_sub="2">
            <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?=path_image?>images_product/<?=$id_product?>/cajon42.png">
                <img class="hidden" src="<?=path_image?>images_product/<?=$id_pro?>/cajon42.png">
            </a>
            <img class="icon_subfamilia img_table_info" src="<?=path_image?>images_product/<?=$id_product?>/cajon42.png">
        </td>
    </tr>
    <tr>
        <td class="bg-yellow">1,60</td>
        <td class="bg-red">1,60</td>
        <td class="bg-green">2,20</td>
        <td class="bg-grey">ANCHO</td>
        <td class="bg-dark-grey" rowspan="2">MÁXIMOS A FABRICAR</td>
    </tr>
    <tr>
        <td class="bg-yellow">1,45</td>
        <td class="bg-red">1,75</td>
        <td class="bg-green">2,35</td>
        <td class="bg-grey">ALTO</td>
    </tr>
    <tr>
        <td class="bg-dark-grey" colspan="5">ANCHO MÍNIMO A FABRICAR 0,41 EN LOS 3 MODELOS</td>
    </tr>
</table>