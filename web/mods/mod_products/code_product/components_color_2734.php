<?php

switch ($arsubf):
    case ("02"):
        if ($group_color == 0):
            include 'components_group_color.php';
        endif;
        break;
    case ("03"):
        break;
    case ("04"):
        if ($group_color == 2):
            include 'components_group_color.php';
        endif;
        break;
    case ("05"):
        break;
    default:

        $class_hidden = "";
        if($arsubf == "06" AND $ararti == "000003"):
            $class_hidden = "hidden";
        endif;

        ?>

        <div class="fila_components_products col-md-12 col-xs-12 row text-center <?=$class_hidden?>">
            <div class="border_components row">
                <div class="col-md-1 col-xs-1 padding-top-20 padding-left-0 padding-right-0">
                    <?php if ($tipo_medida == 1 OR $tipo_medida == 2): ?>
                        <input type="hidden" name="input_unit_components[]" class="input_unit_components" value="0">
                        <input type="button" class="btn btn-xs bg-border-info add_medidas_2" value="<?=lang_boton_add_unidades?>">
                    <?php else: ?>
                        <input name="input_unit_components[]" type="number" value="0" min="0"
                               class="nopadding form-control input_unit_components">
                        <span class="hidden linea_medida_componente_check_units"></span>
                    <?php endif; ?>
                </div>
                <div class="col-md-1 col-xs-1 padding-top-20 padding-left-0 padding-right-0">
                    <?= getTipoMedida($tipo_medida) ?>
                </div>
                <div class="col-md-2 col-xs-2">
                    <img class="img_components" src="<?= $imagen ?>">
                    <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= $imagen ?>">
                        <img class="hidden" src="<?= $imagen ?>">
                    </a>
                </div>
                <div class="col-md-2 col-xs-2 padding-top-20"><?= $referencia ?></div>
                <div
                    class="col-md-3 col-xs-3 padding-top-20 text-left"><?= hideColorListComponent($item['ardesc']) ?></div>
                <div class="col-md-1 col-xs-1 padding-top-20"><?= $precio_fabrica ?></div>
                <div class="col-md-1 col-xs-1 padding-top-20"><span class="total_component"></span></div>
                <input type="hidden" class="id_component" name="id_component[]" value="<?= $id_component ?>">
                <input type="hidden" class="precio_fabrica" name="precio_fabrica[]" value="<?= $precio_fabrica ?>">
                <input type="hidden" class="arsubf" name="arsubf[]" value="<?= $arsubf ?>">
                <input type="hidden" class="ararti" name="ararti[]" value="<?= $ararti ?>">
                <input type="hidden" class="ararti_first" name="ararti_first[]" value="<?= substr($ararti, 0, 2) ?>">

                <input type="hidden" class="arprpr" name="arprpr[]" value="<?= $item['arprpr'] ?>">
                <input type="hidden" class="descripcion" name="descripcion[]" value="<?= $item['ardesc'] ?>">
                <input type="hidden" class="total_component_hidden" name="precio[]" value="">
                <input type="hidden" class="type_medida_component" name="type_medida_component[]"
                       value="<?= $tipo_medida ?>">
                <input type="hidden" class="embalaje" name="embalaje" value="<?= $embalaje ?>">
                <input type="hidden" class="max_measure" name="max_measure" value="<?= $max_measure ?>">
                <input type="hidden" class="type_button_hidden" name="button_component" value="">
                <input type="hidden" class="total_units_metros" name="total_units_metros" value="">
                <input type="hidden" class="ancho" name="ancho[]" value="">
                <input type="hidden" class="alto" name="alto[]" value="">
                <input type="hidden" class="metros" name="metros[]" value="">
                <?php if ($tipo_medida == 1 OR $tipo_medida == 2): ?>
                    <div class="hidden col-xs-12 col-md-12 row content_lineas_medidas_componente text-left"></div>
                <?php endif; ?>
                <input type="hidden" name="data_component[]" class="data_component" value="">
                <input type="hidden" name="components_color[]" class="components_color" value="-">
                <input type="hidden" name="is_almacen[]" class="is_almacen" value="0">
            </div>
        </div>


        <?php

endswitch;