<div class="panel panel-default form-inline col-md-12 col-xs-12 nopadding div_producto_versiones">
    <div class="panel-heading">
        <h4 class="panel-title">
            <div class="title_panel_title title_versiones"><?=lang_title_versiones?></div>
        </h4>
    </div>

    <div class="panel-collapse text-center producto_acabado nopadding">
        <div class="row col-xs-12 col-md-12">
<!--            <div class="col-xs-3 col-md-1">
                <a class="image-popup-no-margins icono-lupa mt100 fa fa-search" href="<?=path_image?>images_product/<?=$id_pro?>/cajon33.png">
                    <img class="hidden" src="<?=path_image?>images_product/<?=$id_pro?>/cajon33.png">
                </a>
                <span class="text_lupa"><?=lang_title_ver_imagen?></span>
            </div>
            <div sub_familia="0" class="radio radio_subfamilia col-md-3 col-xs-3 text-left">
                <img class="icon_subfamilia" src="<?=path_image?>images_product/<?=$id_pro?>/cajon33.png"><label class="padding-left-0-mobile"><span class="title_label title_subfamilia" style="background-color: #ffee02;"><?=lang_title_cajon?> 33</span></label>
            </div>-->
            <div class="col-xs-3 col-md-1">
                <a class="image-popup-no-margins mt100 icono-lupa fa fa-search" href="<?=path_image?>images_product/<?=$id_pro?>/cajon35.png">
                    <img class="hidden" src="<?=path_image?>images_product/<?=$id_pro?>/cajon35.png">
                </a>
                <span class="text_lupa"><?=lang_title_ver_imagen?></span>
            </div>
            <div sub_familia="1" class="radio radio_subfamilia col-md-4 col-xs-3 text-left">
                <img class="icon_subfamilia" src="<?=path_image?>images_product/<?=$id_pro?>/cajon35.png"><label class="padding-left-0-mobile"><span class="title_label title_subfamilia" style="background-color: #e7302a;"><?=lang_title_cajon?> 35</span></label><input type="radio" class="click_radio_version"/>
            </div>
            <div class="col-xs-3 col-md-1">
                <a class="image-popup-no-margins mt100 icono-lupa fa fa-search" href="<?=path_image?>images_product/<?=$id_pro?>/cajon42.png">
                    <img class="hidden" src="<?=path_image?>images_product/<?=$id_pro?>/cajon42.png">
                </a>
                <span class="text_lupa"><?=lang_title_ver_imagen?></span>
            </div>
            <div sub_familia="2" class="radio radio_subfamilia col-md-4 col-xs-3 text-left">
                <img class="icon_subfamilia" src="<?=path_image?>images_product/<?=$id_pro?>/cajon42.png"><label class="padding-left-0-mobile"><span class="title_label title_subfamilia" style="background-color: #62a730;"><?=lang_title_cajon?> 42</span></label><input type="radio" class="click_radio_version"/>
            </div>
        </div>
    </div>

    <input value="" class="hidden input_subfamily_product" name="subfamily_product"/>

</div>