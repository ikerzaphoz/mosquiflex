<div class="panel panel-default form-inline col-md-12 col-xs-12 nopadding">
    <div class="panel-heading">
        <h4 class="panel-title">
            <div class="title_panel_title title_versiones"><?=lang_title_versiones?></div>
        </h4>
    </div>

    <div class="panel-collapse text-center producto_acabado nopadding">
        <div class="row col-xs-12 col-md-12">
            <div sub_familia="0" class="radio radio_subfamilia col-md-6 col-xs-6"><?=lang_title_puerta_unica?></div>
            <div sub_familia="1" class="radio radio_subfamilia col-md-6 col-xs-6"><?=lang_title_puerta_doble?></div>
        </div>
    </div>

    <input value="" class="hidden input_subfamily_product" name="subfamily_product"/>

</div>