<form name="form_product" class="form_product" method="post" action="<?= path_web ?>productos.php">
    <div id="content_product" class="panel panel-default content_product_expositores">
        <div class="panel-heading selected">
            <h4 class="panel-title">
                <div class="title_panel_title"><?=lang_title_expositores_punto_venta?></div>
            </h4>
        </div>
        <div class="panel-body text-center">
            <div class="col-md-12 col-xs-12">
                <div class="radio col-md-7 col-xs-7">
                    <div class="col-md-6 col-xs-6 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <span class="text">Enrollable ventana</span>
                            <span class="price">75€</span>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">
                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                                    <img class="hidden"
                                         src="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                                </a>
                                <span class="text_lupa"><?=lang_title_ver_imagen?></span>
                            </div>
                            <div class="col-md-12 col-xs-12 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="75"/>
                                <input type="hidden" class="total_line">
                                <input class="info_submit" type="hidden" name="enr_ventana" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <span class="text">Enrollable puerta</span>
                            <span class="price">150€</span>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">
                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                                    <img class="hidden"
                                         src="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                                </a>
                                <span class="text_lupa"><?=lang_title_ver_imagen?></span>
                            </div>
                            <div class="col-md-12 col-xs-12 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="150"/>
                                <input type="hidden" class="total_line">
                                <input class="info_submit" type="hidden" name="enr_puerta" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 mt-3 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <span class="text">Plisadas</span>
                            <span class="price">150€</span>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">
                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/plisada.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/plisada.png">
                                    <img class="hidden" src="<?= path_image ?>images_product/expositores/plisada.png">
                                </a>
                                <span class="text_lupa"><?=lang_title_ver_imagen?></span>
                            </div>
                            <div class="col-md-12 col-xs-12 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="150"/>
                                <input type="hidden" class="total_line">
                                <input class="info_submit" type="hidden" name="exp_plisada" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 mt-3 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <span class="text">Abatibles</span>
                            <span class="price">150€</span>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">

                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/abatible.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/abatible.png">
                                    <img class="hidden" src="<?= path_image ?>images_product/expositores/abatible.png">
                                </a>
                                <span class="text_lupa"><?=lang_title_ver_imagen?></span>
                            </div>
                            <div class="col-md-12 col-xs-12 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="150"/>
                                <input type="hidden" class="total_line">
                                <input class="info_submit" type="hidden" name="exp_abatible" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 mt-3 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <span class="text">Fijas y Correderas</span>
                            <span class="price">75€</span>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">

                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/fija_corredera.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/fija_corredera.png">
                                    <img class="hidden"
                                         src="<?= path_image ?>images_product/expositores/fija_corredera.png">
                                </a>
                                <span class="text_lupa"><?=lang_title_ver_imagen?></span>
                            </div>
                            <div class="col-md-12 col-xs-12 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="75"/>
                                <input type="hidden" class="total_line">
                                <input class="info_submit" type="hidden" name="exp_fija_corredera" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 mt-3 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <span class="text">Selector de Colores</span>
                            <span class="price">10€</span>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">

                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/selector.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/selector.png">
                                    <img class="hidden"
                                         src="<?= path_image ?>images_product/expositores/selector.png">
                                </a>
                                <span class="text_lupa"><?=lang_title_ver_imagen?></span>
                            </div>
                            <div class="col-md-12 col-xs-12 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="10"/>
                                <input type="hidden" class="total_line">
                                <input class="info_submit" type="hidden" name="exp_selector_color" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="radio col-md-5 col-xs-5 div_imagen_expositor contenido_expositor">
                    <div class="col-md-12 col-xs-12 div_title_expositores">
                        <span class="text">Expositor personalizable</span>
                        <span class="price">500€</span>
                    </div>
                    <div class="col-md-12 col-xs-12 contenido_expositor_border">

                        <div class="col-md-12 col-xs-12 div_img_expositores">
                            <img src="<?= path_image ?>images_product/expositores/expositor.png">
                        </div>
                        <div>
                            <a class="image-popup-no-margins icono-lupa fa fa-search"
                               href="<?= path_image ?>images_product/expositores/expositor.png">
                                <img class="hidden" src="<?= path_image ?>images_product/expositores/expositor.png">
                            </a>
                            <span class="text_lupa"><?=lang_title_ver_imagen?></span>
                        </div>
                        <div class="col-md-12 col-xs-12 div_unidades_expositores">
                            <input class="unidades_expositores" type="number" value="0" min="0">
                            <span class="text_uds">Uds.</span>
                            <input type="hidden" class="precio_expo hidden" value="500"/>
                            <input type="hidden" class="total_line">
                            <input class="info_submit" type="hidden" name="exp_fija_expositor" value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-6"></div>
    <div class="col-md-6 col-xs-6 text-right div_resumen_expositores hidden">
        <div class="col-xs-12 col-md-12 div_content_resumen_expositores">
            <div class="col-md-12 text-left col-xs-12 title_resumen_expositores"><?=lang_title_resumen?>:</div>
            <div class="col-md-12 text-left col-xs-12 content_resumen_expositores"></div>
            <div class="col-md-12 text-right col-xs-12 line_total_expositores"><strong><?=lang_title_total?> (<?=lang_title_impuestos_no_incluidos?>):</strong> <span class="total_expo">0.00</span>&nbsp;€
            </div>
        </div>
    </div>
    <div class="form-inline col-md-12 col-xs-12 div_buttons_expo invisible">
        <div class="panel-collapse text-center">
            <button type="button" class="btn-sm btn_modal_ini_pro btn bg-border-info" value="presupuesto"><?=lang_title_continuar_pres?>
            </button>
            <button type="button" class="btn-sm btn_modal_ini_pro btn bg-border-warning" value="pedido"><?=lang_title_continuar_ped?>
            </button>
        </div>
    </div>
    <input type="hidden" class="form_expo" name="form_expo" value="1">
    <input value="" class="hidden type_button" name="type_button"/>
</form>