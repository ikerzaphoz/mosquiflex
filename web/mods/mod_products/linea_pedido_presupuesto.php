<?php if (isset($array_familia[$id_product])): ?>
    <div class="line_product effect7 col-md-12 col-xs-12 text-left">
        <div class="row col-md-12 col-xs-12">
            <h4 class="title_product"><?= $array_familia[$id_product]['fadesc'] ?>&nbsp;&nbsp;&nbsp;<span
                    data-tooltip="tooltip" data-placement="top" title="<?= lang_text_ver_mas_info ?>"
                    class="title_info_product title-warning fa fa-info-circle image-popup-no-margins icono-lupa fa fa-search"
                    href="<?= path_image ?>images_product/<?= $id_product ?>/medidas.png"><img class="hidden"
                                                                                           src="<?= path_image ?>images_product/<?= $id_pro ?>/medidas.png">
                </span></h4>
        </div>
        <?php
        
        if ($id_product == "2735"):
            include 'linea_pedido_presupuesto_corredera.php';
            else:
        
        ?>
        <div class="row col-md-12 col-xs-12 text-center">
            <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_version ?></div>
            <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_color ?></div>
            <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_unidades ?></div>
            <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_ancho ?></div>
            <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_alto ?></div>
            <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_precio ?> (€)</div>
        </div>
        <div class="col-md-12 col-xs-12 row text-center">
            <div class="col-md-2 col-xs-2">
                <?php
                $version = "";
                switch ($id_product):
                    case '2731':
                        ?>
                        <select class="form-control change-subfamily input_subfamily_product">
<!--                            <option <?php /* if (empty($subfamily_product)): echo "selected";
            endif;
                        */ ?> value="">C.33
                            </option>-->
                            <option <?php if ($subfamily_product == 1): echo "selected";
                    endif;
                    ?> value="1">C.35
                            </option>
                            <option <?php if ($subfamily_product == 2): echo "selected";
                    endif;
                    ?> value="2">C.42
                            </option>
                        </select>
                        <?php
                        break;
                    case '2732':
                        ?>
                        <select class="form-control change-subfamily input_subfamily_product">
                            <option <?php if (empty($subfamily_product)): echo "selected";
                    endif;
                        ?> value="">P.Única
                            </option>
                            <option <?php if ($subfamily_product == 2): echo "selected";
                    endif;
                        ?> value="2">P.Doble
                            </option>
                        </select>
            <?php
            break;
        case '2733':
        case '2736':
            ?>
                        <select class="form-control change-subfamily input_subfamily_product">
                            <option <?php if ($subfamily_product == 0): echo "selected";
            endif;
            ?> value="">P.Única
                            </option>
                            <option <?php if ($subfamily_product == 1): echo "selected";
            endif;
            ?> value="1">P.Doble
                            </option>
                        </select>
                        <input type="hidden" name="input_tipologia" value="<?= $tipologia ?>" class="input_tipologia">
                        <?php
                        break;
                endswitch;
                ?>
            </div>
            <div class="col-md-2 col-xs-2 content_color_pedido">
                <?php
                if ($group_color == 3 OR $group_color == 4):
                    $url_imagen = $id_product . $group_color . $color_product . ".jpg";
                    ?>
                    <div class="color_product_line">
                        <img class="img_color_product" src="<?= path_image_colors . $url_imagen ?>">
                    </div>
    <?php else: ?>
                    <div color_ral="<?= hideRal($array_color['codesc']) ?>"
                         style="background-color: <?= $array_color['html'] ?>" class="color_product_line">
                        <img class="img_color_product hidden" src="">
                    </div>
    <?php endif;
    ?>
                <span class="title_color"><?= $array_color['codesc'] ?></span>
            </div>
            <!--HASTA AQUI EN linea_pedido_presupuesto_corredera.php-->
            <div class="col-md-2 col-xs-2">
                <input required placeholder="1" title="" value="1" min="1" type="number" name="input_unit_product"
                       class="input_unit_product form-control w75"/>
            </div>
            <div class="col-md-2 col-xs-2">
                <input pattern="[0-9]+([\.,][0-9]+)"
                       title="Valor mínimo: <?= $ancho_minimo ?> y máximo: <?= $ancho_maximo ?>. Ejemplo formato: 1.010"
                       required placeholder="<?= $ancho_minimo ?>" type="text" name="input_width_product"
                       class="input_width_product form-control border-warning check_size w100" placeholder="0.000"
                       value=""/>
            </div>
            <div class="col-md-2 col-xs-2">
                <input pattern="[0-9]+([\.,][0-9]+)" required placeholder="<?= $alto_minimo ?>"
                       title="Valor mínimo: <?= $alto_minimo ?> y máximo: <?= $alto_maximo ?>. Ejemplo formato: 1.010"
                       type="text" name="input_height_product"
                       class="input_height_product form-control border-warning check_size w100" placeholder="0.000"
                       value=""/>
            </div>
            <div class="col-md-2 col-xs-2">
                <span class="product_price"><span class="text-danger"><?= lang_text_revisar_medidas ?></span></span>
            </div>
        </div>

        <div class="segunda_linea_producto col-md-12 col-xs-12 row">
            <div class="col-md-2 col-xs-2 text-left">
    <?php if ($id_product == "2731" OR $id_product == "2734"): ?>
                    <button data-toggle="modal" data-target="#modal_option_product"
                            class="btn btn-sm btn-grey btn_option_product"><?= lang_text_anadir_incrementos ?>
                    </button>
    <?php endif; ?>
            </div>
            <div class="col-md-2 col-xs-2 text-left">
                <button data-toggle="modal" data-target="#modal_edit_indicaciones"
                        class="btn btn-sm btn-grey btn_indicaciones_producto">
                    Añadir indicaciones
                </button>
            </div>
            <div class="hidden col-md-6 col-xs-6">
                <textarea name="observaciones_product" class="observaciones_product"
                          placeholder="Observaciones..."></textarea>
                <textarea name="indicaciones_producto" class="indicaciones_producto"
                          placeholder=""></textarea>
            </div>
            <div class="col-md-10 col-xs-10 increments text-right"></div>
        </div>

        <div class="hidden info_product_line">
            <span class="hidden input_id_product" span_value="<?= $id_product ?>" span_name="id_product"></span>
            <span class="hidden input_color_product" span_value="<?= $color_product ?>"
                  span_name="color_product"></span>
            <span class="hidden input_group_color" span_value="<?= $group_color ?>" span_name="group_color"></span>
            <span class="hidden input_danch" span_value="<?= str_pad($ancho_minimo, 5, "0") ?>"
                  span_name="input_danch"></span>
            <span class="hidden input_hanch" span_value="<?= str_pad($ancho_maximo, 5, "0") ?>"
                  span_name="input_hanch"></span>
            <span class="hidden input_dalto" span_value="<?= str_pad($alto_minimo, 5, "0") ?>"
                  span_name="input_dalto"></span>
            <span class="hidden input_halto" span_value="<?= str_pad($alto_maximo, 5, "0") ?>"
                  span_name="input_halto"></span>
        </div>
        <div class="segunda_linea_producto row col-md-12 col-xs-12 text-right">
            <span data-tooltip="tooltip" data-placement="top" title="<?= lang_text_copiar_linea_pedido ?>"
                  class="fa fa-2x fa-copy btn_copy_line text-warning btn-grey btn-option-line"></span>
            <span data-tooltip="tooltip" data-placement="top" title="<?= lang_text_eliminar_linea_pedido ?>"
                  class="fa fa-2x fa-close btn_delete_line text-danger btn-grey btn-option-line"></span>
            <span data-tooltip="tooltip" data-toggle="modal" data-target="#modal_add_product" data-placement="top"
                  title="<?= lang_text_añadir_producto ?>"
                  class="fa fa-2x fa-plus hidden btn_add_product text-success btn-grey btn-option-line"></span>
                  <input type="hidden" class="is_almacen" value="0">
        </div>
        <?php endif; ?>
    </div>

<?php endif; ?>