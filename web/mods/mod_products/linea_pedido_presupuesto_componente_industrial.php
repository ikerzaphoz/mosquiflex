<?php

$array_unidades_componentes = "";
if (isset($_POST['input_unit_components']))
    $array_unidades_componentes = $_POST['input_unit_components'];
$array_id_componentes = "";
if (isset($_POST['id_component']))
    $array_id_componentes = $_POST['id_component'];
$array_subf_componentes = "";
if (isset($_POST['arsubf']))
    $array_subf_componentes = $_POST['arsubf'];
$array_ararti_componentes = "";
if (isset($_POST['ararti']))
    $array_ararti_componentes = $_POST['ararti'];
$array_arprpr_componentes = "";
if (isset($_POST['arprpr']))
    $array_arprpr_componentes = $_POST['arprpr'];
$array_components_color_componentes = "";
if ($_POST['components_color'])
    $array_components_color_componentes = $_POST['components_color'];
$array_desc_componentes = "";
if (isset($_POST['descripcion']))
    $array_desc_componentes = $_POST['descripcion'];
$array_precio_componentes = "";
if (isset($_POST['precio']))
    $array_precio_componentes = $_POST['precio'];
$button_component = "";
if (isset($_POST['button_component']))
    $button_component = $_POST['button_component'];
$array_tipo_medida = "";
if (isset($_POST['type_medida_component']))
    $array_tipo_medida = $_POST['type_medida_component'];
$embalaje = "";
if (isset($_POST['embalaje']))
    $embalaje = $_POST['embalaje'];
$change_components = "";
if (isset($_POST['change_components']))
    $change_components = $_POST['change_components'];
$arumiv = "";
if (isset($_POST['arumiv']))
    $arumiv = $_POST['arumiv'];

if (!empty($_SESSION['products'])):
    //include root . 'web/mods/mod_cart/product_cart.php';
else:
    $type_session_product = "component_industrial";
    //include root . 'web/mods/mod_cart/save_cart_product_init.php';
endif;

?>
<?php if (isset($array_unidades_componentes)): ?>
    <div class="line_product effect7 col-md-12 col-xs-12 text-left line_product_component_aux">
    <div class="row col-md-12 col-xs-12">
        <h4 class="title_product">Componentes</h4>
    </div>
    <div class="row col-md-12 col-xs-12">
        <div class="col-md-2 col-xs-2 title_line_product">Unidades</div>
        <div class="col-md-2 col-xs-2 title_line_product">Color</div>
        <div class="col-md-2 col-xs-2 title_line_product">Referencia</div>
        <div class="col-md-3 col-xs-3 title_line_product">Descripción</div>
        <div class="col-md-2 col-xs-2 title_line_product">Precio (€)</div>
        <div class="col-md-1 col-xs-1"></div>
    </div>

    <?php foreach ($array_unidades_componentes as $index => $componente_unidad): ?>

        <div class="line_product_component col-md-12 col-xs-12 text-left">

            <?php

            $max_measure = $familia->getMaxMeasure($array_id_componentes[$index])[0]['max_measure'];

            $cod_colores = "";
            switch($array_id_componentes[$index]):
                case '1791':
                    $cod_colores = "2731";
                    break;
                case '1792':
                    $cod_colores = "2732";
                    break;
                case '1793':
                    $cod_colores = "2733";
                    break;
                case '1794':
                    $cod_colores = "2734";
                    break;
                case '1795':
                    $cod_colores = "2736";
                    break;
                case '1796':
                    $cod_colores = "2736";
                    break;
            endswitch;

            $array_color['codesc'] = "Bruto";
            $array_color['html'] = "#F3F3F3";
            $class_imagen_hidden = "";
            $src_imagen = "";

            //Comprobación para no obtener los datos del color 'bruto'
            if (isset($array_components_color_componentes[$index]) AND $array_components_color_componentes[$index] != '000001' AND $array_components_color_componentes[$index] != '010001' AND $array_components_color_componentes[$index] != '020001' AND $array_components_color_componentes[$index] != 'bruto' AND $array_components_color_componentes[$index] != '-'):
                $array_color = $familia->getColoressByCocolo($cod_colores, $array_components_color_componentes[$index])[0];
                if (!empty($array_color['codesc']) AND !empty($array_color['html'])):
                    $class_imagen_hidden = "hidden";
                else:
                    $url_image = $array_color['cofami'] . $array_color['cosubf'] . $array_color['cocolo'];
                    $src_imagen = path_image_colors . $url_image . ".jpg";
                endif;
            endif;

            $referencia = "";

            $descripcion = "";

            $is_color = "";

            if (!isset($array_ararti_first_componentes[$index])):
                $array_ararti_first_componentes[$index] = "";
            endif;

            //Agrupado por color
            if (isset($array_components_color_componentes[$index]) AND $array_components_color_componentes[$index] != "-"):
                $ararti = $array_ararti_componentes[$index];
                if (strlen($ararti) != 6) {
                    $ararti = $array_ararti_first_componentes[$index] . $array_components_color_componentes[$index];
                }
                $referencia = $array_id_componentes[$index] . "-" . str_pad($array_subf_componentes[$index], 2, "0", STR_PAD_LEFT) . "-" . $ararti;
                $array_info_component = $familia->getInfoComponent($array_id_componentes[$index], $array_subf_componentes[$index], $ararti, $array_arprpr_componentes[$index]);
                $precio_component = round2decimals($array_info_component['arpval']);

                $descripcion = $array_info_component['ardesc'];
            else:
                $is_color = "hidden";
                $referencia = $array_id_componentes[$index] . "-" . str_pad($array_subf_componentes[$index], 2, "0", STR_PAD_LEFT) . "-" . $array_ararti_componentes[$index];
                $ararti = $array_ararti_componentes[$index];
                if (strlen($ararti) != 6) {
                    $ararti = $array_ararti_first_componentes[$index] . $array_components_color_componentes[$index];
                }
                $array_info_component = $familia->getInfoComponent($array_id_componentes[$index], $array_subf_componentes[$index], $array_ararti_componentes[$index], $array_arprpr_componentes[$index]);
                $precio_component = round2decimals($array_info_component['arpval']);
                $descripcion = $array_info_component['ardesc'];


            endif;

            $color = "-";
            if (!empty($array_components_color_componentes[$index]) AND $array_components_color_componentes[$index] != "-")
                $color = $array_components_color_componentes[$index];

            $precio = round2decimals($precio_component * $array_unidades_componentes[$index]);
            
            $precio = $precio / $arumiv;
            
            $precio = round2decimals($precio);

            $total_presupuesto = $total_presupuesto + $precio;

            $embalaje = $familia->getEmbalaje($array_id_componentes[$index], $array_subf_componentes[$index], $ararti, $array_arprpr_componentes[$index]);

            ?>
            <div class="row col-md-12 col-xs-12 line_component">
                <div class="col-md-2 col-xs-2 text-right">
                    <input type="number" min="0" step="<?=$embalaje?>" value="<?= $array_unidades_componentes[$index] ?>" class="border_units_components change_units_resumen_component"> Uds.
                </div>
                <div class="col-md-2 col-xs-2">
                    Bruto
                </div>
                <div class="col-md-2 col-xs-2"><?= $referencia ?></div>
                <div class="col-md-3 col-xs-3"><?= $descripcion ?></div>
                <div class="col-md-2 col-xs-2 product_price total_component"><?= round2decimals($precio) ?></div>
                <div class="col-md-1 col-xs-1"><span><i class='fa fa-times-circle btn_delete_line_component' aria-hidden='true'></i></span></div>
                <input type="hidden" class="price_component_hidden" value="<?= $precio_component ?>">
                <input type="hidden" class="ararti_component_hidden"
                       value="<?= $array_ararti_first_componentes[$index] . $array_components_color_componentes[$index] ?>">
                <input type="hidden" class="arfami id_component_product"
                       value="<?= $array_id_componentes[$index] ?>">
                <input type="hidden" class="arprpr" value="<?= $array_arprpr_componentes[$index] ?>">
                <input type="hidden" class="arsubf" value="<?= $array_subf_componentes[$index] ?>">
                <input type="hidden" class="total_units_metros" value="<?= $total_metros ?>">
                <input type="hidden" class="ararti" value="<?= $array_ararti_componentes[$index] ?>">
                <input type="hidden" class="fami_color" value="<?= $cod_colores ?>">
                <input type="hidden" class="tipo_medida_component" value="3">
                <input type="hidden" class="max_measure" value="<?= $max_measure ?>">
                <input type="hidden" class="type_product" value="1">
                <input type="hidden" class="is_almacen" value="1">
                <input type="hidden" class="arumiv" value="<?=$arumiv?>">
                <input type="hidden" class="unidades_unidades_medidas"
                       value="<?= $array_unidades_componentes[$index] ?>">
            </div>

            <?php

            if (isset($array_data_component[$index])):
                $pos = 0;
                $total_metros = 0; ?>
                <div class="content_lines_component row col-md-12 col-xs-12">
                    <?php foreach (json_decode($array_data_component[$index], true) as $unidades_medidas):

                        if ($unidades_medidas['arsubf'] == $array_subf_componentes[$index] AND $unidades_medidas['ararti'] == $array_ararti_componentes[$index]):

                            $pos++;
                            if (isset($unidades_medidas['ancho']) AND isset($unidades_medidas['alto'])):

                                $metros = str_replace(",", ".", $unidades_medidas['ancho']) * str_replace(",", ".", $unidades_medidas['alto']);
                                $total_metros += $metros;

                                ?>
                                <div class="segunda_linea_producto mt-10 row col-md-12 col-xs-12 text-right">
                                    <div class="col-md-2 col-xs-2">
                                        <?= $unidades_medidas['unidades'] ?> Uds.
                                    </div>
                                    <div class="col-md-2 col-xs-2">
                                        Ancho: <span
                                            class="resumen_medidas_ancho"><?= $unidades_medidas['ancho'] ?></span>
                                    </div>
                                    <div class="col-md-2 col-xs-2">
                                        Alto: <span
                                            class="resumen_medidas_alto"><?= $unidades_medidas['alto'] ?></span>
                                    </div>
                                    <div class="col-md-2 col-xs-2">
                                        Total: <?= round2decimals($metros) ?> m<sup>2</sup>
                                    </div>
                                    <?php

                                    $aux_ancho = str_replace("m", "", $unidades_medidas['ancho']);
                                    $aux_ancho = str_replace(" ", "", $unidades_medidas['ancho']);
                                    $aux_ancho = str_replace(",", ".", $unidades_medidas['ancho']);

                                    $aux_alto = str_replace("m", "", $unidades_medidas['alto']);
                                    $aux_alto = str_replace(" ", "", $unidades_medidas['alto']);
                                    $aux_alto = str_replace(",", ".", $unidades_medidas['alto']);

                                    $aux_precio = ($aux_ancho * $aux_alto) * $precio_component;
                                    $aux_precio = round2decimals($aux_precio);

                                    ?>
                                    <input type="hidden" class="ancho_unidades_medidas"
                                           value="<?= $aux_ancho ?>">
                                    <input type="hidden" class="alto_unidades_medidas"
                                           value="<?= $unidades_medidas['alto'] ?>">
                                    <input type="hidden" class="precio_unidades_medidas"
                                           value="<?= $aux_precio ?>">
                                    <input type="hidden" class="unidades_unidades_medidas"
                                           value="<?= $unidades_medidas['unidades'] ?>">
                                </div>

                                <?php

                            elseif (isset($unidades_medidas['metros'])):

                                $metros = str_replace(",", ".", $unidades_medidas['metros']);
                                $total_metros += $metros;

                                ?>
                                <div class="segunda_linea_producto row col-md-12 col-xs-12 text-left">
                                    <div class="col-xs-3 col-md-3">
                                        <?= $unidades_medidas['unidades'] ?> Uds. Metros: <span
                                            class="resumen_medidas_metros"><?= $unidades_medidas['metros'] ?></span>
                                    </div>

                                    <?php
                                    $aux_metros = str_replace("metros", "", $metros);
                                    $aux_metros = str_replace(" ", "", $aux_metros);
                                    $aux_metros = round2decimals($aux_metros);

                                    $aux_precio = $aux_metros * $precio_component;
                                    $aux_precio = round2decimals($aux_precio);

                                    ?>
                                    <input type="hidden" class="metros_unidades_medidas"
                                           value="<?= $aux_metros ?>">

                                    <input type="hidden" class="precio_unidades_medidas"
                                           value="<?= $aux_precio ?>">
                                    <input type="hidden" class="unidades_unidades_medidas"
                                           value="<?= $unidades_medidas['unidades'] ?>">
                                </div>

                            <?php endif; ?>

                        <?php endif; ?>

                    <?php endforeach;
                    ?>                                    </div>
                <?php

            endif; ?>

        </div>
    <?php endforeach; ?>

    <div class="segunda_linea_producto row col-md-12 col-xs-12 text-right">
        <span data-tooltip="tooltip" data-toggle="modal" data-target="#modal_add_product" data-placement="top"
              title="Añadir nuevo producto"
              class="fa fa-2x fa-plus hidden btn_add_product text-success btn-grey btn-option-line"></span>
    </div>

<?php endif; ?>