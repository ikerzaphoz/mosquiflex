<div class="form-horizontal col-md-12 col-xs-12">
    <div class="col-md-12 col-xs-12">
        <span class="title_line_product">Marcos</span>
    </div>
    <div class="col-md-3 col-xs-3">
        <input type="number" class="form-control cantidad_marco_corredera input_unit_product" placeholder="Cantidad"><br>
    </div>
    <div class="col-md-3 col-xs-3">
        <span class="content_medidas">
            <input type="hidden" class="min" value="1.200">
            <input type="hidden" class="max" value="2.400">
            <input type='text' class="form-control ancho_marco_corredera input_width_product" placeholder="Medidas ancho"><br>
        </span>
    </div>
    <div class="col-md-3 col-xs-3">
        <span class="content_medidas">
            <input type="hidden" class="min" value="1.000">
            <input type="hidden" class="max" value="2.200"> 
            <input type='text' class="form-control alto_marco_corredera input_height_product" placeholder="Medidas alto"><br>
        </span>
    </div>
    <div class="col-md-3 col-xs-3">
        <input span_value="" readonly class="form-control product_price price_marco"/>
    </div>

    <input type="hidden" class="subfamily_product" value=""/>

</div>
<div class="form-horizontal col-md-12 col-xs-12">
    <div class="col-md-12 col-xs-12">
        <span class="title_line_product">Hojas</span>
    </div>    
    <div class="col-md-3 col-xs-3">
        <input type="number" class="form-control cantidad_hoja_corredera input_unit_product" placeholder="Cantidad"><br>
    </div>
    <div class="col-md-3 col-xs-3">
        <span class="content_medidas">
            <input type="hidden" class="min" value="0.600">
            <input type="hidden" class="max" value="1.200">
            <input type='text' class="form-control ancho_hoja_corredera input_width_product" placeholder="Medidas ancho"><br>
        </span>
    </div>
    <div class="col-md-3 col-xs-3">
        <span class="content_medidas">
            <input type="hidden" class="min" value="1.000">
            <input type="hidden" class="max" value="2.200">
            <input type='text' class="form-control alto_hoja_corredera input_height_product" placeholder="Medidas alto"><br>
        </span>
    </div>
    <div class="col-md-3 col-xs-3">
        <input span_value="" readonly class="form-control product_price price_hojas"/>
    </div>
    <input type="hidden" class="subfamily_product" value="1"/>
    <div class="col-xs-12 col-md-12 text-right">
        <span class="mt-10 product_price">0.00</span>€
    </div>
</div>

<div class="col-md-2 col-xs-2 content_color_pedido">
    <?php
    if ($group_color == 3 OR $group_color == 4):
        $url_imagen = $id_product . $group_color . $color_product . ".jpg";
        ?>
        <div class="color_product_line">
            <img class="img_color_product" src="<?= path_image_colors . $url_imagen ?>">
        </div>
    <?php else: ?>
        <div color_ral="<?= hideRal($array_color['codesc']) ?>"
             style="background-color: <?= $array_color['html'] ?>" class="color_product_line">
            <img class="img_color_product hidden" src="">
        </div>
    <?php endif;
    ?>
    <span class="title_color"><?= $array_color['codesc'] ?></span>
</div>

<div class="hidden info_product_line">
    <span class="hidden input_id_product" span_value="<?= $id_product ?>" span_name="id_product"></span>
    <span class="hidden input_color_product" span_value="<?= $color_product ?>"
          span_name="color_product"></span>
    <span class="hidden input_group_color" span_value="<?= $group_color ?>" span_name="group_color"></span>
</div>
<div class="segunda_linea_producto row col-md-12 col-xs-12 text-right">
    <span data-tooltip="tooltip" data-placement="top" title="<?= lang_text_copiar_linea_pedido ?>"
          class="fa fa-2x fa-copy btn_copy_line text-warning btn-grey btn-option-line"></span>
    <span data-tooltip="tooltip" data-placement="top" title="<?= lang_text_eliminar_linea_pedido ?>"
          class="fa fa-2x fa-close btn_delete_line text-danger btn-grey btn-option-line"></span>
    <span data-tooltip="tooltip" data-toggle="modal" data-target="#modal_add_product" data-placement="top"
          title="<?= lang_text_añadir_producto ?>"
          class="fa fa-2x fa-plus hidden btn_add_product text-success btn-grey btn-option-line"></span>
</div>

<input type="hidden" class="group_color_corredera" value="<?= $group_color ?>">
<input type="hidden" class="color_product_corredera" value="<?= $color_product ?>">