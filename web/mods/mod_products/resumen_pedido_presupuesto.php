<?php

if (!isset($_SESSION)) {
    session_start();
}

$classHidden_lacado = "hidden";
$total_lacado = 0;
$total_producto = 0;
$type_button = "";
if (isset($_POST['type_button'])): $type_button = $_POST['type_button']; endif;
$id_product = "";
if (isset($_POST['id_product'])): $id_product = $_POST['id_product']; endif;
$color_product = "";
if (isset($_POST['color_product'])): $color_product = $_POST['color_product']; endif;
$group_color = "";
if (isset($_POST['group_color'])): $group_color = $_POST['group_color']; endif;
$subfamily_product = "";
if (isset($_POST['subfamily_product'])): $subfamily_product = $_POST['subfamily_product']; endif;
$tipologia = "";
if (isset($_POST['input_tipologia'])): $tipologia = $_POST['input_tipologia']; endif;

$is_form_expo = 0;
if(isset($_POST['form_expo']) AND $_POST['form_expo'] == 1):
    include 'detalle_pedido_expositor.php';

elseif (!empty($_POST['change_components'])  AND ($_POST['change_components'] == 1 OR $_POST['change_components'] == 3)):

    include 'detalle_pedido_componente.php' ?>

    <?php
else:

    $is_lacado = "";
    $is_lacado = $familia->getIsLacado($color_product, $id_product);
    
//    if ($id_product == 2732):
//        $subfamily_product = "2";
//    endif;
    if ($is_lacado == "1"):
        $classHidden_lacado = "";
        set_array_ral($color_product, 1);
        $group_color = 2;
        if($id_product == "2734" || $id_product == "2735"):
            $group_color = 1;
        endif;
    endif;

    $code_sub = $subfamily_product . $group_color;
    $familia = new Familia();

    if (isset($id_product) AND $code_sub)
        $ancho_minimo = $familia->getAnchoMin($id_product, $code_sub);
    if (isset($id_product) AND $code_sub)
        $ancho_maximo = $familia->getAnchoMax($id_product, $code_sub);
    if (isset($id_product) AND $code_sub)
        $alto_minimo = $familia->getAltoMin($id_product, $code_sub);
    if (isset($id_product) AND $code_sub)
        $alto_maximo = $familia->getAltoMax($id_product, $code_sub);

    $array_familia = $familia->getFamilia('fafami');
    if ($is_lacado == 1):
        $array_color = $familia->getColorsRalLineaPedido($color_product, $id_product);
    else:
        if (isset($id_product) AND $color_product)
            $array_color = $familia->getColoressByCocolo($id_product, $color_product)[0];
    endif;

    $customer = new Customers();
    $dire = $customer->get_direccion_envio($_SESSION['clproc'], $_SESSION['clcodi'])[0];
    $pais = $customer->getPais($dire['clpais']);
    $provincia = $customer->getProvincia($dire['clpais'], $dire['clprov']);
    $poblacion = $customer->getPoblacion($dire['clpais'], $dire['clprov'], $dire['clpobl']);

    ?>

    <div id="content_product" class="content_div_linea row">
        <div class="panel-group">
            <div class="panel panel-default form-inline col-md-12 pb-3">
                <div class="panel-heading selected">
                    <h4 class="panel-title">
                        <div class="title_panel_title"><?=lang_text_detalle?></div>
                    </h4>
                </div>
                <div class="panel-collapse text-left">
                    <div class="row">
                        <label><?=lang_title_referencia?>:</label><input class="referencia" name="referencia" type="text"
                                                         placeholder="<?=lang_title_referencia?>...">
                    </div>
                </div>
                <div class="panel-content-lineas-pedido panel-collapse text-center">
                    <div class="content_lineas_pedido row">
                        <?php

                        if (isset($_SESSION['products']) AND !empty($_SESSION['products'])):
                            include root . 'web/mods/mod_cart/product_cart.php';
                        endif;

                        ?>
                        <?php include 'linea_pedido_presupuesto.php' ?>
                    </div>
                </div>

                <?php if (!isset($exist_lacado)): ?>

                    <div class="panel-content-lineas-pedido panel-collapse text-center">
                    <div class="content-lacados row">
                    <div class="line_lacados <?= $classHidden_lacado ?> effect7 col-md-12 col-xs-12">
                        <div class="row col-md-12 col-xs-12">
                            <h4 class="title_product text-left">Incrementos lacados&nbsp;&nbsp;&nbsp;<span
                                    data-tooltip="tooltip" data-placement="top" title="Ver más información lacados"
                                    class="title_info_product title-warning fa fa-info-circle image-popup-no-margins icono-lupa fa fa-search"
                                    href="<?= path_image ?>images_product/<?= $id_product ?>/lacados.jpg"><img
                                        class="hidden" src="<?= path_image ?>images_product/<?= $id_pro ?>/lacados.jpg"></span>
                            </h4>

                            <div class="row col-md-12 col-xs-12 text-center">
                                <div class="col-md-3 col-xs-3 title_line_product"></div>
                                <div class="col-md-3 col-xs-3 title_line_product">Color</div>
                                <div class="col-md-3 col-xs-3 title_line_product">Unidades</div>
                                <div class="col-md-3 col-xs-3 title_line_product">Precio (€)</div>
                            </div>

                            <div class="content_lacado_ral row col-md-12 col-xs-12 text-center">
                                <?php

                                if ($is_lacado == 1):

                                    foreach ($_SESSION['array_colores_ral'] as $color_session => $item):?>
                                        <div class="col-md-12 col-xs-12 line_lacado_ral">
                                            <div class="col-md-3 col-xs-3 title_line_product">
                                                <div class="preview_color_lacado"
                                                     style="background-color: <?= $array_color['html'] ?>;"></div>
                                            </div>
                                            <div class="col-md-3 col-xs-3 color_lacado"><?= $color_session ?></div>
                                            <div class="col-md-3 col-xs-3 unit_lacado"><?= $item['unidades'] ?></div>
                                            <div class="col-md-3 col-xs-3 product_price_lacado">75.00</div>
                                        </div>
                                    <?php endforeach;
                                    $total_presupuesto = $total_presupuesto + 75.00;
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

                <?php endif; ?>

                <div class="col-md-12 col-xs-12 hidden table_product_0">
                    <div class="col-md-12 col-xs-12 text-center"><i class="fa fa-exclamation-triangle"></i><span>&nbsp;<?=lang_text_no_productos?>.</span>
                    </div>
                </div>
                <div class="segunda_linea_producto mt-10 text-right row col-md-12 col-xs-12">
                    <button data-toggle="modal" data-target="#modal_add_product" title="Añadir nuevo producto"
                            class="btn btn_add_product btn-sm bg-border-warning"><?=lang_text_añadir_producto?>
                    </button>
                </div>
                <div class="segunda_linea_producto row total_line col-md-12 col-xs-12 text-right">
                    <div class="col-md-10 col-xs-10"><b>Total <?= $type_button ?> PVP (€):</b></div>
                    <div><input readonly type="text" class="col-md-2 col-xs-2 price_total"
                                value="<?= round2decimals($total_presupuesto) ?>"/></div>
                    <div class="hidden col-md-10 col-xs-10"><b>Total <?= $type_button ?> condiciones cliente (€):</b>
                    </div>
                    <div class="col-md-10 col-xs-10">(<?=lang_text_impuestos_no_incluidos?>)</div>
                    <div class="hidden"><input readonly type="text" class="col-md-2 col-xs-2 price_cond_cliente"
                                               value="PENDIENTE"/></div>
                </div>

                <input type="hidden" value="<?= $_SESSION['clproc'] ?>" class="clproc_cliente">
                <input type="hidden" value="<?= $total_lacado ?>" class="total_lacado">
                <input type="hidden" value="<?= $_SESSION['clcodi'] ?>" class="clcodi_cliente">
                <input type="hidden" value="fiscal" class="id_dire">
                <input type="hidden" value="<?= $type_button ?>" class="is_pedido" name="is_pedido">
                <input type="hidden" value="seleccionar_dire" class="agencia" name="agencia">

                <div class="row col-md-12 col-xs-12 mt-10 segunda_linea_producto">
                    <?php if (isset($_SESSION['ini_log']) == 1): ?>
                        <div class="row col-md-12 col-xs-12 text-center">
                            <?php if ($type_button == "presupuesto"): ?>
                                <button class="btn-sm btn-editar-dire btn bg-border-info"
                                        aria-hidden="true"><?=lang_text_guardar_presupuesto?>
                                </button>
                            <?php else: ?>
                                <button class="btn-sm btn-editar-dire btn bg-border-warning"
                                        aria-hidden="true"><?=lang_text_guardar_pedido?>
                                </button>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>