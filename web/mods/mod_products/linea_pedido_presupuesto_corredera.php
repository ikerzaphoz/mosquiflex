<div class="form-horizontal col-md-12 col-xs-12 corredera_completa producto_corredera">
    <div class="col-md-12 col-xs-12 text-center">
        <span class="title_line_product">Corredera completa (Marco + Hoja)</span>
    </div>
    <div class="col-md-3 col-xs-3">Cantidad</div>
    <div class="col-md-3 col-xs-3">Ancho</div>
    <div class="col-md-3 col-xs-3">Alto</div>
    <div class="col-md-1 col-xs-1">Color</div>
    <div class="col-md-1 col-xs-1">1 hoja</div>
    <div class="col-md-1 col-xs-1">Precio</div>
    <div class="col-md-3 col-xs-3">
        <input type="number" min="0" class="form-control cantidad_corredera_completa no_check input_unidades" placeholder="Unidades"><br>
    </div>
    <div class="col-md-3 col-xs-3">
        <span class="content_medidas">
            <input type="hidden" class="min" value="1.200">
            <input type="hidden" class="max" value="2.400">
            <input type='text' class="form-control ancho_corredera_completa input_ancho" placeholder="Ancho"><br>
        </span>
    </div>
    <div class="col-md-3 col-xs-3">
        <span class="content_medidas">
            <input type="hidden" class="min" value="1.000">
            <input type="hidden" class="max" value="2.200"> 
            <input type='text' class="form-control alto_corredera_completa input_alto" placeholder="Alto"><br>
        </span>
    </div>
    <div class="col-md-1 col-xs-1 content_color_pedido">
        <?php
        if ($group_color == 3 OR $group_color == 4):
            $url_imagen = $id_product . $group_color . $color_product . ".jpg";
            ?>
            <div class="color_product_line">
                <img class="img_color_product" src="<?= path_image_colors . $url_imagen ?>">
            </div>
        <?php else: ?>
            <div color_ral="<?= hideRal($array_color['codesc']) ?>"
                 style="background-color: <?= $array_color['html'] ?>" class="color_product_line">
                <img class="img_color_product hidden" src="">
            </div>
        <?php endif;
        ?>
        <span class="title_color"><?= $array_color['codesc'] ?></span>
    </div>
    <div class="col-md-1 col-xs-1">
        <input type="checkbox" class="completa_una_hoja no_check" value="">
    </div>
    <div class="col-md-1 col-xs-1">
        <span span_value="" readonly class="product_price precio_corredera_completa">0.00</span>€
    </div>

    <span class="hidden input_color_product color_corredera_completa" span_value="<?= $color_product ?>"
          span_name="color_product"></span>
    <span class="hidden input_group_color group_color_corredera_completa" span_value="<?= $group_color ?>" span_name="group_color"></span>
    <input type="hidden" class="arsubf subfamily_product" value="0">

    <div class="hidden info_corredera_completa">
        <input type="hidden" class="precio_hoja">
        <input type="hidden" class="precio_marco">
        <input type="hidden" class="cantidad_hojas">
        <input type="hidden" class="ancho_hojas">
    </div>

    <input type="hidden" class="tipo_producto" value="corredera_completa">

    <span class="hidden input_id_product" span_value="<?= $id_product ?>" span_name="id_product"></span>

</div>

<div class="form-horizontal col-md-12 col-xs-12 producto_corredera">
    <div class="col-md-12 col-xs-12 text-center">
        <span class="title_line_product">Marco</span>
    </div>
    <div class="col-md-3 col-xs-3">Cantidad</div>
    <div class="col-md-3 col-xs-3">Ancho</div>
    <div class="col-md-3 col-xs-3">Alto</div>
    <div class="col-md-1 col-xs-1">Color</div>
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-1 col-xs-1">Precio</div>
    <div class="col-md-3 col-xs-3">
        <input type="number" min='0' class="form-control cantidad_corredera_marco no_check input_unidades" placeholder="Unidades"><br>
    </div>
    <div class="col-md-3 col-xs-3">
        <span class="content_medidas">
            <input type="hidden" class="min" value="1.200">
            <input type="hidden" class="max" value="2.400">
            <input type='text' class="form-control ancho_corredera_marco input_ancho" placeholder="Ancho"><br>
        </span>
    </div>
    <div class="col-md-3 col-xs-3">
        <span class="content_medidas">
            <input type="hidden" class="min" value="1.000">
            <input type="hidden" class="max" value="2.200"> 
            <input type='text' class="form-control alto_corredera_marco input_alto" placeholder="Alto"><br>
        </span>
    </div>
    <div class="col-md-1 col-xs-1 content_color_pedido">
        <?php
        if ($group_color == 3 OR $group_color == 4):
            $url_imagen = $id_product . $group_color . $color_product . ".jpg";
            ?>
            <div class="color_product_line">
                <img class="img_color_product" src="<?= path_image_colors . $url_imagen ?>">
            </div>
        <?php else: ?>
            <div color_ral="<?= hideRal($array_color['codesc']) ?>"
                 style="background-color: <?= $array_color['html'] ?>" class="color_product_line">
                <img class="img_color_product hidden" src="">
            </div>
        <?php endif;
        ?>
        <span class="title_color"><?= $array_color['codesc'] ?></span>
    </div>
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-1 col-xs-1">
        <span span_value="" readonly class="product_price precio_corredera_marco">0.00</span>€
    </div>

    <span class="hidden input_color_product color_corredera_marco" span_value="<?= $color_product ?>"
          span_name="color_product"></span>
    <span class="hidden input_group_color group_color_corredera_marco" span_value="<?= $group_color ?>" span_name="group_color"></span>
    <input type="hidden" class="arsubf subfamily_product" value="0">

    <input type="hidden" class="tipo_producto" value="corredera_marco">

    <span class="hidden input_id_product" span_value="<?= $id_product ?>" span_name="id_product"></span>

</div>

<div class="form-horizontal col-md-12 col-xs-12 producto_corredera">
    <div class="col-md-12 col-xs-12 text-center">
        <span class="title_line_product">Hoja&nbsp;*</span>
    </div>
    <div class="col-md-3 col-xs-3">Cantidad</div>
    <div class="col-md-3 col-xs-3">Ancho</div>
    <div class="col-md-3 col-xs-3">Alto</div>
    <div class="col-md-1 col-xs-1">Color</div>
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-1 col-xs-1">Precio</div>
    <div class="col-md-3 col-xs-3">
        <input type="number" min='0' class="form-control cantidad_corredera_hoja no_check input_unidades" placeholder="Unidades"><br>
    </div>
    <div class="col-md-3 col-xs-3">
        <span class="content_medidas">
            <input type="hidden" class="min" value="0.32">
            <input type="hidden" class="max" value="1.200">
            <input type='text' class="form-control ancho_corredera_hoja input_ancho" placeholder="Ancho"><br>
        </span>
    </div>
    <div class="col-md-3 col-xs-3">
        <span class="content_medidas">
            <input type="hidden" class="min" value="0.32">
            <input type="hidden" class="max" value="2.200"> 
            <input type='text' class="form-control alto_corredera_hoja input_alto" placeholder="Alto"><br>
        </span>
    </div>
    <div class="col-md-1 col-xs-1 content_color_pedido">
        <?php
        if ($group_color == 3 OR $group_color == 4):
            $url_imagen = $id_product . $group_color . $color_product . ".jpg";
            ?>
            <div class="color_product_line">
                <img class="img_color_product" src="<?= path_image_colors . $url_imagen ?>">
            </div>
        <?php else: ?>
            <div color_ral="<?= hideRal($array_color['codesc']) ?>"
                 style="background-color: <?= $array_color['html'] ?>" class="color_product_line">
                <img class="img_color_product hidden" src="">
            </div>
        <?php endif;
        ?>
        <span class="title_color"><?= $array_color['codesc'] ?></span>
    </div>
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-1 col-xs-1">
        <span span_value="" readonly class="product_price precio_corredera_hoja">0.00</span>€
    </div>

    <div class="col-md-12 col-xs-12">
        <span>*&nbsp;<i class="fa fa-warning text-warning"></i>&nbsp;MEDIDA A FABRICAR</span>
    </div>

    <span class="hidden input_color_product color_corredera_hoja" span_value="<?= $color_product ?>"
          span_name="color_product"></span>
    <span class="hidden input_group_color group_color_corredera_hoja" span_value="<?= $group_color ?>" span_name="group_color"></span>
    <input type="hidden" class="arsubf subfamily_product" value="1">

    <input type="hidden" class="tipo_producto" value="corredera_hoja">

    <span class="hidden input_id_product" span_value="<?= $id_product ?>" span_name="id_product"></span>
</div>

<div class="segunda_linea_producto col-md-12 col-xs-12 row">
    <div class="col-md-2 col-xs-2 text-left">
        <button data-toggle="modal" data-target="#modal_option_product"
                class="btn btn-sm btn-grey btn_option_product"><?= lang_text_anadir_incrementos ?>
        </button>
    </div>
    <div class="col-md-10 col-xs-10 increments text-right"></div>
</div>

<div class="segunda_linea_producto row col-md-12 col-xs-12 text-right">
    <span data-tooltip="tooltip" data-placement="top" title="<?= lang_text_copiar_linea_pedido ?>"
          class="fa fa-2x fa-copy btn_copy_line text-warning btn-grey btn-option-line"></span>
    <span data-tooltip="tooltip" data-placement="top" title="<?= lang_text_eliminar_linea_pedido ?>"
          class="fa fa-2x fa-close btn_delete_line text-danger btn-grey btn-option-line"></span>
    <span data-tooltip="tooltip" data-toggle="modal" data-target="#modal_add_product" data-placement="top"
          title="<?= lang_text_añadir_producto ?>"
          class="fa fa-2x fa-plus hidden btn_add_product text-success btn-grey btn-option-line"></span>
</div>