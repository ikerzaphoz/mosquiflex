<?php

switch($id_pro):
    case '2731':
        $id_component = "1791";
        break;
    case '2732':
        $id_component = "1792";
        break;
    case '2733':
        $id_component = "1793";
        break;
    case '2734':
        $id_component = "1794";
        break;
    case '2735':
        $id_component = "1794";
        break;
    case '2736':
        $id_component = "1795";
        break;
endswitch;

$array_components = $familia->getComponentsBySubfamily($id_component, "status_1", "status_3");

if ($change_components == "1"):?>

    <div class="body_components">

        <?php

        $group_color = 0;

        foreach ($array_components as $item):

            $arsubf = str_pad($item['arsubf'], 2, 0, STR_PAD_LEFT);
            $ararti = str_pad($item['ararti'], 6, 0, STR_PAD_LEFT);

            $embalaje = $familia->getEmbalaje($id_component, $arsubf, $ararti, $item['arprpr']);

            $max_measure = $familia->getMaxMeasure($id_component)[0]['max_measure'];

            $tipo_medida = $familia->get_tipoMedida_tarifa($id_component, $arsubf, $ararti);

            $precio_fabrica = round2decimals($item['arpvta']);

            $referencia = $id_component."-".$arsubf."-".$ararti;

            $referencia_img = $id_component.$arsubf.$ararti;

            if ($id_pro == "2735"):
                $imagen = path_image_components . "/2734/" . $referencia_img . ".jpg";
            else:
                $imagen = path_image_components . "/" . $id_pro . "/" . $referencia_img . ".jpg";
            endif;

            //Agrupamos por color / subfamilia dependiendo del componente
            if(isset($id_pro)):
                include root.'web/mods/mod_products/code_product/components_color_'.$id_pro.'.php';
            endif;

        endforeach; ?>

    </div>

    <input type="hidden" name="id_component_product[]" class="id_component_product" value="<?= $id_component ?>">

    <?php
else:

    $is_industrial = "1";

    ?>

    <div class="body_components">

        <?php

        $group_color = 0;

        foreach ($array_components as $item):

            $arsubf = str_pad($item['arsubf'], 2, 0, STR_PAD_LEFT);
            $ararti = str_pad($item['ararti'], 6, 0, STR_PAD_LEFT);

            $embalaje = $familia->getEmbalaje($id_component, $arsubf, $ararti, $item['arprpr']);

            $precio_almacen = round2decimals($item['arpval']);

            $referencia = $id_component . "-" . $arsubf . "-" . $ararti;

            $referencia_img = $id_component.$arsubf.$ararti;

            if ($id_pro == "2735"):
                $imagen = path_image_components . "/2734/" . $referencia_img . ".jpg";
            else:
                $imagen = path_image_components . "/" . $id_pro . "/" . $referencia_img . ".jpg";
            endif;

            //Agrupamos por color / subfamilia dependiendo del componente
            if(isset($id_pro)):
                include root.'web/mods/mod_products/code_product/components_color_industrial_'.$id_pro.'.php';
            endif;

        endforeach; ?>

    </div>

    <input type="hidden" name="id_component_product[]" class="id_component_product" value="<?= $id_component ?>">

    <?php

endif;
exit;
