<?php

//COMPONENTES
if ($change_components == "1"):
    if ($type_search == "codigo"):
        $array_ref = explode('-', $text);
        $id_component = ltrim($array_ref['0'], '0');
        $arsubf = ltrim($array_ref['1'], '0');
        $ararti = ltrim($array_ref['2'], '0');
        $array_components = $familia->searchComponentByCode($id_component, $arsubf, $ararti);
    else:

        switch($id_pro):
            case '2731':
                $id_component = "1791";
                break;
            case '2732':
                $id_component = "1792";
                break;
            case '2733':
                $id_component = "1793";
                break;
            case '2734':
                $id_component = "1794";
                break;
            case '2735':
                $id_component = "1794";
                break;
            case '2736':
                $id_component = "1795";
                break;
        endswitch;

        $array_components = $familia->searchComponent($id_component, $text, $type_search); ?>
    <?php endif;

    ?>

    <?php

    if (!empty($array_components)): ?>

        <div class="body_components">

            <?php

            $group_color = 0;

            foreach ($array_components as $item):

                $arsubf = str_pad($item['arsubf'], 2, 0, STR_PAD_LEFT);
                $ararti = str_pad($item['ararti'], 6, 0, STR_PAD_LEFT);

                $embalaje = $familia->getEmbalaje($id_component, $arsubf, $ararti, $item['arprpr']);

                $max_measure = $familia->getMaxMeasure($id_component)[0]['max_measure'];

                $tipo_medida = $familia->get_tipoMedida_tarifa($id_component, $arsubf, $ararti);

                $precio_fabrica = round2decimals($item['arpvta']);

                $referencia = $id_component."-".$arsubf."-".$ararti;

                $referencia_img = $id_component.$arsubf.$ararti;

                if ($id_pro == "2735"):
                    $imagen = path_image_components . "/2734/" . $referencia_img . ".jpg";
                else:
                    $imagen = path_image_components . "/" . $id_pro . "/" . $referencia_img . ".jpg";
                endif;

                //Agrupamos por color / subfamilia dependiendo del componente
                if(isset($id_pro)):
                    include root.'web/mods/mod_products/code_product/components_color_'.$id_pro.'.php';
                endif;

            endforeach; ?>

        </div>

    <?php else:

        echo "<div class='text-center'>No se encuentran resultados con esos parámetros de búsqueda</div>";

        include root . 'web/mods/mod_search/empty_search.php';

    endif; ?>

    <?php

else:
    $is_industrial = 1;
    if ($type_search == "codigo"):
        $array_ref = explode('-', $text);
        $id_component = ltrim($array_ref['0'], '0');
        $arsubf = ltrim($array_ref['1'], '0');
        $ararti = ltrim($array_ref['2'], '0');
        $array_components = $familia->searchComponentByCode($id_component, $arsubf, $ararti);
    else:

        switch($id_pro):
            case '2731':
                $id_component = "1791";
                break;
            case '2732':
                $id_component = "1792";
                break;
            case '2733':
                $id_component = "1793";
                break;
            case '2734':
                $id_component = "1794";
                break;
            case '2735':
                $id_component = "1794";
                break;
            case '2736':
                $id_component = "1795";
                break;
        endswitch;

        $array_components = $familia->searchComponent($id_component, $text, $type_search);
    endif;

    ?>

    <?php if (!empty($array_components)): ?>

    <div class="body_components">

        <?php

        $group_color = 0;

        foreach ($array_components as $item):

            $arsubf = str_pad($item['arsubf'], 2, 0, STR_PAD_LEFT);
            $ararti = str_pad($item['ararti'], 6, 0, STR_PAD_LEFT);

            $embalaje = $familia->getEmbalaje($id_component, $arsubf, $ararti, $item['arprpr']);

            $precio_almacen = round2decimals($item['arpval']);

            $referencia = $id_component . "-" . $arsubf . "-" . $ararti;

            $referencia_img = $id_component.$arsubf.$ararti;

            if ($id_pro == "2735"):
                $imagen = path_image_components . "/2734/" . $referencia_img . ".jpg";
            else:
                $imagen = path_image_components . "/" . $id_pro . "/" . $referencia_img . ".jpg";
            endif;

            //Agrupamos por color / subfamilia dependiendo del componente
            if(isset($id_pro)):
                include root.'web/mods/mod_products/code_product/components_color_industrial_'.$id_pro.'.php';
            endif;

        endforeach; ?>

    </div>

<?php else:

    echo "<div class='text-center'>No se encuentran resultados con esos parámetros de búsqueda</div>";

    include root . 'web/mods/mod_search/empty_search.php';

endif; ?>

    <?php
endif;
?>

<input type="hidden" name="id_component_product[]" class="id_component_product" value="<?= $id_component ?>">
