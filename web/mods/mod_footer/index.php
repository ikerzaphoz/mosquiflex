<?php

$is_login = 0;
if(isset($_SESSION['ini_log']) == 1):
    $is_login = "1";
endif; ?>

<input type="hidden" value="<?=$is_login?>" class="is_login" name="is_login"/>
<div class="footer mt-3 col-md-12 col-xs-12 text-center">
    <div class="col-sm-4 col-xs-4">
        <ul class="list-unstyled">
            <li class="option_footer"><a href="<?=path_web?>condiciones_legales.php?page=aviso_legal"><?=lang_footer_aviso_legal?></a></li>
        </ul>
    </div>
    <div class="col-sm-4 col-xs-4">
        <ul class="list-unstyled">
            <li class="option_footer"><a href="<?=path_web?>condiciones_legales.php?page=proteccion"><?=lang_footer_proteccion?></a></li>
        </ul>
    </div>
    <div class="col-sm-4 col-xs-4">
        <ul class="list-unstyled">
            <li class="option_footer"><a href="<?=path_web?>condiciones_legales.php?page=venta"><?=lang_footer_condiciones?></a></li>
        </ul>
    </div>
</div>
</div>
<script type="application/javascript" src="<?=path_js?>bootstrap.min.js"></script>
<script type="application/javascript" src="<?=path_js?>toastr.min.js"></script>
<script type="application/javascript" src="<?=path_js?>lang_<?=$_SESSION['lang']?>.js"></script>

<?php

if(isset($_SESSION['is_cliente_final']) == 1): ?>
<script type="application/javascript" src="<?=path_js?>paypal_script.js?v=<?= microtime()?>"></script>
<?php endif;

?>

<script type="application/javascript" src="<?=path_js?>script.js?v=<?=microtime();?>"></script>
<script type="application/javascript" src="<?=path_js?>bootstrap-sortable.js"></script>
<script type="application/javascript" src="<?=path_js?>bootstrap-confirmation.js"></script>
<script type="application/javascript" src="<?=path_js?>summernote.min.js"></script>
<script type="application/javascript" src="<?=path_js?>magnific-popup.min.js"></script>
<script type="application/javascript" src="<?=path_js?>number-polyfill.min.js"></script>
<script type="application/javascript" src="<?=path_js?>float-pattern.js"></script>

<?php

    if($page == "index" AND $error == "ini_log"){ ?>
        <script>setTimeout(function(){$('.btn_modal_ini').click()}, 200);</script>
    <?php }

?>
</body>