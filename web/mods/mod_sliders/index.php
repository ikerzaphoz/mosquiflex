<div id="sliders">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">

        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="<?=path_image?>sliders/slider1.jpg" alt="Img1">
            </div>

            <div class="item">
                <img src="<?=path_image?>sliders/slider2.jpg" alt="Img2">
            </div>

            <div class="item">
                <img src="<?=path_image?>sliders/slider3.jpg" alt="Img3">
            </div>

        </div>

    </div>
</div>