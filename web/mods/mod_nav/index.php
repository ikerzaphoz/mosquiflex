<?php
if (!isset($_SESSION)):
    session_start();
endif;
$familia = new Familia();
$array_familia = $familia->getFamilia();

$class_hidden_cart = "hidden";
if (!empty($_SESSION['products'])):
    $class_hidden_cart = "";
endif;
?>

<div class="container">
    <header id="header">
        <div class="row col-md-12 col-xs-12 text-right">
            <div class="div_login col-md-12 col-xs-12">

                <a href="<?= path_web ?>productos.php"><i
                        class="<?= $class_hidden_cart ?> icon-carrito fa fa-shopping-cart" aria-hidden="true"></i></a>

<?php if (empty($_SESSION['ini_log'])): ?>
                    <a class="btn_modal_ini" href="#"><?= lang_nav_iniciar_sesion ?></a>
                <?php else: ?>
                    <div class="dropdown">
                    <?= $_SESSION['name_log']; ?>
                        <div class="dropdown-menu dropdown-content">
                            <a href="<?= path_web ?>clientes.php?opt=ficha"><?= lang_nav_mi_cuenta ?></a>
                            <a href="<?= path_web_mods ?>mod_customers/action/log_out.php"><?= lang_nav_cerrar_sesion ?></a>
                        </div>
                    </div>
<?php endif;
?>

                <div class="div_lang dropdown">

                <?php if (empty($_GET)): ?>
                        <a href="http://<?= $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] ?>?lang=es">
                            <div class="lazy icon-flag flag-es"></div>
                        </a>
                        <a href="http://<?= $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] ?>?lang=pt">
                            <div class="lazy icon-flag flag-pt"></div>
                        </a>
<?php else: ?>

    <?php
    $url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $array_get_lang = array("lang");
    ?>

                        <a href=<?= remove_url_query_args($url, $array_get_lang) ?>&lang=es>
                            <div class="lazy icon-flag flag-es"></div>
                        </a>
                        <a href=<?= remove_url_query_args($url, $array_get_lang) ?>&lang=pt>
                            <div class="lazy icon-flag flag-pt"></div>
                        </a>
<?php endif; ?>
                </div>
            </div>
        </div>
        <div class="row col-md-12 col-xs-12">
            <div class="col-md-6 col-xs-6 text-left">
                <a href="index.php"><img id="img_header" src="<?= path_image ?>logo_mosquiflex.png"></a>
            </div>
            <div class="col-md-6 col-xs-6 text-right">
                <button class="btn button-modal-help" aria-hidden="true"><i class="fa fa-question-circle"></i>&nbsp;MANUAL AYUDA</button>
            </div>
            <div class="hidden col-xs-6 col-md-6 text-right">
<?php if ($page != "clientes"): ?>
                    <div id="div_search" class="hidden">
                        <form class="search" name="search">
                            <div class="form-group has-feedback">
                                <input type="search" name="input_search" class="input_search form-control"
                                       placeholder="<?= lang_search_text ?>"/>
                                <i class="glyphicon glyphicon-search form-control-feedback"></i>
                            </div>
                        </form>
                    </div>
<?php endif; ?>
            </div>
        </div>
        <nav id="nav_menu">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse text-center" id="myNavbar">

                <ul class="nav navbar-nav nav-menu-button">

                    <div class="dropdown">
                        <a href="<?= path_web ?>index.php">
                            <button
                                class="btn_menu dropbtn <?php if ($page == 'empresa' OR $page == "index"): echo ' active';
endif; ?>"><?= lang_menu_empresa ?></button>
                        </a>
                        <div class="dropdown-menu dropdown-content">
                            <a class="" href="<?= path_web ?>empresa.php#content_somos"><?= lang_menu_contacto ?></a>
                        </div>
                    </div>
                    <div class="dropdown">
                        <a href="<?= path_web ?>productos.php?all=all">
                            <button
                                class="btn_menu dropbtn <?php if ($page == 'productos'): echo ' active';
endif; ?>"><?= lang_menu_productos ?></button>
                        </a>
                        <div class="dropdown-menu dropdown-content">
<?php foreach ($array_familia as $item): ?>
                                <a href="<?= path_web ?>productos.php?id_pro=<?= $item['fafami'] ?>"><?= $item['fadesc'] ?></a>
<?php endforeach; ?>
                            <a href="<?= path_web ?>productos.php?exp=exp"><?= lang_menu_expositores ?></button></a>
                        </div>
                    </div>
                    <div class="dropdown">
                        <button path="<?= path_web ?>clientes.php?opt=ficha"
                                class="btn_modal_ini_cli btn_menu dropbtn <?php if ($page == 'clientes'): echo ' active';
                            endif; ?>"><?= lang_menu_clientes ?></button>
                        <div class="dropdown-menu dropdown-content">
                            <a path="<?= path_web ?>clientes.php?opt=ficha" class="btn_modal_ini_cli_ref"
                               href="<?= path_web ?>clientes.php?opt=ficha"><?= lang_menu_ficha_cliente ?></a>
                            <a path="<?= path_web ?>productos.php?all=all" class="btn_modal_ini_cli_ref"
                               href="<?= path_web ?>productos.php?all=all"><?= lang_menu_presupuestos_pedidos ?></a>
                            <a path="<?= path_web ?>clientes.php?opt=historial" class="btn_modal_ini_cli_ref"
                               href="<?= path_web ?>clientes.php?opt=historial"><?= lang_menu_historial ?></a>
                            <a path="<?= path_web ?>clientes.php?opt=descargas" class="hidden btn_modal_ini_cli_ref"
                               href="<?= path_web ?>clientes.php?opt=descargas"><?= lang_menu_descargas ?></a>
                            <a path="<?= path_web ?>clientes.php?opt=tarifa" class="btn_modal_ini_cli_ref"
                               href="<?= path_web ?>clientes.php?opt=tarifa"><?= lang_menu_tarifa ?></a>
                        </div>
                    </div>

                </ul>
            </div>
        </nav>
    </header>