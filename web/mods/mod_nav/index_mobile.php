<?php

if (!isset($_SESSION)):
    session_start();
endif;
$familia = new Familia();
$array_familia = $familia->getFamilia();

$class_hidden_cart = "hidden";
if (!empty($_SESSION['products'])):
    $class_hidden_cart = "";
endif;

?>

<div class="container">
    <header id="header">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-8 text-left">
                    <a href="index.php"><img id="img_header" src="<?= path_image ?>logo_mosquiflex.png"></a>
                </div>
                <div class="div_lang dropdown col-xs-4 text-right mt-10">

                    <?php if (empty($_GET)): ?>
                        <a href="http://<?= $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] ?>?lang=es">
                            <div class="lazy icon-flag flag-es"></div>
                        </a>
                        <a href="http://<?= $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] ?>?lang=pt">
                            <div class="lazy icon-flag flag-pt"></div>
                        </a>
                    <?php else: ?>

                        <?php

                        $url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                        $array_get_lang = array("lang");

                        ?>

                        <a href=<?= remove_url_query_args($url, $array_get_lang) ?>&lang=es>
                            <div class="lazy icon-flag flag-es"></div>
                        </a>
                        <a href=<?= remove_url_query_args($url, $array_get_lang) ?>&lang=pt>
                            <div class="lazy icon-flag flag-pt"></div>
                        </a>
                    <?php endif; ?>

                </div>
            </div>
            <div class="col-md-12 col-xs-12 mt-10">
                <div class="div_login text-nowrap col-md-6 col-xs-6 text-left mt-3">

                    <a href="<?= path_web ?>productos.php"><i
                            class="<?= $class_hidden_cart ?> icon-carrito fa fa-shopping-cart"
                            aria-hidden="true"></i></a>

                    <?php

                    if (empty($_SESSION['ini_log'])): ?>
                        <a class="btn_modal_ini" href="#"><?= lang_nav_iniciar_sesion ?></a>
                    <?php else: ?>
                        <div class="dropdown">
                            <?= $_SESSION['name_log']; ?>
                            <div class="dropdown-menu dropdown-content">
                                <a href="<?= path_web ?>clientes.php?opt=ficha"><?= lang_nav_mi_cuenta ?></a>
                                <a href="<?= path_web_mods ?>mod_customers/action/log_out.php"><?= lang_nav_cerrar_sesion ?></a>
                            </div>
                        </div>
                    <?php endif;

                    ?>

                </div>
                <div class="col-xs-6 col-md-6 text-left">
                    <?php if ($page != "clientes"): ?>
                        <div id="div_search" class="hidden">
                            <form class="search" name="search">
                                <div class="form-group has-feedback">
                                    <input type="search" name="input_search" class="input_search form-control"
                                           placeholder="<?= lang_search_text ?>"/>
                                    <i class="glyphicon glyphicon-search form-control-feedback"></i>
                                </div>
                            </form>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-12 col-xs-12 text-right">
                <nav id="nav_menu">

                    <ul class="nav navbar-nav nav-menu-button inline-flex fs-12">

                        <div class="dropdown">
                            <button
                                class="btn_menu dropbtn <?php if ($page == 'empresa' OR $page == "index"): echo ' active'; endif; ?>"><?= lang_menu_empresa ?></button>
                            <div class="dropdown-menu dropdown-content">
                                <a class="" href="<?= path_web ?>index.php"><?= lang_menu_inico ?></a>
                                <a class=""
                                   href="<?= path_web ?>empresa.php#content_somos"><?= lang_menu_contacto ?></a>

                            </div>
                        </div>
                        <div class="dropdown">
                            <button
                                class="btn_menu dropbtn <?php if ($page == 'productos'): echo ' active'; endif; ?>"><?= lang_menu_productos ?></button>
                            <div class="dropdown-menu dropdown-content">
                                <a class="" href="<?= path_web ?>productos.php?all=all"><?= lang_menu_todos_productos ?></a>
                                <?php foreach ($array_familia as $item): ?>
                                    <a href="<?= path_web ?>productos.php?id_pro=<?= $item['fafami'] ?>"><?= $item['fadesc'] ?></a>
                                <?php endforeach; ?>
                                <a href="<?= path_web ?>productos.php?exp=exp"><?= lang_menu_expositores ?></button></a>
                            </div>
                        </div>
                        <div class="dropdown pull-right">
                            <button path="<?= path_web ?>clientes.php?opt=ficha"
                                    class="btn_modal_ini_cli btn_menu dropbtn <?php if ($page == 'clientes'): echo ' active'; endif; ?>"><?= lang_menu_clientes ?></button>
                            <div class="dropdown-menu dropdown-content menu_area_clientes">
                                <a path="<?= path_web ?>clientes.php?opt=ficha" class="btn_modal_ini_cli_ref"
                                   href="<?= path_web ?>clientes.php?opt=ficha"><?= lang_menu_ficha_cliente ?></a>
                                <a path="<?= path_web ?>productos.php?all=all" class="btn_modal_ini_cli_ref"
                                   href="<?= path_web ?>productos.php?all=all"><?= lang_menu_presupuestos_pedidos ?></a>
                                <a path="<?= path_web ?>clientes.php?opt=historial" class="btn_modal_ini_cli_ref"
                                   href="<?= path_web ?>clientes.php?opt=historial"><?= lang_menu_historial ?></a>
                                <a path="<?= path_web ?>clientes.php?opt=descargas" class="btn_modal_ini_cli_ref"
                                   href="<?= path_web ?>clientes.php?opt=descargas"><?= lang_menu_descargas ?></a>
                            </div>
                        </div>

                    </ul>
                </nav>
            </div>
        </div>
    </header>