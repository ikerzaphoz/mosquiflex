<?php
$familia_ses = new Familia();
$array_familia_ses = $familia_ses->getFamilia('fafami');
$count_lacado = 0;
$count_expositor = 0;
$count_component = 0;
$count_corredera = 0;

if (isset($_SESSION['products']['product'])):

    foreach ($_SESSION['products']['product'] as $index => $product_cart):

        $id_product_ses = "";
        if (isset($product_cart['id_product']))
            $id_product_ses = $product_cart['id_product'];
        $type_product_ses = "";
        if (isset($product_cart['type_product']))
            $type_product_ses = $product_cart['type_product'];

        if ($type_product_ses == "product"):

            include root . 'web/mods/mod_cart/product_cart_product.php';

        endif;

    endforeach;

endif;

if (isset($_SESSION['products']['component'])):

    $count_component++;

    if ($count_component == 1):
        ?>
        <div class="line_product effect7 col-md-12 col-xs-12 text-left line_product_component_aux">
            <div class="row col-md-12 col-xs-12">
                <h4 class="title_product">Componentes</h4>
            </div>
            <div class="row col-md-12 col-xs-12">
                <div class="col-md-2 col-xs-2 title_line_product">Unidades</div>
                <div class="col-md-2 col-xs-2 title_line_product">Color</div>
                <div class="col-md-2 col-xs-2 title_line_product">Referencia</div>
                <div class="col-md-3 col-xs-3 title_line_product">Descripción</div>
                <div class="col-md-2 col-xs-2 title_line_product">Precio (€)</div>
                <div class="col-md-1 col-xs-1 title_line_product"></div>
            </div>
        <?php
        endif;

        foreach ($_SESSION['products']['component'] as $index => $product_cart):

            $id_product_ses = "";
            if (isset($product_cart['id_product']))
                $id_product_ses = $product_cart['id_product'];
            $type_product_ses = "";
            if (isset($product_cart['type_product']))
                $type_product_ses = $product_cart['type_product'];

            if ($type_product_ses == "component"):

                include root . 'web/mods/mod_cart/product_cart_component.php';

            endif;

        endforeach;

        if ($count_component == 1):
            ?>

        </div>

    <?php
    endif;

endif;


if (isset($_SESSION['products']['lacado'])):

    foreach ($_SESSION['products']['lacado'] as $index => $product_cart):

        $id_product_ses = "";
        if (isset($product_cart['id_product']))
            $id_product_ses = $product_cart['id_product'];
        $type_product_ses = "";
        if (isset($product_cart['type_product']))
            $type_product_ses = $product_cart['type_product'];

        $is_lacado = "";
        if (isset($product_cart['is_lacado']))
            $is_lacado = $product_cart['is_lacado'];

        if (!empty($is_lacado)):

            $count_lacado++;

            include root . 'web/mods/mod_cart/product_cart_lacado.php';

        endif;

    endforeach;

endif;

if (isset($_SESSION['products']['expositor'])):

    $count_expositor++;

    if ($count_expositor == 1):
        ?>

        <div class="effect7 col-md-12 col-xs-12 text-left line_product_expositor">
            <div class="row col-md-12 col-xs-12">
                <h4 class="title_product"><?= lang_title_expositores_punto_venta ?></h4>
            </div>
            <div class="row col-md-12 col-xs-12 text-center">
                <div class="col-md-4 col-xs-4 title_line_product">Producto</div>
                <div class="col-md-4 col-xs-4 title_line_product">Unidades</div>
                <div class="col-md-3 col-xs-3 title_line_product">Precio (€)</div>
                <div class="col-md-1 col-xs-1 title_line_product"></div>
            </div>

        <?php
        endif;

        foreach ($_SESSION['products']['expositor'] as $index => $product_cart):

            $id_product_ses = "";
            if (isset($product_cart['id_product']))
                $id_product_ses = $product_cart['id_product'];
            $type_product_ses = "";
            if (isset($product_cart['type_product']))
                $type_product_ses = $product_cart['type_product'];


            include root . 'web/mods/mod_cart/product_cart_expositor.php';


        endforeach;

        if ($count_expositor == 1):
            ?>

        </div>

    <?php
    endif;

endif;