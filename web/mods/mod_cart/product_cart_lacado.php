<?php if ($count_lacado == 1): ?>
    <div class="content-lacados effect7 col-md-12 col-xs-12 text-left line_lacados col-md-12 col-xs-12">
    <h4 class="title_product text-left">Incrementos lacados&nbsp;&nbsp;&nbsp;<span
            data-tooltip="tooltip" data-placement="top" title="Ver más información lacados"
            class="title_info_product title-warning fa fa-info-circle image-popup-no-margins icono-lupa fa fa-search"
            href="<?= path_image ?>images_product/<?= $id_product ?>/lacados.jpg"><img
                class="hidden" src="<?= path_image ?>images_product/<?= $id_pro ?>/lacados.jpg"></span>
    </h4>

    <div class="row col-md-12 col-xs-12 text-center">
        <div class="col-md-3 col-xs-3 title_line_product"></div>
        <div class="col-md-3 col-xs-3 title_line_product">Color</div>
        <div class="col-md-3 col-xs-3 title_line_product">Unidades</div>
        <div class="col-md-3 col-xs-3 title_line_product">Precio (€)</div>
    </div>

    <div class="content_lacado_ral row col-md-12 col-xs-12 text-center">
    <?php

$array_color = $familia->getColorsRalLineaPedido($product_cart['color_lacado'], $product_cart['id_product']);

if($product_cart['unit_lacado'] == 1):
    $precio = "75.00";
elseif($product_cart['unit_lacado'] > 1 AND $product_cart['unit_lacado'] <= 6):
    $precio = "125.00";
elseif($product_cart['unit_lacado'] > 6 AND $product_cart['unit_lacado'] <= 15):
    $precio = "200.00";
elseif($product_cart['unit_lacado'] > 15 AND $product_cart['unit_lacado'] <= 25):
    $precio = "275.00";
elseif($product_cart['unit_lacado'] > 25 AND $product_cart['unit_lacado'] <= 35):
    $precio = "350.00";
elseif($product_cart['unit_lacado'] > 35 AND $product_cart['unit_lacado'] <= 50):
    $precio = "475.00";
elseif($product_cart['unit_lacado'] > 50):
endif;

?>

    <div class="col-md-12 col-xs-12 line_lacado_ral">
        <div class="col-md-3 col-xs-3 title_line_product">
            <div class="preview_color_lacado"
                 style="background-color: <?= $array_color['html'] ?>;"></div>
        </div>
        <div class="col-md-3 col-xs-3 color_lacado"><?= $product_cart['color_lacado'] ?></div>
        <div class="col-md-3 col-xs-3 unit_lacado"><?= $product_cart['unit_lacado'] ?></div>
        <div class="col-md-3 col-xs-3 product_price_lacado"><?=$precio?></div>
    </div>

<?php
$total_presupuesto = $total_presupuesto + $precio;

$exist_lacado = "1";

?>

    </div>
    </div>

<?php endif; ?>