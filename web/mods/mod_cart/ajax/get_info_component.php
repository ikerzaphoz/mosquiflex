<?php

require '../../../../config.php';

$familia = new Familia();

$array_component = "";
if (isset($_POST['data']))
    $array_component = json_decode(stripslashes($_POST['data']), true);

print_r($_POST);

if (!empty($array_component)):
    foreach ($array_component as $index => $component):

        $arfami = "";
        $arsubf = "";
        $ararti = "";
        $units = "";
        $color = "";
        $referencia = "";
        $descripcion = "";
        $precio = "";
        $tipo_medida = "";
        $precio_unitario = "";
        $arprpr = "";
        $medida_maxima = "";
        $is_almacen = "0";
        $arumiv = "";
        if (isset($component[0]['tipo_medida']))
            $tipo_medida = $component[0]['tipo_medida'];
        if (isset($component[0]['input_unit_components']))
            $units = $component[0]['input_unit_components'];
        if (isset($component[0]['components_color']))
            $color = $component[0]['components_color'];
        if (isset($component[0]['id_component']))
            $arfami = $component[0]['id_component'];
        if (isset($component[0]['arsubf']))
            $arsubf = $component[0]['arsubf'];
        if (isset($component[0]['ararti']))
            $ararti = $component[0]['ararti'];
        if (isset($component[0]['total_component']))
            $precio = $component[0]['total_component'];
        if (isset($arfami) AND isset($arsubf) AND isset($ararti)):
            $arsubf = str_pad($arsubf, 2, "0", STR_PAD_LEFT);
            $referencia = $arfami . "-" . $arsubf . "-" . $ararti;
        endif;
        if (isset($component[0]['arumiv'])):
            $arumiv = $component[0]['arumiv'];
        endif;
        if (isset($component[0]['precio_fabrica']))
            $precio_unitario = $component[0]['precio_fabrica'];
        if (isset($component[0]['is_almacen']))
            $is_almacen = $component[0]['is_almacen'];
        $data_component = $familia->searchComponentByCode($arfami, $arsubf, $ararti);
        if (isset($data_component[0]['ardesc']))
            $descripcion = $data_component[0]['ardesc'];
        if (isset($data_component[0]['arprpr']))
            $arprpr = $data_component[0]['arprpr'];
        if (isset($data_component[0]['medida_maxima']))
            $medida_maxima = $data_component[0]['medida_maxima'];

        $cofami = "";
        if ($arfami == "1791"):
            $cofami = "2731";
        endif;

        $is_color = "";
        if ($color == "-" OR $color == "000001" OR $color == "bruto" OR $color == "010001"):
            $is_color = "hidden";
        else:
            $ararti = substr($ararti,0,2);
            $ararti = $ararti.str_pad($color, 4, "0", STR_PAD_LEFT);
        endif;

        $array_color = "";
        if (isset($familia->getColoressByCocolo($cofami, $color)[0])):
            $array_color = $familia->getColoressByCocolo($cofami, $color)[0];
            if (!empty($array_color['codesc']) AND !empty($array_color['html']) AND ($array_color['cosubf'] != 3 AND $array_color['cosubf'] != 4)):
                $class_imagen_hidden = "hidden";
            else:
                $url_image = $array_color['cofami'] . $array_color['cosubf'] . $array_color['cocolo'];
                $src_imagen = path_image_colors . $url_image . ".jpg";
            endif;
        endif;

        $total_metros = 0;
        foreach ($array_component[$index] as $component_aux):
            if ($component_aux['tipo_medida'] == 2):
                $total_metros = $total_metros + $component_aux['metros'] * $component_aux['unidades'];
            endif;
        endforeach;

        if ($is_almacen == "1"):
            $embalaje = $familia->getEmbalaje($arfami, $arsubf, $ararti, $arprpr);
        endif;

        ?>


        <div class="line_product_component col-md-12 col-xs-12 text-left">

            <div class="row col-md-12 col-xs-12 line_component">

            <div class="col-md-2 col-xs-2 text-right">
                <?php if ($tipo_medida != 3 AND !empty($tipo_medida)): ?>
                    <button class="btn btn-sm btn_edit_units_medidas_resumen bg-border-info"><?=lang_title_editar_medidas?>
                    </button>
                <?php else: ?>
                    <?php if (empty($is_almacen)): ?>
                        <input type="number" value="<?= $units ?>"
                               class="border_units_components change_units_resumen_component"> <?=lang_title_abreviatura_unidades?>.
                    <?php else: ?>
                        <input type="number" step="<?= $embalaje ?>" min="0" value="<?= $units ?>"
                               class="border_units_components change_units_resumen_component"> <?=lang_title_abreviatura_unidades?>.
                    <?php endif; ?>
                <?php endif; ?>
            </div>

            <div class="col-md-2 col-xs-2">
                <div class="change_color_component_resumen <?= $is_color ?>"
                     style="background-color: <?= $array_color['html'] ?>;">
                    <img class="img_color_component <?= $class_imagen_hidden ?>" src="<?= $src_imagen ?>">
                </div>
                <?php if ($is_color == "hidden"): ?>
                    <?= $color; ?>
                <?php else: ?>
                    <span class="title_color_component"><?= $array_color['codesc'] ?></span>
                <?php endif; ?>
            </div>

            <div class="col-md-2 col-xs-2"><?= $referencia ?></div>
            <div class="col-md-3 col-xs-3"><?= $descripcion ?></div>
            <div class="col-md-2 col-xs-2 product_price total_component"><?= $precio ?></div>
            <div class="col-md-1 col-xs-1"><span><i class='fa fa-times-circle btn_delete_line_component' aria-hidden='true'></i></span></div>
            <input type="hidden" class="is_almacen lolo" value="<?= $is_almacen ?>">
            <input type="hidden" class="price_component_hidden" value="<?= $precio_unitario ?>">
            <input type="hidden" class="precio_unidades_medidas" value="<?= $precio_unitario ?>">
            <input type="hidden" class="arfami id_component_product" value="<?= $arfami ?>">
            <input type="hidden" class="arsubf" value="<?= $arsubf ?>">
            <input type="hidden" class="arprpr" value="<?= $arprpr ?>">
            <input type="hidden" class="total_units_metros" value="<?= $total_metros ?>">
            <input type="hidden" class="max_measure" value="<?= $medida_maxima ?>">
            <input type="hidden" class="ararti" value="<?= $ararti ?>">
            <input type="hidden" class="arumiv" value="<?= $arumiv ?>">
            <input type="hidden" class="tipo_medida_component" value="<?= $tipo_medida ?>">
            <input type="hidden" class="type_product" value="1">
            <input type="hidden" class="unidades_unidades_medidas" value="<?= $units ?>">

            <?php if ($tipo_medida != 3 AND !empty($tipo_medida)): ?>

            <div class="content_lines_component row col-md-12 col-xs-12">

                <?php foreach ($array_component[$index] as $component_aux): ?>
            <?php if ($component_aux['tipo_medida'] == 1):
                $total_metros = $component_aux['ancho'] * $component_aux['alto'];

                ?>
                <div class="segunda_linea_producto mt-10 ml-5 row col-md-12 col-xs-12 text-right">
                    <div class="col-md-2 col-xs-2">
                        <span class="resumen_medidas_unidades"><?= $component_aux['unidades'] ?></span> Uds.
                    </div>
                    <div class="col-md-2 col-xs-2">Ancho:<span
                            class="resumen_medidas_ancho"><?= $component_aux['ancho'] ?></span>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        Alto: <span
                            class="resumen_medidas_alto"><?= $component_aux['alto'] ?></span>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        Total: <?= round2decimals($total_metros) ?> m<sup>2</sup>
                    </div>
                    <input type="hidden" class="unidades_unidades_medidas" value="<?= $component_aux['unidades'] ?>">
                    <input type="hidden" class="ancho_unidades_medidas" value="<?= $component_aux['ancho'] ?>">
                    <input type="hidden" class="alto_unidades_medidas" value="<?= $component_aux['alto'] ?>">
                    <input type="hidden" class="precio_unidades_medidas" value="<?= $precio_unitario ?>">
                </div>
            <?php else: ?>
                <div class="segunda_linea_producto row col-md-12 col-xs-12 text-left">
                    <div class="col-xs-3 col-md-3">
                        <span class="resumen_medidas_unidades"><?= $component_aux['unidades'] ?></span> Uds. de <span
                            class="resumen_medidas_metros"><?= $component_aux['metros'] ?></span>
                        metro(s)
                    </div>
                    <input type="hidden" class="unidades_unidades_medidas" value="<?= $component_aux['unidades'] ?>">
                    <input type="hidden" class="precio_unidades_medidas" value="<?= $precio_unitario ?>">
                    <input type="hidden" class="metros_unidades_medidas"
                           value="<?= round3decimals(str_replace(",", ".", $component_aux['metros'])) ?>">
                </div>

                    <?php endif; ?>
                    <?php endforeach; ?>

                <?php endif; ?>

            </div>

            </div>

        </div>


        <?php
    endforeach;

else:
    echo "Error al añadir datos...";
endif;