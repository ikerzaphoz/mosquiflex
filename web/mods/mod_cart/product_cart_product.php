<?php
$color_product_ses = "";
if (isset($product_cart['color_product']))
    $color_product_ses = $product_cart['color_product'];
$group_color_ses = "";
if (isset($product_cart['group_color']))
    $group_color_ses = $product_cart['group_color'];
$subfamily_product_ses = "";
if (isset($product_cart['subfamily_product']))
    $subfamily_product_ses = $product_cart['subfamily_product'];
$unit_product_ses = "";
if (isset($product_cart['unit_product']))
    $unit_product_ses = $product_cart['unit_product'];
$width_product_ses = "";
if (isset($product_cart['width_product']))
    $width_product_ses = $product_cart['width_product'];
$height_product_ses = "";
if (isset($product_cart['height_product']))
    $height_product_ses = $product_cart['height_product'];
$observaciones_product_ses = "";
if (isset($product_cart['observaciones_product']))
    $observaciones_product_ses = $product_cart['observaciones_product'];
$array_increment_ses = "";
if (isset($product_cart['array_increment']))
    $array_increment_ses = $product_cart['array_increment'];

$code_sub_ses = $subfamily_product_ses . $group_color_ses;
$ancho_minimo_ses = $familia_ses->getAnchoMin($id_product_ses, $code_sub_ses);
$ancho_maximo_ses = $familia_ses->getAnchoMax($id_product_ses, $code_sub_ses);
$alto_minimo_ses = $familia_ses->getAltoMin($id_product_ses, $code_sub_ses);
$alto_maximo_ses = $familia_ses->getAltoMax($id_product_ses, $code_sub_ses);

$class_warning_width_ses = "border-warning";
if ($width_product_ses >= $ancho_minimo_ses AND $width_product_ses <= $ancho_maximo_ses) {
    $class_warning_width_ses = "";
}
$class_warning_height_ses = "border-warning";
if ($height_product_ses >= $alto_minimo_ses AND $height_product_ses <= $alto_maximo_ses) {
    $class_warning_height_ses = "";
}

$is_lacado = "";
$is_lacado = $familia->getIsLacado($color_product_ses, $id_product_ses);

//COMENTADO EL 5/10/17
//if ($is_lacado == "1"):
//    $color_product_ses = 2;
//endif;

$array_familia = $familia->getFamilia('fafami');
if ($is_lacado == "1"):
    $array_color_ses = $familia->getColorsRalLineaPedido($color_product_ses, $id_product_ses);
else:
    if (isset($id_product_ses) AND $color_product_ses)
        $array_color_ses = $familia_ses->getColoressByCocolo($id_product_ses, $color_product_ses)[0];
endif;

$rango_ses = "";

if ($id_product_ses == 2731):
    $input_width_product_rango_ses = calc_precio_enrollable_ventana($width_product_ses, $array_familia_ses[$id_product_ses]['min_ran_anc']);
    $input_height_product_rango_ses = calc_precio_enrollable_ventana($height_product_ses, $array_familia_ses[$id_product_ses]['min_ran_alt']);
endif;

if ($id_product_ses == 2732):
    $input_width_product_rango_ses = calc_precio_ancho_enrollable_puerta($width_product_ses, $array_familia_ses[$id_product_ses]['min_ran_anc']);
    $input_height_product_rango_ses = calc_precio_alto_enrollable_puerta($height_product_ses, $array_familia_ses[$id_product_ses]['min_ran_alt']);
endif;

if ($id_product_ses == 2733):
    $input_width_product_rango_ses = calc_precio_ancho_plisada($width_product_ses, $array_familia_ses[$id_product_ses]['min_ran_anc'], $subfamily_product_ses);
    $input_height_product_rango_ses = calc_precio_alto_plisada($height_product_ses, $array_familia_ses[$id_product_ses]['min_ran_alt']);
endif;

if ($id_product_ses == 2734):
    $input_width_product_rango_ses = calc_precio_ancho_fija($width_product_ses, $array_familia_ses[$id_product_ses]['min_ran_anc']);
    $input_height_product_rango_ses = calc_precio_alto_fija($height_product_ses, $array_familia_ses[$id_product_ses]['min_ran_alt']);
    $input_width_hoja_product_rango = calc_precio_hoja_corredera($width_product_ses);
endif;

if ($id_product_ses == 2735):
//    $input_width_product_rango_ses = calc_precio_ancho_corredera($width_product_ses, $array_familia_ses[$id_product_ses]['min_ran_anc']);
//    $input_height_product_rango_ses = calc_precio_alto_corredera($height_product_ses, $array_familia_ses[$id_product_ses]['min_ran_alt']);
//    $input_width_hoja_product_rango_ses = calc_precio_hoja_corredera($width_product_ses);
endif;

if ($id_product_ses == 2736):
    $input_width_product_rango_ses = calc_precio_ancho_abatible($width_product_ses, $array_familia_ses[$id_product_ses]['min_ran_anc'], $subfamily_product_ses);
    $input_height_product_rango_ses = calc_precio_alto_abatible($height_product_ses, $array_familia_ses[$id_product_ses]['min_ran_alt']);
endif;

if ($id_product_ses == 2735):

//    $rango_ses_hoja = $input_width_hoja_product_rango_ses . $input_height_product_rango_ses;
//    $precio_rango_ses_hoja = $familia_ses->getPriceOfRango($id_product_ses, "1" . $code_sub_ses, $rango_ses_hoja);
//    $total_price_ses_hoja = $precio_rango_ses_hoja * 2 * $unit_product_ses;
//    $total_price_ses_hoja = round2decimals($total_price_ses_hoja);
//
//    $rango_ses = $input_width_product_rango_ses . $input_height_product_rango_ses;
//    $precio_rango_ses = $familia_ses->getPriceOfRango($id_product_ses, $code_sub_ses, $rango_ses);
//    $total_price_ses = $precio_rango_ses * $unit_product_ses;
//    $total_price_ses = round2decimals($total_price_ses);
//
//    $total_presupuesto = $total_presupuesto + $total_price_ses + $total_price_ses_hoja;

else:

    $rango_ses = $input_width_product_rango_ses . $input_height_product_rango_ses;

    $precio_rango_ses = $familia_ses->getPriceOfRango($id_product_ses, $code_sub_ses, $rango_ses);
    $total_price_ses = $precio_rango_ses * $unit_product_ses;
    $total_price_ses = round2decimals($total_price_ses);
    $total_presupuesto = $total_presupuesto + $total_price_ses;

endif;

if (isset($array_familia_ses[$id_product_ses])):
    ?>

    <?php if ($id_product_ses != "2735"): ?>
        <div class="line_product effect7 col-md-12 col-xs-12 text-left">
            <div class="row col-md-12 col-xs-12">
                <h4 class="title_product"><?= $array_familia_ses[$id_product_ses]['fadesc'] ?>&nbsp;&nbsp;&nbsp;<span
                        data-tooltip="tooltip" data-placement="top" title="<?= lang_text_ver_mas_info ?>"
                        class="title_info_product title-warning fa fa-info-circle image-popup-no-margins icono-lupa fa fa-search"
                        href="<?= path_image ?>images_product/<?= $id_product_ses ?>/medidas.png"><img class="hidden"
                                                                                                   src="<?= path_image ?>images_product/<?= $id_product_ses ?>/medidas.png">
                    </span></h4>
            </div>

            <div class="row col-md-12 col-xs-12 text-center">
                <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_version ?></div>
                <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_color ?></div>
                <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_unidades ?></div>
                <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_ancho ?></div>
                <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_alto ?></div>
                <div class="col-md-2 col-xs-2 title_line_product"><?= lang_text_precio ?> (€)</div>
            </div>
            <div class="col-md-12 col-xs-12 row text-center">
                <div class="col-md-2 col-xs-2">
                    <?php
                    $version = "";
                    switch ($id_product_ses):
                        case '2731':
                            ?>
                            <select class="form-control change-subfamily input_subfamily_product">
<!--                                <option <?php /*
                                if (empty($subfamily_product_ses)): echo "selected";
                                endif;
                                */?> value="">C.33
                                </option>-->
                                <option <?php
                                if ($subfamily_product_ses == 1): echo "selected";
                                endif;
                                ?> value="1">C.35
                                </option>
                                <option <?php
                                if ($subfamily_product_ses == 2): echo "selected";
                                endif;
                                ?> value="2">C.42
                                </option>
                            </select>
                            <?php
                            break;
                        case '2733':
                        case '2736':
                            ?>
                            <select class="form-control change-subfamily input_subfamily_product">
                                <option <?php
                                if ($subfamily_product_ses == 0): echo "selected";
                                endif;
                                ?> value="">P.Única
                                </option>
                                <option <?php
                                if ($subfamily_product_ses == 1): echo "selected";
                                endif;
                                ?> value="1">P.Doble
                                </option>
                            </select>
                            <input type="hidden" name="input_tipologia" value="<?= $tipologia ?>" class="input_tipologia">
                            <?php
                            break;
                    endswitch;
                    ?>
                </div>
                <div class="col-md-2 col-xs-2 content_color_pedido">
                    <?php
                    if ($group_color_ses == 3 OR $group_color_ses == 4):
                        $url_imagen = $id_product_ses . $group_color_ses . $color_product_ses . ".jpg";
                        ?>
                        <div class="color_product_line">
                            <img class="img_color_product" src="<?= path_image_colors . $url_imagen ?>">
                        </div>
                    <?php else: ?>
                        <div color_ral="<?= hideRal($array_color_ses['codesc']) ?>"
                             style="background-color: <?= $array_color_ses['html'] ?>" class="color_product_line">
                            <img class="img_color_product hidden" src="">
                        </div>
                    <?php endif;
                    ?>
                    <span class="title_color"><?= $array_color_ses['codesc'] ?></span>
                </div>
                <div class="col-md-2 col-xs-2">
                    <input required placeholder="1" title="" value="<?= $unit_product_ses ?>" min="1" type="number"
                           name="input_unit_product"
                           class="input_unit_product form-control w75"/>
                </div>
                <div class="col-md-2 col-xs-2">
                    <input pattern="[0-9]+([\.,][0-9]+)"
                           title="Valor mínimo: <?= $ancho_minimo_ses ?> y máximo: <?= $ancho_maximo_ses ?>. Ejemplo formato: 1.010"
                           required placeholder="<?= $ancho_minimo_ses ?>" type="text" name="input_width_product"
                           class="input_width_product form-control <?= $class_warning_width_ses ?> check_size w100"
                           placeholder="0.000"
                           value="<?= $width_product_ses ?>"/>
                </div>
                <div class="col-md-2 col-xs-2">
                    <input pattern="[0-9]+([\.,][0-9]+)" required placeholder="<?= $alto_minimo_ses ?>"
                           title="Valor mínimo: <?= $alto_minimo_ses ?> y máximo: <?= $alto_maximo_ses ?>. Ejemplo formato: 1.010"
                           type="text" name="input_height_product"
                           class="input_height_product form-control <?= $class_warning_height_ses ?> check_size w100"
                           placeholder="0.000"
                           value="<?= $height_product_ses ?>"/>
                </div>
                <div class="col-md-2 col-xs-2">
                    <?php if (!empty($class_warning_height_ses) AND ! empty($class_warning_height_ses)): ?>
                        <span class="product_price"><span
                                class="text-danger"><?= lang_text_revisar_medidas ?></span></span>
                        <?php else: ?>
                        <span class="product_price"><?= $total_price_ses ?></span>
                    <?php endif; ?>

                </div>
            </div>

            <div class="segunda_linea_producto col-md-12 col-xs-12 row">
                <div class="col-md-2 col-xs-2 text-left">
                    <?php if ($id_product_ses == 2731 OR $id_product_ses == 2734): ?>
                        <button data-toggle="modal" data-target="#modal_option_product"
                                class="btn btn-sm btn-grey btn_option_product"><?= lang_text_anadir_incrementos ?>
                        </button>
                    <?php endif; ?>
                </div>
                <div class="hidden col-md-6 col-xs-6">
                    <textarea name="observaciones_product" class="observaciones_product"
                              placeholder="Observaciones..."></textarea>
                </div>
                <div class="col-md-10 col-xs-10 increments text-right">
                    <?php
                    if (!empty($array_increment_ses) AND ( $id_product_ses == 2731 OR $id_product_ses == 2734)):
                        foreach ($array_increment_ses as $increment):
                            ?>
                            <?php
                            $class_bg_warning = "border-warning";
                            if (!empty($increment['input_unit_product_component'])):
                                $class_bg_warning = "";
                                $total_presupuesto = $total_presupuesto + $increment['product_price_component'];
                            endif;
                            ?>
                            <div class='row_option_product row col-md-12 col-xs-12'>
                                <div class='col-xs-1 col-md-1'><span data-tooltip='tooltip' title='Eliminar incremento'
                                                                     data-placement='top'
                                                                     class='fa fa-2x fa-close text-danger delete_increment'></span>
                                </div>
                                <div class='col-md-1 col-xs-1'>-</div>
                                <div class='col-md-5 col-xs-5 text-left'>
                                    <span
                                        class='title_product title_increment_product'><?= $increment['title_increment'] ?></span>
                                </div>
                                <div class='col-md-3 col-xs-3'><input required placeholder='0' title=''
                                                                      value='<?= $increment['input_unit_product_component'] ?>'
                                                                      min='0' type='number' name='input_unit_product'
                                                                      class='unit_increment <?= $class_bg_warning ?> form-control w75'/>
                                </div>
                                <div class='col-md-2 col-xs-2'><span
                                        class='product_price'><?= $increment['product_price_component'] ?></span>
                                </div>
                                <div class='hidden'><span
                                        class='product_price_fixed'><?= $increment['product_price_fixed'] ?></span>
                                </div>
                                <input type='hidden' class='id_component' value='" <?= $increment['id_component'] ?> "'>
                            </div>
                            <div class='row col-md-12 col-xs-12'>
                                <div class='col-xs-1 col-md-1'></div>
                                <div class='col-md-1 col-xs-1'></div>
                                <div class='col-md-4 col-xs-4'></div>
                                <div class='col-md-5 col-xs-5 text-left'><span class='message_units_increment'>*Unidades por mosquitera completa*</span>
                                </div>
                                <div class='col-md-1 col-xs-1'></div>
                                <div class='hidden'></div>
                            </div>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>

            <div class="hidden info_product_line">
                <span class="hidden input_id_product" span_value="<?= $id_product_ses ?>" span_name="id_product"></span>
                <span class="hidden input_color_product" span_value="<?= $color_product_ses ?>"
                      span_name="color_product"></span>
                <span class="hidden input_group_color" span_value="<?= $group_color_ses ?>"
                      span_name="group_color"></span>
                <span class="hidden input_danch" span_value="<?= str_pad($ancho_minimo_ses, 5, "0") ?>"
                      span_name="input_danch"></span>
                <span class="hidden input_hanch" span_value="<?= str_pad($ancho_maximo_ses, 5, "0") ?>"
                      span_name="input_hanch"></span>
                <span class="hidden input_dalto" span_value="<?= str_pad($alto_minimo_ses, 5, "0") ?>"
                      span_name="input_dalto"></span>
                <span class="hidden input_halto" span_value="<?= str_pad($alto_maximo_ses, 5, "0") ?>"
                      span_name="input_halto"></span>
            </div>
            <div class="segunda_linea_producto row col-md-12 col-xs-12 text-right">
                <span data-tooltip="tooltip" data-placement="top" title="<?= lang_text_copiar_linea_pedido ?>"
                      class="fa fa-2x fa-copy btn_copy_line text-warning btn-grey btn-option-line"></span>
                <span data-tooltip="tooltip" data-placement="top" title="<?= lang_text_eliminar_linea_pedido ?>"
                      class="fa fa-2x fa-close btn_delete_line text-danger btn-grey btn-option-line"></span>
                <span data-tooltip="tooltip" data-toggle="modal" data-target="#modal_add_product" data-placement="top"
                      title="<?= lang_text_añadir_producto ?>"
                      class="fa fa-2x fa-plus hidden btn_add_product text-success btn-grey btn-option-line"></span>
            </div>
            <input type="hidden" class="is_almacen" value="0">
        </div>
        <?php
    else:

        if (empty($count_corredera)):
            include 'product_cart_corredera_session.php';
            $count_corredera++;
        endif;

    endif;
    ?>
<?php endif; ?>