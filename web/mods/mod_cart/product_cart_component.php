<?php

$unidades = "";
if (isset($product_cart['unit_product']))
    $unidades = $product_cart['unit_product'];
$id_product = "";
if (isset($product_cart['id_product']))
    $id_product = $product_cart['id_product'];
$subfamilia = "";
if (isset($product_cart['subfamily_product']))
    $subfamilia = $product_cart['subfamily_product'];
$color = "";
if (isset($product_cart['color_product']))
    $color = $product_cart['color_product'];
$arprpr = "";
if (isset($product_cart['arprpr']))
    $arprpr = $product_cart['arprpr'];
$tipo_medida_componente = "";
if(isset($product_cart['tipo_medida_componente']))
    $tipo_medida_componente = $product_cart['tipo_medida_componente'];

$is_color = "hidden";
$referencia = $id_product . "-" . str_pad($subfamilia, 2, "0", STR_PAD_LEFT) . "-" . $color;
$ararti = $color;
if (strlen($ararti) != 6) {
    $ararti = $color . $color;
}
$array_info_component = $familia->getInfoComponent($id_product, $subfamilia, $color, $arprpr);

$is_almacen = "0";
if (isset($product_cart['is_almacen']))
    $is_almacen = $product_cart['is_almacen'];
if ($is_almacen == "1"):
    $embalaje = $familia->getEmbalaje($id_product, $subfamilia, '1', $arprpr);
    $precio_component = round2decimals($array_info_component['arpval']);
else:
    $embalaje = "1";
    $precio_component = round2decimals($array_info_component['arpvta']);
endif;

$descripcion = $array_info_component['ardesc'];

$precio = "";
if ($tipo_medida_componente == 1):
    if (isset($product_cart['product_price']))
        $precio = $precio_component * $unidades * $product_cart['width_product'] * $product_cart['height_product'];
endif;

?>
<div class="line_product_component col-md-12 col-xs-12 text-left">
    <div class="row col-md-12 col-xs-12 line_component">
        <div class="col-md-2 col-xs-2 text-right">
            <input type="number" min="0" step="<?= $embalaje ?>" value="<?= $unidades ?>"
                   class="border_units_components change_units_resumen_component"> Uds.
        </div>
        <div class="col-md-2 col-xs-2">
            Bruto
        </div>
        <div class="col-md-2 col-xs-2"><?= $referencia ?></div>
        <div class="col-md-4 col-xs-4"><?= $descripcion ?></div>
        <div class="col-md-2 col-xs-2 product_price total_component"><?= $precio ?></div>
        <input type="hidden" class="price_component_hidden" value="<?= $precio_component ?>">
        <input type="hidden" class="ararti_component_hidden" value="<?= $ararti ?>">
        <input type="hidden" class="arfami id_component_product" value="<?= $id_product ?>">
        <input type="hidden" class="arprpr" value="<?= $arprpr ?>">
        <input type="hidden" class="arsubf" value="<?= $subfamilia ?>">
        <input type="hidden" class="ararti" value="<?= $ararti ?>">
        <input type="hidden" class="fami_color" value="<?= $product_cart['group_color'] ?>">
        <input type="hidden" class="tipo_medida_component" value="<?=$tipo_medida_componente?>">
        <input type="hidden" class="type_product" value="1">
        <input type="hidden" class="is_almacen" value="<?= $is_almacen ?>">
    </div>
</div>
