<div class="col-md-12 col-xs-12 row text-center">

    <?php

    $unidades_producto = "";
    $titulo_producto = "";
    $precio = "";
    $arfami = "1999";
    $arsubf = "52";
    $ararti = "";
    $alto = "0.740";
    $ancho = "0.530";
    if (isset($product_cart['unit_product']) AND $product_cart['unit_product'] > 0):
        switch ($product_cart['observaciones_product']):
            case 'enr_ventana':
                $titulo_producto = "enrollable ventana";
                $precio_unitario = 75;
                $precio = $precio_unitario * $product_cart['unit_product'];
                $ararti = "000001";
                break;
            case 'enr_puerta':
                $titulo_producto = "enrollable puerta";
                $precio_unitario = 150;
                $precio = $precio_unitario * $product_cart['unit_product'];
                $ararti = "000003";
                break;
            case 'exp_plisada':
                $titulo_producto = "plisada";
                $precio_unitario = 150;
                $precio = $precio_unitario * $product_cart['unit_product'];
                $ararti = "000004";
                break;
            case 'exp_abatible':
                $titulo_producto = "abatible";
                $precio_unitario = 150;
                $precio = $precio_unitario * $product_cart['unit_product'];
                $ararti = "000005";
                break;
            case 'exp_fija_corredera':
                $titulo_producto = "fijas y correderas";
                $precio_unitario = 75;
                $precio = $precio_unitario * $product_cart['unit_product'];
                $ararti = "000006";
                break;
            case 'exp_selector_color':
                $titulo_producto = "selector de colores";
                $precio_unitario = 10;
                $precio = $precio_unitario * $product_cart['unit_product'];
                $ararti = "000101";
                break;
            case 'exp_fija_expositor':
                $titulo_producto = "expositor personalizable";
                $precio_unitario = 500;
                $precio = $precio_unitario * $product_cart['unit_product'];
                $ararti = "000201";
                $alto = "2.100";
                $ancho = "0.620";
                break;
        endswitch;
        $unidades_producto = $product_cart['unit_product'];
        $total_presupuesto = $total_presupuesto + $precio;

    endif;

    if (!empty($product_cart['unit_product']) AND !empty($titulo_producto)):

        ?>
        <div class="line_product col-md-12 col-xs-12 line_expositor line_product">
            <div class="col-md-4 col-xs-4 text-left">Muestra <?= strtolower($titulo_producto) ?></div>
            <div class="col-md-4 col-xs-4 text-center">
                <input type="number" min="0" value="<?= $product_cart['unit_product'] ?>"
                       class="border_units_components change_units_resumen_expositor">
            </div>
            <div class="col-md-3 col-xs-3 product_price text-center"><?= round2decimals($precio) ?></div>
            <div class="col-md-1 col-xs-1"><span><i
                        class="fa fa-times-circle btn_delete_line_component" aria-hidden='true'></i></span>
            </div>
            <input type="hidden" class="price_expositor_hidden" value="<?= $precio_unitario ?>">
            <input type="hidden" class="arfami input_id_product" span_value="<?= $arfami ?>" value="<?= $arfami ?>">
            <input type="hidden" class="arsubf input_subfamily_product" value="<?= $arsubf ?>">
            <input type="hidden" class="ararti" value="<?= $ararti ?>">
            <input type="hidden" class="fami_color input_color_product" span_value="0" value="0">
            <input type="hidden" class="input_group_color" span_value="0">
            <input type="hidden" class="ancho_expositor_hidden input_width_product" value="<?= $ancho ?>">
            <input type="hidden" class="alto_expositor_hidden input_height_product" value="<?= $alto ?>">
            <input type="hidden" class="precio_unidades_medidas" value="<?= $precio_unitario ?>">
            <input type="hidden" class="is_almacen" value="0">
            <input type="hidden" class="observaciones_product"
                   value="<?= $product_cart['observaciones_product'] ?>">
        </div>
        <?php

    endif;

    ?>

</div>
