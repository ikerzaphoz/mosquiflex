<div id="modal_set_email" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Actualizar email cliente</h4>
            </div>
            <div class="modal-body row">
                <div class="col-md-12 col-xs-12 row">
                    <?php
                    include root . 'web/mods/mod_customers/form_email.php';
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>