<div id="modal_view_pdfs" class="mt-25 modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog w90">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ver presupuestos / pedidos</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-xs-12 row text-center">
                        <div class="col-md-6 col-xs-6">
                            <a target="_blank" class="btn_view_pdf_pvp" href=""><button class="btn bg-border-warning btn_view_pdf_modal">Ver <span class="type_doc"></span> PVP</button></a>
                            <button data-toggle="modal" data-target="#modal_add_concepto" title="Añadir concepto"
                                    class="btn btn_add_concepto bg-border-warning"><?=lang_text_add_concepto?>
                            </button>
                            <button data-toggle="modal" data-target="#modal_pres_pers" title="Editar cabecera"
                                    class="btn btn_add_editar_datos_cliente bg-border-warning">Editar cabecera
                            </button>
                        </div>
                        <div class="col-md-6 col-xs-6 content_view_cond">
                            <a target="_blank" class="btn_view_pdf_condiciones" href=""><button class="btn bg-border-info">Ver <span class="type_doc"></span> Condiciones Cliente</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<input type="hidden" class="url_pdf_pvp" value="<?= path_web_mods ?>mod_pdf/index.php?pres=">
<input type="hidden" class="url_pdf_condiciones" value="<?= path_web_mods ?>mod_pdf/index.php?pres_cond=">
<input type="hidden" class="id_pres_modal" value="">