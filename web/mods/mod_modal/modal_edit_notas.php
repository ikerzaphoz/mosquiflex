<div id="modal_edit_notas" class="modal fade" role="dialog">
    <div class="modal-dialog w75">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_modal_notas">&times;</button>
                <h4 class="modal-title">Editar notas</h4>
            </div>
            <div class="modal-body row">
                <div class="editor_text"></div>
                <div class="row col-md-12">
                    <div class="col-md-8 col-xs-8">
                        *Todas las anotaciones de este apartado serán visibles únicamente por el usuario, por tanto no serán tenidas en cuenta por MOSQUITERAS TÉCNICAS S.L.*
                    </div>
                    <div class="col-md-4 col-xs-4 text-right">
                        <button class="btn bg-border-success btn-sm btn-save-observaciones">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>