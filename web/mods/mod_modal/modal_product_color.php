<div id="modal_product_color" class="modal fade" role="dialog">
    <div class="modal-dialog w90">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?=lang_title_color_producto?></h4>
            </div>
            <div class="modal-body row">
                <?php

                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?=lang_title_cerrar?></button>
            </div>
        </div>

    </div>
</div>