<div id="modal_add_medidas_component" class="modal fade" role="dialog">
    <div class="modal-dialog w75">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_modal_medidas">&times;</button>
                <h4 class="modal-title"><?=lang_title_editar_medidas?></h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <div class="col-xs-12 col-md-12">
                    <div class="text-right col-xs-12 col-md-12">
                        <button type="button" class="btn btn-success btn_save_medidas_componente"><?=lang_title_guardar_medidas?>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>