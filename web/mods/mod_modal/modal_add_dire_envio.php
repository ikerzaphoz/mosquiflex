<?php
$array_paises = $cliente->getSelectPaises();
?>

<div id="modal_add_dire_envio" class="modal fade" role="dialog">
    <div class="modal-dialog w75">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?= lang_title_modal_add_dire_envio ?></h4>
            </div>
            <div class="modal-body row">
                <form class="add_dire_entre_cliente">
                    <div class="form-group col-md-6 col-xs-12">
                        <label class="col-md-6 col-xs-6" for="dire">Destinatario:</label>
                        <input name="destinatario" placeholder="Ej: Juan" class="input_change_dir col-md-6 col-xs-6 nopadding" value="">
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label class="col-md-6 col-xs-6" for="dire">Dirección 1:</label>
                        <input name="cldir1" placeholder="Ej: C/Rosal nº5" class="input_change_dir col-md-6 col-xs-6 nopadding" value="">
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label class="col-md-6 col-xs-6" for="dire">Dirección 2:</label>
                        <input name="cldir2" class="col-md-6 col-xs-6 nopadding" value="">
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label class="col-md-6 col-xs-6" for="pais">Pais:</label>
                        <select name="pais" class="select_change_pais col-md-6 col-xs-6 nopadding">
                            <option selected value="">Selecciona pais</option>
<?= selectPaises($array_paises) ?>
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label class="col-md-6 col-xs-6" for="pais">Provincia:</label>
                        <select name="provincia" class="select_change_provincias col-md-6 col-xs-6 nopadding">
                            <option value="">Selecciona primero pais</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label class="col-md-6 col-xs-6" for="select_pobl">Poblacion:</label>
                        <select name="select_pobl" class="select_change_poblacion col-md-6 col-xs-6 nopadding">
                            <option value="">Selecciona primero provincia</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label class="col-md-6 col-xs-6" for="poblacion">Código Postal:</label>
                        <select name="poblacion" class="select_change_cp nopadding">
                            <option value="">Selecciona primero poblacion</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label class="col-md-6 col-xs-6" for="tlf1">Teléfono:</label>
                        <input pattern="[0-9]{9}" title="Campo teléfono. 9 números necesarios." placeholder="Ej. 668899999" class="col-md-6 col-xs-6 nopadding" name="cltlf1" value="">
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label class="col-md-6 col-xs-6" for="tlf1">Móvil:</label>
                        <input pattern="[0-9]{9}" title="Campo teléfono. 9 números necesarios." placeholder="Ej. 668899999" class="col-md-6 col-xs-6 nopadding" name="cltlf2" value="">
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label class="input_name_dire col-md-6 col-xs-6" for="name">Nombre:</label>
                        <input name="nombre" placeholder="Ej: Casa" title="Introduzca algún valor" class="col-md-6 col-xs-6 input_name input_name_dire nopadding" value="">
                    </div>
                    <div class="form-group col-md-6 col-xs-12 text-right">
                        <button type="submit" class="btn bg-border-success btn_add_new_dire">Añadir</button>
                    </div>
                    <input type="hidden" name="is_temporal" value="0">
                    <input type="hidden" name="clproc" value="<?= $_SESSION['clproc'] ?>">
                    <input type="hidden" name="clcodi" value="<?= $_SESSION['clcodi'] ?>">
                </form>
            </div>
        </div>

    </div>
</div>