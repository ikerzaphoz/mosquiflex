<div id="modal_option_product" class="modal fade" role="dialog">
    <div class="w90 modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Incrementos</h4>
            </div>
            <div class="modal-body row">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?=lang_title_cerrar?></button>
            </div>
        </div>

    </div>
</div>