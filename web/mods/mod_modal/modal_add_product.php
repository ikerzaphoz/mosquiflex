<div id="modal_add_product" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_modal_add_product">&times;</button>
                <h4 class="modal-title"><?=lang_text_anadir_producto?></h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <div class="boton_componentes_flotante_add text-center hidden">
                    <button type="button" value="presupuesto" name="type_button" class="btn-flotante btn_flotante_add btn bg-border-info"><span>Añadir <span class="hidden type_document"></span></span></button><br>
                </div>
                <button type="button" class="btn btn-default close_modal_add_product"><?=lang_title_cerrar?></button>
            </div>
        </div>

    </div>
</div>