<div id="modal_add_concepto" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_modal_concepto">&times;</button>
                <h4 class="modal-title"><?=lang_text_add_concepto?></h4>
            </div>
            <div class="modal-body">
                
                <form class="add_concepto" name="add_concepto">
                    <div class="form-group">
                    <textarea name="text_concepto" class="form-control text_concepto" placeholder="<?=lang_text_add_concepto?>"></textarea>
                    </div>
                    <div class="form-group">
                    <select class="form-control tipo_concepto" name="tipo_concepto">
                        <option value="" disabled selected><?=lang_text_select_opcion?></option>
                        <option value="0"><?=lang_text_select_importe?></option>
                        <option value="1"><?=lang_text_select_porcentaje?></option>
                    </select>
                    </div>
                    <div class="form-group">
                    <select class="form-control aumento_concepto" name="aumento_concepto">
                        <option value="" disabled selected><?=lang_text_select_opcion?></option>
                        <option value="0"><?=lang_text_select_incremento?></option>
                        <option value="1"><?=lang_text_select_descuento?></option>
                    </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control valor_concepto" name="valor_concepto" placeholder="<?=lang_text_select_introduce_importe?>">
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <div class="boton_componentes_flotante_add text-center">
                    <button type="button" value="presupuesto" name="type_button" class="btn-flotante btn_flotante_add_concepto btn bg-border-info"><span><?=lang_text_add_al?> <span class="type_document_concepto"></span></span></button><br>
                </div>
                <button type="button" class="btn btn-default close_modal_concepto"><?= lang_title_cerrar ?></button>
            </div>
        </div>

    </div>
</div>