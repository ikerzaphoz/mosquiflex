<div id="modal_help" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">AYUDA</h4>
            </div>
            <div class="modal-body content_pdf_help">
                <?php 
                
                include root.'web/mods/mod_modal/help/help_'.$page.'.php';
                
                ?>
            </div>
        </div>

    </div>
</div>