<div id="modal_edit_indicaciones" class="modal fade" role="dialog">
    <div class="modal-dialog w75">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss='modal' class="close">&times;</button>
                <h4 class="modal-title">Editar indicaciones</h4>
            </div>
            <div class="modal-body row">
                <div class="editor_text"></div>
                <div class="row col-md-12">
                    
                    <div class="col-md-4 col-xs-4 text-right">
                        <button class="btn bg-border-success btn-sm btn-save-indicaciones">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>