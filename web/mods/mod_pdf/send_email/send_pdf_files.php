<?php

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$mail = new PHPMailer();                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 1;                                 // Enable verbose debug output
    //$mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'mail.mosquiflex.es';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'mosquiterastecnicas@mosquiflex.es';                 // SMTP username
    $mail->Password = 'mMosS..TteE..17';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to
    // Activo condificacción utf-8
    $mail->CharSet = 'UTF-8';
    //Recipients
    $mail->setFrom('ventas@mosquiflex.es', 'Pedido / Presupuesto Mosquiflex');
    $mail->addAddress('ikerzaphoz@gmail.com', 'Email');     // Add a recipient

    if ($adjuntos_incremento > 0):
        $mensaje = "Adjuntan croquis de incrementos";
        $adjuntos = $presupuesto->get_adjunto_incremento($id_pres);
        foreach ($adjuntos as $index => $item):
            if (!empty($item['adjunto'])):
                $fecha_hoy = date("Y/m/d");
                $mail->addAttachment(path_uploads_files."/" . $fecha_hoy . "/".$item['adjunto'], $item['adjunto']);
            endif;
        endforeach;
    endif;

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Nuevo pedido';
    $mail->Body = 'Recibido el ' . $is_pedido . " número " . $id_pres . "del cliente id->" . $_SESSION['name_log'] . "<br>" . $mensaje;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
} catch (Exception $e) {
    echo '0.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
