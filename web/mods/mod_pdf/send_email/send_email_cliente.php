<?php

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$mail = new PHPMailer();                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 1;                                 // Enable verbose debug output
    //$mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'mail.mosquiflex.es';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'mosquiterastecnicas@mosquiflex.es';                 // SMTP username
    $mail->Password = 'mMosS..TteE..17';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to
    //Activo condificacción utf-8
    $mail->CharSet = 'UTF-8';
    //Recipients
    $mail->setFrom('ventas@mosquiflex.es', "Pedido / Presupuesto Mosquiflex Nº " . $id_pres);
    if (!empty($_SESSION['email_log'])):
        $mail->addAddress($_SESSION['email_log'], 'Email');     // Add a recipient
    else:
        $mail->addAddress('iker@flexol.es', 'Email');     // Add a recipient
    endif;

    include root . 'web/mods/mod_emails/header.php';
    include root . 'web/mods/mod_emails/footer.php';
    //Content
    $mail->isHTML(true);       // Set email format to HTML    
    if (!empty($_SESSION['email_log'])):
        $mail->Subject = "Pedido / Presupuesto Mosquiflex Nº " . $id_pres;
        $email_body = 'Hemos recibido el ' . $is_pedido . " número " . $id_pres . ".<br>Puede consultarlo en el área de clientes.";
        $mail->Body = $email_header . $email_body . $email_footer;
    else:
        $mail->Subject = "EMAIL Pedido / Presupuesto Mosquiflex Nº " . $id_pres;
        $email_body = 'PRUEBAS Hemos recibido el ' . $is_pedido . " número " . $id_pres . " pero no existe el email del cliente en nuestra base de datos.";
        $mail->Body = $email_header . $email_body . $email_footer;
    endif;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
} catch (Exception $e) {
    echo '0.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
