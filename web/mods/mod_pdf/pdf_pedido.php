<?php
ob_start();
include(dirname(__FILE__).'/datos/pdf_pedido.php');
$filename = $datos_presupuesto[0]['num_pre'].'.pdf';
$filename = str_replace(" ", "-", $filename);
$filename = str_replace("%20", "-", $filename);
require_once("dompdf-master/autoload.inc.php");
ob_start();
use Dompdf\Dompdf;
$dompdf = new DOMPDF();
include(dirname(__FILE__).'/vistas/pdf_pedido.php');
$dompdf->load_html(ob_get_clean());
$dompdf->setPaper('A4');
$dompdf->render();
$dompdf->stream($filename, array("Attachment" => false));
?>