<?php

if(empty($_SESSION)):
    session_start();
endif;

$presupuesto = new Presupuesto();
$cliente = new Customers();
$familia = new Familia();

$id_pres = "";
if(isset($_GET['pres'])) $id_pres = $_GET['pres'];

$datos_presupuesto = $presupuesto->get_presupuesto($id_pres);

if($datos_presupuesto[0]['id_cliente'] != $_SESSION['clproc'].$_SESSION['clcodi']):
    header("Location:".path_web."clientes.php?opt=historial&pres_error=1");
else:
endif;

$is_pedido = $datos_presupuesto[0]['is_pedido'];
$class_hidden_pedido = "";
if(!empty($datos_presupuesto[0]['num_pre_pers'])):
    $num_pres_aux = $datos_presupuesto[0]['num_pre_pers'];
    else:
    $num_pres_aux = $datos_presupuesto[0]['num_pre'];
endif;
if($is_pedido == 1):
    $class_hidden_pedido = "hidden";
    $num_presupuesto = hideNumPed($num_pres_aux);
else:
    $num_presupuesto = hideNumPre($num_pres_aux);
endif;
$datos_cliente = $cliente->get_cliente($_SESSION['clproc'], $_SESSION['clcodi']);
$nombre_cliente = $datos_cliente[0]['clnomb'];
$fecha = $datos_presupuesto[0]['fecha'];
$fecha = date("d-m-Y", strtotime($fecha));
$fecha_condiciones = date("Ym", strtotime($fecha));
$total_presupuesto = $datos_presupuesto[0]['total'];
$tipo_iva = $datos_cliente[0]['cltipi'];
$datos_iva = $presupuesto->getIva($tipo_iva);
$iva = "";
if($tipo_iva != "E"):
    $iva = $datos_iva['iva'];
    $recargo = $datos_iva['rec_eq'];
    $iva = str_replace(".","", $iva);
    $iva = "0.".$iva;
    $recargo = str_replace(".","", $recargo);
    $recargo = "0.".$recargo;
endif;

$forma_pago = $datos_cliente['0']['clforp'];
$datos_forma_pago = $presupuesto->getFormaPago($forma_pago);
$texto_forma_pago = $datos_forma_pago['fpdsfp'];

$aplazamiento = $datos_cliente['0']['clncfr'];
$datos_aplazamiento = $presupuesto->getAplazamiento($aplazamiento);
$texto_aplazamiento = $datos_aplazamiento['APDENC'];

$fecha_validez = "";
$fecha_validez = strtotime ( '+1 month' , strtotime ( $fecha ) ) ;
$fecha_validez = date ( 'd.m.Y' , $fecha_validez );

$dire_aux = "";
if($datos_presupuesto[0]['dire_envio'] == "fiscal"):
    $dire_aux = $cliente->get_direccion_envio_pdf($_SESSION['clproc'],$_SESSION['clcodi']);
elseif($datos_presupuesto[0]['dire_envio'] == "0"):
    $dire_aux = $cliente->get_direccion_recogida();
else:
    if (empty($datos_presupuesto[0]['is_dire_fija'])):
    $dire_aux = $cliente->get_direccion_envio_id_pdf($datos_presupuesto[0]['dire_envio']);
    else:
        $dire_aux = $cliente->get_direccion_envio_is_fija($_SESSION['clproc'],$_SESSION['clcodi'], $datos_presupuesto[0]['dire_envio']);
    endif;
endif;

$dire = $dire_aux[0];

$dire_fiscal = $cliente->get_direccion_envio($_SESSION['clproc'],$_SESSION['clcodi'])[0];
$pais_fiscal = $cliente->getPais($dire_fiscal['clpais']);
$provincia_fiscal = $cliente->getProvincia($dire_fiscal['clpais'], $dire_fiscal['clprov']);
$poblacion_fiscal = $cliente->getPoblacion($dire_fiscal['clpais'], $dire_fiscal['clprov'], $dire_fiscal['clpobl']);


$pais = $cliente->getPais($dire['clpais']);
$provincia = $cliente->getProvincia($dire['clpais'], $dire['clprov']);
$poblacion = $cliente->getPoblacion($dire['clpais'], $dire['clprov'], $dire['clpobl']);

$agencia = $datos_presupuesto[0]['agencia'];
if($agencia != "rsa" OR $agencia != "rec"):
    $agencia = $datos_cliente[0]['claget'];
else:
    $agencia = $datos_presupuesto[0]['agencia'];
endif;

$albaranes = $presupuesto->get_albaranes($id_pres);

if(isset($_GET['order'])):
$albaranes = ordernar_array_albaranes_impresion($albaranes);
endif;

$total_presupuesto = 0.00;

$num_lineas_presupuesto = 0;
$saltos_pagina = 1;

$total_conceptos = $presupuesto->count_concepto($id_pres);
$total_albaranes = $presupuesto->count_albaranes($id_pres);
$total_lineas = $total_conceptos + $total_albaranes;
$consciente = $total_lineas / 32;
$total_page = ceil($consciente);

$array_conceptos_pedidos = $presupuesto->get_conceptos_pedido($id_pres);