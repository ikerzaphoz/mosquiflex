<span class="info_firma <?= $class_hidden_pedido ?>">
    LA FIRMA DE ESTE DOCUMENTO IMPLICA LA ACEPTACIÓN DEL DETALLE TOTAL DEL PRODUCTO, FORMA DE PAGO E IMPORTE
</span>
<hr width="106.6%" class="<?= $class_hidden_pedido ?>">
<div class="content_precios <?= $class_hidden_pedido ?>">
    <div class="content_precios_line1 line">
        <div class="line1_1 line">I.V.A</div>
        <div class="line1_2 line">RECARGO EQUIVALENCIA</div>
    </div>
    <br>
    <div class="content_precios_line2 line">
        <div class="line2_1 line">BASE IMPONIBLE</div>
        <div class="line2_2 line">%</div>
        <div class="line2_3 line">CUOTA</div>
        <div class="line2_4 line">%</div>
        <div class="line2_5 line">CUOTA</div>
    </div>
    <br>
    <div class="content_precios_line3 line">
        <div class="line3_1 line text-center"><span><?= $total_presupuesto ?></span></div>
        <div class="line3_2 line text-center"><span>21,0</span></div>
        <div class="line3_3 line text-right"><span><?= $total_presupuesto_iva ?></span></div>
        <div class="line3_4 line text-center"><span>5,20</span></div>
        <div class="line3_5 line text-right"><span><?= $total_presupuesto_recargo ?></span></div>
    </div>
    <div class="total_presupuesto">
        <span>TOTAL PRESUPUESTO</span>
        <div class="border_total_presupuesto">
            <span><?= $total_presupuesto_impuestos ?>€</span>
        </div>
    </div>
</div>
<div class="file_2_total <?= $class_hidden_pedido ?>">
    <div class="file_2_w50">
        <div class="border_text texto_validez <?= $class_hidden_pedido ?>">
            LA PRESENTE OFERTA TIENE UNA VALIDEZ HASTA EL <?= $fecha_validez ?>
        </div>
        <div class="border_text texto_pago <?= $class_hidden_pedido ?>">
            FORMA DE PAGO:
        </div>
    </div>
    <div class="file_2_w50_2 <?= $class_hidden_pedido ?>">
        <div class="border_text texto_conforme">
            CONFORME CLIENTE
        </div>
        <div class="border_text texto_sello <?= $class_hidden_pedido ?>">
            MOSQUITERAS TÉCNICAS, S.L.
        </div>
    </div>
</div>
<?php if ($existe_expositor == 1): ?>
    <div>Los precios de expositores y muestras son NETOS</div>
<?php endif; ?>
</div>