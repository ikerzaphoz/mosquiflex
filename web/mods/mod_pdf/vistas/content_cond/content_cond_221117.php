<div class="content">
            <div class="content_texto_lateral">
                <img class="img_texto" src="img_pdf/texto_lateral_plantilla.png">
            </div>

            <table class="table_pdf" style="text-align: center;">
                <tr>
                    <td class="columna1_cabecera table_border_left table_border_bottom table_border_top title_info_pedido">
                        CANTIDAD
                    </td>
                    <td class="columna2_cabecera table_border_bottom table_border_top title_info_pedido">ARTÍCULO</td>
                    <td class="columna3_cabecera table_border_bottom table_border_top title_info_pedido">ANCHO</td>
                    <td class="columna4_cabecera table_border_bottom table_border_top title_info_pedido">ALTO</td>
                    <td class="columna5_cabecera table_border_bottom table_border_top title_info_pedido">MANDOS</td>
                    <td class="columna6_cabecera table_border_bottom table_border_top title_info_pedido">REP</td>
                    <td class="columna7_cabecera table_border_bottom table_border_top title_info_pedido">LM</td>
                    <td class="columna8_cabecera table_border_bottom table_border_top title_info_pedido">TOTAL M<SUP>2</SUP>
                    </td>
                    <td class="columna9_cabecera table_border_bottom table_border_top title_info_pedido <?= $class_hidden_pedido ?>">PRECIO
                        UNIDAD/M<SUP>2</SUP></td>
                    <td class="columna10_cabecera table_border_bottom table_border_top title_info_pedido <?= $class_hidden_pedido ?>">DTO.</td>
                    <td class="columna11_cabecera table_border_bottom table_border_top title_info_pedido table_border_right <?= $class_hidden_pedido ?>">
                        IMPORTE
                    </td>
                </tr>
<?php foreach ($albaranes as $item): ?>
    <?php
    //Producto acabado
    if ($item['albanc'] > 0 AND empty($item['albobs']) AND $item['albfami'] != "1999" OR ( $item['albfami'] == "2736")):

        $title_product = $familia->getTitleProduct($item['albfami'], $item['albsub']);
        $color = "";
        if (isset($familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'])):
            $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];
        else:
            $color = $item['albco'];
        endif;

        $color = ucfirst(str_replace("Lacado ", "", $color));

        $precio_unitario = round2decimals($item['albpre']) / $item['albunit'];
        $total_importe = round2decimals($precio_unitario) * $item['albunit'];
        $total_importe = round2decimals($total_importe);


        $array_condiciones = $presupuesto->getCondicionesEspeciales($_SESSION['clprov'], $_SESSION['clcodi'], $item['albfami']);
        $dto = "";
        if (isset($array_condiciones[0]['cedtoa'])):
            $dto = $array_condiciones[0]['cedtoa'];
        endif;

        if (!isset($dto)):
            $total_presupuesto = $total_presupuesto + round2decimals($total_importe);
        else:
            $total_presupuesto = $total_presupuesto + calc_dto($total_importe, $dto);
        endif;

        //Ultima linea antes del salto
        if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0):
            ?>

                            <tr>
                                <td class="columna1 table_border_left table_border_bottom"><?= $item['albunit'] ?></td>
                                <td class="columna2 table_border_right table_border_bottom"><?= $title_product ?> <?= $color ?></td>
                                <td class="columna3 table_border_right table_border_bottom"><?= number_format($item['albanc'], 3) ?></td>
                                <td class="columna4 table_border_right table_border_bottom"><?= number_format($item['albalt'], 3) ?></td>
                                <td class="columna5 table_border_right table_border_bottom"></td>
                                <td class="columna6 table_border_right table_border_bottom"></td>
                                <td class="columna7 table_border_right table_border_bottom"></td>
                                <td class="columna8 table_border_right table_border_bottom"></td>
                                <td class="columna9 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= round2decimals($precio_unitario) ?></td>
                                <td class="columna10 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= $dto ?></td>
            <?php if (isset($dto)): ?>
                                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"><?= calc_dto($total_importe, $dto); ?></td>
            <?php else: ?>
                                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"></td>
                                <?php endif; ?>
                            </tr>

                            <?php else: ?>

                            <tr>
                                <td class="columna1 table_border_left table_border_right"><?= $item['albunit'] ?></td>
                                <td class="columna2 table_border_right"><?= $title_product ?> <?= $color ?></td>
                                <td class="columna3 table_border_right "><?= number_format($item['albanc'], 3) ?></td>
                                <td class="columna4 table_border_right "><?= number_format($item['albalt'], 3) ?></td>
                                <td class="columna5 table_border_right "></td>
                                <td class="columna6 table_border_right "></td>
                                <td class="columna7 table_border_right "></td>
                                <td class="columna8 table_border_right "></td>
                                <td class="columna9 table_border_right <?= $class_hidden_pedido ?>"><?= round2decimals($precio_unitario) ?></td>
                                <td class="columna10 table_border_right <?= $class_hidden_pedido ?>"><?= $dto ?></td>
            <?php if (isset($dto)): ?>
                                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"><?= calc_dto($total_importe, $dto); ?></td>
            <?php else: ?>
                                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"></td>
                                <?php endif; ?>
                            </tr>

                            <?php endif; ?>

    <?php endif; ?>


                    <?php
                    //componente metros
                    if ($item['albobs'] == "is_component_meter"):

                        $aux_title = str_replace('279990', '', $item['albco']);
                        $title_component = $familia->getTitleComponent($item['albfami'], $item['albsub'], $aux_title);

                        $version_product = getCajon($item['albsub']);
                        $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];

                        $total_importe = round2decimals($item['albpre']) * $item['albunit'] * $item['albanc'];
                        $total_importe = round2decimals($total_importe);

                        $is_almacen = "";
                        if (isset($item['is_almacen'])):
                            $is_almacen = $item['is_almacen'];
                        endif;

                        $array_condiciones = $presupuesto->getCondicionesEspeciales($_SESSION['clprov'], $_SESSION['clcodi'], $item['albfami'], $is_almacen);
                        $dto = "";
                        if (isset($array_condiciones[0]['cedtoa'])):
                            $dto = $array_condiciones[0]['cedtoa'];
                        endif;

                        if (!isset($dto)):
                            $total_presupuesto = $total_presupuesto + round2decimals($total_importe);
                        else:
                            $total_presupuesto = $total_presupuesto + calc_dto($total_importe, $dto);
                        endif;

                        //Ultima linea
                        if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0):
                            ?>

                            <tr>
                                <td class="columna1 table_border_left table_border_bottom"><?= $item['albunit'] ?></td>
                                <td class="columna2 table_border_right table_border_bottom"><?= $title_component ?></td>
                                <td class="columna3 table_border_right table_border_bottom"><?= $item['albanc'] ?></td>
                                <td class="columna4 table_border_right table_border_bottom"></td>
                                <td class="columna5 table_border_right table_border_bottom"></td>
                                <td class="columna6 table_border_right table_border_bottom"></td>
                                <td class="columna7 table_border_right table_border_bottom"></td>
                                <td class="columna8 table_border_right table_border_bottom"></td>
                                <td class="columna9 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= round2decimals($item['albpre']) ?></td>
                                <td class="columna10 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= $dto ?></td>
            <?php if (isset($dto)): ?>
                                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"><?= calc_dto($total_importe, $dto); ?></td>
                                <?php else: ?>
                                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"></td>
                                <?php endif; ?>
                            </tr>

        <?php else: ?>

                            <tr>

                                <td class="columna1 table_border_left table_border_right"><?= $item['albunit'] ?></td>
                                <td class="columna2 table_border_right"><?= $title_component ?></td>
                                <td class="columna3 table_border_right "><?= $item['albanc'] ?></td>
                                <td class="columna4 table_border_right "></td>
                                <td class="columna5 table_border_right "></td>
                                <td class="columna6 table_border_right "></td>
                                <td class="columna7 table_border_right "></td>
                                <td class="columna8 table_border_right "></td>
                                <td class="columna9 table_border_right <?= $class_hidden_pedido ?>"><?= round2decimals($item['albpre']) ?></td>
                                <td class="columna10 table_border_right <?= $class_hidden_pedido ?>"><?= $dto ?></td>
            <?php if (isset($dto)): ?>
                                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"><?= calc_dto($total_importe, $dto); ?></td>
                                <?php else: ?>
                                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"></td>
                                <?php endif; ?>
                            </tr>

        <?php endif; ?>

                    <?php endif; ?>

                    <?php
                    //componente metros
                    if ($item['albobs'] == "is_component"):

                        $title_component = str_replace('279990', '', $item['albco']);
                        $title_component = $familia->getTitleComponent($item['albfami'], $item['albsub'], $title_component);
                        $version_product = getCajon($item['albsub']);
                        $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];

                        $total_importe = round2decimals($item['albpre']) * $item['albunit'];
                        $total_importe = round2decimals($total_importe);

                        $is_almacen = "";
                        if (isset($item['is_almacen'])):
                            $is_almacen = $item['is_almacen'];
                        endif;

                        $array_condiciones = $presupuesto->getCondicionesEspeciales($_SESSION['clprov'], $_SESSION['clcodi'], $item['albfami'], $is_almacen);
                        $dto = "";
                        if (isset($array_condiciones[0]['cedtoa'])):
                            $dto = $array_condiciones[0]['cedtoa'];
                        endif;

                        if (!isset($dto)):
                            $total_presupuesto = $total_presupuesto + round2decimals($total_importe);
                        else:
                            $total_presupuesto = $total_presupuesto + calc_dto($total_importe, $dto);
                        endif;

                        //Ultima linea
                        if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0):
                            ?>

                            <tr>
                                <td class="columna1 table_border_left table_border_bottom"><?= $item['albunit'] ?></td>
                                <td class="columna2 table_border_right table_border_bottom"><?= $title_component ?></td>
                                <td class="columna3 table_border_right table_border_bottom"></td>
                                <td class="columna4 table_border_right table_border_bottom"></td>
                                <td class="columna5 table_border_right table_border_bottom"></td>
                                <td class="columna6 table_border_right table_border_bottom"></td>
                                <td class="columna7 table_border_right table_border_bottom"></td>
                                <td class="columna8 table_border_right table_border_bottom"></td>
                                <td class="columna9 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= round2decimals($item['albpre']) ?></td>
                                <td class="columna10 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= $dto ?></td>
                                <?php if (isset($dto)): ?>
                                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"><?= calc_dto($total_importe, $dto); ?></td>
                                <?php else: ?>
                                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"></td>
                                <?php endif; ?>
                            </tr>

                        <?php else: ?>

                            <tr>

                                <td class="columna1 table_border_left table_border_right"><?= $item['albunit'] ?></td>
                                <td class="columna2 table_border_right"><?= $title_component ?></td>
                                <td class="columna3 table_border_right "></td>
                                <td class="columna4 table_border_right "></td>
                                <td class="columna5 table_border_right "></td>
                                <td class="columna6 table_border_right "></td>
                                <td class="columna7 table_border_right "></td>
                                <td class="columna8 table_border_right "></td>
                                <td class="columna9 table_border_right <?= $class_hidden_pedido ?>"><?= round2decimals($item['albpre']) ?></td>
                                <td class="columna10 table_border_right <?= $class_hidden_pedido ?>"><?= $dto ?></td>
                                <?php if (isset($dto)): ?>
                                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"><?= calc_dto($total_importe, $dto); ?></td>
                                <?php else: ?>
                                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"></td>
                                <?php endif; ?>
                            </tr>

                        <?php endif; ?>

                    <?php endif; ?>

                    <?php
                    //incremento
                    if (strpos($item['albobs'], "is_increment") !== false):

                        $aux_id = str_replace("is_increment", "", $item['albobs']);
                        $arfami = substr($aux_id, 0, 4);
                        $arsubf = substr($aux_id, 4, 2);
                        $ararti = substr($aux_id, 6, 6);

                        $precio_unitario = round2decimals($item['albpre']) / $item['albunit'];
                        $precio_unitario = round2decimals($precio_unitario);

                        $total_importe = round2decimals($precio_unitario) * $item['albunit'];
                        $total_importe = round2decimals($total_importe);

                        $componente = $familia->searchComponentByCode($arfami, $arsubf, $ararti);

                        if (isset($componente[0]['ardesc'])):
                            $title_component = $componente[0]['ardesc'];
                        endif;

                        $array_condiciones = $presupuesto->getCondicionesEspeciales($_SESSION['clprov'], $_SESSION['clcodi'], $item['albfami']);
                        $dto = "";
                        if (isset($array_condiciones[0]['cedtoa'])):
                            $dto = $array_condiciones[0]['cedtoa'];
                        endif;

                        if (!isset($dto)):
                            $total_presupuesto = $total_presupuesto + round2decimals($total_importe);
                        else:
                            $total_presupuesto = $total_presupuesto + calc_dto($total_importe, $dto);
                        endif;

                        //Ultima linea
                        if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0):
                            ?>

                            <tr>
                                <td class="columna1 table_border_left table_border_bottom"><?= $item['albunit'] ?></td>
                                <td class="columna2 table_border_right table_border_bottom"><?= $title_component ?></td>
                                <td class="columna3 table_border_right table_border_bottom"></td>
                                <td class="columna4 table_border_right table_border_bottom"></td>
                                <td class="columna5 table_border_right table_border_bottom"></td>
                                <td class="columna6 table_border_right table_border_bottom"></td>
                                <td class="columna7 table_border_right table_border_bottom"></td>
                                <td class="columna8 table_border_right table_border_bottom"></td>
                                <td class="columna9 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= $precio_unitario ?></td>
                                <td class="columna10 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= $dto ?></td>
                                <?php if (isset($dto)): ?>
                                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"><?= calc_dto($total_importe, $dto); ?></td>
                                <?php else: ?>
                                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"></td>
                            <?php endif; ?>                </tr>

        <?php else: ?>

                            <tr>

                                <td class="columna1 table_border_left table_border_right"><?= $item['albunit'] ?></td>
                                <td class="columna2 table_border_right"><?= $title_component ?></td>
                                <td class="columna3 table_border_right "></td>
                                <td class="columna4 table_border_right "></td>
                                <td class="columna5 table_border_right "></td>
                                <td class="columna6 table_border_right "></td>
                                <td class="columna7 table_border_right "></td>
                                <td class="columna8 table_border_right "></td>
                                <td class="columna9 table_border_right <?= $class_hidden_pedido ?>"><?= $precio_unitario ?></td>
                                <td class="columna10 table_border_right <?= $class_hidden_pedido ?>"><?= $dto ?></td>
                                <?php if (isset($dto)): ?>
                                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"><?= calc_dto($total_importe, $dto); ?></td>
                                <?php else: ?>
                                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"></td>
                            <?php endif; ?>
                            </tr>

                        <?php endif; ?>

                    <?php endif; ?>

                    <?php
                    //Expositores
                    if ($item['albfami'] == "1999" AND ! empty($item['albunit'])):

                        $title_product = obtener_titulo_expositor($item['albarti']);
                        $color = "-";

                        $precio_unitario = round2decimals($item['albpre']);

                        $total_importe = round2decimals($precio_unitario) * $item['albunit'];
                        $total_importe = round2decimals($total_importe);

                        $total_presupuesto = $total_presupuesto + round2decimals($total_importe);


                        //Ultima linea antes del salto
                        if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0):
                            ?>

                            <tr>
                                <td class="columna1 table_border_left table_border_bottom"><?= $item['albunit'] ?></td>
                                <td class="columna2 table_border_right table_border_bottom"><?= $title_product ?> </td>
                                <td class="columna3 table_border_right table_border_bottom"><?= number_format($item['albanc'], 3) ?></td>
                                <td class="columna4 table_border_right table_border_bottom"><?= number_format($item['albalt'], 3) ?></td>
                                <td class="columna5 table_border_right table_border_bottom"></td>
                                <td class="columna6 table_border_right table_border_bottom"></td>
                                <td class="columna7 table_border_right table_border_bottom"></td>
                                <td class="columna8 table_border_right table_border_bottom"></td>
                                <td class="columna9 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= round2decimals($precio_unitario) ?></td>
                                <td class="columna10 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"></td>
                                <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"><?= $total_importe ?></td>
                            </tr>

        <?php else: ?>

                            <tr>
                                <td class="columna1 table_border_left table_border_right"><?= $item['albunit'] ?></td>
                                <td class="columna2 table_border_right"><?= $title_product ?> </td>
                                <td class="columna3 table_border_right "><?= number_format($item['albanc'], 3) ?></td>
                                <td class="columna4 table_border_right "><?= number_format($item['albalt'], 3) ?></td>
                                <td class="columna5 table_border_right "></td>
                                <td class="columna6 table_border_right "></td>
                                <td class="columna7 table_border_right "></td>
                                <td class="columna8 table_border_right "></td>
                                <td class="columna9 table_border_right <?= $class_hidden_pedido ?>"><?= round2decimals($precio_unitario) ?></td>
                                <td class="columna10 table_border_right <?= $class_hidden_pedido ?>"></td>
                                <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"><?= $total_importe ?></td>
                            </tr>

                        <?php endif; ?>

                    <?php endif; ?>

                    <?php
                    //componente unidad
                    if ($item['albobs'] == "is_component_unit"):

                        $title_component = str_replace('279990', '', $item['albco']);
                        $title_component = $familia->getTitleComponent($item['albfami'], $item['albsub'], $title_component);
                        $version_product = getCajon($item['albsub']);
                        $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];

                        $total_importe = $item['albanc'] * $item['albalt'];
                        $total_importe = round2decimals($total_importe) * round2decimals($item['albpre']) * $item['albunit'];
                        $total_importe = round2decimals($total_importe);

                        $is_almacen = "";
                        if (isset($item['is_almacen'])):
                            $is_almacen = $item['is_almacen'];
                        endif;

                        $array_condiciones = $presupuesto->getCondicionesEspeciales($_SESSION['clprov'], $_SESSION['clcodi'], $item['albfami'], $is_almacen);
                        $dto = "";
                        if (isset($array_condiciones[0]['cedtoa'])):
                            $dto = $array_condiciones[0]['cedtoa'];
                        endif;

                        if (!isset($dto)):
                            $total_presupuesto = $total_presupuesto + round2decimals($total_importe);
                        else:
                            $total_presupuesto = $total_presupuesto + calc_dto($total_importe, $dto);
                        endif;

                        if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0):
                            ?>

                            <tr>

                                <td class="columna1 table_border_left table_border_bottom"><?= $item['albunit'] ?></td>
                                <td class="columna2 table_border_right table_border_bottom"><?= $title_component ?></td>
                                <td class="columna3 table_border_right table_border_bottom"><?= $item['albanc'] ?></td>
                                <td class="columna4 table_border_right table_border_bottom"><?= $item['albalt'] ?></td>
                                <td class="columna5 table_border_right table_border_bottom"></td>
                                <td class="columna6 table_border_right table_border_bottom"></td>
                                <td class="columna7 table_border_right table_border_bottom"></td>
                                <td class="columna8 table_border_right table_border_bottom"></td>
                                <td class="columna9 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= round2decimals($item['albpre']) ?></td>
                                <td class="columna10 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= $dto ?></td>
                                <?php if (isset($dto)): ?>
                                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"><?= calc_dto($total_importe, $dto); ?></td>
                            <?php else: ?>
                                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"></td>
            <?php endif; ?>
                            </tr>

        <?php else: ?>

                            <tr>

                                <td class="columna1 table_border_left table_border_right"><?= $item['albunit'] ?></td>
                                <td class="columna2 table_border_right"><?= $title_component ?></td>
                                <td class="columna3 table_border_right "><?= $item['albanc'] ?></td>
                                <td class="columna4 table_border_right "><?= $item['albalt'] ?></td>
                                <td class="columna5 table_border_right "></td>
                                <td class="columna6 table_border_right "></td>
                                <td class="columna7 table_border_right "></td>
                                <td class="columna8 table_border_right "></td>
                                <td class="columna9 table_border_right <?= $class_hidden_pedido ?>"><?= round2decimals($item['albpre']) ?></td>
                                <td class="columna10 table_border_right <?= $class_hidden_pedido ?>"><?= $dto ?></td>
                                <?php if (isset($dto)): ?>
                                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"><?= calc_dto($total_importe, $dto); ?></td>
                            <?php else: ?>
                                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"></td>
                            <?php endif; ?>                </tr>

                        <?php endif; ?>

                    <?php
                    endif;

                    //lacado
                    if ($item['albobs'] == "lacado"):

                        $color = "";
                        $color = $item['albco'];
                        $unidades = $item['albunit'];
                        $precio = round2decimals($item['albpre']);

                        $array_condiciones = $presupuesto->getCondicionesEspeciales($_SESSION['clprov'], $_SESSION['clcodi'], $item['albfami'], $is_almacen);
                        $dto = "";
                        if (isset($array_condiciones[0]['cedtoa'])):
                            $dto = $array_condiciones[0]['cedtoa'];
                        endif;

                        if (!isset($dto)):
                            $total_presupuesto = $total_presupuesto + round2decimals($precio);
                        else:
                            $total_presupuesto = $total_presupuesto + calc_dto($precio, $dto);
                        endif;

                        $total_importe = $precio;

                        if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0):
                            ?>

                            <tr>

                                <td class="columna1 table_border_left table_border_bottom"><?= $unidades ?></td>
                                <td class="columna2 table_border_right table_border_bottom">Lacado -
                                    RAL. <?= $version_product ?> <?= $color ?></td>
                                <td class="columna3 table_border_right table_border_bottom"></td>
                                <td class="columna4 table_border_right table_border_bottom"></td>
                                <td class="columna5 table_border_right table_border_bottom"></td>
                                <td class="columna6 table_border_right table_border_bottom"></td>
                                <td class="columna7 table_border_right table_border_bottom"></td>
                                <td class="columna8 table_border_right table_border_bottom"></td>
                                <td class="columna9 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= round2decimals($item['albpre']) ?></td>
                                <td class="columna10 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= $dto ?></td>
            <?php if (isset($dto)): ?>
                                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"><?= calc_dto($total_importe, $dto); ?></td>
                            <?php else: ?>
                                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"></td>
            <?php endif; ?>

                            </tr>

        <?php else: ?>

                            <tr>

                                <td class="columna1 table_border_left table_border_right"><?= $unidades ?></td>
                                <td class="columna2 table_border_right">Lacado -
                                    RAL. <?= $version_product ?> <?= $color ?></td>
                                <td class="columna3 table_border_right "></td>
                                <td class="columna4 table_border_right "></td>
                                <td class="columna5 table_border_right "></td>
                                <td class="columna6 table_border_right "></td>
                                <td class="columna7 table_border_right "></td>
                                <td class="columna8 table_border_right "></td>
                                <td class="columna9 table_border_right <?= $class_hidden_pedido ?>"><?= round2decimals($item['albpre']) ?></td>
                                <td class="columna10 table_border_right <?= $class_hidden_pedido ?>"><?= $dto ?></td>
                            <?php if (isset($dto)): ?>
                                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"><?= calc_dto($total_importe, $dto); ?></td>
                            <?php else: ?>
                                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"></td>
                            <?php endif; ?>
                            </tr>

                        <?php endif; ?>

    <?php
    endif;

    $num_lineas_presupuesto++;

    if ($num_lineas_presupuesto % 32 == 0):
        ?>
        <?php $saltos_pagina++; ?>
                    </table>
                    <div class="text_continua">Continua...</div>
                    <div style="page-break-after: always;"></div>
                    <div class="content_texto_lateral">
                        <div class="img_texto img_texto_break"></div>
                    </div>
                    <table class="table_pdf break_page" style="text-align: center;">
                        <tr>
                            <td class="columna1_cabecera table_border_left table_border_bottom table_border_top title_info_pedido">
                                CANTIDAD
                            </td>
                            <td class="columna2_cabecera table_border_bottom table_border_top title_info_pedido">ARTÍCULO</td>
                            <td class="columna3_cabecera table_border_bottom table_border_top title_info_pedido">ANCHO</td>
                            <td class="columna4_cabecera table_border_bottom table_border_top title_info_pedido">ALTO</td>
                            <td class="columna5_cabecera table_border_bottom table_border_top title_info_pedido">MANDOS</td>
                            <td class="columna6_cabecera table_border_bottom table_border_top title_info_pedido">REP</td>
                            <td class="columna7_cabecera table_border_bottom table_border_top title_info_pedido">LM</td>
                            <td class="columna8_cabecera table_border_bottom table_border_top title_info_pedido">TOTAL M<SUP>2</SUP>
                            </td>
                            <td class="columna9_cabecera table_border_bottom table_border_top title_info_pedido <?= $class_hidden_pedido ?>">PRECIO
                                UNIDAD/M<SUP>2</SUP></td>
                            <td class="columna10_cabecera table_border_bottom table_border_top title_info_pedido <?= $class_hidden_pedido ?>">DTO.</td>
                            <td class="columna11_cabecera table_border_bottom table_border_top title_info_pedido table_border_right <?= $class_hidden_pedido ?>">
                                IMPORTE
                            </td>
                        </tr>
                    <?php endif;

                endforeach;
                ?>

                <?php
                $total_presupuesto_iva = round2decimals($total_presupuesto * $iva);
                $total_presupuesto_recargo = round2decimals($total_presupuesto * 0.520);
                $total_presupuesto_impuestos = $total_presupuesto + $total_presupuesto_iva + $total_presupuesto_recargo;
                $aux_lineas = $saltos_pagina * 32;

                if ($class_hidden_pedido == "hidden"):
                    $aux_lineas = $aux_lineas + 10;
                endif;

                //LINEAS EN BLANCO
                for ($i = $num_lineas_presupuesto; $i < $aux_lineas; $i++):
                    ?>
                    <tr>
                        <td class="columna1 table_border_left table_border_right">&nbsp;</td>
                        <td class="columna2 table_border_right">&nbsp;</td>
                        <td class="columna3 table_border_right ">&nbsp;</td>
                        <td class="columna4 table_border_right ">&nbsp;</td>
                        <td class="columna5 table_border_right ">&nbsp;</td>
                        <td class="columna6 table_border_right ">&nbsp;</td>
                        <td class="columna7 table_border_right ">&nbsp;</td>
                        <td class="columna8 table_border_right ">&nbsp;</td>
                        <td class="columna9 table_border_right <?= $class_hidden_pedido ?> ">&nbsp;</td>
                        <td class="columna10 table_border_right <?= $class_hidden_pedido ?>">&nbsp;</td>
                        <td class="columna11 table_border_right <?= $class_hidden_pedido ?>">&nbsp;</td>
                    </tr>
<?php endfor;
?>

                <tr>
                    <td class="columna1 table_border_left table_border_bottom table_border_right">&nbsp;</td>
                    <td class="columna2 table_border_bottom table_border_right">&nbsp;</td>
                    <td class="columna3 table_border_bottom table_border_right ">&nbsp;</td>
                    <td class="columna4 table_border_bottom table_border_right ">&nbsp;</td>
                    <td class="columna5 table_border_bottom table_border_right ">&nbsp;</td>
                    <td class="columna6 table_border_bottom table_border_right ">&nbsp;</td>
                    <td class="columna7 table_border_bottom table_border_right ">&nbsp;</td>
                    <td class="columna8 table_border_bottom table_border_right ">&nbsp;</td>
                    <td class="columna9 table_border_bottom table_border_right <?= $class_hidden_pedido ?>">&nbsp;</td>
                    <td class="columna10 table_border_bottom table_border_right <?= $class_hidden_pedido ?>">&nbsp;</td>
                    <td class="columna11 table_border_bottom table_border_right <?= $class_hidden_pedido ?>">&nbsp;</td>
                </tr>
            </table>
            <span class="info_firma <?= $class_hidden_pedido ?>">
                LA FIRMA DE ESTE DOCUMENTO IMPLICA LA ACEPTACIÓN DEL DETALLE TOTAL DEL PRODUCTO, FORMA DE PAGO E IMPORTE
            </span>
            <hr width="106.6%" class="<?= $class_hidden_pedido ?>">
            <div class="content_precios <?= $class_hidden_pedido ?>">
                <div class="content_precios_line1 line">
                    <div class="line1_1 line">I.V.A</div>
                    <div class="line1_2 line">RECARGO EQUIVALENCIA</div>
                </div>
                <br>
                <div class="content_precios_line2 line">
                    <div class="line2_1 line">BASE IMPONIBLE</div>
                    <div class="line2_2 line">%</div>
                    <div class="line2_3 line">CUOTA</div>
                    <div class="line2_4 line">%</div>
                    <div class="line2_5 line">CUOTA</div>
                </div>
                <br>
                <div class="content_precios_line3 line">
                    <div class="line3_1 line text-center"><span><?= $total_presupuesto ?></span></div>
                    <div class="line3_2 line text-center"><span>21,0</span></div>
                    <div class="line3_3 line text-right"><span><?= $total_presupuesto_iva ?></span></div>
                    <div class="line3_4 line text-center"><span>5,20</span></div>
                    <div class="line3_5 line text-right"><span><?= $total_presupuesto_recargo ?></span></div>
                </div>
                <div class="total_presupuesto">
                    <span>TOTAL PRESUPUESTO</span>
                    <div class="border_total_presupuesto">
                        <span><?= $total_presupuesto_impuestos ?>€</span>
                    </div>
                </div>
            </div>
            <div class="file_2_total <?= $class_hidden_pedido ?>">
                <div class="file_2_w50">
                    <div class="border_text texto_validez <?= $class_hidden_pedido ?>">
                        LA PRESENTE OFERTA TIENE UNA VALIDEZ HASTA EL <?= $fecha_validez ?>
                    </div>
                    <div class="border_text texto_pago <?= $class_hidden_pedido ?>">
                        FORMA DE PAGO:
                    </div>
                </div>
                <div class="file_2_w50_2 <?= $class_hidden_pedido ?>">
                    <div class="border_text texto_conforme">
                        CONFORME CLIENTE
                    </div>
                    <div class="border_text texto_sello <?= $class_hidden_pedido ?>">
                        MOSQUITERAS TÉCNICAS, S.L.
                    </div>
                </div>
            </div>
        </div>