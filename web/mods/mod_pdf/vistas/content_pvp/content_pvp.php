<div class="content">
    <div class="content_texto_lateral">
        <img class="img_texto" src="img_pdf/texto_lateral_plantilla.png">
    </div>

    <table class="table_pdf" style="text-align: center;">
        <tr>
            <td class="columna1_cabecera table_border_left table_border_bottom table_border_top title_info_pedido">
                CANTIDAD
            </td>
            <td class="columna2_cabecera table_border_bottom table_border_top title_info_pedido">ARTÍCULO</td>
            <td class="columna3_cabecera table_border_bottom table_border_top title_info_pedido">ANCHO</td>
            <td class="columna4_cabecera table_border_bottom table_border_top title_info_pedido">ALTO</td>
            <td class="columna5_cabecera table_border_bottom table_border_top title_info_pedido">MANDOS</td>
            <td class="columna6_cabecera table_border_bottom table_border_top title_info_pedido">REP</td>
            <td class="columna7_cabecera table_border_bottom table_border_top title_info_pedido">LM</td>
            <td class="columna8_cabecera table_border_bottom table_border_top title_info_pedido">TOTAL M<SUP>2</SUP>
            </td>
            <td class="columna9_cabecera table_border_bottom table_border_top title_info_pedido <?= $class_hidden_pedido ?>">
                PRECIO UNIDAD/M<SUP>2</SUP></td>
            <td class="columna10_cabecera table_border_bottom table_border_top title_info_pedido <?= $class_hidden_pedido ?>">
                DTO.
            </td>
            <td class="columna11_cabecera table_border_bottom table_border_top title_info_pedido table_border_right <?= $class_hidden_pedido ?>">
                IMPORTE
            </td>
        </tr>
        <?php
        foreach ($albaranes as $item):

            $units_product = "";
            $title_product = "";
            $color = "";
            $ancho_product = "";
            $alto_product = "";
            $precio_unitario = "";
            $total_importe = "";

            //Producto acabado
            if ($item['albanc'] > 0 AND empty($item['albobs']) AND $item['albfami'] != "1999" OR ( $item['albfami'] == "2736")):

                $units_product = $item['albunit'];
                $title_product = $familia->getTitleProduct($item['albfami'], $item['albsub']);
                if ($item['albfami'] == '2736' && !empty($item['albobs'])):
                    $title_product = $title_product . " - " . strtoupper($item['albobs']) . " - ";
                endif;
                $color = "";
                if (isset($familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'])):
                    $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];
                else:
                    $color = $item['albco'];
                endif;

                $color = ucfirst(str_replace("Lacado ", "", $color));

                $is_lacado = $familia->getIsLacado($item['albco'], $item['albfami']);
                if (!empty($is_lacado)):
                    $color = " LACADO " . $color;
                endif;

                $ancho_product = number_format($item['albanc'], 3);
                $alto_product = number_format($item['albalt'], 3);

                $precio_unitario = round2decimals($item['albpre']) / $item['albunit'];
                $precio_unitario = round2decimals($precio_unitario);

                $total_importe = round2decimals($precio_unitario) * $item['albunit'];
                $total_importe = round2decimals($total_importe);

                $total_presupuesto = $total_presupuesto + round2decimals($total_importe);

            //componente metros
            elseif ($item['albobs'] == "is_component_meter"):

                $units_product = $item['albunit'];
                $aux_title = str_replace('279990', '', $item['albco']);
                $title_product = $familia->getTitleComponent($item['albfami'], $item['albsub'], $aux_title);

                $version_product = getCajon($item['albsub']);
                $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];

                $ancho_product = number_format($item['albanc'], 3);

                $precio_unitario = round2decimals($item['albpre']);
                $precio_unitario = round2decimals($precio_unitario);

                $total_importe = $precio_unitario * $units_product * $ancho_product;
                $total_importe = round2decimals($total_importe);

                $total_presupuesto = $total_presupuesto + $total_importe;

            //componente
            elseif ($item['albobs'] == "is_component"):

                $units_product = $item['albunit'];
                $title_product = str_replace('279990', '', $item['albco']);
                $title_product = $familia->getTitleComponent($item['albfami'], $item['albsub'], $title_product);
                $version_product = getCajon($item['albsub']);
                $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];

                $precio_unitario = round2decimals($item['albpre']);
                $precio_unitario = round2decimals($precio_unitario);

                $total_importe = $precio_unitario * $units_product;
                $total_importe = round2decimals($total_importe);

                $total_presupuesto = $total_presupuesto + $total_importe;

            //incremento
            elseif (strpos($item['albobs'], "is_increment") !== false):

                $units_product = $item['albunit'];
                $aux_id = str_replace("is_increment", "", $item['albobs']);
                $aux_id = str_replace('"', "", $aux_id);
                $aux_id = str_replace(' ', "", $aux_id);
                $arfami = substr($aux_id, 0, 4);
                $arsubf = substr($aux_id, 4, 2);
                $ararti = substr($aux_id, 6, 6);
                $componente = $familia->searchComponentByCode($arfami, $arsubf, $ararti);
                $title_product = "";

                if (!empty($item['adjunto'])):
                    $title_product .= "-------¡¡¡OJO CROQUIS!!!-------<br>";
                endif;
                if (isset($componente[0]['ardesc'])):
                    $title_product .= $componente[0]['ardesc'];
                endif;

                $precio_unitario = round2decimals($item['albpre']) / $units_product;
                $precio_unitario = round2decimals($precio_unitario);

                $total_importe = round2decimals($precio_unitario) * $units_product;
                $total_importe = round2decimals($total_importe);

                $total_presupuesto = $total_presupuesto + round2decimals($total_importe);

                if (!empty($item['adjunto'])):
                    $title_product .= "<br>-------¡¡¡FIN CROQUIS!!!-------";
                endif;

            //componente unidad
            elseif ($item['albobs'] == "is_component_unit"):

                $units_product = $item['albunit'];
                $title_product = str_replace('279990', '', $item['albco']);
                $title_product = $familia->getTitleComponent($item['albfami'], $item['albsub'], $title_product);
                $ancho_product = number_format($item['albanc'], 3);
                $alto_product = number_format($item['albalt'], 3);

                $precio_unitario = round2decimals($item['albpre']);
                $precio_unitario = round2decimals($precio_unitario);

                $version_product = getCajon($item['albsub']);
                $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];
                $total_importe = $ancho_product * $alto_product;
                $total_importe = round2decimals($total_importe) * round2decimals($precio_unitario) * $units_product;
                $total_importe = round2decimals($total_importe);

                $total_presupuesto = $total_presupuesto + $total_importe;

            //lacado
            elseif ($item['albobs'] == "lacado"):

                $color = "";
                $color = $item['albco'];
                $units_product = $item['albunit'];
                $precio_unitario = round2decimals($item['albpre']);
                $title_product = "Lacado - RAL. ";
                $total_importe = round2decimals($item['albpre']);

                $total_presupuesto = $total_presupuesto + $total_importe;

            //expositores
            elseif ($item['albfami'] == "1999" AND ! empty($item['albunit'])):

                $units_product = $item['albunit'];
                $title_product = obtener_titulo_expositor($item['albarti']);
                $color = "";

                $ancho_product = number_format($item['albanc'], 3);
                $alto_product = number_format($item['albalt'], 3);

                $precio_unitario = round2decimals($item['albpre']);

                $total_importe = round2decimals($precio_unitario) * $item['albunit'];
                $total_importe = round2decimals($total_importe);

                $total_presupuesto = $total_presupuesto + round2decimals($total_importe);

            //linea3
            elseif ($item['albobs'] == "Linea3"):
                $title_product = "-----------------------------------------------------------------";
            endif;

            //Ultima linea antes del salto
            if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0):
                ?>

                <tr>
                    <td class="columna1 table_border_left table_border_bottom"><?= $units_product ?></td>
                    <td class="columna2 table_border_right table_border_bottom"><?= $title_product ?> <?= $color ?></td>
                    <td class="columna3 table_border_right table_border_bottom"><?= $ancho_product ?></td>
                    <td class="columna4 table_border_right table_border_bottom"><?= $alto_product ?></td>
                    <td class="columna5 table_border_right table_border_bottom"></td>
                    <td class="columna6 table_border_right table_border_bottom"></td>
                    <td class="columna7 table_border_right table_border_bottom"></td>
                    <td class="columna8 table_border_right table_border_bottom"></td>
                    <td class="columna9 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= $precio_unitario ?></td>
                    <td class="columna10 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"></td>
                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"><?= $total_importe ?></td>
                </tr>

            <?php else: ?>

                <tr>
                    <td class="columna1 table_border_left table_border_right"><?= $units_product ?></td>
                    <td class="columna2 table_border_right"><?= $title_product ?> <?= $color ?></td>
                    <td class="columna3 table_border_right "><?= $ancho_product ?></td>
                    <td class="columna4 table_border_right "><?= $alto_product ?></td>
                    <td class="columna5 table_border_right "></td>
                    <td class="columna6 table_border_right "></td>
                    <td class="columna7 table_border_right "></td>
                    <td class="columna8 table_border_right "></td>
                    <td class="columna9 table_border_right <?= $class_hidden_pedido ?>"><?= $precio_unitario ?></td>
                    <td class="columna10 table_border_right <?= $class_hidden_pedido ?>"></td>
                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"><?= $total_importe ?></td>
                </tr>

            <?php
            endif;

            $num_lineas_presupuesto++;

            if ($num_lineas_presupuesto % 32 == 0):
                ?>
                <?php $saltos_pagina++; ?>
            </table>
            <div class="text_continua">Continua...</div>
            <div style="page-break-after: always;"></div>
            <div class="content_texto_lateral">
                <div class="img_texto img_texto_break"></div>
            </div>
            <table class="table_pdf break_page" style="text-align: center;">
                <tr>
                    <td class="columna1_cabecera table_border_left table_border_bottom table_border_top title_info_pedido">
                        CANTIDAD
                    </td>
                    <td class="columna2_cabecera table_border_bottom table_border_top title_info_pedido">ARTÍCULO</td>
                    <td class="columna3_cabecera table_border_bottom table_border_top title_info_pedido">ANCHO</td>
                    <td class="columna4_cabecera table_border_bottom table_border_top title_info_pedido">ALTO</td>
                    <td class="columna5_cabecera table_border_bottom table_border_top title_info_pedido">MANDOS</td>
                    <td class="columna6_cabecera table_border_bottom table_border_top title_info_pedido">REP</td>
                    <td class="columna7_cabecera table_border_bottom table_border_top title_info_pedido">LM</td>
                    <td class="columna8_cabecera table_border_bottom table_border_top title_info_pedido">TOTAL M<SUP>2</SUP>
                    </td>
                    <td class="columna9_cabecera table_border_bottom table_border_top title_info_pedido <?= $class_hidden_pedido ?>">
                        PRECIO UNIDAD/M<SUP>2</SUP></td>
                    <td class="columna10_cabecera table_border_bottom table_border_top title_info_pedido <?= $class_hidden_pedido ?>">
                        DTO.
                    </td>
                    <td class="columna11_cabecera table_border_bottom table_border_top title_info_pedido table_border_right <?= $class_hidden_pedido ?>">
                        IMPORTE
                    </td>
                </tr>
                <?php
            endif;

        endforeach;

        foreach ($array_conceptos_pedidos as $linea_concepto):

            $text_concepto = "";
            $text_concepto = $linea_concepto['texto_concepto'];
            $valor_concepto = "";
            $valor_concepto = $linea_concepto['valor_concepto'];
            $valor_concepto = round2decimals($valor_concepto);
            $tipo_concepto = "";
            $tipo_concepto = $linea_concepto['tipo_concepto'];
            $simbolo = "";
            $aumento_concepto = "";
            $aumento_concepto = $linea_concepto['aumento_concepto'];
            $precio = "";
            //Importe (0) - Porcentaje (1)
            if ($tipo_concepto == "0"):
                $simbolo = "€";
                //Incremento (0) - Descuento (1)
                if ($aumento_concepto == "0"):
                    $total_presupuesto = $total_presupuesto + $valor_concepto;
                else:
                    $total_presupuesto = $total_presupuesto - $valor_concepto;
                endif;
            else:
                $simbolo = "%";
                if ($aumento_concepto == "0"):
                    $total_presupuesto = $total_presupuesto + ($valor_concepto * $total_presupuesto / 100);
                else:
                    $total_presupuesto = $total_presupuesto - ($valor_concepto * $total_presupuesto / 100);
                endif;
            endif;

            if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0):
                ?>

                <tr>
                    <td class="columna1 table_border_left table_border_bottom">1</td>
                    <td class="columna3 table_border_right table_border_bottom"><?= $text_concepto ?></td>
                    <td class="columna4 table_border_right table_border_bottom"></td>
                    <td class="columna5 table_border_right table_border_bottom"></td>
                    <td class="columna6 table_border_right table_border_bottom"></td>
                    <td class="columna7 table_border_right table_border_bottom"></td>
                    <td class="columna8 table_border_right table_border_bottom"></td>
                    <td class="columna9 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"><?= $valor_concepto ?></td>
                    <td class="columna10 table_border_right table_border_bottom <?= $class_hidden_pedido ?>"></td>
                    <td class="columna11 table_border_right table_border_bottom table_border_right <?= $class_hidden_pedido ?>"><?= $valor_concepto ?></td>
                </tr>

            <?php else: ?>

                <tr>

                    <td class="columna1 table_border_left table_border_right">1</td>
                    <td class="columna2 table_border_right"><?= $text_concepto ?></td>
                    <td class="columna3 table_border_right "></td>
                    <td class="columna4 table_border_right "></td>
                    <td class="columna5 table_border_right "></td>
                    <td class="columna6 table_border_right "></td>
                    <td class="columna7 table_border_right "></td>
                    <td class="columna8 table_border_right "></td>
                    <td class="columna9 table_border_right <?= $class_hidden_pedido ?>"><?= $valor_concepto ?></td>
                    <td class="columna10 table_border_right <?= $class_hidden_pedido ?>"></td>
                    <td class="columna11 table_border_right <?= $class_hidden_pedido ?>"><?= $valor_concepto ?> </td>
                </tr>

            <?php
            endif;

            $num_lineas_presupuesto++;

            if ($num_lineas_presupuesto % 32 == 0):
                ?>
                <?php $saltos_pagina++; ?>
            </table>
            <div class="text_continua">Continua...</div>
            <div style="page-break-after: always;"></div>
            <div class="content_texto_lateral">
                <div class="img_texto img_texto_break"></div>
            </div>
            <table class="table_pdf break_page" style="text-align: center;">
                <tr>
                    <td class="columna1_cabecera table_border_left table_border_bottom table_border_top title_info_pedido">
                        CANTIDAD
                    </td>
                    <td class="columna2_cabecera table_border_bottom table_border_top title_info_pedido">ARTÍCULO</td>
                    <td class="columna3_cabecera table_border_bottom table_border_top title_info_pedido">ANCHO</td>
                    <td class="columna4_cabecera table_border_bottom table_border_top title_info_pedido">ALTO</td>
                    <td class="columna5_cabecera table_border_bottom table_border_top title_info_pedido">MANDOS</td>
                    <td class="columna6_cabecera table_border_bottom table_border_top title_info_pedido">REP</td>
                    <td class="columna7_cabecera table_border_bottom table_border_top title_info_pedido">LM</td>
                    <td class="columna8_cabecera table_border_bottom table_border_top title_info_pedido">TOTAL M<SUP>2</SUP>
                    </td>
                    <td class="columna9_cabecera table_border_bottom table_border_top title_info_pedido <?= $class_hidden_pedido ?>">
                        PRECIO UNIDAD/M<SUP>2</SUP></td>
                    <td class="columna10_cabecera table_border_bottom table_border_top title_info_pedido <?= $class_hidden_pedido ?>">
                        DTO.
                    </td>
                    <td class="columna11_cabecera table_border_bottom table_border_top title_info_pedido table_border_right <?= $class_hidden_pedido ?>">
                        IMPORTE
                    </td>
                </tr>
                <?php
            endif;

        endforeach;


        $total_presupuesto_iva = round2decimals($total_presupuesto * $iva);
        $total_presupuesto_recargo = round2decimals($total_presupuesto * 0.520);
        $total_presupuesto_impuestos = $total_presupuesto + $total_presupuesto_iva + $total_presupuesto_recargo;
        $aux_lineas = $saltos_pagina * 32;

        if ($class_hidden_pedido == "hidden"):
            $aux_lineas = $aux_lineas + 10;
        endif;

        //LINEAS EN BLANCO
        for ($i = $num_lineas_presupuesto; $i < $aux_lineas; $i++):
            ?>
            <tr>
                <td class="columna1 table_border_left table_border_right">&nbsp;</td>
                <td class="columna2 table_border_right">&nbsp;</td>
                <td class="columna3 table_border_right ">&nbsp;</td>
                <td class="columna4 table_border_right ">&nbsp;</td>
                <td class="columna5 table_border_right ">&nbsp;</td>
                <td class="columna6 table_border_right ">&nbsp;</td>
                <td class="columna7 table_border_right ">&nbsp;</td>
                <td class="columna8 table_border_right ">&nbsp;</td>
                <td class="columna9 table_border_right <?= $class_hidden_pedido ?>">&nbsp;</td>
                <td class="columna10 table_border_right <?= $class_hidden_pedido ?>">&nbsp;</td>
                <td class="columna11 table_border_right <?= $class_hidden_pedido ?>">&nbsp;</td>
            </tr>
        <?php endfor;
        ?>

        <tr>
            <td class="columna1 table_border_left table_border_bottom table_border_right">&nbsp;</td>
            <td class="columna2 table_border_bottom table_border_right">&nbsp;</td>
            <td class="columna3 table_border_bottom table_border_right ">&nbsp;</td>
            <td class="columna4 table_border_bottom table_border_right ">&nbsp;</td>
            <td class="columna5 table_border_bottom table_border_right ">&nbsp;</td>
            <td class="columna6 table_border_bottom table_border_right ">&nbsp;</td>
            <td class="columna7 table_border_bottom table_border_right ">&nbsp;</td>
            <td class="columna8 table_border_bottom table_border_right ">&nbsp;</td>
            <td class="columna9 table_border_bottom table_border_right <?= $class_hidden_pedido ?> ">&nbsp;</td>
            <td class="columna10 table_border_bottom table_border_right <?= $class_hidden_pedido ?>">&nbsp;</td>
            <td class="columna11 table_border_bottom table_border_right <?= $class_hidden_pedido ?>">&nbsp;</td>
        </tr>
    </table>
