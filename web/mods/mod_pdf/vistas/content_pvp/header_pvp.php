<div class="header">
            <table class="page_header noborder">
                <tr>
                    <td>
                        <img class="img_logo" src="img_pdf/logo_plantilla.png">
                    </td>
                </tr>
            </table>
            <table class="table_direcciones">
                <tr>
                    <td class="title_datos datos_fiscales">DATOS FISCALES</td>
                    <?php
                    if ($datos_presupuesto[0]['dire_envio'] == 0) {
                        $title_envio = "DIRECCIÓN DE RECOGIDA MERCANCIA";
                        $title_nombre = "MOSQUIFLEX S.L.";
                    } else {
                        $title_envio = "DIRECCIÓN DE ENVÍO MERCANCIA";
                        $title_nombre = $_SESSION['name_log'];
                    }
                    ?>
                    <td class="td_space"></td>
                    <td class="title_datos dire_reco"><?= $title_envio ?></td>
                </tr>
                <tr>
                    <td class="border datos_fiscales">
                        <br><span class="font-bold dire_clnomb"><?= $_SESSION['name_log'] ?></span><br><br><br>
                        <span class="dire_cldir1"><?= $dire_fiscal['cldir1'] ?></span>&nbsp;
                        <span class="dire_cldir2"><?= $dire_fiscal['cldir2'] ?></span><br>
                        <span class="dire_poblacion"><?= $poblacion_fiscal ?></span><br>
                        <span class="dire_provincia"><?= $provincia_fiscal ?></span><br>
                        <span class="dire_pais"><?= $pais_fiscal ?></span><br>
                        <span class="dire_tlf1"><?= $dire_fiscal['cltlf1'] ?></span><br>
                        <span class="dire_tlf2"><?= $dire_fiscal['cltlf2'] ?></span><br>
                        <span
                            class="font-bold">CÓDIGO CLIENTE: </span><?= $dire_fiscal['clproc'] ?><?= $dire_fiscal['clcodi'] ?>
                        <span class="font-bold cif">CIF:</span><?= $dire_fiscal['cldnic'] ?><br>
                    </td>
                    <td class="td_space"></td>
                    <td class="border dire_reco">
                        <br><span class="font-bold dire_clnomb"><?= $title_nombre ?></span><br><br><br>
                        <span class="dire_cldir1"><?= $dire['cldir1'] ?></span>&nbsp;
                        <span class="dire_cldir2"><?= $dire['cldir2'] ?></span><br>
                        <span class="dire_poblacion"><?= $poblacion ?></span><br>
                        <span class="dire_provincia"><?= $provincia ?></span><br>
                        <span class="dire_pais"><?= $pais ?></span><br>
                        <span class="dire_tlf1"><?= $dire['cltlf1'] ?></span><br>
                        <span class="dire_tlf2"><?= $dire['cltlf2'] ?></span><br>
                    </td>
                </tr>
            </table>
            <table class="table_datos_pre">
                <tr>
                    <?php if ($is_pedido == "0"): ?>
                        <td class="border1" rowspan="2"><span
                                class="font-bold">Nº PRESUPUESTO:</span> <?= hidePpto($num_presupuesto) ?></td>

                    <?php else: ?>
                        <td class="border1" rowspan="2"><span
                                class="font-bold">Nº PEDIDO:</span> <?= hidePed($num_presupuesto) ?></td>

                    <?php endif; ?>

                    <td class="border3"><span class="font-bold">FECHA:</span><?= $fecha ?>
                        &nbsp;&nbsp;&nbsp;Pág. <span class="page-number"></span>/<?= $total_page ?></td>
                </tr>
                <tr>
                    <td class="border3"><span class="font-bold">AGENCIA:</span><?= $agencia ?></td>
                </tr>
            </table>
        </div>