<style>

    .hidden{
        display: none;
    }

    .font-10{
        font-size: 10px;
    }

    sup{
        font-size: 6px;
    }

    .break_page{
        margin-top: 200px;
    }

    .page-number:after {
        content: counter(page);
    }

    .page_break {
        page-break-before: always;
    }

    @page {
        margin-top: 150px;
    }

    .header {
        position: fixed;
        left: 0px;
        top: -80px;
        right: 0px;
        height: 280px;
        text-align: center;
    }

    .footer {
        position: fixed;
        left: 0px;
        bottom: 0px;
        right: 0px;
        height: 20px;
    }

    .footer .page:after {
        content: counter(page, upper-roman);
    }

    table.page_header {
        width: 100%;
        border: none;
    }

    .img_logo {
        width: 240px;
        height: auto;
        background-repeat: no-repeat;
        display: inline-block;
    }

    .img_pie {
        width: 700px;
        height: auto;
        background-repeat: no-repeat;
        display: inline-block;
    }

    .content_texto_lateral {
        width: 30px;
        display: inline-block;
        position: fixed;
        margin-left: -40px;
    }

    .img_texto {
        height: 400px;
        background-repeat: no-repeat;
        display: inline-block;
        margin-top: 250px;
        width: auto;
    }

    .img_texto_break{
        height: 500px;
        width: 10px;
        margin-left: 40px;
        background: url(img_pdf/texto_lateral_plantilla_repeat.png) no-repeat;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    .content {
        width: 90%;
        margin-top: 200px;
        margin-left: 20px;
    }

    * {
        font-size: 11px;
        font-family: "Helvetica Neue", Helvetica;
    }

    .table_datos_pre {
        margin-left: 205px;
    }

    .columna1_cabecera {
        width: 30px;
        padding-left: 5px;
        text-align: left;
    }

    .columna1 {
        width: 30px;
        padding-left: 5px;
        text-align: right;
        font-size: 10px;
    }

    .columna2_cabecera{
        width: 250px;
        padding-left: 5px;
        text-align: center;
    }

    .columna2 {
        width: 250px;
        padding-left: 5px;
        text-align: left;
        font-size: 10px;
    }

    .columna3_cabecera{
        width: 30px;
        padding-left: 5px;
        text-align: center;
    }

    .columna3{
        width: 30px;
        padding-left: 5px;
        text-align: right;
        font-size: 10px;
    }

    .columna4_cabecera{
        width: 30px;
        padding-left: 5px;
        text-align: center;
    }

    .columna4{
        width: 30px;
        padding-left: 5px;
        text-align: right;
        font-size: 10px;
    }

    .columna5_cabecera{
        width: 20px;
        padding-left: 5px;
        text-align: center;
    }

    .columna5{
        width: 20px;
        padding-left: 5px;
        text-align: right;
        font-size: 10px;
    }

    .columna6_cabecera{
        width: 20px;
        padding-left: 5px;
        text-align: center;
    }

    .columna6{
        width: 20px;
        padding-left: 5px;
        text-align: right;
        font-size: 10px;
    }

    .columna7_cabecera{
        width: 20px;
        padding-left: 5px;
        text-align: center;
    }

    .columna7{
        width: 20px;
        padding-left: 5px;
        text-align: right;
        font-size: 10px;
    }

    .columna8_cabecera{
        width: 40px;
        padding-left: 5px;
        text-align: center;
    }

    .columna8{
        width: 40px;
        padding-left: 5px;
        text-align: right;
        font-size: 10px;
    }

    .columna9_cabecera{
        width: 50px;
        padding-left: 5px;
        text-align: center;
    }

    .columna9{
        width: 50px;
        padding-left: 5px;
        text-align: right;
        font-size: 10px;
    }

    .columna10_cabecera{
        width: 37px;
        padding-left: 5px;
        text-align: center;
    }

    .columna10{
        width: 37px;
        padding-left: 5px;
        text-align: right;
        font-size: 10px;
    }

    .columna11_cabecera{
        width: 50px;
        padding-left: 5px;
        text-align: center;
    }

    .columna11{
        width: 50px;
        padding-left: 5px;
        text-align: right;
        font-size: 10px;
    }

    .table_pdf {
        min-width: 673px !important;
        border: 1px solid #000;
        border-collapse: collapse;
    }

    .table_border_right {
        border-right: 1px solid black;
    }

    .table_border_top {
        border-top: 1px solid black;
    }

    .table_border_left {
        border-left: 1px solid black;
    }

    .table_border_bottom {
        border-bottom: 1px solid black;
    }

    table.page_footer {
        width: 100%;
        border: none;
    }

    .title_info_pedido {
        font-size: 8px;
        font-weight: bold;
        background-color: #dadada
    }

    .cif {
        margin-left: 50px;
    }

    .title_datos {
        font-weight: bold;
    }

    .font-bold {
        font-weight: bold;
    }

    .border {
        border: 1px solid black;
        border-radius: 4px;
        width: 340px;
        font-size: 12px;
    }

    .border1 {
        font-size: 12px;
        border: 1px solid black;
        border-radius: 4px;
        height: 30px;
        padding-right: 10px;
        padding-left: 10px;
        vertical-align: middle;
        width: 80px !important;
        max-width: 80px !important;
        text-align: center;
    }

    .border_text {
        border: 1px solid black;
        vertical-align: middle;
        border-radius: 4px;
    }

    .texto_validez, .texto_pago {
        width: 340px;
        font-size: 10px;
        padding-left: 10px;
        padding-top: 4px;
        padding-bottom: 2px;
        height: 15px;
    }

    .texto_pago {
        margin-top: 2px;
        height: 43px;
    }

    .texto_conforme {
        width: 145px;
        font-size: 10px;
        margin-left: 20px;
        font-weight: bold;
        text-align: center;
        height: 75px;
        display: inline-block;
        margin-top: 7.5px;
    }

    .texto_sello {
        font-size: 10px;
        font-weight: bold;
        text-align: center;
        display: inline-block;
        height: 75px;
        margin-left: 10px;
        width: 158px;
    }

    .datos_fiscales {
        width: 331px;
        margin-left: -2px !important;
    }

    .dire_reco {
        width: 331px;
    }

    .td_space {
        width: 20px;
    }

    .border3 {
        border: 1px solid black;
        border-radius: 4px;
        margin-left: 108px !important;
        width: 160px;
    }

    .info_firma {
        font-weight: bold;
        font-size: 9.8px;
        margin-left: 8px;
    }

    hr {
        width: 106.6%;
    }

    .content_precios_line1 {
        width: 75%;
    }

    .total_presupuesto {
        width: 25%;
        margin-left: 82%;
        text-align: right;
        font-weight: bold;
        font-size: 14px;
        margin-top: -55px;
    }

    .border_total_presupuesto {
        border: 1px solid black;
        border-radius: 4px;
        height: 28px;
        font-size: 22px !important;
        padding-right: 4px;
        padding-top: 4px;
    }

    .content_precios_line3 {
        margin-top: 3px;
    }

    .content_precios_line3 {
        width: 75%;
    }

    .line {
        display: inline-block;
        font-size: 8px;
    }

    .line1_1 {
        margin-left: 170px;
    }

    .line1_2 {
        margin-left: 130px;
    }

    .line2_1 {
        margin-left: 10px;
    }

    .line2_2 {
        margin-left: 52px;
    }

    .line2_3 {
        margin-left: 75px;
    }

    .line2_4 {
        margin-left: 72px;
    }

    .line2_5 {
        margin-left: 72px;
    }

    .line3_1 {
        margin-left: 1px;
    }

    .line3_1, .line3_2, .line3_3, .line3_4, .line3_5 {
        width: 98px;
        font-size: 10px;
        border: 1px solid #000000;
        background-color: #dadada;
        margin-left: -2px;
        height: 15px;
        padding-top: 2px;
    }

    .text-center {
        text-align: center;
    }

    .text-right {
        text-align: right;
    }

    .file_2_total {
        width: 107%;
        display: inline-flex;
        height: 80px;
    }

    .file_2_w50 {
        width: 50%;

    }

    .file_2_w50_2 {
        width: 50%;
        margin-left: 50%;
    }

    .table_direcciones {
        margin-left: 15px;
    }

    .text_continua{
        margin-left: 98%;
        margin-top: 90px;
        font-weight: bold;
    }

    .margin_ped{
        margin-left: 156px !important;
    }

</style>