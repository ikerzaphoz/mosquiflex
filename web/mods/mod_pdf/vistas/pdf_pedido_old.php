<html>
<head>
    <style>

        .hidden{
            display: none;
        }

        .font-10{
            font-size: 10px;
        }

        sup{
            font-size: 6px;
        }

        .break_page{
            margin-top: 200px;
        }

        .page-number:after {
            content: counter(page);
        }

        .page_break {
            page-break-before: always;
        }

        @page {
            margin-top: 150px;
        }

        .header {
            position: fixed;
            left: 0px;
            top: -80px;
            right: 0px;
            height: 280px;
            text-align: center;
        }

        .footer {
            position: fixed;
            left: 0px;
            bottom: 0px;
            right: 0px;
            height: 20px;
        }

        .footer .page:after {
            content: counter(page, upper-roman);
        }

        table.page_header {
            width: 100%;
            border: none;
        }

        .img_logo {
            width: 240px;
            height: auto;
            background-repeat: no-repeat;
            display: inline-block;
        }

        .img_pie {
            width: 700px;
            height: auto;
            background-repeat: no-repeat;
            display: inline-block;
        }

        .content_texto_lateral {
            width: 30px;
            display: inline-block;
            position: fixed;
            margin-left: -40px;
        }

        .img_texto {
            height: 400px;
            background-repeat: no-repeat;
            display: inline-block;
            margin-top: 250px;
            width: auto;
        }

        .img_texto_break{
            height: 500px;
            width: 10px;
            margin-left: 40px;
            background: url(img_pdf/texto_lateral_plantilla_repeat.png) no-repeat;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .content {
            width: 90%;
            margin-top: 200px;
            margin-left: 20px;
        }

        * {
            font-size: 11px;
            font-family: "Helvetica Neue", Helvetica;
        }

        .table_datos_pre {
            margin-left: 205px;
        }

        .columna1_cabecera {
            width: 30px;
            padding-left: 5px;
            text-align: left;
        }

        .columna1 {
            width: 30px;
            padding-left: 5px;
            text-align: right;
            font-size: 10px;
        }

        .columna2_cabecera{
            width: 200px;
            padding-left: 5px;
            text-align: center;
        }

        .columna2 {
            width: 200px;
            padding-left: 5px;
            text-align: left;
            font-size: 10px;
        }

        .columna3_cabecera{
            width: 37px;
            padding-left: 5px;
            text-align: center;
        }

        .columna3{
            width: 37px;
            padding-left: 5px;
            text-align: right;
            font-size: 10px;
        }

        .columna4_cabecera{
            width: 37px;
            padding-left: 5px;
            text-align: center;
        }

        .columna4{
            width: 37px;
            padding-left: 5px;
            text-align: right;
            font-size: 10px;
        }

        .columna5_cabecera{
            width: 37px;
            padding-left: 5px;
            text-align: center;
        }

        .columna5{
            width: 37px;
            padding-left: 5px;
            text-align: right;
            font-size: 10px;
        }

        .columna6_cabecera{
            width: 37px;
            padding-left: 5px;
            text-align: center;
        }

        .columna6{
            width: 37px;
            padding-left: 5px;
            text-align: right;
            font-size: 10px;
        }

        .columna7_cabecera{
            width: 37px;
            padding-left: 5px;
            text-align: center;
        }

        .columna7{
            width: 37px;
            padding-left: 5px;
            text-align: right;
            font-size: 10px;
        }

        .columna8_cabecera{
            width: 40px;
            padding-left: 5px;
            text-align: center;
        }

        .columna8{
            width: 40px;
            padding-left: 5px;
            text-align: right;
            font-size: 10px;
        }

        .columna9_cabecera{
            width: 50px;
            padding-left: 5px;
            text-align: center;
        }

        .columna9{
            width: 50px;
            padding-left: 5px;
            text-align: right;
            font-size: 10px;
        }

        .columna10_cabecera{
            width: 37px;
            padding-left: 5px;
            text-align: center;
        }

        .columna10{
            width: 37px;
            padding-left: 5px;
            text-align: right;
            font-size: 10px;
        }

        .columna11_cabecera{
            width: 50px;
            padding-left: 5px;
            text-align: center;
        }

        .columna11{
            width: 50px;
            padding-left: 5px;
            text-align: right;
            font-size: 10px;
        }

        .table_pdf {
            min-width: 673px !important;
            border: 1px solid #000;
            border-collapse: collapse;
        }

        .table_border_right {
            border-right: 1px solid black;
        }

        .table_border_top {
            border-top: 1px solid black;
        }

        .table_border_left {
            border-left: 1px solid black;
        }

        .table_border_bottom {
            border-bottom: 1px solid black;
        }

        table.page_footer {
            width: 100%;
            border: none;
        }

        .title_info_pedido {
            font-size: 8px;
            font-weight: bold;
            background-color: #dadada
        }

        .cif {
            margin-left: 50px;
        }

        .title_datos {
            font-weight: bold;
        }

        .font-bold {
            font-weight: bold;
        }

        .border {
            border: 1px solid black;
            border-radius: 4px;
            width: 340px;
            font-size: 12px;
        }

        .border1 {
            font-size: 12px;
            border: 1px solid black;
            border-radius: 4px;
            height: 30px;
            padding-right: 10px;
            padding-left: 10px;
            vertical-align: middle;
            width: 125px !important;
            text-align: center;
        }

        .border_text {
            border: 1px solid black;
            vertical-align: middle;
            border-radius: 4px;
        }

        .texto_validez, .texto_pago {
            width: 340px;
            font-size: 10px;
            padding-left: 10px;
            padding-top: 4px;
            padding-bottom: 2px;
            height: 15px;
        }

        .texto_pago {
            margin-top: 2px;
            height: 43px;
        }

        .texto_conforme {
            width: 145px;
            font-size: 10px;
            margin-left: 20px;
            font-weight: bold;
            text-align: center;
            height: 75px;
            display: inline-block;
            margin-top: 7.5px;
        }

        .texto_sello {
            font-size: 10px;
            font-weight: bold;
            text-align: center;
            display: inline-block;
            height: 75px;
            margin-left: 10px;
            width: 158px;
        }

        .datos_fiscales {
            width: 331px;
            margin-left: -2px !important;
        }

        .dire_reco {
            width: 331px;
        }

        .td_space {
            width: 20px;
        }

        .border3 {
            border: 1px solid black;
            border-radius: 4px;
            margin-left: 108px !important;
            width: 160px;
        }

        .info_firma {
            font-weight: bold;
            font-size: 9.8px;
            margin-left: 8px;
        }

        hr {
            width: 106.6%;
        }

        .content_precios_line1 {
            width: 75%;
        }

        .total_presupuesto {
            width: 25%;
            margin-left: 82%;
            text-align: right;
            font-weight: bold;
            font-size: 14px;
            margin-top: -55px;
        }

        .border_total_presupuesto {
            border: 1px solid black;
            border-radius: 4px;
            height: 28px;
            font-size: 22px !important;
            padding-right: 4px;
            padding-top: 4px;
        }

        .content_precios_line3 {
            margin-top: 3px;
        }

        .content_precios_line3 {
            width: 75%;
        }

        .line {
            display: inline-block;
            font-size: 8px;
        }

        .line1_1 {
            margin-left: 170px;
        }

        .line1_2 {
            margin-left: 130px;
        }

        .line2_1 {
            margin-left: 10px;
        }

        .line2_2 {
            margin-left: 52px;
        }

        .line2_3 {
            margin-left: 75px;
        }

        .line2_4 {
            margin-left: 72px;
        }

        .line2_5 {
            margin-left: 72px;
        }

        .line3_1 {
            margin-left: 1px;
        }

        .line3_1, .line3_2, .line3_3, .line3_4, .line3_5 {
            width: 98px;
            font-size: 10px;
            border: 1px solid #000000;
            background-color: #dadada;
            margin-left: -2px;
            height: 15px;
            padding-top: 2px;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .file_2_total {
            width: 107%;
            display: inline-flex;
            height: 80px;
        }

        .file_2_w50 {
            width: 50%;

        }

        .file_2_w50_2 {
            width: 50%;
            margin-left: 50%;
        }

        .table_direcciones {
            margin-left: 15px;
        }

        .text_continua{
            margin-left: 98%;
            margin-top: 90px;
            font-weight: bold;
        }

        .margin_ped{
            margin-left: 156px !important;
        }

    </style>
<body>
<div class="header">
    <table class="page_header noborder">
        <tr>
            <td>
                <img class="img_logo" src="img_pdf/logo_plantilla.png">
            </td>
        </tr>
    </table>
    <table class="table_direcciones">
        <tr>
            <td class="title_datos datos_fiscales">DATOS FISCALES</td>
            <?php

            if ($datos_presupuesto[0]['dire_envio'] == 0) {
                $title_envio = "DIRECCIÓN DE RECOGIDA MERCANCIA";
                $title_nombre = "MOSQUIFLEX S.L.";
            } else {
                $title_envio = "DIRECCIÓN DE ENVÍO MERCANCIA";
                $title_nombre = $_SESSION['name_log'];
            }

            ?>
            <td class="td_space"></td>
            <td class="title_datos dire_reco"><?= $title_envio ?></td>
        </tr>
        <tr>
            <td class="border datos_fiscales">
                <br><span class="font-bold dire_clnomb"><?= $_SESSION['name_log'] ?></span><br><br><br>
                <span class="dire_cldir1"><?= $dire_fiscal['cldir1'] ?></span>&nbsp;
                <span class="dire_cldir2"><?= $dire_fiscal['cldir2'] ?></span><br>
                <span class="dire_poblacion"><?= $poblacion_fiscal ?></span><br>
                <span class="dire_provincia"><?= $provincia_fiscal ?></span><br>
                <span class="dire_pais"><?= $pais_fiscal ?></span><br>
                <span class="dire_tlf1"><?= $dire_fiscal['cltlf1'] ?></span><br>
                <span class="dire_tlf2"><?= $dire_fiscal['cltlf2'] ?></span><br>
                <span
                    class="font-bold">CÓDIGO CLIENTE: </span><?= $dire_fiscal['clproc'] ?><?= $dire_fiscal['clcodi'] ?>
                <span class="font-bold cif">CIF:</span><?= $dire_fiscal['cldnic'] ?><br>
            </td>
            <td class="td_space"></td>
            <td class="border dire_reco">
                <br><span class="font-bold dire_clnomb"><?= $title_nombre ?></span><br><br><br>
                <span class="dire_cldir1"><?= $dire['cldir1'] ?></span>&nbsp;
                <span class="dire_cldir2"><?= $dire['cldir2'] ?></span><br>
                <span class="dire_poblacion"><?= $poblacion ?></span><br>
                <span class="dire_provincia"><?= $provincia ?></span><br>
                <span class="dire_pais"><?= $pais ?></span><br>
                <span class="dire_tlf1"><?= $dire['cltlf1'] ?></span><br>
                <span class="dire_tlf2"><?= $dire['cltlf2'] ?></span><br>
            </td>
        </tr>
    </table>
    <table class="table_datos_pre">
        <tr>
            <?php

            if ($is_pedido == "0"): ?>
                <td class="border1" rowspan="2"><span
                        class="font-bold">Nº PRESUPUESTO:</span> <?= hidePpto($num_presupuesto) ?></td>

            <?php else: ?>
                <td class="border1" rowspan="2"><span
                        class="font-bold">Nº PEDIDO:</span> <?= hidePed($num_presupuesto) ?></td>

            <?php endif; ?>

            <td class="border3"><span class="font-bold">FECHA:</span><?= $fecha ?>
                &nbsp;&nbsp;&nbsp;Pág. <span class="page-number"></span>/<?= $total_page ?></td>
        </tr>
        <tr>
            <td class="border3"><span class="font-bold">AGENCIA:</span><?=$agencia?></td>
        </tr>
    </table>
</div>
<div class="footer">
    <table class="page_footer">
        <tr>
            <td>
                <img class="img_pie" src="img_pdf/pie_plantilla.png">
            </td>
        </tr>
    </table>
</div>
<div class="content">
    <div class="content_texto_lateral">
        <img class="img_texto" src="img_pdf/texto_lateral_plantilla.png">
    </div>

    <table class="table_pdf" style="text-align: center;">
        <tr>
            <td class="columna1_cabecera table_border_left table_border_bottom table_border_top title_info_pedido">CANTIDAD</td>
            <td class="columna2_cabecera table_border_bottom table_border_top title_info_pedido">ARTÍCULO</td>
            <td class="columna3_cabecera table_border_bottom table_border_top title_info_pedido">ANCHO</td>
            <td class="columna4_cabecera table_border_bottom table_border_top title_info_pedido">ALTO</td>
            <td class="columna5_cabecera table_border_bottom table_border_top title_info_pedido">MANDOS</td>
            <td class="columna6_cabecera table_border_bottom table_border_top title_info_pedido">REP</td>
            <td class="columna7_cabecera table_border_bottom table_border_top title_info_pedido">LM</td>
            <td class="columna8_cabecera table_border_bottom table_border_top title_info_pedido">TOTAL M<SUP>2</SUP></td>
            <td class="columna9_cabecera table_border_bottom table_border_top title_info_pedido <?=$class_hidden_pedido?>">PRECIO UNIDAD/M<SUP>2</SUP></td>
            <td class="columna10_cabecera table_border_bottom table_border_top title_info_pedido <?=$class_hidden_pedido?>">DTO.</td>
            <td class="columna11_cabecera table_border_bottom table_border_top title_info_pedido table_border_right <?=$class_hidden_pedido?>">IMPORTE</td>
        </tr>
        <?php foreach ($albaranes as $item): ?>
        <?php

        //Producto acabado
        if ($item['albanc'] > 0 AND empty($item['albobs']) AND $item['albfami'] != "1999" OR ($item['albfami'] == "2736")):
                        
            if($item['albfami'] == "2736"):
                $title_product = obtener_titulo_expositor($item['albarti']);
            else:
                $title_product = $familia->getTitleProduct($item['albfami'], $item['albsub']);
            endif;
            $color = "";
            if (isset($familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'])):
                $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];
            else:
                $color = $item['albco'];
            endif;

            $color = ucfirst(str_replace("Lacado ", "", $color));

            $precio_unitario = round2decimals($item['albpre']) / $item['albunit'];

            $total_importe = round2decimals($precio_unitario) * $item['albunit'];
            $total_importe = round2decimals($total_importe);

            $total_presupuesto = $total_presupuesto + round2decimals($total_importe);


            //Ultima linea antes del salto
            if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0): ?>

                <tr>
                    <td class="columna1 table_border_left table_border_bottom"><?= $item['albunit'] ?></td>
                    <td class="columna2 table_border_right table_border_bottom"><?= $title_product ?> <?=$color?></td>
                    <td class="columna3 table_border_right table_border_bottom"><?= number_format($item['albanc'], 3) ?></td>
                    <td class="columna4 table_border_right table_border_bottom"><?= number_format($item['albalt'], 3) ?></td>
                    <td class="columna5 table_border_right table_border_bottom"></td>
                    <td class="columna6 table_border_right table_border_bottom"></td>
                    <td class="columna7 table_border_right table_border_bottom"></td>
                    <td class="columna8 table_border_right table_border_bottom"></td>
                    <td class="columna9 table_border_right table_border_bottom <?=$class_hidden_pedido?>"><?= round2decimals($precio_unitario) ?></td>
                    <td class="columna10 table_border_right table_border_bottom <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right table_border_bottom table_border_right <?=$class_hidden_pedido?>"><?=$total_importe?></td>
                </tr>

            <?php else: ?>

                <tr>
                    <td class="columna1 table_border_left table_border_right"><?= $item['albunit'] ?></td>
                    <td class="columna2 table_border_right"><?= $title_product ?> <?=$color?></td>
                    <td class="columna3 table_border_right "><?= number_format($item['albanc'], 3) ?></td>
                    <td class="columna4 table_border_right "><?= number_format($item['albalt'], 3) ?></td>
                    <td class="columna5 table_border_right "></td>
                    <td class="columna6 table_border_right "></td>
                    <td class="columna7 table_border_right "></td>
                    <td class="columna8 table_border_right "></td>
                    <td class="columna9 table_border_right <?=$class_hidden_pedido?>"><?= round2decimals($precio_unitario) ?></td>
                    <td class="columna10 table_border_right <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right <?=$class_hidden_pedido?>"><?=$total_importe?></td>
                </tr>

            <?php endif; ?>

        <?php endif; ?>


        <?php //componente metros
        if ($item['albobs'] == "is_component_meter"):

            $aux_title = str_replace('279990', '', $item['albco']);
            $title_component = $familia->getTitleComponent($item['albfami'], $item['albsub'], $aux_title);

            $version_product = getCajon($item['albsub']);
            $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];

            $total_importe = round2decimals($item['albpre']) * $item['albunit'] * $item['albanc'];
            $total_importe = round2decimals($total_importe);

            $total_presupuesto = $total_presupuesto + $total_importe;

            //Ultima linea
            if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0): ?>

                <tr>
                    <td class="columna1 table_border_left table_border_bottom"><?= $item['albunit'] ?></td>
                    <td class="columna2 table_border_right table_border_bottom"><?= $title_component ?></td>
                    <td class="columna3 table_border_right table_border_bottom"><?= $item['albanc'] ?></td>
                    <td class="columna4 table_border_right table_border_bottom"></td>
                    <td class="columna5 table_border_right table_border_bottom"></td>
                    <td class="columna6 table_border_right table_border_bottom"></td>
                    <td class="columna7 table_border_right table_border_bottom"></td>
                    <td class="columna8 table_border_right table_border_bottom"></td>
                    <td class="columna9 table_border_right table_border_bottom <?=$class_hidden_pedido?>"><?= round2decimals($item['albpre']) ?></td>
                    <td class="columna10 table_border_right table_border_bottom <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right table_border_bottom table_border_right <?=$class_hidden_pedido?>"><?=$total_importe?></td>
                </tr>

            <?php else: ?>

                <tr>

                    <td class="columna1 table_border_left table_border_right"><?= $item['albunit'] ?></td>
                    <td class="columna2 table_border_right"><?= $title_component ?></td>
                    <td class="columna3 table_border_right "><?= $item['albanc'] ?></td>
                    <td class="columna4 table_border_right "></td>
                    <td class="columna5 table_border_right "></td>
                    <td class="columna6 table_border_right "></td>
                    <td class="columna7 table_border_right "></td>
                    <td class="columna8 table_border_right "></td>
                    <td class="columna9 table_border_right <?=$class_hidden_pedido?>"><?= round2decimals($item['albpre']) ?></td>
                    <td class="columna10 table_border_right <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right <?=$class_hidden_pedido?>"><?=$total_importe?></td>
                </tr>

            <?php endif; ?>

        <?php endif; ?>

        <?php //componente metros
        if ($item['albobs'] == "is_component"):

            $title_component = str_replace('279990', '', $item['albco']);
            $title_component = $familia->getTitleComponent($item['albfami'], $item['albsub'], $title_component);
            $version_product = getCajon($item['albsub']);
            $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];

            $total_importe = round2decimals($item['albpre']) * $item['albunit'];
            $total_importe = round2decimals($total_importe);

            $total_presupuesto = $total_presupuesto + $total_importe;

            //Ultima linea
            if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0): ?>

                <tr>
                    <td class="columna1 table_border_left table_border_bottom"><?= $item['albunit'] ?></td>
                    <td class="columna2 table_border_right table_border_bottom"><?= $title_component ?></td>
                    <td class="columna3 table_border_right table_border_bottom"></td>
                    <td class="columna4 table_border_right table_border_bottom"></td>
                    <td class="columna5 table_border_right table_border_bottom"></td>
                    <td class="columna6 table_border_right table_border_bottom"></td>
                    <td class="columna7 table_border_right table_border_bottom"></td>
                    <td class="columna8 table_border_right table_border_bottom"></td>
                    <td class="columna9 table_border_right table_border_bottom <?=$class_hidden_pedido?>"><?= round2decimals($item['albpre']) ?></td>
                    <td class="columna10 table_border_right table_border_bottom <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right table_border_bottom table_border_right <?=$class_hidden_pedido?>"><?=$total_importe?></td>
                </tr>

            <?php else: ?>

                <tr>

                    <td class="columna1 table_border_left table_border_right"><?= $item['albunit'] ?></td>
                    <td class="columna2 table_border_right"><?= $title_component ?></td>
                    <td class="columna3 table_border_right "></td>
                    <td class="columna4 table_border_right "></td>
                    <td class="columna5 table_border_right "></td>
                    <td class="columna6 table_border_right "></td>
                    <td class="columna7 table_border_right "></td>
                    <td class="columna8 table_border_right "></td>
                    <td class="columna9 table_border_right <?=$class_hidden_pedido?>"><?= round2decimals($item['albpre']) ?></td>
                    <td class="columna10 table_border_right <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right <?=$class_hidden_pedido?>"><?=$total_importe?></td>
                </tr>

            <?php endif; ?>

        <?php endif; ?>

        <?php //incremento
        if (strpos($item['albobs'], "is_increment") !== false):

            $aux_id = str_replace("is_increment","",$item['albobs']);
            $arfami = substr($aux_id, 0, 4);
            $arsubf = substr($aux_id, 4, 2);
            $ararti = substr($aux_id, 6, 6);

            $componente = $familia->searchComponentByCode($arfami, $arsubf, $ararti);

            if(isset($componente[0]['ardesc'])):
                $title_component = $componente[0]['ardesc'];
            endif;

            $precio_unitario = round2decimals($item['albpre']) / $item['albunit'];
            $precio_unitario = round2decimals($precio_unitario);

            $total_importe = round2decimals($precio_unitario) * $item['albunit'];
            $total_importe = round2decimals($total_importe);

            $total_presupuesto = $total_presupuesto + round2decimals($total_importe);


            //Ultima linea
            if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0): ?>

                <tr>
                    <td class="columna1 table_border_left table_border_bottom"><?= $item['albunit'] ?></td>
                    <td class="columna2 table_border_right table_border_bottom"><?= $title_component ?></td>
                    <td class="columna3 table_border_right table_border_bottom"></td>
                    <td class="columna4 table_border_right table_border_bottom"></td>
                    <td class="columna5 table_border_right table_border_bottom"></td>
                    <td class="columna6 table_border_right table_border_bottom"></td>
                    <td class="columna7 table_border_right table_border_bottom"></td>
                    <td class="columna8 table_border_right table_border_bottom"></td>
                    <td class="columna9 table_border_right table_border_bottom <?=$class_hidden_pedido?>"><?= $precio_unitario ?></td>
                    <td class="columna10 table_border_right table_border_bottom <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right table_border_bottom table_border_right <?=$class_hidden_pedido?>"><?=$total_importe?></td>
                </tr>

            <?php else: ?>

                <tr>

                    <td class="columna1 table_border_left table_border_right"><?= $item['albunit'] ?></td>
                    <td class="columna2 table_border_right"><?= $title_component ?></td>
                    <td class="columna3 table_border_right "></td>
                    <td class="columna4 table_border_right "></td>
                    <td class="columna5 table_border_right "></td>
                    <td class="columna6 table_border_right "></td>
                    <td class="columna7 table_border_right "></td>
                    <td class="columna8 table_border_right "></td>
                    <td class="columna9 table_border_right <?=$class_hidden_pedido?>"><?= $precio_unitario ?></td>
                    <td class="columna10 table_border_right <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right <?=$class_hidden_pedido?>"><?=$total_importe?></td>
                </tr>

            <?php endif; ?>

        <?php endif; ?>

        <?php

        //Expositores
        if ($item['albfami'] == "1999" AND !empty($item['albunit'])):

            $title_product = obtener_titulo_expositor($item['albarti']);
            $color = "-";

            $precio_unitario = round2decimals($item['albpre']);

            $total_importe = round2decimals($precio_unitario) * $item['albunit'];
            $total_importe = round2decimals($total_importe);

            $total_presupuesto = $total_presupuesto + round2decimals($total_importe);


            //Ultima linea antes del salto
            if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0): ?>

                <tr>
                    <td class="columna1 table_border_left table_border_bottom"><?= $item['albunit'] ?></td>
                    <td class="columna2 table_border_right table_border_bottom"><?= $title_product ?> </td>
                    <td class="columna3 table_border_right table_border_bottom"><?= number_format($item['albanc'], 3) ?></td>
                    <td class="columna4 table_border_right table_border_bottom"><?= number_format($item['albalt'], 3) ?></td>
                    <td class="columna5 table_border_right table_border_bottom"></td>
                    <td class="columna6 table_border_right table_border_bottom"></td>
                    <td class="columna7 table_border_right table_border_bottom"></td>
                    <td class="columna8 table_border_right table_border_bottom"></td>
                    <td class="columna9 table_border_right table_border_bottom <?=$class_hidden_pedido?>"><?= round2decimals($precio_unitario) ?></td>
                    <td class="columna10 table_border_right table_border_bottom <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right table_border_bottom table_border_right <?=$class_hidden_pedido?>"><?=$total_importe?></td>
                </tr>

            <?php else: ?>

                <tr>
                    <td class="columna1 table_border_left table_border_right"><?= $item['albunit'] ?></td>
                    <td class="columna2 table_border_right"><?= $title_product ?> </td>
                    <td class="columna3 table_border_right "><?= number_format($item['albanc'], 3) ?></td>
                    <td class="columna4 table_border_right "><?= number_format($item['albalt'], 3) ?></td>
                    <td class="columna5 table_border_right "></td>
                    <td class="columna6 table_border_right "></td>
                    <td class="columna7 table_border_right "></td>
                    <td class="columna8 table_border_right "></td>
                    <td class="columna9 table_border_right <?=$class_hidden_pedido?>"><?= round2decimals($precio_unitario) ?></td>
                    <td class="columna10 table_border_right <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right <?=$class_hidden_pedido?>"><?=$total_importe?></td>
                </tr>

            <?php endif; ?>

        <?php endif; ?>

        <?php //componente unidad
        if ($item['albobs'] == "is_component_unit"):

            $title_component = str_replace('279990', '', $item['albco']);
            $title_component = $familia->getTitleComponent($item['albfami'], $item['albsub'], $title_component);
            $version_product = getCajon($item['albsub']);
            $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];
            $total_importe = $item['albanc'] * $item['albalt'];
            $total_importe = round2decimals($total_importe) * round2decimals($item['albpre']) * $item['albunit'];
            $total_importe = round2decimals($total_importe);

            $total_presupuesto = $total_presupuesto + $total_importe;

            if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0): ?>

                <tr>

                    <td class="columna1 table_border_left table_border_bottom"><?= $item['albunit'] ?></td>
                    <td class="columna2 table_border_right table_border_bottom"><?= $title_component ?></td>
                    <td class="columna3 table_border_right table_border_bottom"><?= $item['albanc'] ?></td>
                    <td class="columna4 table_border_right table_border_bottom"><?= $item['albalt'] ?></td>
                    <td class="columna5 table_border_right table_border_bottom"></td>
                    <td class="columna6 table_border_right table_border_bottom"></td>
                    <td class="columna7 table_border_right table_border_bottom"></td>
                    <td class="columna8 table_border_right table_border_bottom"></td>
                    <td class="columna9 table_border_right table_border_bottom <?=$class_hidden_pedido?>"><?= round2decimals($item['albpre']) ?></td>
                    <td class="columna10 table_border_right table_border_bottom <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right table_border_bottom table_border_right <?=$class_hidden_pedido?>"><?=$total_importe?></td>
                </tr>

            <?php else: ?>

                <tr>

                    <td class="columna1 table_border_left table_border_right"><?= $item['albunit'] ?></td>
                    <td class="columna2 table_border_right"><?= $title_component ?></td>
                    <td class="columna3 table_border_right "><?= $item['albanc'] ?></td>
                    <td class="columna4 table_border_right "><?= $item['albalt'] ?></td>
                    <td class="columna5 table_border_right "></td>
                    <td class="columna6 table_border_right "></td>
                    <td class="columna7 table_border_right "></td>
                    <td class="columna8 table_border_right "></td>
                    <td class="columna9 table_border_right <?=$class_hidden_pedido?>"><?= round2decimals($item['albpre']) ?></td>
                    <td class="columna10 table_border_right <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right <?=$class_hidden_pedido?>"><?=$total_importe?></td>
                </tr>

            <?php endif; ?>

        <?php endif;

        //lacado
        if ($item['albobs'] == "lacado"):

            $color = "";
            $color = $item['albco'];
            $unidades = $item['albunit'];
            $precio = round2decimals($item['albpre']);

            $total_presupuesto = $total_presupuesto + $precio;

            if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0): ?>

                <tr>

                    <td class="columna1 table_border_left table_border_bottom"><?= $unidades ?></td>
                    <td class="columna2 table_border_right table_border_bottom">Lacado -
                        RAL. <?= $version_product ?> <?=$color?></td>
                    <td class="columna3 table_border_right table_border_bottom"></td>
                    <td class="columna4 table_border_right table_border_bottom"></td>
                    <td class="columna5 table_border_right table_border_bottom"></td>
                    <td class="columna6 table_border_right table_border_bottom"></td>
                    <td class="columna7 table_border_right table_border_bottom"></td>
                    <td class="columna8 table_border_right table_border_bottom"></td>
                    <td class="columna9 table_border_right table_border_bottom <?=$class_hidden_pedido?>"><?= round2decimals($item['albpre']) ?></td>
                    <td class="columna10 table_border_right table_border_bottom <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right table_border_bottom table_border_right <?=$class_hidden_pedido?>"><?=$precio?></td>
            </tr>

                <?php else: ?>

                <tr>

                    <td class="columna1 table_border_left table_border_right"><?= $unidades ?></td>
                    <td class="columna2 table_border_right">Lacado -
                        RAL. <?= $version_product ?> <?= $color ?></td>
                    <td class="columna3 table_border_right "></td>
                    <td class="columna4 table_border_right "></td>
                    <td class="columna5 table_border_right "></td>
                    <td class="columna6 table_border_right "></td>
                    <td class="columna7 table_border_right "></td>
                    <td class="columna8 table_border_right "></td>
                    <td class="columna9 table_border_right <?=$class_hidden_pedido?>"><?= round2decimals($item['albpre']) ?></td>
                    <td class="columna10 table_border_right <?=$class_hidden_pedido?>"></td>
                    <td class="columna11 table_border_right <?=$class_hidden_pedido?>"><?=$precio?></td>
                </tr>

                <?php endif; ?>

        <?php endif;
               
        $num_lineas_presupuesto++;

        endforeach; ?>
        
        <?php 
        
                 foreach($array_conceptos_pedidos as $linea_concepto):

            if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0): ?>



                <tr>



                    <td class="columna1 table_border_left table_border_bottom">1</td>

                    <td class="columna3 table_border_right table_border_bottom"><?=$linea_concepto['texto_concepto']?></td>

                    <td class="columna4 table_border_right table_border_bottom"></td>

                    <td class="columna5 table_border_right table_border_bottom"></td>

                    <td class="columna6 table_border_right table_border_bottom"></td>

                    <td class="columna7 table_border_right table_border_bottom"></td>

                    <td class="columna8 table_border_right table_border_bottom"></td>

                    <td class="columna9 table_border_right table_border_bottom <?=$class_hidden_pedido?>"><?= $linea_concepto['valor_concepto'] ?></td>

                    <td class="columna10 table_border_right table_border_bottom <?=$class_hidden_pedido?>"></td>

                    <td class="columna11 table_border_right table_border_bottom table_border_right <?=$class_hidden_pedido?>"><?=$precio?></td>

            </tr>



                <?php else: ?>



                <tr>



                    <td class="columna1 table_border_left table_border_right">1</td>

                    <td class="columna2 table_border_right"><?=$linea_concepto['texto_concepto']?></td>

                    <td class="columna3 table_border_right "></td>

                    <td class="columna4 table_border_right "></td>

                    <td class="columna5 table_border_right "></td>

                    <td class="columna6 table_border_right "></td>

                    <td class="columna7 table_border_right "></td>

                    <td class="columna8 table_border_right "></td>

                    <td class="columna9 table_border_right <?=$class_hidden_pedido?>"><?= $linea_concepto['valor_concepto'] ?></td>

                    <td class="columna10 table_border_right <?=$class_hidden_pedido?>"></td>

                    <td class="columna11 table_border_right <?=$class_hidden_pedido?>"><?=$precio?></td>

                </tr>



                <?php endif; 
                
                $num_lineas_presupuesto++;

        endforeach;
        
                
                if ($num_lineas_presupuesto % 32 == 0): ?>
        <?php $saltos_pagina++; ?>
    </table>
    <div class="text_continua">Continua...</div>
    <div style="page-break-after: always;"></div>
    <div class="content_texto_lateral">
        <div class="img_texto img_texto_break"></div>
    </div>
    <table class="table_pdf break_page" style="text-align: center;">
        <tr>
            <td class="columna1_cabecera table_border_left table_border_bottom table_border_top title_info_pedido">CANTIDAD</td>
            <td class="columna2_cabecera table_border_bottom table_border_top title_info_pedido">ARTÍCULO</td>
            <td class="columna3_cabecera table_border_bottom table_border_top title_info_pedido">ANCHO</td>
            <td class="columna4_cabecera table_border_bottom table_border_top title_info_pedido">ALTO</td>
            <td class="columna5_cabecera table_border_bottom table_border_top title_info_pedido">MANDOS</td>
            <td class="columna6_cabecera table_border_bottom table_border_top title_info_pedido">REP</td>
            <td class="columna7_cabecera table_border_bottom table_border_top title_info_pedido">LM</td>
            <td class="columna8_cabecera table_border_bottom table_border_top title_info_pedido">TOTAL M<SUP>2</SUP></td>
            <td class="columna9_cabecera table_border_bottom table_border_top title_info_pedido <?=$class_hidden_pedido?>">PRECIO UNIDAD/M<SUP>2</SUP></td>
            <td class="columna10_cabecera table_border_bottom table_border_top title_info_pedido <?=$class_hidden_pedido?>">DTO.</td>
            <td class="columna11_cabecera table_border_bottom table_border_top title_info_pedido table_border_right <?=$class_hidden_pedido?>">IMPORTE</td>
        </tr>
        <?php endif;

        $total_presupuesto_iva = round2decimals($total_presupuesto * $iva);
        $total_presupuesto_recargo = round2decimals($total_presupuesto * 0.520);
        $total_presupuesto_impuestos = $total_presupuesto + $total_presupuesto_iva + $total_presupuesto_recargo;
        $aux_lineas = $saltos_pagina * 32;

        if($class_hidden_pedido == "hidden"):
            $aux_lineas = $aux_lineas + 10;
        endif;

        //LINEAS EN BLANCO
        for ($i = $num_lineas_presupuesto; $i < $aux_lineas; $i++): ?>
            <tr>
                <td class="columna1 table_border_left table_border_right">&nbsp;</td>
                <td class="columna2 table_border_right">&nbsp;</td>
                <td class="columna3 table_border_right ">&nbsp;</td>
                <td class="columna4 table_border_right ">&nbsp;</td>
                <td class="columna5 table_border_right ">&nbsp;</td>
                <td class="columna6 table_border_right ">&nbsp;</td>
                <td class="columna7 table_border_right ">&nbsp;</td>
                <td class="columna8 table_border_right ">&nbsp;</td>
                <td class="columna9 table_border_right <?=$class_hidden_pedido?>">&nbsp;</td>
                <td class="columna10 table_border_right <?=$class_hidden_pedido?>">&nbsp;</td>
                <td class="columna11 table_border_right <?=$class_hidden_pedido?>">&nbsp;</td>
            </tr>
        <?php endfor;

        ?>

        <tr>
            <td class="columna1 table_border_left table_border_bottom table_border_right">&nbsp;</td>
            <td class="columna2 table_border_bottom table_border_right">&nbsp;</td>
            <td class="columna3 table_border_bottom table_border_right ">&nbsp;</td>
            <td class="columna4 table_border_bottom table_border_right ">&nbsp;</td>
            <td class="columna5 table_border_bottom table_border_right ">&nbsp;</td>
            <td class="columna6 table_border_bottom table_border_right ">&nbsp;</td>
            <td class="columna7 table_border_bottom table_border_right ">&nbsp;</td>
            <td class="columna8 table_border_bottom table_border_right ">&nbsp;</td>
            <td class="columna9 table_border_bottom table_border_right <?=$class_hidden_pedido?> ">&nbsp;</td>
            <td class="columna10 table_border_bottom table_border_right <?=$class_hidden_pedido?>">&nbsp;</td>
            <td class="columna11 table_border_bottom table_border_right <?=$class_hidden_pedido?>">&nbsp;</td>
        </tr>
    </table>
    <span class="info_firma <?=$class_hidden_pedido?>">
        LA FIRMA DE ESTE DOCUMENTO IMPLICA LA ACEPTACIÓN DEL DETALLE TOTAL DEL PRODUCTO, FORMA DE PAGO E IMPORTE
    </span>
    <hr width="106.6%" class="<?=$class_hidden_pedido?>">
    <div class="content_precios <?=$class_hidden_pedido?>">
        <div class="content_precios_line1 line">
            <div class="line1_1 line">I.V.A</div>
            <div class="line1_2 line">RECARGO EQUIVALENCIA</div>
        </div>
        <br>
        <div class="content_precios_line2 line">
            <div class="line2_1 line">BASE IMPONIBLE</div>
            <div class="line2_2 line">%</div>
            <div class="line2_3 line">CUOTA</div>
            <div class="line2_4 line">%</div>
            <div class="line2_5 line">CUOTA</div>
        </div>
        <br>
        <div class="content_precios_line3 line">
            <div class="line3_1 line text-center"><span><?= round2decimals($total_presupuesto) ?></span></div>
            <div class="line3_2 line text-center"><span>21,0</span></div>
            <div class="line3_3 line text-right"><span><?= $total_presupuesto_iva ?></span></div>
            <div class="line3_4 line text-center"><span>5,20</span></div>
            <div class="line3_5 line text-right"><span><?= $total_presupuesto_recargo ?></span></div>
        </div>
        <div class="total_presupuesto">
            <span>TOTAL PRESUPUESTO</span>
            <div class="border_total_presupuesto">
                <span><?= $total_presupuesto_impuestos ?>€</span>
            </div>
        </div>
    </div>
    <div class="file_2_total <?=$class_hidden_pedido?>">
        <div class="file_2_w50">
            <div class="border_text texto_validez <?=$class_hidden_pedido?>">
                LA PRESENTE OFERTA TIENE UNA VALIDEZ HASTA EL <?= $fecha_validez ?>
            </div>
            <div class="border_text texto_pago <?=$class_hidden_pedido?>">
                <span class="font-10 font-bold">FORMA DE PAGO:</span><br>
                <span class="font-10"><?=$texto_forma_pago?></span>&nbsp;&nbsp;&nbsp;
                <span class="font-10"><?=$texto_aplazamiento?></span>
            </div>
        </div>
        <div class="file_2_w50_2 <?=$class_hidden_pedido?>">
            <div class="border_text texto_conforme">
                CONFORME CLIENTE
            </div>
            <div class="border_text texto_sello <?=$class_hidden_pedido?>">
                MOSQUITERAS TÉCNICAS, S.L.
            </div>
        </div>
    </div>
</div>
</body>
</html>