<div class="row col-xs-12 col-md-12 div_select_dire">
    <div class="mt-3">
        <div class="col-xs-4 col-md-4">
            <input class="check_forma_pago" checked type="radio" value="forma_pago_paypal" name="forma_pago">
            <label class="font-light text_forma_pago">&nbsp;&nbsp;Paypal</label>
        </div>
        <div class="col-xs-4 col-md-4">
            <input class="check_forma_pago" type="radio" value="forma_pago_redsys" name="forma_pago">
            <label class="font-light text_forma_pago">&nbsp;&nbsp;Tarjeta de crédito</label><br>
        </div>
        <div class="col-xs-4 col-md-4">
            <input class="check_forma_pago" type="radio" value="forma_pago_transferencia" name="forma_pago">
            <label class="font-light text_forma_pago">&nbsp;&nbsp;Transferencia</label><br>
        </div>
    </div>
</div>
<div class='row col-xs-12 col-md-12'><hr></div>
<div class="row col-xs-12 col-md-12 div_content_formas_pago">
    <div class="col-xs-4 col-md-4 content_forma_pago forma_pago_paypal">
        <div id="paypal-button"></div>
    </div>
    <div class="col-xs-4 col-md-4 content_forma_pago forma_pago_redsys hidden">
        <form class="form-amount" action="<?=path_web?>mods/mod_customers/action/pago_redsys.php" method="post">
            <div class="form-group">
                <label for="amount_redsys">Cantidad</label>
                <input type="text" id="amount_redsys" readonly name="amount" class="form-control amount_redsys" placeholder="Por ejemplo: 50.00">
            </div>
            <input class="btn btn-lg btn-primary btn-block" name="submitPayment" type="submit" value="Pagar">
        </form>
    </div>    
    <div class="col-xs-4 col-md-4 content_forma_pago forma_pago_transferencia hidden">
        Número cuenta: ES00 0000 0000 00 0000000000
    </div>
</div>
<div class='row col-xs-12 col-md-12'>
    <hr>
    Una vez recibido el pago se procederá a la fabricación del producto y el posterior envio del mismo.
</div>