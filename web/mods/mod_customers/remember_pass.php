<?php
session_destroy();

$is_token_ok = false;
$token = "";
if (isset($_GET['token'])):
    $token = $_GET['token'];

    $cliente = new Customers();
    $datos_token = $cliente->getToken($token);

    $fecha_actual = date('Y-m-d H:i:s');

    if (empty($datos_token) OR ( $datos_token['fecha_creacion'] > $fecha_actual) OR $datos_token['fecha_caduca'] < $fecha_actual):

        session_destroy();
        header("Location:" . path_web . "index.php");
        die();

    endif;

    if (!empty($datos_token) AND $fecha_actual >= $datos_token['fecha_creacion'] AND $fecha_actual <= $datos_token['fecha_caduca']):

        $is_token_ok = true;

    endif;

endif;
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12">
<?php if (!empty($token) AND $is_token_ok): ?>
                <h3 class="text-center"><?= lang_modificar_contraseña ?></h3>
                <form class="change_pass">
                    <div class="form-group col-md-12 col-xs-12">
                        <label for="cif_remember"><?= lang_cif_contraseña ?>:</label>
                        <input type="text" readonly value="<?= $datos_token['cldnic'] ?>" class="cif_remember" name="cif_remember" placeholder="<?= lang_introduce_cif ?>">
                    </div>
                    <div class="form-group col-md-12 col-xs-12">
                        <label for="change_pass1"><?= lang_texto_contraseña ?>:</label>
                        <input type="password" class="change_pass1" name="change_pass1" placeholder="<?= lang_texto_introduce_contraseña ?>">
                    </div>
                    <div class="form-group col-md-12 col-xs-12">
                        <label for="change_pass2"><?= lang_texto_repite_contraseña ?>:</label>
                        <input type="password" class="change_pass2" name="change_pass2" placeholder="<?= lang_texto_repite_contraseña ?>">
                    </div>
                    <div class="form-group col-md-12 col-xs-12">
                        <span class="hidden check_change_pass text-danger"><?= lang_texto_error_datos ?></span>
                    </div>
                    <div class="form-group col-md-12 col-xs-12">
                        <button class="hidden button_send_form_change btn btn-default"><?= lang_texto_modificar_contraseña ?></button>
                    </div>
                    <input type="hidden" value="<?= $datos_token['cldnic'] ?>" class="check_change_pass_cif">
                </form>
<?php else: ?>
                <h3 class="text-center"><?= lang_generar_contraseña ?></h3>
                <form class="remember_pass">
                    <div class="form-group col-md-12 col-xs-12">
                        <label class="col-md-1 col-xs-1" for="cif_remember"><?= lang_cif_contraseña ?>:</label>
                        <input type="text" name="cif_remember" placeholder="<?= lang_introduce_cif ?>">
                    </div>
                    <div class="form-group col-md-12 col-xs-12">
                        <label class="col-md-1 col-xs-1" for="cif_remember"><?= lang_email_contraseña ?>:</label>
                        <input type="text" name="email_remember" placeholder="<?= lang_introduce_email ?>">
                    </div>
                    <div class="form-group col-md-12 col-xs-12">
                        <button class="btn btn-default"><?= lang_recordar_contraseña_button ?></button>
                    </div>
                </form>
<?php endif; ?>
        </div>
    </div>
</div>