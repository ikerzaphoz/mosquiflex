<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <h3 class="text-center"></h3>
            <div class="tabbable div_ficha_mobile">
                <ul class="nav col-md-3 col-xs-12">
                    <li class="col-xs-6 active btn-default border-radius4"><a href="#misdatos" data-toggle="tab"><?=lang_text_mis_datos?></a></li>
                    <li class="col-xs-6"><a href="#misdirecciones" data-toggle="tab"><?=lang_text_mis_direcciones_mobile?></a></li>
                </ul>
                <div class="tab-content col-md-9 col-xs-12">
                    <div class="tab-pane active" id="misdatos">
                        <h4 class="text-center"><?=lang_text_datos_fiscales?></h4>
                        <form class="ver_datos_cliente">
                            <div class="form-group col-md-3 col-xs-6">
                                <label for="clnomb"><?=lang_text_nombre?>:</label>
                                <input readonly type="text" name="clnomb" value="<?=$datos_cliente['clnomb']?>">
                            </div>
                            <div class="form-group col-md-3 col-xs-6">
                                <label for="cldir1"><?=lang_text_direccion?> 1:</label>
                                <input readonly type="text" name="cldir1" value="<?=$datos_cliente['cldir1']?>">
                            </div>
                            <div class="form-group col-md-3 col-xs-6">
                                <label for="cldir2"><?=lang_text_direccion?> 2:</label>
                                <input readonly type="text" name="cldir2" value="<?=$datos_cliente['cldir2']?>">
                            </div>
                            <div class="form-group col-md-3 col-xs-6">
                                <label for="clpais"><?=lang_text_pais?>:</label>
                                <input readonly type="text" name="clpais" value="<?=$cliente->getPais($datos_cliente['clpais'])?>">
                            </div>
                            <div class="form-group col-md-3 col-xs-6">
                                <label for="clprov"><?=lang_text_provincia?>:</label>
                                <input readonly type="text" name="clprov" value="<?=$cliente->getProvincia($datos_cliente['clpais'],$datos_cliente['clprov'])?>">
                            </div>
                            <div class="form-group col-md-3 col-xs-6">
                                <label for="clpobl"><?=lang_text_poblacion?>:</label>
                                <input readonly type="text" name="clpobl" value="<?=$cliente->getPoblacion($datos_cliente['clpais'],$datos_cliente['clprov'],$datos_cliente['clpobl'])?>">
                            </div>
                            <div class="form-group col-md-3 col-xs-6">
                                <label for="cldnic"><?=lant_text_dni_cif?>:</label>
                                <input readonly type="text" name="cldnic" value="<?=$datos_cliente['cldnic']?>">
                            </div>
                            <div class="form-group col-md-3 col-xs-6">
                                <label for="cltlf1"><?=lang_company_telefono?> 1:</label>
                                <input readonly type="text" name="cltlf1" value="<?=$datos_cliente['cltlf1']?>">
                            </div>
                            <div class="form-group col-md-3 col-xs-6">
                                <label for="cltlf2"><?=lang_company_telefono?> 2:</label>
                                <input readonly type="text" name="cltlf2" value="<?=$datos_cliente['cltlf2']?>">
                            </div>
                            <div class="form-group col-md-3 col-xs-6">
                                <label for="clfax"><?=lang_text_fax?>:</label>
                                <input readonly type="text" name="clfax" value="<?=$datos_cliente['clfax']?>">
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="misdirecciones">
                        <ul class="nav nav-stacked col-md-3 col-xs-6">
                            <?php foreach($dire_cliente as $index => $item):

                                $active = "";
                                if($index == 0): $active = "active"; endif;

                                ?>
                                <li class="<?=$active?> btn-default border-radius4"><a href="#<?=$index?>" data-toggle="tab"><?=$item['nombre']?></a></li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content col-md-9 col-xs-6">
                            <?php foreach($dire_cliente as $index => $item):

                                $active = "";
                                if($index == 0): $active = "active"; endif;

                                ?>
                                <div class="tab-pane <?=$active?>" id_dire="<?=$item['id']?>" id="<?=$index?>">
                                    <div class="form-group col-md-4 col-xs-12">
                                        <label for="cldir1"><?=lang_text_direccion?> 1:</label>
                                        <input readonly type="text" name="cldir1" value="<?=$item['cldir1']?>">
                                    </div>
                                    <div class="form-group col-md-4 col-xs-12">
                                        <label for="cldir2"><?=lang_text_direccion?> 2:</label>
                                        <input readonly type="text" name="cldir2" value="<?=$item['cldir2']?>">
                                    </div>
                                    <div class="form-group col-md-4 col-xs-12">
                                        <label for="clpais"><?=lang_text_pais?>:</label>
                                        <input readonly type="text" name="clpais" value="<?=$cliente->getPais($item['clpais'])?>">
                                    </div>
                                    <div class="form-group col-md-4 col-xs-12">
                                        <label for="clprov"><?=lang_text_provincia?>:</label>
                                        <input readonly type="text" name="clprov" value="<?=$cliente->getProvincia($item['clpais'],$item['clprov'])?>">
                                    </div>
                                    <div class="form-group col-md-4 col-xs-12">
                                        <label for="clpobl"><?=lang_text_poblacion?>:</label>
                                        <input readonly type="text" name="clpobl" value="<?=$cliente->getPoblacion($item['clpais'],$item['clprov'],$item['clpobl'])?>">
                                    </div>
                                    <div class="form-group col-md-4 col-xs-12">
                                        <label for="cltlf1"><?=lang_company_telefono?> 1:</label>
                                        <input readonly type="text" name="cltlf1" value="<?=$item['cltlf1']?>">
                                    </div>
                                    <div class="form-group col-md-4 col-xs-12">
                                        <label for="cltlf2"><?=lang_company_telefono?> 2:</label>
                                        <input readonly type="text" name="cltlf2" value="<?=$item['cltlf2']?>">
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <div class="row text-center col-md-12 col-xs-12">
                                <?php if(!empty($dire_cliente)):?>
                                    <button class="btn-sm btn btn-danger btn_delete_dire_cliente col-xs-12"><?=lang_text_eliminar_direccion?></button>
                                <?php endif; ?>
                                <button data-toggle="modal" data-target="#modal_add_dire_envio" title="Añadir dirección" class="btn_add_dire_cliente btn-sm btn btn-info col-xs-12 mt-10" aria-hidden="true"><?=lang_text_add_direccion?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>