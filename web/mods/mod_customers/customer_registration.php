<?php 

$array_paises = $cliente->getSelectPaises();

?>
<div class="container mt-3">
    <div class="col-md-12 col-xs-12">
        <h3>Registro cliente</h3>
        <hr>
        <form role="form" action="" method="post" class="form_customer_register">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-6 col-xs-6">
                    <div class="form-group">
                        <label class="control-label">Nombre</label>
                        <input name="nombre_registro" required="required" maxlength="100" type="text" class="form-control" placeholder="Introduce tu nombre"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Apellidos</label>
                        <input name="apellidos_registro" maxlength="100" type="text" class="form-control" placeholder="Introduce tus apellidos" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Dirección</label>
                        <textarea name="direccion_registro" required="required" class="form-control" placeholder="Introduce tu direccion" ></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Pais</label>
                        <select required="required" name="pais_registro" class="form-control select_pais_registro">
                            <option selected value="">Selecciona pais</option>
                            <?=selectPaises($array_paises)?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-xs-6">
                    <div class="form-group">
                        <label class="control-label">Provincia</label>
                        <select required="required" name="provincia_registro" class="form-control select_provincia_registro">
                            <option value="">Selecciona primero pais</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Poblacion</label>
                        <select required="required" name="poblacion_registro" class="form-control select_poblacion_registro">
                            <option value="">Selecciona una provincia</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Telefono</label>
                        <input pattern="[0-9]{9}" name="telefono_registro" maxlength="200" type="text" class="form-control" placeholder="Introduce tu telefono"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label">DNI</label>
                        <input pattern="(([X-Z]{1})([-]?)(\d{7})([-]?)([A-Z]{1}))|((\d{8})([-]?)([A-Z]{1}))" name="dni_registro" maxlength="200" type="text" required="required" class="form-control" placeholder="Introduce tu DNI"  />
                    </div>
                </div>

                <div class="col-md-12 col-xs-12">
                    <h4>Datos cuenta</h4>
                    <hr>
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input name="email_registro" maxlength="200" type="email" required="required" class="form-control" placeholder="Introduce tu email"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Contraseña</label>
                        <input name="contrasenna_registro" maxlength="200" type="password" required="required" class="form-control" placeholder="Introduce tu contraseña"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Repite contraseña</label>
                        <input name="repite_contrasenna_registro" maxlength="200" type="password" required="required" class="form-control" placeholder="Repite contraseña"  />
                    </div>
                    <button class="btn btn-sm border-radius4 btn-submit-register-final pull-right" type="submit">ENVIAR</button>
                </div>
            </div>
        </form>
    </div>
</div>