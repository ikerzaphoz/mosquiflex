<?php

require '../../../../config.php';

$clproc = "";
if(isset($_POST['clproc'])) $clproc = $_POST['clproc'];
$clcodi = "";
if(isset($_POST['clcodi'])) $clcodi = $_POST['clcodi'];
$id_ire = "";
if(isset($_POST['id_ire'])) $id_ire = $_POST['id_ire'];

$cliente = new Customers();
$dire_aux = "";
if($id_ire == "fiscal"):
    $dire_aux = $cliente->get_direccion_envio($clproc,$clcodi);
else:
    $dire_aux = $cliente->get_direccion_envio_id($id_ire);
endif;

$dire = $dire_aux[0];

$pais = $cliente->getPais($dire['clpais']);
$provincia = $cliente->getProvincia($dire['clpais'], $dire['clprov']);
$poblacion = $cliente->getPoblacion($dire['clpais'], $dire['clprov'], $dire['clpobl']);

$array_paises = $cliente->getSelectPaises();
$dire_cliente = $cliente->get_direcciones_envio($_SESSION['clproc'], $_SESSION['clcodi']);
$dire_cliente_fijas = $cliente->get_direcciones_fijas($_SESSION['clproc'], $_SESSION['clcodi']);
$array_direcciones = array_merge($dire_cliente, $dire_cliente_fijas);

?>
<div class="col-md-12 col-xs-12" xmlns="http://www.w3.org/1999/html">
    <?php if($_SESSION['is_cliente_final'] == 1):
     include '../pagos_cliente_final.php';
        else: ?>
    <div class="row col-xs-12 col-md-12 div_select_dire">
        <div class="mt-3">
            <div class="col-xs-4 col-md-4">
                <input class="check_agencia" checked type="radio" value="seleccionar_dire" name="no_envio">
                <label class="font-light text_agencia">&nbsp;&nbsp;Seleccionar dirección</label>
            </div>
            <div class="col-xs-4 col-md-4">
                <input class="check_agencia" type="radio" value="recoge_cliente" name="no_envio">
                <label class="font-light text_agencia">&nbsp;&nbsp;Recoge cliente</label><br>
            </div>
            <div class="col-xs-4 col-md-4">
                <input class="check_agencia" type="radio" value="recoge_agencia_cliente" name="no_envio">
                <label class="font-light text_agencia">&nbsp;&nbsp;Recoge agencia cliente</label><br>
            </div>
        </div>
    </div>
    <div class="row col-xs-12 col-md-12">
        <hr>
    </div>
    <div class="row mt-10 col-xs-12 col-md-12">
        <div class="col-md-4 col-xs-4 select_dire_check">
            <div>
                <select title="" class="form-control select_dir">
                    <option value="fiscal">Dir. Fiscal</option>
                    <?=selectDirecciones($array_direcciones)?>
                    <option class="btn_add_temporal_dire" value="otra_dire">Otra direccion</option>
                </select>
            </div>
            <span class="hidden message_info_add_dire">*La dirección aquí añadida se asignará a este presupuesto y no se guardará en sus direcciones del área de clientes*</span>
        </div>
        <div class="ver_dire col-md-8 col-xs-8">
        <form class="act_dire_entre">
            <div class="form-group col-md-12 col-xs-12">
                <label class="col-md-12 col-xs-12" for="dire">Dirección</label>
                <div class="col-md-6 col-xs-6">
                    <textarea readonly name="cldir1" class="noresize input_change_dir nopadding"><?=$dire['cldir1']?></textarea>
                </div>
                <div class="col-md-6 col-xs-6">
                    <textarea readonly name="cldir2" class="noresize input_change_dir nopadding"><?=$dire['cldir2']?></textarea>
                </div>
            </div>
            <div class="form-group col-md-12 col-xs-12">
                <label class="col-md-6 col-xs-6" for="pais">Pais</label>
                <label class="col-md-6 col-xs-6" for="pais">Provincia</label>
                <div class="form-group col-md-6 col-xs-6">
                    <input name="pais" readonly class="w100 col-md-6 col-xs-6 nopadding" value="<?=$pais?>">
                </div>
                <div class="form-group col-md-6 col-xs-6">
                    <input name="provincia" readonly class="w100 col-md-6 col-xs-6 nopadding" value="<?=$provincia?>">
                </div>
            </div>
            <div class="form-group col-md-12 col-xs-12">
                <label class="col-md-6 col-xs-6" for="pais">Poblacion</label>
                <label class="col-md-6 col-xs-6" for="tlf1">Teléfono</label>
                <div class="form-group col-md-6 col-xs-6">
                    <input readonly name="poblacion" class="w100 col-md-6 col-xs-6 nopadding" value="<?=$poblacion?>">
                </div>
                <div class="form-group col-md-6 col-xs-6">
                    <input readonly class="w100 col-md-6 col-xs-6 nopadding" name="cltlf1" value="<?=$dire['cltlf1']?>">
                </div>
            </div>

            <div class="form-group col-md-12 col-xs-12">
                <label class="col-md-6 col-xs-6" for="tlf1">Móvil</label>
            </div>
            <div class="form-group col-md-12 col-xs-12">
                <div class="form-group col-md-6 col-xs-6">
                    <input readonly class="w100 col-md-6 col-xs-6 nopadding" name="cltlf2" value="<?=$dire['cltlf2']?>">
                </div>
                <div class="form-group col-md-6 col-xs-6 text-right">
                    <button type="button" class="btn-sm btn bg-border-success btn_select_dire">Seleccionar y guardar</button>
                </div>
            </div>

            <input type="hidden" name="clproc" value="<?=$clproc?>">
            <input type="hidden" name="clcodi" value="<?=$clcodi?>">

        </form>
        <form class="hidden add_dire_entre">
            <div class="form-group col-md-12 col-xs-12">
                <label class="col-md-12 col-xs-12" for="dire">Dirección</label>
                <div class="col-md-6 col-xs-6">
                    <textarea name="cldir1" placeholder="Ej: C/Rosal nº5" class="noresize input_change_dir nopadding"></textarea>
                </div>
                <div class="col-md-6 col-xs-6">
                    <textarea name="cldir2" placeholder="Ej: Polígono Industrial" class="noresize input_change_dir nopadding"></textarea>
                </div>
            </div>
            <div class="form-group col-md-12 col-xs-12">
                <label class="col-md-6 col-xs-6" for="pais">Pais</label>
                <label class="col-md-6 col-xs-6" for="pais">Provincia</label>
                <div class="form-group col-md-6 col-xs-6">
                    <select name="pais" class="select_change_pais nopadding">
                        <option selected value="">Selecciona pais</option>
                        <?=selectPaises($array_paises)?>
                    </select>                </div>
                <div class="form-group col-md-6 col-xs-6">
                    <select name="provincia" class="select_change_provincias nopadding">
                        <option value="">Selecciona primero pais</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-12 col-xs-12">
                <label class="col-md-6 col-xs-6" for="select_pobl">Poblacion</label>
                <label class="col-md-6 col-xs-6" for="poblacion">Código Postal</label>
                <div class="form-group col-md-6 col-xs-6">
                    <select name="select_pobl" class="select_change_poblacion nopadding">
                        <option value="">Selecciona primero provincia</option>
                    </select>                
                </div>
                <div class="form-group col-md-6 col-xs-6">
                    <select name="poblacion" class="select_change_cp nopadding">
                        <option value="">Selecciona primero poblacion</option>
                    </select>                
                </div>

            </div>
            <div class="form-group col-md-12 col-xs-12">
                <label class="col-md-6 col-xs-6" for="tlf1">Teléfono</label>
                <label class="col-md-6 col-xs-6" for="tlf2">Móvil</label>
            </div>
            <div class="form-group col-md-12 col-xs-12">
                <div class="form-group col-md-6 col-xs-6">
                    <input pattern="[0-9]{9}" title="Campo teléfono. 9 números necesarios." placeholder="Ej. 668899999" class="col-md-6 col-xs-6 nopadding" name="cltlf1" value="">
                </div>
                <div class="form-group col-md-6 col-xs-6">
                    <input pattern="[0-9]{9}" title="Campo teléfono. 9 números necesarios." placeholder="Ej. 668899999" class="col-md-6 col-xs-6 nopadding" name="cltlf2" value="">
                </div>
                <div class="hidden form-group col-md-6 col-xs-6">
                    <label class="input_name_dire col-md-6 col-xs-6" for="name">Nombre:</label>
                    <input name="nombre" placeholder="Ej: Casa" title="Introduzca algún valor" class="hidden col-md-6 col-xs-6 input_name input_name_dire nopadding" value="Temporal">
                </div>
                <div class="form-group col-md-6 col-xs-6 text-right">
                    <button type="submit" class="btn bg-border-success btn_add_new_dire">Añadir y guardar</button>
                </div>
            </div>

            <input type="hidden" name="is_temporal" value="1">
            <input type="hidden" name="clproc" value="<?=$_SESSION['clproc']?>">
            <input type="hidden" name="clcodi" value="<?=$_SESSION['clcodi']?>">
        </form>
    </div>
    <div class="hidden col-md-8 col-xs-8 div_check_agencia">
        <form class="form_check_agencia">
            <div class="form-group col-md-12 col-xs-12 text-right">
                <button type="submit" class="btn bg-border-success">Guardar</button>
            </div>
        </form>
    </div>
    </div>
    <?php endif;?>
</div>