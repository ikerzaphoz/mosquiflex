<?php

require '../../../../config.php';

$presupuesto = new Presupuesto();

$id_pres = "";
if(isset($_POST['id_pres'])) $id_pres = $_POST['id_pres'];

$affected = $presupuesto->deletePresupuesto($id_pres);

if($affected == 1):
    $array_error = array('error' => '0', 'message' => 'Eliminado correctamente');
else:
    $array_error = array('error' => '-1', 'message' => 'Error al eliminar');
endif;

echo json_encode($array_error);