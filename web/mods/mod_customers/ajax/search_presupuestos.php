<?php

require '../../../../config.php';

$search = "";
if(isset($_POST['text'])) $search = $_POST['text'];
$id_cliente = "";
if(isset($_POST['id_cliente'])) $id_cliente = $_POST['id_cliente'];

$presupuesto = new Presupuesto();
$array_presupuesto = $presupuesto->search_presupuestos($id_cliente, $search);

if(empty($array_presupuesto)):?>

    <div class="col-md-12 text-center mt-10 row">
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> No se encuentran resultados
    </div>

<?php

else: ?>

    <table class="table_historial table sortable">
        <thead>
        <tr>
            <th class="order_table">Número&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></th>
            <th data-defaultsort='disabled'>Referencia</th>
            <th class="order_table">Fecha&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></th>
            <th data-defaultsort='disabled'>Total (€)</th>
            <th data-defaultsort='disabled'>Opciones</th>
            <th data-defaultsort='disabled'>Mis notas</th>
        </tr>
        </thead>

        <tbody>

        <?php

        foreach($array_presupuesto as $item):

            $referencia = "-";
            if(!empty($item['referencia'])):
                $referencia = $item['referencia'];
            endif;

            ?>
            <tr>
                <td><?=hideNumPre($item['num_pre'])?></td>
                <td><?=$referencia?></td>
                <td><?=$item['fecha']?></td>
                <td><?=round2decimals($item['total'])?></td>
                <td><a target="_blank" href="<?=path_web_mods?>mod_pdf/index.php?pres=<?=$item['id']?>" <span class="btn-sm btn_pdf_pedido btn btn-danger fa fa-file-pdf-o">&nbsp;Ver</span></a></td>
                <td><button data-toggle="modal" data-target="#modal_edit_notas" title="Mis notas" class="btn_ver_notas btn btn-info" aria-hidden="true">Ver notas</button></td>
                <input type="hidden" class="id_pres" value="<?=$item['id']?>">
                <input type="hidden" class="id_cliente" value="<?=$id_cliente?>">
            </tr>
        <?php endforeach; ?>

        </tbody>

    </table>

<?php endif; ?>