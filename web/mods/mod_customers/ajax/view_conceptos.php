<?php
require '../../../../config.php';

$id_pres = "";
if (isset($_POST['id_pres']))
    $id_pres = $_POST['id_pres'];
$presupuesto = new Presupuesto();
$array_conceptos = $presupuesto->get_conceptos_pedido($id_pres);
?>

<form class="view_conceptos" name="view_conceptos">
    <div class="form-group">
        <textarea name="text_concepto" class="form-control text_concepto" placeholder="<?= lang_text_add_concepto ?>"></textarea>
    </div>
    <div class="form-group">
        <select class="form-control tipo_concepto" name="tipo_concepto">
            <option value="" disabled selected><?= lang_text_select_opcion ?></option>
            <option value="0"><?= lang_text_select_importe ?></option>
            <option value="1"><?= lang_text_select_porcentaje ?></option>
        </select>
    </div>
    <div class="form-group">
        <select class="form-control aumento_concepto" name="aumento_concepto">
            <option value="" disabled selected><?= lang_text_select_opcion ?></option>
            <option value="0"><?= lang_text_select_incremento ?></option>
            <option value="1"><?= lang_text_select_descuento ?></option>
        </select>
    </div>
    <div class="form-group">
        <input type="text" class="form-control valor_concepto" name="valor_concepto" placeholder="<?= lang_text_select_introduce_importe ?>">
    </div>
    <button type="button" class="btn_add_concepto_historial btn bg-border-success"><span>Añadir concepto</span></button>
    <input type="hidden" class="id_pres_view" value="<?= $id_pres ?>">
</form>

<?php if ($array_conceptos): ?>
    <div class="row content_conceptos_view form-group">
        <div class="col-md-12 col-xs-12">
            <div class="col-md-3 col-xs-3">Concepto</div>
            <div class="col-md-3 col-xs-3">Tipo</div>
            <div class="col-md-3 col-xs-3">Tipo</div>
            <div class="col-md-2 col-xs-2">Cantidad</div>
            <div class="col-md-1 col-xs-1"></div>
        </div>
        <?php
        foreach ($array_conceptos as $linea_concepto):

            $texto_concepto = $linea_concepto['texto_concepto'];
            $tipo_concepto = $linea_concepto['tipo_concepto'];
            if (empty($tipo_concepto)):
                $tipo_concepto = "Importe";
            else:
                $tipo_concepto = "Porcentaje";
            endif;
            $aumento_concepto = $linea_concepto['aumento_concepto'];
            if (empty($aumento_concepto)):
                $aumento_concepto = "Incremento";
            else:
                $aumento_concepto = "Descuento";
            endif;
            $valor_concepto = $linea_concepto['valor_concepto'];
            ?>
            <div class="col-md-12 col-xs-12 line_concepto">
                <div class="col-md-3 col-xs-3">
                    <?= $texto_concepto ?>
                </div>
                <div class="col-md-3 col-xs-3">
                    <?= $tipo_concepto ?>
                </div>
                <div class="col-md-3 col-xs-3">
                    <?= $aumento_concepto ?>
                </div>
                <div class="col-md-2 col-xs-2">
                    <?= $linea_concepto['valor_concepto'] ?>
                </div>
                <div class="col-md-1 col-xs-1">
                    <span class="fa fa-close text-danger delete-concepto" id_linea_concepto="<?=$linea_concepto['id']?>"></span>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>