<?php

require '../../../../config.php';

$cliente = new Customers();

$array_error = array('error' => '-1', 'message' => 'Error desconocido. Inténtelo más tarde');

$nombre_registro = "";
if(isset($_POST['nombre_registro'])) $nombre_registro = $_POST['nombre_registro'];
$apellidos_registro = "";
if(isset($_POST['apellidos_registro'])) $apellidos_registro = $_POST['apellidos_registro'];
$direccion_registro = "";
if(isset($_POST['direccion_registro'])) $direccion_registro = $_POST['direccion_registro'];
$pais_registro = "";
if(isset($_POST['pais_registro'])) $pais_registro = $_POST['pais_registro'];
$provincia_registro = "";
if(isset($_POST['provincia_registro'])) $provincia_registro = $_POST['provincia_registro'];
$poblacion_registro = "";
if(isset($_POST['poblacion_registro'])) $poblacion_registro = $_POST['poblacion_registro'];
$telefono_registro = "";
if(isset($_POST['telefono_registro'])) $telefono_registro = $_POST['telefono_registro'];
$dni_registro = "";
if(isset($_POST['dni_registro'])) $dni_registro = $_POST['dni_registro'];
$email_registro = "";
if(isset($_POST['email_registro'])) $email_registro = $_POST['email_registro'];
$contrasenna_registro = "";
if(isset($_POST['contrasenna_registro'])) $contrasenna_registro = $_POST['contrasenna_registro'];
$repite_contrasenna_registro = "";
if(isset($_POST['repite_contrasenna_registro'])) $repite_contrasenna_registro = $_POST['repite_contrasenna_registro'];

$existe_email = "";
$existe_email = $cliente->comprobar_existe_email($email_registro);

if(!empty($existe_email)):
    $array_error = array('error' => '-1', 'message' => 'Email ya registrado. Revise los datos');
    echo json_encode($array_error);
    exit;
endif;

if(!empty($nombre_registro) && !empty($direccion_registro) && !empty($direccion_registro) && !empty($pais_registro) && !empty($provincia_registro) && !empty($poblacion_registro) && !empty($dni_registro) && !empty($email_registro) && !empty($contrasenna_registro) && !empty($repite_contrasenna_registro) && empty($existe_email)):
    if($contrasenna_registro == $repite_contrasenna_registro):
        
        $clcodi = $cliente->numero_cliente_final()+1;
        $nombre_final = $nombre_registro." ".$apellidos_registro;
        $contrasenna_sha = sha1($contrasenna_registro);
    
        $total_insertadas = $cliente->registro_cliente_final('99', $clcodi, $nombre_final, $direccion_registro, $pais_registro, $provincia_registro, $poblacion_registro, $dni_registro, $telefono_registro, $contrasenna_sha, $email_registro, '1');
        
        if($total_insertadas >= "1"):
            $array_error = array('error' => '0', 'message' => 'Cliente registrado correctamente.');
        else:
            $array_error = array('error' => '1', 'message' => 'Error al añadir el cliente. Revise los datos o inténtelo más tarde.');
        endif;
        
    else:
        $array_error = array('error' => '2', 'message' => 'No coinciden las contraseñas. Revise los campos.');
    endif;
else:
    $array_error = array('error' => '3', 'message' => 'Falta infomación para completar el resgistro. Revise los datos');
endif;

echo json_encode($array_error);