<?php

require '../../../../config.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$cliente = new Customers();

$result_update = 0;

$cif_remember = "";
if (isset($_POST['cif_remember']))
    $cif_remember = $_POST['cif_remember'];

$change_pass1 = "";
if (isset($_POST['change_pass1']))
    $change_pass1 = $_POST['change_pass1'];

$change_pass2 = "";
if (isset($_POST['change_pass2']))
    $change_pass2 = $_POST['change_pass2'];

if (!empty($cif_remember) AND $change_pass1 == $change_pass2):

    $change_pass1_sha = sha1($change_pass1);
    $result_update = $cliente->setPassword($cif_remember, $change_pass1_sha);
    $cliente->empty_token($cif_remember);
    $email_cliente = $cliente->getEmail($cif_remember);

endif;

if (!empty($email_cliente)):

    $mail = new PHPMailer();                              // Passing `true` enables exceptions
    try {
        $imagen_logo = url_web . path_image . "logo_mosquiflex.png";
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        //$mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.mosquiflex.es';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'mosquiterastecnicas@mosquiflex.es';                 // SMTP username
        $mail->Password = 'mMosS..TteE..17';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;   
        // Activo condificacción utf-8
        $mail->CharSet = 'UTF-8';// TCP port to connect to
        //Recipients
        $mail->setFrom('administracion@mosquiflex.es', 'Mosquiflex - Modificación contraseña');
        $mail->addAddress($email_cliente, 'Email');     // Add a recipient
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "Cambio de contraseña";
        $email_body = "Su contraseña ha sido modificada correctamente.<br>Si usted no ha solicitado el cambio de la misma póngase en contacto con nosotros.";
        include root . 'web/mods/mod_emails/header.php';
        include root . 'web/mods/mod_emails/footer.php';
        $mail->Body = $email_header . $email_body . $email_footer;
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        $mail->send();
    } catch (Exception $e) {
        
    }

endif;

echo $result_update;
