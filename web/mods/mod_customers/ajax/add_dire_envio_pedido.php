<?php

require '../../../../config.php';

$cliente = new Customers();
$presupuesto = new Presupuesto();

$nombre = "";
if(isset($_POST['nombre'])) $nombre = $_POST['nombre'];
$destinatario = "";
if(isset($_POST['destinatario'])) $destinatario = $_POST['destinatario'];
$clproc = "";
if(isset($_POST['clproc'])) $clproc = $_POST['clproc'];
$clcodi = "";
if(isset($_POST['clcodi'])) $clcodi = $_POST['clcodi'];
$cldir1 = "";
if(isset($_POST['cldir1'])) $cldir1 = $_POST['cldir1'];
$cldir2 = "";
if(isset($_POST['cldir1'])) $cldir2 = $_POST['cldir2'];
$pais = "";
if(isset($_POST['pais'])) $pais = $_POST['pais'];
$provincia = "";
if(isset($_POST['provincia'])) $provincia = $_POST['provincia'];
$poblacion = "";
if(isset($_POST['poblacion'])) $poblacion = $_POST['poblacion'];
$cltlf1 = "";
if(isset($_POST['cltlf1'])) $cltlf1 = $_POST['cltlf1'];
$cltlf2 = "";
if(isset($_POST['cltlf2'])) $cltlf2 = $_POST['cltlf2'];
$is_temporal = "1";
if(isset($_POST['is_temporal'])) $is_temporal = $_POST['is_temporal'];
$id_presupuesto = "";
$cont_pre = $presupuesto->count()+1;
$num_pre = "Ppto. MT. CL ".$_SESSION['clproc']. $_SESSION['clcodi']."-".str_pad($cont_pre, 8, "0", STR_PAD_LEFT);

$array_error = array('error' => '-2', 'message' => 'Error desconocido');

$affected = $cliente->add_dire_envio($cldir1, $destinatario, $cldir2, $pais, $provincia, $poblacion, $cltlf1, $cltlf2, $nombre, $clproc, $clcodi, $is_temporal);

if($affected == 1):
    $last_id = $cliente->get_last_id_dire($_SESSION['clproc'],$_SESSION['clcodi']);
    $array_error = array('error' => '0', 'message' => 'Dirección creada correctamente', 'last_id' => $last_id[0]['id']);
elseif($affected < 0):
    $array_error = array('error' => '-1', 'message' => 'Error al crear dirección');
else:
    $array_error = array('error' => '1', 'message' => 'No se han encontrado cambios');
endif;

echo json_encode($array_error);