<?php

require '../../../../config.php';

$id_pais = "";
if (isset($_POST['id_pais']))
    $id_pais = $_POST['id_pais'];
$id_prov = "";
if (isset($_POST['id_prov']))
    $id_prov = $_POST['id_prov'];
$nomb_pobl = "";
if (isset($_POST['nomb_pobl']))
    $nomb_pobl = $_POST['nomb_pobl'];

$cliente = new Customers();
$array_codigos_postales = $cliente->getCodigosPostales($id_pais, $id_prov, $nomb_pobl);
print_r($array_codigos_postales);
$total_codigos = count($array_codigos_postales);
foreach ($array_codigos_postales as $cp):
    $prov = str_pad($cp['cpprov'], 2, "0", STR_PAD_LEFT);
    $pobl = str_pad($cp['cppobl'], 3, "0", STR_PAD_LEFT);
    echo "<option value=" . $cp['cppobl'] . ">" . $prov . $pobl . "</option>";
endforeach;