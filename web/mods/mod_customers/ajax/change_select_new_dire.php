<?php

require '../../../../config.php';
$last_id = "";
if(isset($_POST['last_id'])) $last_id = $_POST['last_id'];

$cliente = new Customers();
$array_dire = $cliente->get_direcciones_envio($_SESSION['clproc'], $_SESSION['clcodi']);

?>

<option value="fiscal">Dir. Fiscal</option>
<?php foreach($array_dire as $dire):?>
    <?php

    $select = "";
    if($dire['id'] == $last_id) $select = "selected";

    ?>
<option <?=$select?> value="<?=$dire['id']?>"><?=$dire['nombre']?></option>
<?php endforeach;?>
