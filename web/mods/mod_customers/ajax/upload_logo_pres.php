<?php

require '../../../../config.php';
ini_set("display_errors", 1);

$fecha_hoy = "logos";
if(!is_dir(path_uploads_files.$fecha_hoy)){
    mkdir(path_uploads_files.$fecha_hoy, 0755, true);
}

$nombre_fichero = strtolower($_SESSION['clproc'].$_SESSION['clcodi']);
$nombre_fichero = str_replace(" ", "", $nombre_fichero);

$titulo_respuesta = "Logo presupuesto";

$array_error = array('error' => '1', 'message' => 'Error al subir archivo', 'title' => $titulo_respuesta);

foreach ($_FILES["archivo1"] as $key => $error) {
    if ($error == UPLOAD_ERR_OK) {
        $array_nombre_extension = explode('.', $_FILES["archivo1"]["name"]);
        $extension = array_pop($array_nombre_extension);
        move_uploaded_file( $_FILES["archivo1"]["tmp_name"], path_uploads_files . "/" . $fecha_hoy . "/" . $nombre_fichero.".".$extension);
        $array_error = array('error' => '0', 'message' => 'Archivo subido correctamente', 'title' => $titulo_respuesta, 'file_title_hidden' => $nombre_fichero."_.".$extension, 'file_title' => $_FILES["archivo1"]["name"]);
    }
}

echo json_encode($array_error);