<?php

require '../../../../config.php';

$presupuesto = new Presupuesto();
$familia = new Familia();
$array_familia = $familia->getFamilia('fafami');

$bd = new Db();

$id_pres = "";
if (isset($_POST['id_pres'])): $id_pres = $_POST['id_pres'];
endif;

$total_alb = $presupuesto->count_albaranes($id_pres);
$sql_alb = "SELECT * FROM albaranes WHERE num_pre = '$id_pres' AND empresa = '".empresa."'";
$array_alb = $bd->obtener_consultas($sql_alb);
$precio_total = 0;
foreach ($array_alb as $index => $item):

    $id_linea = $item['id'];

    $is_almacen = "0";
    if (isset($item['is_almacen'])): $is_almacen = $item['is_almacen'];
    endif;
    if (empty($item['albobs'])):

        $arfami = $item['albfami'];
        $arsubf = $item['albsub'];
        $aranch = $item['albanc'];
        $aralt = $item['albalt'];
        $arunits = $item['albunit'];

        if ($arfami == 2731):
            $ancho_rango = calc_precio_enrollable_ventana($aranch, $array_familia[$arfami]['min_ran_anc']);
            $alto_rango = calc_precio_enrollable_ventana($aralt, $array_familia[$arfami]['min_ran_alt']);
        endif;

        if ($arfami == 2732):
            $ancho_rango = calc_precio_ancho_enrollable_puerta($aranch, $array_familia[$arfami]['min_ran_anc']);
            $alto_rango = calc_precio_alto_enrollable_puerta($aralt, $array_familia[$arfami]['min_ran_alt']);
        endif;

        if ($arfami == 2733):
            $ancho_rango = calc_precio_ancho_plisada($aranch, $array_familia[$arfami]['min_ran_anc'], $arsubf);
            $alto_rango = calc_precio_alto_plisada($aralt, $array_familia[$arfami]['min_ran_alt']);
        endif;

        if ($arfami == 2734):
            $ancho_rango = calc_precio_ancho_fija($aranch, $array_familia[$arfami]['min_ran_anc']);
            $alto_rango = calc_precio_alto_fija($aralt, $array_familia[$arfami]['min_ran_alt']);
        endif;

        if ($arfami == 2735):
//            $ancho_rango = calc_precio_ancho_corredera($aranch, $array_familia[$arfami]['min_ran_anc']);
//            $alto_rango = calc_precio_alto_corredera($aralt, $array_familia[$arfami]['min_ran_alt']);
//            $aranch_hoja_rango = calc_precio_hoja_corredera($aranch);
        endif;

        if ($arfami == 2736):
            $ancho_rango = calc_precio_ancho_abatible($aranch, $array_familia[$arfami]['min_ran_anc'], $arsubf);
            $alto_rango = calc_precio_alto_abatible($aralt, $array_familia[$arfami]['min_ran_alt']);
        endif;

        if ($arfami == 2735):

//            $rango_hoja = $aranch_hoja_rango . $alto_rango;
//            $precio_rango_hoja = $familia->getPriceOfRango($arfami, "1" . $arsubf, $rango_hoja);
//            $precio_rango_hoja = $precio_rango_hoja * 2;
//            $precio_rango_hoja = number_format($precio_rango_hoja, 2);
//            if (strpos($precio_rango_hoja, ",")):
//                $precio_rango_hoja = str_replace(",", "", $precio_rango_hoja);
//            endif;
//
//            $rango = $ancho_rango . $alto_rango;
//            $precio_rango = $familia->getPriceOfRango($arfami, $arsubf, $rango);
//            $precio_rango = $precio_rango;
//            $precio_rango = number_format($precio_rango, 2);
//            if (strpos($precio_rango, ",")):
//                $precio_rango = str_replace(",", "", $precio_rango);
//            endif;
//
//            $precio_rango = $precio_rango + $precio_rango_hoja;

        else:

            $rango = $ancho_rango . $alto_rango;

            $precio_rango = $familia->getPriceOfRango($arfami, $arsubf, $rango);
            $precio_rango = $precio_rango;
            $precio_rango = number_format($precio_rango, 2);
            if (strpos($precio_rango, ",")):
                $precio_rango = str_replace(",", "", $precio_rango);
            endif;

        endif;
    elseif (!empty($item['albobs'] AND $item['albobs'] == "lacado")):
        $precio_rango = round2decimals($item['albpre']);
    elseif (!empty($item['albobs'] AND $item['albobs'] == "is_component")):
        $arfami = $item['albfami'];
        $arsubf = $item['albsub'];
        $arcolo = $item['albco'];
        $array_info_component = $familia->searchComponentByCode($arfami, $arsubf, $arcolo);
        if ($is_almacen == 1):
            $precio_rango = $array_info_component[0]['arpval'];
        else:
            $precio_rango = $array_info_component[0]['arpvta'];
        endif;
    elseif (!empty($item['albobs'] AND $item['albobs'] == "is_component_unit")):
        $arfami = $item['albfami'];
        $arsubf = $item['albsub'];
        $arcolo = $item['albco'];
        $array_info_component = $familia->searchComponentByCode($arfami, $arsubf, $arcolo);
        if ($is_almacen == 1):
            $precio_rango = $array_info_component[0]['arpval'];
        else:
            $precio_rango = $array_info_component[0]['arpvta'];
        endif;
    elseif (!empty($item['albobs'] AND $item['albobs'] == "is_component_meter")):
        $arfami = $item['albfami'];
        $arsubf = $item['albsub'];
        $arcolo = $item['albco'];
        $array_info_component = $familia->searchComponentByCode($arfami, $arsubf, $arcolo);
        if ($is_almacen == 1):
            $precio_rango = $array_info_component[0]['arpval'];
        else:
            $precio_rango = $array_info_component[0]['arpvta'];
        endif;
    elseif (!empty($item['albobs'] AND strpos($item['albobs'], "is_increment") !== false)):
        $aux_id = str_replace("is_increment","",$item['albobs']);
        $arfami = substr($aux_id, 0, 4);
        $arsubf = substr($aux_id, 4, 2);
        $ararti = substr($aux_id, 6, 6);
        $array_info_component = $familia->searchComponentByCode($arfami, $arsubf, $ararti);
        if ($is_almacen == 1):
            $precio_rango = $array_info_component[0]['arpvp1'];
        else:
            $precio_rango = $array_info_component[0]['arpvp1'];
        endif;
    elseif ($item['albfami'] == "1999" AND !empty($item['albunit'])):

            $precio_rango = round2decimals($item['albpre']);
            $precio_rango = round2decimals($precio_unitario);

    endif;
    
    $precio_total = $precio_total + $precio_rango;

    $presupuesto->actualizar_precio_linea_pedido($id_linea, $precio_rango);

endforeach;

$precio_total = round2decimals($precio_total);

$presupuesto->actualizar_fecha_pedido($id_pres, $precio_total);

$array_response = Array("error" => '0', "message" => "Recalculado el precio del presupuesto");
echo json_encode($array_response, true);
