<?php

require '../../../../config.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$enviar_email = false;
$acceso_nuevo = 0;

$tipo_email = "contraseña";

if (isset($_POST)):

    $cif_remember = "";
    if (isset($_POST['cif_remember'])):
        $cif_remember = $_POST['cif_remember'];
    endif;
    $email_remember = "";
    if (isset($_POST['email_remember'])):
        $email_remember = $_POST['email_remember'];
    endif;

    $token = "";
    $respuesta = "";

    $cliente = new Customers();
    $email_cliente = $cliente->getEmail($cif_remember);
    $fecha_creacion = date('Y-m-d H:i:s');
    $fecha_caduca = date('Y-m-d H:i:s', strtotime('+5 minutes'));
    
    $existe_email = "";
    $existe_email = $cliente->comprobar_existe_email($email_remember);

    if(!empty($existe_email)):
        $array_error = array('error' => '-1', 'message' => 'Email ya registrado. Revise los datos');
        echo json_encode($array_error);
        exit;
    endif;

    if (!empty($email_cliente)):
        $acceso_nuevo = 0;
        if ($email_cliente == $email_remember):
            $enviar_email = true;
            $recuperar_pass = $cliente->getInfoPass($cif_remember);
            $token = sha1($cif_remember . time());
            if (!empty($recuperar_pass[0]['cldnic'])):
                $cliente->setToken($cif_remember, $token, $fecha_creacion, $fecha_caduca);
            else:
                $cliente->setTokenInsert($cif_remember, $token, $fecha_creacion, $fecha_caduca);
            endif;
        else:
            $respuesta = array('error' => '-1', 'mensaje' => 'Error. El email del cliente no coincide');
            echo json_encode($respuesta);
            exit;
        endif;

    else:
        $acceso_nuevo = 1;
        if (is_valid_email($email_remember) == 1):
            $cliente->setEmail($cif_remember, $email_remember);
            $enviar_email = true;
            $token = sha1($cif_remember . time());
            $cliente->setTokenInsert($cif_remember, $token, $fecha_creacion, $fecha_caduca);
            $recuperar_pass = $cliente->getInfoPass($email_remember);
        else:
            $respuesta = array('error' => '-2', 'mensaje' => 'Error. No se ha podido actualizar el email');
            echo json_encode($respuesta);
            exit;
        endif;

    endif;

else:
    $respuesta = array('error' => '-3', 'mensaje' => 'Error. No se han podido enviar los datos');
    echo json_encode($respuesta);
    exit;
endif;

if (isset($enviar_email)):
    
    $mail = new PHPMailer();                              // Passing `true` enables exceptions
    try {
        $imagen_logo = '';
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        //$mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.mosquiflex.es';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'mosquiterastecnicas@mosquiflex.es';                 // SMTP username
        $mail->Password = 'mMosS..TteE..17';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to
        //Recipients
        // Activo condificacción utf-8
        $mail->CharSet = 'UTF-8';
        $mail->setFrom('administracion@mosquiflex.es', 'Mosquiflex - Recordatorio contraseña');
        $mail->addAddress($email_remember, 'Email');     // Add a recipient
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "Solicitud de contraseña";
        if($acceso_nuevo == 1):
            $email_body = "Bienvenido. Para acceder a la plataforma, deberás crear una contraseña.<br>Haga click <a href='" . url_web . path_web . "clientes.php?opt=remember_pass&token=" . $token . "'>aquí</a> para continuar.";
            else:
            $email_body = "Solicitado cambio de contraseña.<br>Si usted no ha solicitado el cambio de la misma póngase en contacto con nosotros.<br>Haga click <a href='" . url_web . path_web . "clientes.php?opt=remember_pass&token=" . $token . "'>aquí</a> para continuar.";
        endif;
        
        include root . 'web/mods/mod_emails/header.php';
        include root . 'web/mods/mod_emails/footer.php';
        $mail->Body = $email_header . $email_body . $email_footer;
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        $mail->send();
        $respuesta = array('error' => '1', 'mensaje' => 'Email enviado a ' . $email_remember);
        echo json_encode($respuesta);
        exit;
    } catch (Exception $e) {
        $respuesta = array('error' => '-4', 'mensaje' => 'Error al enviar el email. ' . $mail->ErrorInfo);
        echo json_encode($respuesta);
        exit;
    }

endif;
