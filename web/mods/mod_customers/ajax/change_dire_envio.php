<?php

require '../../../../config.php';
$cliente = new Customers();

$id_ire = "";
if(isset($_POST['id_dire'])) $id_ire = $_POST['id_dire'];
$clproc = "";
if(isset($_POST['clproc'])) $clproc = $_POST['clproc'];
$clcodi = "";
if(isset($_POST['clcodi'])) $clcodi = $_POST['clcodi'];
$dire = "";
$dire_aux = "";
if($id_ire == "fiscal"):
    $dire_aux = $cliente->get_direccion_envio($clproc,$clcodi);
else:
    $dire_aux = $cliente->get_direccion_envio_id($id_ire);
endif;

$dire = $dire_aux[0];

$pais = $cliente->getPais($dire['clpais']);
$provincia = $cliente->getProvincia($dire['clpais'], $dire['clprov']);
$poblacion = $cliente->getPoblacion($dire['clpais'], $dire['clprov'], $dire['clpobl']);

?>

<form class="act_dire_entre">
    <div class="form-group col-md-6 col-xs-6">
        <label class="col-md-6 col-xs-6" for="dire">Dirección 1:</label>
        <input name="cldir1" class="input_change_dir col-md-6 col-xs-6 nopadding" value="<?=$dire['cldir1']?>">
    </div>
    <div class="form-group col-md-6 col-xs-6">
        <label class="col-md-6 col-xs-6" for="dire">Dirección 2:</label>
        <input name="cldir2" class="col-md-6 col-xs-6 nopadding" value="<?=$dire['cldir2']?>">
    </div>
    <div class="form-group col-md-6 col-xs-6">
        <label class="col-md-6 col-xs-6" for="pais">Pais:</label>
        <input name="pais" class="col-md-6 col-xs-6 nopadding" value="<?=$pais?>">
    </div>
    <div class="form-group col-md-6 col-xs-6">
        <label class="col-md-6 col-xs-6" for="pais">Provincia:</label>
        <input name="provincia" class="col-md-6 col-xs-6 nopadding" value="<?=$provincia?>">
    </div>
    <div class="form-group col-md-6 col-xs-6">
        <label class="col-md-6 col-xs-6" for="pais">Poblacion:</label>
        <input name="poblacion" class="col-md-6 col-xs-6 nopadding" value="<?=$poblacion?>">
    </div>
    <div class="form-group col-md-6 col-xs-6">
        <label class="col-md-6 col-xs-6" for="tlf1">Teléfono:</label>
        <input class="col-md-6 col-xs-6 nopadding" name="cltlf1" value="<?=$dire['cltlf1']?>">
    </div>
    <div class="form-group col-md-6 col-xs-6">
        <label class="col-md-6 col-xs-6" for="tlf1">Móvil:</label>
        <input class="col-md-6 col-xs-6 nopadding" name="cltlf2" value="<?=$dire['cltlf2']?>">
    </div>
    <div class="form-group col-md-6 col-xs-6 text-right">
        <button type="button" class="hidden btn bg-border-success btn_select_dire">Seleccionar</button>
    </div>
    <input type="hidden" name="clproc" value="<?=$clproc?>">
    <input type="hidden" name="clcodi" value="<?=$clcodi?>">
</form>