<?php

if (empty($_SESSION)):
    session_start();
endif;

$id_cliente = $_SESSION['clproc'] . $_SESSION['clcodi'];

$presupuesto = new Presupuesto();

//Paginado
$limit = 50;
$page = 1;
if(isset($_GET['page'])):
    $page = $_GET['page'];
endif;
$num_pres = $presupuesto->count($id_cliente);
$num_ped = $presupuesto->count_ped($id_cliente);

$total_page_pres = ceil($num_pres / $limit);
$total_page_ped = ceil($num_ped / $limit);

$array_presupuestos = $presupuesto->getAll($id_cliente, "0", $limit);
$array_pedidos = $presupuesto->getAll($id_cliente, "1", $limit);

$tab = "";
if (isset($_GET['tab']))
    $tab = $_GET['tab'];

$id_select = "";
if (isset($_GET['id']))
    $id_select = $_GET['id'];
if (!empty($id_select)):
    $class_select = ".id" . $id_select;
    ?>
    <script>
        $(document).ready(function () {
            $('html, body').animate({
                scrollTop: $("<?=$class_select?>").offset().top
            }, 100);
        });
    </script>
    <?php
endif;

?>

<div class="container container_historial">

    <h4 class="text-center">Historial</h4>
    <div class="tabbable">
        <ul class="nav nav-stacked col-md-3">
            <li class="<?php if (empty($tab) OR $tab == "presupuesto"): echo "active"; endif; ?> btn-default border-radius4">
                <a href="#presupuestos" data-toggle="tab">Mis presupuestos</a>
            </li>
            <li class="<?php if ($tab == "pedidos"): echo "active"; endif; ?> btn-default border-radius4"><a
                    href="#pedidos" data-toggle="tab">Mis pedidos</a></li>
        </ul>
        <div class="tab-content col-md-9">
            <div class="tab-pane <?php if (empty($tab) OR $tab == "presupuesto"): echo "active"; endif; ?>"
                 id="presupuestos">
                <form class="search_historial" name="search_historial">
                    <div class="form-group has-feedback">
                        <input type="search" class="mb-10 form-control btn_search_presupuesto" name="search_presupuesto"
                               placeholder="Introduce búsqueda...">
                        <i class="glyphicon glyphicon-search form-control-feedback"></i>
                    </div>
                </form>
                <div class="content_table_historial row col-md-12">
                    <table class="table_historial table sortable">
                        <thead>
                        <tr>
                            <th class="order_table">#&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></th>
                            <th data-defaultsort='disabled'>Ref.</th>
                            <th class="order_table">Fecha&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></th>
                            <th class="order_table">Validez&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></th>
                            <th data-defaultsort='disabled'>Total(€)</th>
                            <th data-defaultsort='disabled'>Documentos</th>
                            <th data-defaultsort='disabled'>Opciones</th>
                            <th data-defaultsort='disabled'>Mis notas</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php

                        foreach ($array_presupuestos as $item): ?>
                            <tr class="line_pres id<?=$item['ref_pres']?> <?php if(!empty($id_select) AND $id_select == $item['ref_pres']): echo "tr_id_select"; endif;?>">
                                <?php

                                $referencia = "-";

                                if (!empty($item['referencia'])):
                                    $referencia = $item['referencia'];
                                endif;

                                $fecha = $item['fecha'];
                                $fecha = date_create($fecha);
                                $fecha = date_format($fecha, "d/m/Y");

                                $fecha_validez = "";
                                $fecha_validez = strtotime('+1 month', strtotime($item['fecha']));
                                $fecha_validez_aux = $fecha_validez;
                                $fecha_validez = date('d/m/Y', $fecha_validez);

                                $fecha_actual = strtotime(date("d-m-Y H:i:00", time()));

                                $is_valido = "1";

                                if ($fecha_actual > $fecha_validez_aux):
                                    $is_valido = "0";
                                endif;

                                ?>
                                <td><?= hideNumPre($item['num_pre']) ?></td>
                                <td><?= $referencia ?></td>
                                <td><?= $fecha ?></td>
                                <td>
                                    <?= $fecha_validez ?>
                                </td>
                                <td><?= round2decimals($item['total']) ?></td>
                                <td>
                                    <a target="_blank"
                                       href="<?= path_web_mods ?>mod_pdf/index.php?pres=<?= $item['id'] ?>">
                                        <span class="btn-xs btn_pdf_pedido btn btn-default">PVP</span>
                                    </a>
                                    <a target="_blank"
                                       href="<?= path_web_mods ?>mod_pdf/index.php?pres_cond=<?= $item['id'] ?>">
                                        <span class="btn-xs bg-border-warning btn_pdf_pedido btn btn-default">Condiciones cliente</span>
                                    </a>
                                    <button class="hidden btn-xs btn-grey btn">Confirmar</button>
                                    <button class="hidden btn-xs btn-danger btn_delete_presupuesto btn">Eliminar
                                    </button>
                                </td>
                                <td>
                                    <?php if ($is_valido == 1): ?>
                                        <?php if (!isset($item['ref_pres'])): ?>
                                            <button class="btn btn-default btn-xs icon_exchange_presupuestos bg-border-success" id_pres="<?= $item['id'] ?>">Confirmar</button>
                                            <button class="btn btn-default btn-xs icon_trash_presupuestos bg-border-danger" id_pres="<?= $item['id'] ?>">Eliminar</button>
                                        <?php else: ?>
                                            <a target="_blank"
                                               href="<?= path_web ?>clientes.php?opt=historial&tab=pedidos&id=<?= $item['id'] ?>">
                                                <button class="btb btn-xs bg-border-success">Ir al pedido</button>
                                            </a>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <span class="fa icon_repeat_presupuestos fa-repeat" id_pres="<?= $item['id'] ?>"
                                              aria-hidden="true"></span>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if (!isset($item['observaciones'])): ?>
                                        <button data-toggle="modal" data-target="#modal_edit_notas" title="Mis notas"
                                                class="btn_ver_notas btn-sm btn bg-border-success" aria-hidden="true"><i
                                                class="fa fa-plus"></i>&nbsp;Añadir
                                        </button>
                                    <?php else: ?>
                                        <button data-toggle="modal" data-target="#modal_edit_notas" title="Mis notas"
                                                class="btn_ver_notas btn-sm btn bg-border-info" aria-hidden="true"><i
                                                class="fa fa-eye"></i>&nbsp;Ver
                                        </button>
                                    <?php endif; ?>
                                </td>
                                <input type="hidden" class="id_pres" value="<?= $item['id'] ?>">
                                <input type="hidden" class="id_cliente" value="<?= $id_cliente ?>">
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>
                    <?php if($total_page_pres > 1):?>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination" type_doc="0">
                                <?php if($page !=1): ?>
                                    <li class="page_pres_ped"><a class="page-link" page="<?=$page-1?>" href="#">Anterior</a></li>
                                <?php endif; ?>
                                <?php for ($i=1;$i<=$total_page_pres;$i++): ?>
                                    <?php if ($page == $i): ?>
                                        <li><a><?=$i?></a></li>
                                    <?php else: ?>
                                        <li class="page_pres_ped"><a class="page-link" page="<?=$i?>" href="#"><?=$i?></a></li>
                                    <?php endif; ?>
                                <?php endfor ?>
                                <?php if($page != $total_page_pres): ?>
                                    <li class="page_pres_ped"><a class="page-link" page="<?=$page+1?>" href="#">Siguiente</a></li>
                                <?php endif; ?>
                            </ul>
                        </nav>
                    <?php endif; ?>
                </div>
            </div>
            <div class="tab-pane <?php if ($tab == "pedidos"): echo "active"; endif; ?>" id="pedidos">
                <form class="search_historial" name="search_historial">
                    <div class="form-group has-feedback">
                        <input type="search" class="mb-10 form-control btn_search_presupuesto" name="search_presupuesto"
                               placeholder="Introduce búsqueda...">
                        <i class="glyphicon glyphicon-search form-control-feedback"></i>
                    </div>
                </form>
                <div class="content_table_historial row col-md-12">
                    <table class="table_historial table sortable">
                        <thead>
                        <tr>
                            <th class="order_table">Número&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></th>
                            <th data-defaultsort='disabled'>Referencia</th>
                            <th class="order_table">Fecha&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></th>
                            <th data-defaultsort='disabled'>Total (€)</th>
                            <th data-defaultsort='disabled'>Documentos</th>
                            <th data-defaultsort='disabled'>Mis notas</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php

                        foreach ($array_pedidos as $item): ?>
                            <tr class="id<?= $item['ref_pres'] ?> <?php if (!empty($id_select) AND $id_select == $item['ref_pres']): echo "tr_id_select"; endif; ?>">
                                <?php

                                $referencia = "-";

                                if (!empty($item['referencia'])):
                                    $referencia = $item['referencia'];
                                endif;

                                $fecha = $item['fecha'];
                                $fecha = date_create($fecha);
                                $fecha = date_format($fecha, "d/m/Y");

                                ?>
                                <td><?= hideNumPed($item['num_pre']) ?></td>
                                <td><?= $referencia ?></td>
                                <td><?= $fecha ?></td>
                                <td><?= round2decimals($item['total']) ?></td>
                                <td>
                                    <?php if (isset($item['ref_pres'])): ?>
                                        <a target="_blank"
                                           href="<?= path_web ?>clientes.php?opt=historial&tab=presupuesto&id=<?= $item['id'] ?>">
                                            <button class="btb btn-xs bg-border-success">Ir al presupuesto</button>
                                        </a>
                                    <?php endif; ?>
                                    <a target="_blank"
                                       href="<?= path_web_mods ?>mod_pdf/index.php?pres=<?= $item['id'] ?>">
                                        <span class="btn-xs btn_pdf_pedido btn btn-default">PVP</span>
                                    </a>
                                    <a target="_blank"
                                       href="<?= path_web_mods ?>mod_pdf/index.php?pres_cond=<?= $item['id'] ?>">
                                        <span class="btn-xs bg-border-warning btn_pdf_pedido btn btn-default">Condiciones cliente</span>
                                    </a>
                                </td>
                                <td>
                                    <?php if (!isset($item['observaciones'])): ?>
                                        <button data-toggle="modal" data-target="#modal_edit_notas" title="Mis notas"
                                                class="btn_ver_notas btn-sm btn bg-border-success" aria-hidden="true"><i
                                                class="fa fa-plus"></i>&nbsp;Añadir
                                        </button>
                                    <?php else: ?>
                                        <button data-toggle="modal" data-target="#modal_edit_notas" title="Mis notas"
                                                class="btn_ver_notas btn-sm btn bg-border-info" aria-hidden="true"><i
                                                class="fa fa-eye"></i>&nbsp;Ver
                                        </button>
                                    <?php endif; ?>
                                </td>
                                <input type="hidden" class="id_pres" value="<?= $item['id'] ?>">
                                <input type="hidden" class="id_cliente" value="<?= $id_cliente ?>">
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>
                    <?php if($total_page_ped > 1):?>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination" type_doc="1">
                                <?php if($page !=1): ?>
                                    <li class="page_pres_ped"><a class="page-link" page="<?=$page-1?>" href="#">Anterior</a></li>
                                <?php endif; ?>
                                <?php for ($i=1;$i<=$total_page_ped;$i++): ?>
                                    <?php if ($page == $i): ?>
                                        <li><a><?=$i?></a></li>
                                    <?php else: ?>
                                        <li class="page_pres_ped"><a class="page-link" page="<?=$i?>" href="#"><?=$i?></a></li>
                                    <?php endif; ?>
                                <?php endfor ?>
                                <?php if($page != $total_page_ped): ?>
                                    <li class="page_pres_ped"><a class="page-link" page="<?=$page+1?>" href="#">Siguiente</a></li>
                                <?php endif; ?>
                            </ul>
                        </nav>
                    <?php endif; ?>
                    <input type="hidden" class="limit_page" value="<?=$limit?>">
                </div>
            </div>
        </div>
    </div>
</div>
