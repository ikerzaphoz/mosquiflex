<?php 

if(!isset($_SESSION)):
    session_start();
endif;

?>

<form class="set_email_cliente">
    <div class="form-group col-md-12 col-xs-12">
        <label>Email: </label>
        <input class="check_input_email form-control set_email" type="email" name="set_email" placeholder="Introduce email">
    </div>
    <div class="form-group col-md-12 col-xs-12">
        <label>Email: </label>
        <input class="check_input_email form-control set_email_check" type="email" name="set_email_check" placeholder="Confirma email">
    </div>
    <div class="form-group col-md-12 col-xs-12">
        <span class="check_set_email text-danger"></span>
    </div>
    <div class="form-group col-md-12 col-xs-12 text-center">
        <button type="submit" class="button_send_form_email btn btn-default">Confirmar datos</button>
    </div>
    <input type="hidden" name="clcodi_cliente" value="<?= $_SESSION['clcodi'] ?>" class="clcodi_cliente">
    <input type="hidden" name="clprov_cliente" value="<?= $_SESSION['clprov'] ?>" class="clprov_cliente">
</form>