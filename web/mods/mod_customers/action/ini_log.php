<?php

require '../../../../config.php';

$user = "";
if(isset($_POST['user_login'])) $user = $_POST['user_login'];
$pass = "";
if(isset($_POST['pass_login'])) $pass = sha1($_POST['pass_login']);
$login_modal = "";
if(isset($_POST['login_modal'])) $login_modal = $_POST['login_modal'];
$id_option_modal = "";
if(isset($_POST['id_option_modal'])) $id_option_modal = $_POST['id_option_modal'];

$array_error = array('error' => '-2', 'message' => 'Error desconocido');

if(!isset($_SESSION)):
    session_start();
endif;

if(isset($_SESSION['ini_log'])):
    $array_error = array('error' => '-1', 'message' => 'Ya estas logueado');
else:
    $customer = new Customers();
    $ini_log = $customer->ini_log($user, $pass);
    if(!empty($ini_log)):
        $_SESSION['ini_log'] = "1";
        $_SESSION['name_log'] = $ini_log[0]['clnomb'];
        $_SESSION['email_log'] = $ini_log[0]['email'];
        $_SESSION['tlf_log'] = $ini_log[0]['cltlf1'];
        $_SESSION['clproc'] = $ini_log[0]['clproc'];
        $_SESSION['clcodi'] = $ini_log[0]['clcodi'];
        $_SESSION['clprov'] = $ini_log[0]['clprov'];
        $_SESSION['is_cliente_final'] = $ini_log[0]['is_cliente_final'];
        $array_error = array('error' => '0', 'message' => 'Logueado correctamente');
        if($id_option_modal == "cliente"):
            $array_error = array('error' => '2', 'message' => 'Logueado correctamente');
        endif;
        if($id_option_modal == "product_index"):
            $array_error = array('error' => '3', 'message' => 'Logueado correctamente');
        endif;
        if(empty($_SESSION['email_log'])):
            $array_error = array('error' => '4', 'message' => 'No existe email cliente', 'clcodi' => $_SESSION['clcodi'], 'clprov' => $_SESSION['clprov']);
        endif;
    else:
        $array_error = array('error' => '1', 'message' => 'Error en datos de usuario');
    endif;
endif;

echo json_encode($array_error);