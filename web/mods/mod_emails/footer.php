<?php

$contenido_tipo_email = $tipo_email;

if ($tipo_email == "contraseña"):
    $contenido_tipo_email = "<p class='MsoNormal'><span style='mso-bookmark:_MailAutoSig'><b><i><span
style='font-size:9.0pt;color:gray'>Administraci&oacute;n Mosquiflex S.L. </span></i></b></span><a
href='mailto:administracion@mosquiflex.es'><span style='mso-bookmark:_MailAutoSig'><i><span
style='font-size:9.0pt'>administracion@mosquiflex.es</span></i></span><span
style='mso-bookmark:_MailAutoSig'></span></a><span style='mso-bookmark:_MailAutoSig'><i><span
style='font-size:9.0pt;color:black'>&nbsp; </span></i></span><span
style='mso-bookmark:_MailAutoSig'><i><span style='font-size:9.0pt;mso-ascii-font-family:
Calibri;mso-hansi-font-family:Calibri;color:black'>
  <o:p></o:p>
</span></i></span></p>";
elseif ($tipo_email == "consulta" || $tipo_email == "pedido_presupuesto"):
    $contenido_tipo_email = "<p class='MsoNormal'><span style='mso-bookmark:_MailAutoSig'><b><i><span
style='font-size:9.0pt;color:gray'>Ventas Mosquiflex S.L. </span></i></b></span><a
href='mailto:ventas@mosquiflex.es'><span style='mso-bookmark:_MailAutoSig'><i><span
style='font-size:9.0pt'>ventas@mosquiflex.es</span></i></span><span
style='mso-bookmark:_MailAutoSig'></span></a><span style='mso-bookmark:_MailAutoSig'><i><span
style='font-size:9.0pt;color:black'>&nbsp; </span></i></span><span
style='mso-bookmark:_MailAutoSig'><i><span style='font-size:9.0pt;mso-ascii-font-family:
Calibri;mso-hansi-font-family:Calibri;color:black'>
  <o:p></o:p>
</span></i></span></p>";
endif;

$email_footer = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
<title>Firma Mosquiflex</title>
<style type='text/css'>
<!--
div.MsoNormal {mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:'';
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:'Calibri','sans-serif';
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:'Times New Roman';
	mso-bidi-theme-font:minor-bidi;
	mso-fareast-language:EN-US;}
li.MsoNormal {mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:'';
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:'Calibri','sans-serif';
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:'Times New Roman';
	mso-bidi-theme-font:minor-bidi;
	mso-fareast-language:EN-US;}
p.MsoNormal {mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:'';
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:'Calibri','sans-serif';
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:'Times New Roman';
	mso-bidi-theme-font:minor-bidi;
	mso-fareast-language:EN-US;}
span.SpellE {mso-style-name:'';
	mso-spl-e:yes;}
-->
</style>
</head>

<body>
<p class='MsoNormal'><span style='mso-bookmark:_MailAutoSig'><b
style='mso-bidi-font-weight:normal'><i><span style='font-size:18.0pt;
font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black;text-transform:uppercase;
mso-fareast-language:ES;mso-no-proof:yes'>
  <v:shapetype id='_x0000_t75'
 coordsize='21600,21600' o:spt='75' o:preferrelative='t' path='m@4@5l@4@11@9@11@9@5xe'
 filled='f' stroked='f'>
    <v:stroke joinstyle='miter'/>
    <v:formulas>
      <v:f eqn='if lineDrawn pixelLineWidth 0'/>
      <v:f eqn='sum @0 1 0'/>
      <v:f eqn='sum 0 0 @1'/>
      <v:f eqn='prod @2 1 2'/>
      <v:f eqn='prod @3 21600 pixelWidth'/>
      <v:f eqn='prod @3 21600 pixelHeight'/>
      <v:f eqn='sum @0 0 1'/>
      <v:f eqn='prod @6 1 2'/>
      <v:f eqn='prod @7 21600 pixelWidth'/>
      <v:f eqn='sum @8 21600 0'/>
      <v:f eqn='prod @7 21600 pixelHeight'/>
      <v:f eqn='sum @10 21600 0'/>
    </v:formulas>
    <v:path o:extrusionok='f' gradientshapeok='t' o:connecttype='rect'/>
    <o:lock v:ext='edit' aspectratio='t'/>
  </v:shapetype>
  <v:shape id='_x0000_i1025' type='#_x0000_t75' style='width:136.5pt;
 height:42pt;visibility:visible'>
    <v:imagedata src='Firmas_archivos/image001.png' o:href='cid:image001.png@01D2787D.AB3A3750'/>
  </v:shape>
  </span></i></b></span><span style='mso-bookmark:_MailAutoSig'><b><i><span
style='font-size:18.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black;
text-transform:uppercase'>
  <o:p></o:p>
</span></i></b></span></p>
<p class='MsoNormal' style='margin-top:4.0pt'><span style='mso-bookmark:_MailAutoSig'><b><i><span
style='font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black'>Mosquiflex S.L.
  <o:p></o:p>
</span></i></b></span></p>" . $contenido_tipo_email . "<p class='MsoNormal'><span style='mso-bookmark:_MailAutoSig'><i><span
style='font-size:9.0pt;color:black'>Tel: (+34) 958 566 955
          <o:p></o:p>
</span></i></span></p>
<p class='MsoNormal' style='margin-top:3.0pt;text-align:justify'><span
style='mso-bookmark:_MailAutoSig'><i><span style='font-size:8.0pt;color:green'>Antes
  de imprimir este mensaje, aseg&uacute;rese de que sea imprescindible. Proteger el
  medio ambiente est&aacute; a nuestro alcance.</span></i></span><span style='mso-bookmark:
_MailAutoSig'><i><span style='font-size:8.0pt;color:black'><br />
    En cumplimiento de la Ley Org&aacute;nica 15/1999, de 13 de diciembre de protecci&oacute;n de
    datos de car&aacute;cter personal, se pone en conocimiento del destinatario del
    presente correo electr&oacute;nico, que los datos incluidos en este mensaje, est&aacute;n
    dirigidos exclusivamente al citado destinatario cuyo nombre aparece en el
    encabezamiento, por lo que si usted no es la persona interesada rogamos nos
    comunique el error de env&iacute;o y se abstenga de realizar copias del mensaje o de
    los datos contenidos en el mismo o remitirlo o entregarlo a otra persona, procediendo
    a borrarlo de inmediato. Asimismo le informamos que sus datos de correo han
    quedado incluidos en nuestra base de datos a fin de dirigirle, por este medio,
    comunicaciones comerciales, profesionales e informativas y que usted dispone de
    los derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n y especificaci&oacute;n de los
    mismos, derechos que podr&aacute; hacer efectivos dirigi&eacute;ndose a Mosquiteras T&eacute;cnicas.
    con domicilio en Camino de Barrasa - Calle B, Nave 5, 18320 Santa Fe, Granada.</span></i></span><i><span style='font-size:8.0pt;
color:black'>
  <o:p></o:p>
</span></i></p>
</body>
</html>";
