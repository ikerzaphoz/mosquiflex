<div class="col-md-12 col-xs-12">
    <?=lang_aviso_legal_1?>
</div>
<div class="col-md-12 col-xs-12">
    <span class="title_condiciones"><?=lang_aviso_legal_2?></span>www.mosquiflex.es
</div>
<div class="col-md-12 col-xs-12">
    <span class="title_condiciones"><?=lang_aviso_legal_3?></span>Mosquiteras Técnicas S.L.
</div>
<div class="col-md-12 col-xs-12">
    <span class="title_condiciones"><?=lang_aviso_legal_4?></span>Camino de Barrasa - Calle B, Nave 5, 18320 Santa Fe, Granada.
</div>
<div class="col-md-12 col-xs-12">
    <span class="title_condiciones"><?=lang_aviso_legal_5?></span>ventas@mosquiflex.es
</div>
<div class="col-md-12 col-xs-12">
    <span class="title_condiciones">Tlf:</span>958 566 955
</div>
<div class="col-md-12 col-xs-12">
    <span class="title_condiciones"><?=lang_aviso_legal_6?></span>B19532563
</div>
<div class="col-md-12 col-xs-12">
    <span class="title_condiciones"><?=lang_aviso_legal_7?></span>Inscrita en el Registro Mercantil de Granada, Tomo 1499, folio 166, Inscripción 1 con hoja Gr-43505
</div>
<div class="col-md-12 col-xs-12 title_condiciones_general">
    <span class="title_condiciones_general"><?=lang_aviso_legal_8?>
</div>

<div class="col-md-12 col-xs-12">
    <span class="title_condiciones">1.- <?=lang_aviso_legal_9?>
</div>
<div class="col-md-12 col-xs-12">
    <span><?=lang_aviso_legal_10?><br><?=lang_aviso_legal_11?></span>
</div>

<div class="col-md-12 col-xs-12 mt-10">
    <span class="title_condiciones">2.- <?=lang_aviso_legal_12?>
</div>
<div class="col-md-12 col-xs-12">
    <span>
<?=lang_aviso_legal_13?>
    </span>
</div>

<div class="col-md-12 col-xs-12 mt-10">
    <span class="title_condiciones">3.- <?=lang_aviso_legal_14?>
</div>
<div class="col-md-12 col-xs-12">
    <span>
<?=lang_aviso_legal_15?><br>
<?=lang_aviso_legal_16?>
<br><?=lang_aviso_legal_17?>
    </span>
</div>

<div class="col-md-12 col-xs-12 mt-10">
    <span class="title_condiciones">4.- <?=lang_aviso_legal_18?>
</div>
<div class="col-md-12 col-xs-12">
    <span>
<?=lang_aviso_legal_19?><br>
<?=lang_aviso_legal_20?><br>
<?=lang_aviso_legal_21?><br>
<?=lang_aviso_legal_22?>
    </span>
</div>

<div class="col-md-12 col-xs-12 mt-10">
    <span class="title_condiciones">5.- <?=lang_aviso_legal_23?>
</div>
<div class="col-md-12 col-xs-12">
    <span>
<?=lang_aviso_legal_24?><br>
<?=lang_aviso_legal_25?>
    </span>
</div>

<div class="col-md-12 col-xs-12 mt-10">
    <span class="title_condiciones">6.- <?=lang_aviso_legal_26?>
</div>
<div class="col-md-12 col-xs-12">
    <span>
<?=lang_aviso_legal_27?><br>
<?=lang_aviso_legal_28?><br>
<?=lang_aviso_legal_29?><br>
<?=lang_aviso_legal_30?><br>
<?=lang_aviso_legal_31?>
    </span>
</div>

<div class="col-md-12 col-xs-12 mt-10">
    <span class="title_condiciones">7.- <?=lang_aviso_legal_32?>
</div>
<div class="col-md-12 col-xs-12">
    <span>
<?=lang_aviso_legal_33?>
    </span>
</div>