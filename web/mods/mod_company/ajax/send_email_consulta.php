<?php

require '../../../../config.php';

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$tipo_email = "consulta";


if (isset($_POST)):
    $nombre = "";
    if (isset($_POST['nombre'])):
        $nombre = $_POST['nombre'];
    endif;
    $email = "";
    if (isset($_POST['email'])):
        $email = $_POST['email'];
    endif;
    $telefono = "";
    if (isset($_POST['telefono'])):
        $telefono = $_POST['telefono'];
    endif;
    $contacto = "";
    if (isset($_POST['contacto'])):
        $contacto = $_POST['contacto'];
    endif;
    $horario = "";
    if (isset($_POST['horario'])):
        $horario = $_POST['horario'];
    endif;
    $provincia = "";
    if (isset($_POST['provincia'])):
        $provincia = $_POST['provincia'];
    endif;
    $tipo_consulta = "";
    if (isset($_POST['tipo_consulta'])):
        $tipo_consulta = $_POST['tipo_consulta'];
    endif;
    $consulta = "";
    if (isset($_POST['consulta'])):
        $consulta = $_POST['consulta'];
    endif;
else:
    echo "Error al enviar los datos...";
endif;

$mail = new PHPMailer();                              // Passing `true` enables exceptions
try {
    //$imagen_logo = url_web . path_image . "logo_mosquiflex.png";
    //Server settings
    $mail->SMTPDebug = 1;                                 // Enable verbose debug output
    //$mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'mail.mosquiflex.es';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'mosquiterastecnicas@mosquiflex.es';                 // SMTP username
    $mail->Password = 'mMosS..TteE..17';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to
    // Activo condificacción utf-8
    $mail->CharSet = 'UTF-8';
    //Recipients
    $mail->setFrom('ventas@mosquiflex.es', 'Mosquiflex - '.$tipo_consulta);
    $mail->addAddress($email, 'Email');     // Add a recipient

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Consulta Mosquiflex - '. $tipo_consulta;
    $email_body = "Hemos recibido su consulta:<br> ".$consulta;
    include root . 'web/mods/mod_emails/header.php';
    include root . 'web/mods/mod_emails/footer.php';
    $mail->Body = $email_header . $email_body . $email_footer;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo '1';
} catch (Exception $e) {
    echo '0.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
