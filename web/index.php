<?php

include '../config.php';

//Definimos variable $page para destacar la opción del menú en la cual nos encontramos
$page = "index";

//Cargo contenido para flexol.es
if(empresa == "EN"):
    include '../flexol_mosquiteras/index.php';
    exit;
endif;

$error = "";
if (isset($_GET['error'])): $error = $_GET['error']; endif;

//Cabecera web (css, metas...)
include root . 'web/mods/mod_head/index.php';
//Menu
if (dispositivo == "desktop"):
    include root . 'web/mods/mod_nav/index.php';
else:
    include root . 'web/mods/mod_nav/index_mobile.php';
endif;
?>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="col-md-4 col-xs-12">
                <?php
                //Contenido index
                include root . 'web/mods/mod_index/index.php';
                ?>
            </div>
            <?php
            ?>
            <div class="col-md-8 col-xs-12">
                <?php
                //sliders
                include root . 'web/mods/mod_sliders/index.php';
                ?>
            </div>
        </div>
    </div>
<?php
//footer (js)
include root . 'web/mods/mod_footer/index.php';
//Cargamos modal login
include root . 'web/mods/mod_modal/modal_ini_log.php';
include root . 'web/mods/mod_modal/modal_set_email.php';
include root . 'web/mods/mod_modal/modal_help.php';
