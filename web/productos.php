<?php
include '../config.php';

if(empty($_SESSION['products']) AND empty($_GET) AND empty($_POST)):
    locationToIndex();
endif;

$page = "productos";

//Cargo contenido para flexol.es
if(empresa == "EN"):
    echo "AQUIII";
    include '../flexol_mosquiteras/index.php';
    exit;
endif;

include root.'web/mods/mod_head/index.php';
//Menu
if (dispositivo == "desktop"):
    include root . 'web/mods/mod_nav/index.php';
else:
    include root . 'web/mods/mod_nav/index_mobile.php';
endif;
include root.'web/mods/mod_modal/modal_ini_log.php';
include root.'web/mods/mod_modal/modal_product_color.php';
include root.'web/mods/mod_modal/modal_ini_login_flotante.php';
include root.'web/mods/mod_modal/modal_loading.php';
include root . 'web/mods/mod_modal/modal_set_email.php';

if(!isset($total_presupuesto)):
    $total_presupuesto = "0.00";
endif;

define_array_ral();

if(isset($_GET['id_pro'])):

    $id_pro = "";
    if(isset($_GET['id_pro'])):
        $id_pro = $_GET['id_pro'];
        $image_product = $_GET['id_pro'];

    endif;
    
    $hidden_element = "";
    if($id_pro == "2733"):
        $hidden_element = "hidden";
    endif;

    $familia = new Familia();
    $array_familia = $familia->getFamilia('fafami');
    $array_grupos = $familia->getGrupoColoresFamilia($id_pro);

    $title_product = $array_familia[$id_pro]['fadesc'];

    include root.'web/mods/mod_products/index.php';
elseif(isset($_GET['all'])):
    include root.'web/mods/mod_products/all_products.php';
elseif(isset($_GET['exp'])):
    include root.'web/mods/mod_products/expositores.php';
else:
    include root.'web/mods/mod_modal/modal_option_product.php';
    include root.'web/mods/mod_modal/modal_add_croquis_incremento.php';
    include root.'web/mods/mod_modal/modal_add_product.php';
    include root.'web/mods/mod_modal/modal_edit_dire_envio.php';
    include root.'web/mods/mod_modal/modal_view_pdfs.php';
    include root.'web/mods/mod_products/resumen_pedido_presupuesto.php';
    include root.'web/mods/mod_modal/modal_add_concepto.php';
endif;
include root.'web/mods/mod_footer/index.php';

if(isset($_GET['save'])): ?>
    <script>
        setTimeout(function(){document.getElementById('btn-save-presupuesto').click()}, 300);
    </script>
<?php endif;
include root.'web/mods/mod_modal/modal_add_medidas_component.php';
include root . 'web/mods/mod_modal/modal_help.php';
include root . 'web/mods/mod_modal/modal_editar_datos_cliente.php';
include root . 'web/mods/mod_modal/modal_edit_indicaciones_producto.php';
?>

<input type="hidden" value="<?=$project_name?>" class="path_project">