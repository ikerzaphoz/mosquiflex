<?php

require 'config.php';

//header("Location:" . path_web);
//
//exit;

/*

//CABECERAS
Platform:SANDBOX
Unit:mm

//API
User -> flexol
Password -> 7kXfb65N
API token -> 260f0b0a-c93f-4bfc-984d-a0551321738b

//cURL

CURLOPT_CUSTOMREQUEST => 'GET',
CURLOPT_HTTPHEADER => <vuestras cabeceras>,
CURLOPT_URL => "https://fabricantes.cortinadecor.com/api/orders/v1/all",
CURLOPT_RETURNTRANSFER => 1,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_SSL_VERIFYPEER => false,
CURLOPT_SSL_VERIFYHOST => false

*/

try {
    $ch = curl_init();

    if (FALSE === $ch)
        throw new Exception('failed to initialize');

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
//    curl_setopt($ch, CURLOPT_HTTPHEADER, array("api-token: 260f0b0a-c93f-4bfc-984d-a0551321738b", "authorization: Basic ZmxleG9sOjdrWGZiNjVO", "cache-control: no-cache", "platform: SANDBOX", "unit: mm", "accept: text/plain"));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("api-token: 260f0b0a-c93f-4bfc-984d-a0551321738b", "authorization: Basic ZmxleG9sOjdrWGZiNjVO", "cache-control: no-cache", "platform: SANDBOX", "unit: mm"));
    curl_setopt($ch, CURLOPT_URL, "https://fabricantes.cortinadecor.com/api/orders/v1/all");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    $content = curl_exec($ch);

    $content = nl2br($content);
    $content = explode("<br />", $content);

    foreach ($content as $numLinea => $contenidoLinea):
        if (!empty($contenidoLinea) AND strlen($contenidoLinea) != "1"):
            echo "-----LINEA " . $numLinea . "-----<br>";
            echo $contenidoLinea . "<br>";
        endif;
    endforeach;

    if (FALSE === $content)
        throw new Exception(curl_error($ch), curl_errno($ch));

} catch (Exception $e) {

    echo "-----ERROR-----<br>";
    var_dump($e);
    echo "<br>-----ERROR-----";

    trigger_error(sprintf(
        'Curl failed with error #%d: %s',
        $e->getCode(), $e->getMessage()),
        E_USER_ERROR);

}


//$handler = curl_init();
//
//curl_setopt($handler, CURLOPT_CUSTOMREQUEST, 'GET');
//curl_setopt($handler, CURLOPT_HTTPHEADER,array("Platform:SANDBOX", "Api-Token:260f0b0a-c93f-4bfc-984d-a0551321738b", "user:flexol","pass=7kXfb65N"));
//curl_setopt($handler,CURLOPT_URL, "https://fabricantes.cortinadecor.com/api/orders/v1/all");
//curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($handler, CURLOPT_FOLLOWLOCATION, true);
//curl_setopt($handler, CURLOPT_SSL_VERIFYPEER, false);
//curl_setopt($handler, CURLOPT_SSL_VERIFYHOST, false);
//$response = curl_exec ($handler);
//curl_close($handler);
//
//var_dump($response);