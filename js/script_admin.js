$(document).ready(function () {

    $('.input-color').colorpicker();

    function save_lang(e) {
        e.preventDefault();
        var form_serialize = $(this).closest('tr').find('.form_edit_lang').serialize();
        $.ajax({
            url: "mod_web_lang/ajax/update_lang.php",
            type: "post",
            data: form_serialize,
            dataType: 'json',
            success: function (response) {
                showToastr(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function update_file_lang(e) {
        e.preventDefault();
        var lang_update = $(this).attr('lang_update');
        var data =
                {
                    'lang_update': lang_update
                };
        $.ajax({
            url: "mod_web_lang/ajax/update_file_lang.php",
            type: "post",
            data: data,
            dataType: 'json',
            success: function (response) {
                showToastr(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function update_product(e) {
        e.preventDefault();
        var data = $(this).closest('tr').find('form').serialize();

        $.ajax({
            url: "mod_product/ajax/update_product.php",
            type: "post",
            data: data,
            dataType: 'json',
            success: function (response) {
                showToastr(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function showToastr(response) {
        if (response['error'] == 0) {
            toastr.success(response['message'], "Guardando datos");
        } else if (response['error'] == 1) {
            toastr.warning(response['message'], "Guardando datos");
        } else {
            toastr.error(response['message'], "Guardando datos");
        }
    }

    function update_color(e) {
        e.preventDefault();
        var data = $(this).closest('tr').find('form').serialize();
        $.ajax({
            url: "mod_product_color/ajax/update_product_color.php",
            type: "post",
            data: data,
            dataType: 'json',
            success: function (response) {
                showToastr(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function change_color(e) {
        e.preventDefault();
        var value = $(this).val();
        $(this).css('background-color', value);
    }

    function save_measure(e) {
        e.preventDefault();
        var data = $(this).closest('tr').find('form').serialize();
        $.ajax({
            url: "mod_components/ajax/save_max_measures.php",
            type: "post",
            data: data,
            dataType: 'json',
            success: function (response) {
                showToastr(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function edit_save_status(e) {
        e.preventDefault();
        var status_1 = "0";
        if ($(this).closest('tr').find('.edit_status_1').is(':checked')) {
            status_1 = "1";
        }
        var status_2 = "0";
        if ($(this).closest('tr').find('.edit_status_2').is(':checked')) {
            status_2 = "1";
        }
        var status_3 = "0";
        if ($(this).closest('tr').find('.edit_status_3').is(':checked')) {
            status_3 = "1";
        }
        var arfami = $(this).closest('tr').find('.arfami').val();
        var arsubf = $(this).closest('tr').find('.arsubf').val();
        var ararti = $(this).closest('tr').find('.ararti').val();
        var data = {
            'status_1': status_1,
            'status_2': status_2,
            'status_3': status_3,
            'arfami': arfami,
            'arsubf': arsubf,
            'ararti': ararti
        };
        $.ajax({
            url: "mod_product/ajax/update_product_status.php",
            type: "post",
            data: data,
            dataType: 'json',
            success: function (response) {
                showToastr(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function select_familia_status(e) {
        e.preventDefault();
        var data = {
            arfami: $(this).val()
        }
        $.ajax({
            url: "mod_product/ajax/change_familia_select.php",
            type: "post",
            data: data,
            success: function (response) {
                $('.insert_ajax').html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    $(document).on('click', '.btn_save_lang', save_lang);
    $(document).on('click', '.btn_actualizar_lang', update_file_lang);
    $(document).on('click', '.edit_save_product', update_product);
    $(document).on('click', '.edit_save_color', update_color);
    $(document).on('change', '.input-color', change_color);
    $(document).on('click', '.edit_save_measure_component', save_measure);
    $(document).on('click', '.edit_save_status', edit_save_status);
    $(document).on('change', '.select_familia_status', select_familia_status);

});