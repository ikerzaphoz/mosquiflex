var mensaje_cerrar_ventana = "¿Desea cerrar la ventana sin guardar las medidas?";
var mensaje_faltan_medidas = "Faltan medidas. Revise los datos";
var mensaje_estas_seguro = "¿Estás seguro?";
var texto_si = "Sí";
var texto_no = "No";