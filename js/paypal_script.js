function save_pres() {
    var number_tr = $('.content_lineas_pedido').find('.line_product');
    var total_price = $('.price_total').val();
    var referencia = $('.referencia').val();
    var is_pedido = $('.is_pedido').val();
    var agencia = $('.agencia').val();

    var data = [];
    $(number_tr).each(function (index, element) {
        if ($(element).find('.line_product_component').length) {

            $('.line_product_component').each(function (index, element) {
                var tipo_medida_componente = $(element).find('.tipo_medida_component').val();
                var unit_product = $(element).find('.unidades_unidades_medidas').val();
                var arfami = $(element).find('.arfami').val();
                //ararti = color_product
                var arsubf = $(element).find('.arsubf').val();
                var ararti = $(element).find('.ararti').val();
                var price = $(element).find('.price_component_hidden').val();
                var is_almacen = $(element).find('.is_almacen').val();
                if (tipo_medida_componente == 3) {
                    var array_product = {
                        id_product: arfami,
                        color_product: ararti,
                        subfamily_product: arsubf,
                        unit_product: unit_product,
                        width_product: '0',
                        height_product: '0',
                        observaciones_product: 'is_component',
                        product_price: price,
                        is_almacen: is_almacen
                    };
                    data.push(array_product);
                } else {
                    if (tipo_medida_componente == 1) {
                        $(element).find('.segunda_linea_producto').each(function (index, element) {
                            var unidades = $(element).find('.unidades_unidades_medidas').val();
                            var ancho = $(element).find('.ancho_unidades_medidas').val();
                            var alto = $(element).find('.alto_unidades_medidas').val();
                            var precio_unit = $(element).find('.precio_unidades_medidas').val();
                            var is_almacen = $(element).find('.is_almacen').val();
                            var array_product = {
                                id_product: arfami,
                                color_product: ararti,
                                subfamily_product: arsubf,
                                unit_product: unidades,
                                width_product: ancho,
                                height_product: alto,
                                observaciones_product: 'is_component_unit',
                                product_price: precio_unit,
                                is_almacen: is_almacen
                            };
                            data.push(array_product);
                        });
                    } else {
                        $(element).find('.segunda_linea_producto').each(function (index, element) {
                            var metros_unit = $(element).find('.metros_unidades_medidas').val();
                            var unidades = $(element).find('.unidades_unidades_medidas').val();
                            metros_unit = parseFloat(metros_unit);
                            metros_unit = metros_unit.toFixed(2);
                            var precio_unit = $(element).find('.precio_unidades_medidas').val();
                            var is_almacen = $(element).find('.is_almacen').val();

                            var array_product = {
                                id_product: arfami,
                                color_product: ararti,
                                subfamily_product: arsubf,
                                unit_product: unidades,
                                width_product: metros_unit,
                                height_product: '0',
                                observaciones_product: 'is_component_meter',
                                product_price: precio_unit,
                                is_almacen: is_almacen
                            };
                            data.push(array_product);
                        });
                    }
                }
            })
        } else {
            var id_product = $(element).find('.input_id_product').attr('span_value');
            if (id_product == "2735") {
                $(element).find('.form-horizontal').each(function (index_corredera, element_corredera) {
                    var unit_product = $(element_corredera).find('.input_unidades').val();
                    var width_product = $(element_corredera).find('.input_ancho').val();
                    var height_product = $(element_corredera).find('.input_alto').val();
                    var product_price = $(element_corredera).find('.product_price').attr('span_value');
                    var subfamily_product = $(element_corredera).find('.subfamily_product').val();
                    var color_product = $(element_corredera).find('.input_color_product').attr('span_value');
                    var group_color = $(element_corredera).find('.input_group_color').attr('span_value');
                    if ($(element_corredera).hasClass('corredera_completa')) {

                        var array_product = {
                            id_product: id_product,
                            color_product: color_product,
                            group_color: group_color,
                            subfamily_product: subfamily_product,
                            unit_product: unit_product,
                            width_product: width_product,
                            height_product: height_product,
                            product_price: $(element_corredera).find('.precio_marco').val()
                        };
                        var array_product_hojas = {
                            id_product: id_product,
                            color_product: color_product,
                            group_color: group_color,
                            subfamily_product: '1',
                            unit_product: $(element_corredera).find('.cantidad_hojas').val(),
                            width_product: $(element_corredera).find('.ancho_hojas').val(),
                            height_product: height_product,
                            product_price: $(element_corredera).find('.precio_hoja').val()
                        };
                        if (unit_product > 0) {
                            data.push(array_linea_3);
                            data.push(array_product_hojas);
                            data.push(array_product);
                            data.push(array_linea_3);
                        }
                    } else {
                        var array_product = {
                            id_product: id_product,
                            color_product: color_product,
                            group_color: group_color,
                            subfamily_product: subfamily_product,
                            unit_product: unit_product,
                            width_product: width_product,
                            height_product: height_product,
                            product_price: product_price
                        };
                        if (unit_product > 0) {
                            data.push(array_product);
                        }
                    }
                });

            } else {
                var color_product = $(element).find('.input_color_product').attr('span_value');
                var group_color = $(element).find('.input_group_color').attr('span_value');
                var option_product = $(element).find('.row_option_product');
                var color_product = $(element).find('.input_color_product').attr('span_value');
                var group_color = $(element).find('.input_group_color').attr('span_value');
                var subfamily_product = $(element).find('.input_subfamily_product').val();
                var tipologia = $(element).find('.input_tipologia').val();
                if (id_product == "1999") {
                    var unit_product = $(element).find('.change_units_resumen_expositor').val();
                    var group_color = $(element).find('.ararti').val();
                } else {
                    var unit_product = $(element).find('.input_unit_product').val();
                }
                var width_product = $(element).find('.input_width_product').val();
                var height_product = $(element).find('.input_height_product').val();
                var observaciones_product = $(element).find('.observaciones_product').val();
                var product_price = $(element).find('.product_price').html();
                var is_almacen = $(element).find('.is_almacen').val();
                var array_product = {
                    id_product: id_product,
                    color_product: color_product,
                    group_color: group_color,
                    subfamily_product: subfamily_product,
                    unit_product: unit_product,
                    width_product: width_product,
                    height_product: height_product,
                    observaciones_product: observaciones_product,
                    product_price: product_price,
                    tipologia: tipologia,
                    is_almacen: is_almacen
                };

                if ($(option_product).length > 0) {

                    data.push(array_linea_3);
                    $(option_product).each(function (index_com, element_com) {
                        var input_unit_product_component = $(element_com).find('.unit_increment').val();
                        var product_price_component = $(element_com).find('.product_price').html();
                        var id_component = $(element_com).find('.id_component').val();
                        var title_adjunto = $(element_com).next().find('.name_fichero_upload_hidden').html();
                        var array_component = {
                            id_product: id_product,
                            input_unit_product_component: input_unit_product_component,
                            product_price_component: product_price_component,
                            id_component: id_component,
                            title_adjunto: title_adjunto
                        };
                        data.push(array_component);
                    });
                }
                data.push(array_product);
                if ($(option_product).length > 0) {
                    data.push(array_linea_3);
                }
            }
        }
    });


    var line_lacado = $('.line_lacado_ral');
    $(line_lacado).each(function (index, element) {

        var is_lacado = "is_lacado";
        var color_lacado = $(element).find('.color_lacado').html();
        var unit_lacado = $(element).find('.unit_lacado').html();
        var product_price_lacado = $(element).find('.product_price_lacado').html();
        var array_lacado = {
            is_lacado: is_lacado,
            color_lacado: color_lacado,
            unit_lacado: unit_lacado,
            product_price_lacado: product_price_lacado
        }
        data.push(array_lacado);
    })

    var id_dire = {
        id_dire: $('.id_dire').val()
    }

    data.push(id_dire);

    $.ajax({
        url: "mods/mod_products/ajax/save_presupuesto.php",
        type: "post",
        data: {
            data: data,
            price: total_price,
            referencia: referencia,
            is_pedido: is_pedido,
            agencia: agencia
        },
        dataType: 'json',
        success: function (response) {
            type_doc = response['is_pedido'];
            if (response['error'] == 0) {
                toastr.options = {
                    "positionClass": "toast-top-full-width"
                }
                toastr.success(type_doc, "Datos guardados correctamente");
                //setTimeout(function(){window.location.href = 'clientes.php?opt=historial'}, 800);
                $('#modal_edit_dire_envio').modal('hide');
                $('#modal_view_pdfs').modal('show');
                $('.type_doc').html(type_doc);
                if (type_doc == "pedido") {
                    $('.content_view_cond').hide();
                    $('.btn_view_pdf_modal').html("Ver pedido");
                }
                var id_pres = response['id_pres'];
                $('.id_pres_modal').val(id_pres);
                $('.btn_view_pdf_pvp').attr('href', $('.url_pdf_pvp').val() + id_pres);
                $('.btn_view_pdf_condiciones').attr('href', $('.url_pdf_condiciones').val() + id_pres);
            } else {
                toastr.options = {
                    "positionClass": "toast-top-full-width"
                }
                toastr.error(type_doc, "Error al guardar los datos");
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Error ajax en submitFormNewDni");
            console.log(jqXHR, textStatus, errorThrown);
        }

    });
}

function getInfoProduct(){
    var data = [];
    var number_tr = $('.content_lineas_pedido').find('.line_product');
    $(number_tr).each(function (index, element) {
        if ($(element).find('.line_product_component').length) {

            $('.line_product_component').each(function (index, element) {
                var tipo_medida_componente = $(element).find('.tipo_medida_component').val();
                var unit_product = $(element).find('.unidades_unidades_medidas').val();
                var price = $(element).find('.price_component_hidden').val();
                if (tipo_medida_componente == 3) {
                    var array_product = {
                        unidades: unit_product,
                        precio: price,
                    };
                    data.push(array_product);
                } else {
                    if (tipo_medida_componente == 1) {
                        $(element).find('.segunda_linea_producto').each(function (index, element) {
                            var unidades = $(element).find('.unidades_unidades_medidas').val();
                            var precio_unit = $(element).find('.precio_unidades_medidas').val();
                            var array_product = {
                                unidades: unidades,
                                precio: precio_unit,
                            };
                            data.push(array_product);
                        });
                    } else {
                        $(element).find('.segunda_linea_producto').each(function (index, element) {
                            var unidades = $(element).find('.unidades_unidades_medidas').val();
                            var precio_unit = $(element).find('.precio_unidades_medidas').val();

                            var array_product = {
                                unidades: unidades,
                                precio: precio_unit,
                            };
                            data.push(array_product);
                        });
                    }
                }
            })
        } else {
            var id_product = $(element).find('.input_id_product').attr('span_value');
            if (id_product == "2735") {
                $(element).find('.form-horizontal').each(function (index_corredera, element_corredera) {
                    var unit_product = $(element_corredera).find('.input_unidades').val();
                    var product_price = $(element_corredera).find('.product_price').attr('span_value');
                    if ($(element_corredera).hasClass('corredera_completa')) {

                        var array_product = {
                            unidades: unit_product,
                            precio: $(element_corredera).find('.precio_marco').val()
                        };
                        var array_product_hojas = {
                            unidades: $(element_corredera).find('.cantidad_hojas').val(),
                            precio: $(element_corredera).find('.precio_hoja').val()
                        };
                        if (unit_product > 0) {
                            data.push(array_product_hojas);
                            data.push(array_product);
                        }
                    } else {
                        var array_product = {
                            unidades: unit_product,
                            precio: product_price
                        };
                        if (unit_product > 0) {
                            data.push(array_product);
                        }
                    }
                });

            } else {
                var option_product = $(element).find('.row_option_product');
                if (id_product == "1999") {
                    var unit_product = $(element).find('.change_units_resumen_expositor').val();
                } else {
                    var unit_product = $(element).find('.input_unit_product').val();
                }
                var product_price = $(element).find('.product_price').html();
                var array_product = {
                    unidades: unit_product,
                    precio: product_price,
                };

                if ($(option_product).length > 0) {

                    $(option_product).each(function (index_com, element_com) {
                        var input_unit_product_component = $(element_com).find('.unit_increment').val();
                        var product_price_component = $(element_com).find('.product_price').html();
                        var array_component = {
                            unidades: input_unit_product_component,
                            precio: product_price_component,
                        };
                        data.push(array_component);
                    });
                }
                data.push(array_product);
            }
        }
    });


    var line_lacado = $('.line_lacado_ral');
    $(line_lacado).each(function (index, element) {

        var unit_lacado = $(element).find('.unit_lacado').html();
        var product_price_lacado = $(element).find('.product_price_lacado').html();
        var array_lacado = {
            unidades: unit_lacado,
            precio: product_price_lacado
        }
        data.push(array_lacado);
    })
    return data;
}

function render_button_paypal() {
    
    var item = getInfoProduct();
    var unidades1 = item['0']['unidades'];
    var precio1 = item['0']['precio'];
        
// Render the PayPal button

    paypal.Button.render({

        // Set your environment

        env: 'sandbox', // sandbox | production

        // Specify the style of the button

        style: {
            label: 'checkout',
            size: 'small', // small | medium | large | responsive
            shape: 'pill', // pill | rect
            color: 'gold'      // gold | blue | silver | black
        },

        // PayPal Client IDs - replace with your own
        // Create a PayPal app: https://developer.paypal.com/developer/applications/create

        client: {
            sandbox: 'AUM1EknjqhLTUkDdmhJfa1g922a2Xy7GVDYrkvsy76xkShVtkb9ONJ_x9E4MwGg0ejN9jndyFgKqRqoe',
            production: '<insert production client id>'
        },

        payment: function (data, actions) {           
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            //Total / detalles 
                            "amount": {
                                "total": $('.price_total').val(),
                                "currency": "EUR",
                                "details": {
                                    "subtotal": $('.price_total').val(),
                                    "tax": "0.00",
                                    "shipping": "0.00",
                                    "handling_fee": "0.00",
                                    "shipping_discount": "0.00",
                                    "insurance": "0.00"
                                }
                            },                
                            //Listado productos detallada
                            "item_list": {
                                "items": [
                                    {
                                        "name": "Nombre",
                                        "description": "Descripcion",
                                        "quantity": unidades1,
                                        "price": precio1,
                                        "tax": "0.00",
                                        "sku": "0",
                                        "currency": "EUR"
                                    }
                                ],
                            }
                        }
                    ],
                    //Nota que se muestra antes de confirmar el pago del presupuesto.
                    "note_to_payer": "Contact us for any questions on your order.",
                }
            });
        },

        onAuthorize: function (data, actions) {
            return actions.payment.execute().then(function () {
                save_pres();
                window.alert('Payment Complete!');
            });
        }

    }, '#paypal-button');

}

