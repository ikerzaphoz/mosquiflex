$(document).ready(function () {
    
        var money_number = {
        sepMil: ".", // separador para los miles
        sepDec: ',', // separador para los decimales
        formatear: function (num) {
            num += '';
            var splitStr = num.split('.');
            var splitLeft = splitStr[0];
            var splitRight = splitStr.length > 1 ? this.sepDec + splitStr[1] : '';
            var regx = /(\d+)(\d{3})/;
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + this.sepMil + '$2');
            }
            return this.simbol + splitLeft + splitRight;
        },
        go: function (num, simbol) {
            this.simbol = simbol || '';
            return this.formatear(num);
        }
    }
    
    var money_number_propia = {
        sepMil: ".", // separador para los miles
        sepDec: ',', // separador para los decimales
        formatear: function (num) {
            num += '';
            var splitStr = num.split('.');
            var splitLeft = splitStr[0];
            var splitRight = splitStr.length > 1 ? "," + splitStr[1] : '';
            var regx = /(\d+)(\d{3})/;
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + "." + '$2');
            }
            splitLeft = splitLeft + ".";
            splitRight = splitRight.slice(1);
            return splitLeft + splitRight;
        },
        go: function (num) {
            return this.formatear(num);
        }
    }
    
    var array_linea_3 = {
        unit_product: '1',
        observaciones_product: "Linea3"
    };

    function check_datos_submit_product(id_product) {
        var submit_form = true;
        if (id_product == "2733" || id_product == "2736") {
            var input_subfamily_product = $('.input_subfamily_product').val();
            if (input_subfamily_product) {
                submit_form = true;
                if (id_product == "2736") {
                    var tipologia = $('.input_tipologia').val();
                    if (tipologia) {
                        submit_form = true;
                    } else {
                        submit_form = false;
                    }
                }
            } else {
                submit_form = false;
                if (id_product == "2736") {
                    var tipologia = $('.input_tipologia').val();
                    if (tipologia) {
                        submit_form = true;
                    } else {
                        submit_form = false;
                    }
                }
            }
        }

        if ($('.text_color_ral').hasClass('selected')) {
            if ($('.text_color_ral').html() == "No existe RAL" || $('.text_color_ral').html() == "") {
                $('.text_color_ral').addClass('text-danger');
                $('.text_color_ral').html('No existe RAL');
                submit_form = false;
            }
        }

        return submit_form;
    }

    function check_add_new_product(id_product, input_subfamily_product, tipologia) {
        var submit_form = true;
        if (id_product == "2733" || id_product == "2736") {
            if (input_subfamily_product > 0) {
                submit_form = true;
                if (id_product == "2736") {
                    if (tipologia > 0) {
                        submit_form = true;
                    } else {
                        submit_form = false;
                    }
                }
            } else {
                submit_form = false;
                if (id_product == "2736") {
                    if (tipologia) {
                        submit_form = true;
                    } else {
                        submit_form = false;
                    }
                } else {
                    submit_form = true;
                }
            }
        }

        if ($('.text_color_ral').hasClass('selected')) {
            if ($('.text_color_ral').html() == "No existe RAL" || $('.text_color_ral').html() == "") {
                $('.text_color_ral').addClass('text-danger');
                $('.text_color_ral').html('No existe RAL');
                submit_form = false;
            }
        }

        return submit_form;
    }

    function delete_concepto() {
        $('.delete-concepto').confirmation({
            title: '¿Eliminar concepto?',
            btnOkLabel: texto_si,
            btnCancelLabel: texto_no,
            onConfirm: function () {
                var button_delete = $(this);
                var id_linea_concepto = $(this).attr('id_linea_concepto');
                var id_pres_modal = $('.view_conceptos').find('.id_pres_view').val();
                var data = {
                    id_linea_concepto: id_linea_concepto,
                    id_pres_modal: id_pres_modal
                }
                $.ajax({
                    url: "mods/mod_products/ajax/delete_line_concepto.php",
                    type: "post",
                    data: data,
                    dataType: 'json',
                    success: function (response) {
                        toastr.options = {
                            "positionClass": "toast-top-full-width"
                        }
                        if (response['error'] == "0") {
                            toastr.success("Conceptos", response['mensaje']);
                            $(button_delete).closest('.line_concepto').remove();
                        } else {
                            toastr.error("Conceptos", response['mensaje']);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error ajax en submitFormNewDni");
                        console.log(jqXHR, textStatus, errorThrown);
                    }

                });
            }
        });
    }

    function logout() {
        $.ajax({
            url: "mods/mod_customers/action/log_out.php",
            success: function () {},
            error: function (jqXHR, textStatus, errorThrown) {}

        });
    }

    function icon_exchange_presupuestos() {
        var tr = $(this).closest('.file_historial');
        $('.icon_exchange_presupuestos').confirmation({
            title: '¿Confirmar presupuesto? Si confirma se tramitará el pedido',
            btnOkLabel: texto_si,
            btnCancelLabel: texto_no,
            onConfirm: function () {
                var id_pres = $(this).attr('id_pres');
                var data = {
                    id_pres: id_pres
                }
                $.ajax({
                    url: "mods/mod_customers/ajax/presupuesto_a_pedido.php",
                    type: "post",
                    data: data,
                    dataType: 'json',
                    success: function (response) {
                        if (response['error'] == 0) {
                            toastr.options = {
                                "positionClass": "toast-top-full-width"
                            }
                            toastr.success(response['message'], "Presupuesto");
                            $(tr).find('.icon_exchange_presupuestos').remove();
                            var url = window.location.href;
                            var url2 = url.replace("presupuesto", "pedidos");
                            var url2 = url2.replace("&id=", "&id_x=");
                            window.location.href = url2;
                            setTimeout(function () {
                                window.location.reload()
                            }, 800);
                        } else {
                            toastr.options = {
                                "positionClass": "toast-top-full-width"
                            }
                            toastr.error(response['message'], "Presupuesto");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error ajax en submitFormNewDni");
                        console.log(jqXHR, textStatus, errorThrown);
                    }

                });
            }
        });
    }

    function btn_delete_line_component() {
        $('.btn_delete_line_component').confirmation({
            //rootSelector: '.btn_delete_dire_cliente',
            title: mensaje_estas_seguro,
            btnOkLabel: texto_si,
            btnCancelLabel: texto_no,
            onConfirm: function () {
                $(this).closest('.col-md-12').find('.segunda_linea_producto').remove();
                $(this).closest('.col-md-12').remove();
                var count_tr = $('.content_lineas_pedido .line_product').length;
                if (count_tr == 0) {
                    $('.table_product_0').removeClass('hidden');
                }
                var total = 0;
                $('.product_price').each(function () {
                    if ($.isNumeric($(this).html())) {
                        total += parseFloat($(this).html());
                    }
                })
                $('.product_price_lacado').each(function () {
                    total += parseFloat($(this).html());
                })
                total = total.toFixed(2);
                $('.price_total').val(total);

                var color_product = $(this).closest('.line_product').find('.input_color_product').attr('span_value');
                var id_product = $(this).closest('.line_product').find('.input_id_product').attr('span_value');
                update_session_ral(color_product, id_product);
                save_cart_session();
            }
        });
    }

    function icon_trash_presupuestos() {
        var tr = $(this).closest('.file_historial');
        $('.icon_trash_presupuestos').confirmation({
            title: '¿Eliminar presupuesto?',
            btnOkLabel: texto_si,
            btnCancelLabel: texto_no,
            onConfirm: function () {
                var id_pres = $('.icon_trash_presupuestos').attr('id_pres');
                var data = {
                    id_pres: id_pres
                }
                $.ajax({
                    url: "mods/mod_customers/ajax/eliminar_presupuesto.php",
                    type: "post",
                    data: data,
                    dataType: 'json',
                    success: function (response) {
                        if (response['error'] == 0) {
                            toastr.options = {
                                "positionClass": "toast-top-full-width"
                            }
                            toastr.success(response['message'], "Presupuesto");
                            $(tr).remove();
                        } else {
                            toastr.options = {
                                "positionClass": "toast-top-full-width"
                            }
                            toastr.error(response['message'], "Presupuesto");
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error ajax en submitFormNewDni");
                        console.log(jqXHR, textStatus, errorThrown);
                    }

                });
            }
        });
    }

    function icon_repeat_presupuestos() {
        $('.icon_repeat_presupuestos').confirmation({
            title: '¿Recalcular presupuesto?',
            btnOkLabel: texto_si,
            btnCancelLabel: texto_no,
            onConfirm: function () {
                var id_pres = $(this).attr('id_pres');
                var data = {
                    id_pres: id_pres
                }
                $.ajax({
                    url: "mods/mod_customers/ajax/recalcular_presupuesto.php",
                    type: "post",
                    data: data,
                    dataType: 'json',
                    success: function (response) {
                        if (response['error'] == 0) {
                            toastr.options = {
                                "positionClass": "toast-top-full-width"
                            }
                            toastr.success(response['message'], "Presupuesto");
                            setTimeout(function () {
                                window.location.reload()
                            }, 800);
                        } else {
                            toastr.options = {
                                "positionClass": "toast-top-full-width"
                            }
                            toastr.error(response['message'], "Presupuesto");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error ajax en submitFormNewDni");
                        console.log(jqXHR, textStatus, errorThrown);
                    }

                });
            }
        });
    }

    btn_delete_line_component();

    icon_exchange_presupuestos();

    icon_trash_presupuestos();

    icon_repeat_presupuestos();

    var is_type_product_modal = 0;

    function getResolution() {
        console.log($(window).width() + 'px X ' + $(window).height());
    }

    function show_tooltip() {
        $('[data-tooltip="tooltip"]').tooltip();
    }

    show_tooltip();

    function magnific_image() {

        $('.image-popup-no-margins').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: false,
            fixedContentPos: true,
            mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
            image: {
                verticalFit: true
            },
            zoom: {
                enabled: true,
                duration: 300 // don't foget to change the duration also in CSS
            }
        });
    }

    $('[type=number]').inputNumber();

    function panelheadingselect() {
        $('.panel-heading').each(function () {
            $(this).removeClass('selected');
        });
    }

    $('.editor_text').summernote({
        height: 300, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: true, // set focus to editable area after initializing summernote
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', ]],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
        ]
    });

    magnific_image();

    function pad(input, length, padding) {
        var str = input + "";
        return (length <= str.length) ? str : pad(str + padding, length, padding);
    }

    var type_doc = "";

    function response_save_presupuesto(response) {
        type_doc = response['is_pedido'];
        if (response['error'] == 0) {
            toastr.options = {
                "positionClass": "toast-top-full-width"
            }
            toastr.success(type_doc, "Datos guardados correctamente");
            //setTimeout(function(){window.location.href = 'clientes.php?opt=historial'}, 800);
            $('#modal_edit_dire_envio').modal('hide');
            $('#modal_view_pdfs').modal('show');
            $('.type_doc').html(type_doc);
            if (type_doc == "pedido") {
                $('.content_view_cond').hide();
                $('.btn_add_concepto').hide();
                $('.btn_add_editar_datos_cliente').hide();
                $('.btn_view_pdf_modal').html("Ver pedido");
            }
            var id_pres = response['id_pres'];
            $('.id_pres_modal').val(id_pres);
            $('.btn_view_pdf_pvp').attr('href', $('.url_pdf_pvp').val() + id_pres);
            $('.btn_view_pdf_condiciones').attr('href', $('.url_pdf_condiciones').val() + id_pres);
        } else {
            toastr.options = {
                "positionClass": "toast-top-full-width"
            }
            toastr.error(type_doc, "Error al guardar los datos");
        }
    }

    var path_project = $('.path_project').val();
    var path_images = path_project + 'images/';
    $('.dropdown-toggle').dropdown();

    //Controlador para mostrar u ocultar imagen previa de expositores en el modal de añadir productos
    var opcion_expositor = 0;

    function check_product(e) {
        e.preventDefault();
        var $this = $(this);
        var value = $($this).attr('id_product');
        if (value == "2734" || value == "2735") {}else{
            panelheadingselect();
        }
        $(this).closest('.panel-default').find('.panel-heading').addClass('selected');
        if (value == "plisadas" || value == "fijas_correderas") {
            $('.type-product').addClass('hidden');
            if(value == "plisadas"){
                $('.type_product_group_plisadas').removeClass('hidden');
                $('.type_product_group_fijas_correderas').addClass('hidden');
            }else{
                $('.type_product_group_fijas_correderas').removeClass('hidden');
                $('.type_product_group_plisadas').addClass('hidden');
            }
            $('.title_label').each(function () {
                $(this).removeClass('selected');
            });
            $($this).find('.title_label').addClass('selected');
            $('.div_preview_img').addClass('hidden');
            $('.div_color_acabado').addClass('hidden');
            $('.info_insert').addClass("hidden");
            $('.div_producto_versiones').addClass('hidden');
            $('.div_submit_product ').addClass('hidden');
            return false;
        } else {
            if (value == "2734" || value == "2735") {

            } else {
                $('.type_product_group_fijas_correderas').addClass('hidden');
                $('.type_product_group_plisadas').addClass('hidden');
            }
            $('.input_id_product').val(value);
            $('.title_label').each(function () {
                $(this).removeClass('selected');
            });
            $($this).find('.title_label').addClass('selected');

            if (value != "exp") {
                opcion_expositor = 0;
                $('.div_colors').removeClass("col-md-12 col-xs-12").addClass("col-md-8 col-xs-8");
                $('.titulo_colores_modal_productos').html("Colores");
                $('.div_preview_img').removeClass('hidden');
                $('.type-product').removeClass('hidden');
                $('.div_color_acabado').addClass('hidden');
                $('.info_insert').removeClass("hidden").empty();
                $('.div_producto_versiones').addClass('hidden');
                $('.change_components').prop('checked', false);
                if (value == "2733") {
                    $($this).closest('#content_product').find('.div_button_componente').addClass('hidden');
                    $($this).closest('#content_product').find('.div_button_industrial').addClass('hidden');
                } else {
                    $($this).closest('#content_product').find('.div_button_componente').removeClass('hidden');
                    $($this).closest('#content_product').find('.div_button_industrial').removeClass('hidden');
                }
            } else {
                opcion_expositor = 1;
                $('.div_colors').addClass("col-md-12 col-xs-12").removeClass("col-md-8 col-xs-8");
                $('.titulo_colores_modal_productos').html("Muestras");
                $('.div_preview_img').addClass('hidden');
                $('.type-product').addClass('hidden');
                $('.div_color_acabado').removeClass('hidden');
                $('.info_insert').addClass("hidden");
                $('.boton_componentes_flotante_add').addClass("hidden");
            }
            var is_venta_industrial = check_is_venta_industrial();
            var mensaje_error_industrial = "<span class='text-danger'>Venta industrial no puede grabarse con el resto de productos</span>";
            var mensaje_error_acabado = "<span class='text-danger'>Producto acabado no puede grabarse con venta industrial</span>";
            var mensaje_error_componente = "<span class='text-danger'>Componente no puede grabarse con venta industrial</span>";
            if (is_venta_industrial == "1") {
                $($this).closest('#content_product').find('.div_button_acabado').each(function () {
                    $(this).html(mensaje_error_acabado);
                    $(this).addClass('text-left').removeClass('text-right');
                });
                $($this).closest('#content_product').find('.div_button_componente').each(function () {
                    $(this).html(mensaje_error_componente);
                    $(this).addClass('text-left').removeClass('text-center');
                });
            } else if (is_venta_industrial == "0") {
                $($this).closest('#content_product').find('.div_button_industrial').each(function () {
                    $(this).html(mensaje_error_industrial);
                });
            }

            var data = {
                'id_product': value
            }

            $.ajax({
                url: "mods/mod_products/ajax/change_product.php",
                type: "post",
                data: data,
                success: function (response) {
                    //Panel versiones solamente visible para la familia 2731
                    $('.div_colors').html(response);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }

            });
        }
    }

    function check_subfamilia() {
        //e.preventDefault();
        $('.div_color_acabado').removeClass('hidden');
        var $this = $(this);
        var value = $($this).attr('sub_familia');
        $(this).closest('.panel-default').find('.panel-heading').addClass('selected');
        $(this).closest('.panel-default').find('.input_subfamily_product').val(value);
        $('.radio_subfamilia').find('.title_label').each(function () {
            $(this).removeClass('selected');
        });
        $($this).find('.title_label').addClass('selected');
        $('.click_radio_version').each(function () {
            $(this).prop("checked", false);
        });
        $this.find('.click_radio_version').prop("checked", true);

        if ($($this).hasClass('radio_abatible')) {

            $('.text-select-version').html("Seleccione una tipología:");
            $('.select_tipologia').addClass('hidden');
            $('.tipologia_' + value).removeClass('hidden');

            $('html, body').animate({
                scrollTop: $(".div_tipologia_abatible").offset().top
            }, 1400);

        } else {

            $('html, body').animate({
                scrollTop: $(".div_colors").offset().top
            }, 1400);

        }

    }

    function select_tipologia(e) {
        e.preventDefault();
        $(this).closest('.panel-default').find('.panel-heading').addClass('selected');
        var tipologia = $(this).val();
        $(this).closest('.panel-default').find('.input_tipologia').val(tipologia);
    }

    function check_color(e) {
        e.preventDefault();
        var $this = $(this);
        $(this).closest('.panel-default').find('.panel-heading').addClass('selected');
        var value = $($this).attr('color_product');
        var group_color = $($this).parent().attr('group_color');
        var id_product = $('.input_id_product').val();
        $('.input_color_product').val(value);
        $('.input_group_color').val(group_color);
        $('.title_color_product').each(function () {
            $(this).removeClass('selected');
        });
        $($this).find('.title_color_product').addClass('selected');

        if (group_color == "lacados") {
            $('.div_submit_product').removeClass('hidden');
            $('.text_no_image_preview').html("");
            $('.content_lupa_vista_previa').addClass("hidden");
            var url = path_images + "images_product/" + id_product + "/image_ral.png";
            $('.preview_img').css("background-image", "url(" + url + ")");
        } else {

            $('.content_lupa_vista_previa').removeClass("hidden");
            $('.text_no_image_preview').html("");
            var url = path_images + "images_product/" + id_product + "/" + value + ".png";
            var url_big = path_images + "images_product/" + id_product + "/" + value + "_big.png";
            $('.preview_img').css("background-image", "url(" + url + ")");
            $('.lupa_div_content_preview_img').attr('href', url_big);
            $('.div_submit_product').removeClass('hidden');
        }
    }

    function check_medidas() {
        var width_product = $('.input_width_product').val();
        var height_product = $('.input_height_product').val();
        var min_anc = $('.input_danch').val();
        var max_anc = $('.input_hanch').val();
        var min_alt = $('.input_dalto').val();
        var max_alt = $('.input_halto').val();
        var error_medidas = 0;
        if (width_product < min_anc || width_product > max_anc) {
            error_medidas = 1;
        }
        if (height_product < min_alt || height_product > max_alt) {
            error_medidas = 1;
        }

        return error_medidas;
    }

    function check_login() {
        var is_login = $('.is_login').val();
        return is_login;
    }

    //Según lo que devuelva la función check_medidas hacer una cosa u otra
    //function submit_form_add_line(e){
    //    e.preventDefault();
    //    if(check_medidas() == 1){
    //        toastr.options = {
    //            "positionClass": "toast-top-full-width"
    //        }
    //        toastr.error("Medidas", "Revise las medidas");
    //
    //    }else {
    //        $.ajax({
    //            url: "mods/mod_products/ajax/add_line_product.php",
    //            type: "post",
    //            data: $(this).serialize(),
    //            success: function (response) {
    //                $('.table_product').removeClass('hidden');
    //                $('.table_product_0').hide();
    //                $('.content_table_linea_pedidos').append(response);
    //            },
    //            error: function (jqXHR, textStatus, errorThrown) {
    //                console.log("Error ajax en submitFormNewDni");
    //                console.log(jqXHR, textStatus, errorThrown);
    //            }
    //
    //        });
    //    }
    //}

    function update_session_ral(color_product, id_product) {
        var total_ral = count_lacado_color(color_product);
        var data_ral = {
            'color_product': color_product,
            'option_sesion_ral': 'change_unit',
            'unit_total': total_ral,
            'id_product': id_product
        }
        $.ajax({
            url: "mods/mod_products/ajax/update_session_ral.php",
            type: "post",
            data: data_ral,
            success: function (response) {
                $('.content_lacado_ral').html(response);
                if ($('.preview_color_lacado').length > 0) {
                    $('.line_lacados').removeClass('hidden');
                } else {
                    $('.line_lacados').addClass('hidden');
                }
                var total = 0.00;
                $('.product_price').each(function () {
                    if ($.isNumeric($(this).html())) {
                        total += parseFloat($(this).html());
                    }
                })
                $('.product_price_lacado').each(function () {
                    total += parseFloat($(this).html());
                })
                total = total.toFixed(2);
                $('.price_total').val(total);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function change_color_session_ral(color_product_old, color_product, units, id_product) {
        var data_ral = {
            'color_product_old': color_product_old,
            'color_product_new': color_product,
            'option_sesion_ral': 'change_color',
            'unit_total': units,
            'id_product': id_product
        }

        $.ajax({
            url: "mods/mod_products/ajax/update_session_ral.php",
            type: "post",
            data: data_ral,
            success: function (response) {
                $('.content_lacado_ral').html(response);
                if ($('.preview_color_lacado').length > 0) {
                    $('.line_lacados').removeClass('hidden');
                } else {
                    $('.line_lacados').addClass('hidden');
                }
                var total = parseFloat($('.price_total').val());
                total = total.toFixed(2);
                $('.price_total').val(total);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function delete_product_line() {

        $('.btn_delete_line').confirmation({
            rootSelector: '.btn_delete_dire_cliente',
            title: mensaje_estas_seguro,
            btnOkLabel: texto_si,
            btnCancelLabel: texto_no,
            onConfirm: function () {
                $(this).closest('.line_product').remove();
                var count_tr = $('.content_lineas_pedido .line_product').length;
                if (count_tr == 0) {
                    $('.table_product_0').removeClass('hidden');
                }
                var total = 0;
                $('.product_price').each(function () {
                    if ($.isNumeric($(this).html())) {
                        total += parseFloat($(this).html());
                    }
                })
                $('.product_price_lacado').each(function () {
                    total += parseFloat($(this).html());
                })
                total = total.toFixed(2);
                $('.price_total').val(total);

                var color_product = $(this).closest('.line_product').find('.input_color_product').attr('span_value');
                var id_product = $(this).closest('.line_product').find('.input_id_product').attr('span_value');
                update_session_ral(color_product, id_product);
                save_cart_session();
            }
        });
    }

    function copy_table_line(e) {
        e.preventDefault();
        var content_tr = $(this).closest('.line_product').clone();
        $(content_tr).find('.check_size').attr('value', '').attr('placeholder', '0.000').addClass('border-warning').removeClass('border-success');
        $(content_tr).find('.unit_increment').addClass('border-warning').removeClass('border-success');
        $(content_tr).find('.product_price').first().html('<span class="text-danger">Revisar medidas mínimas y máximas</span>');
        $(content_tr).find('.tooltip').hide();
        content_tr = $(content_tr).html();
        $($(this).closest('.line_product')).after("<div class='line_product effect7 col-md-12 col-xs-12'>" + content_tr + "</div>");
        var total = 0;
        $('.product_price').each(function () {
            if ($.isNumeric($(this).html())) {
                total += parseFloat($(this).html());
            }
        })
        $('.product_price_lacado').each(function () {
            total += parseFloat($(this).html());
        })
        total = total.toFixed(2);
        $('.price_total').val(total);

        delete_product_line();

        $('.delete_increment').confirmation({
            rootSelector: '.delete_increment',
            title: mensaje_estas_seguro,
            btnOkLabel: texto_si,
            btnCancelLabel: texto_no,
            onConfirm: function () {
                $(this).closest('.row_option_product').next().remove();
                $(this).closest('.row_option_product').remove();
                var total = 0;
                $('.product_price').each(function () {
                    if ($.isNumeric($(this).html())) {
                        total += parseFloat($(this).html());
                    }
                })
                $('.product_price_lacado').each(function () {
                    total += parseFloat($(this).html());
                })
                total = total.toFixed(2);
                $('.price_total').val(total);

                save_cart_session();
            }
        });

        var color_product = $(this).closest('.line_product').find('.input_color_product').attr('span_value');
        var id_product = $(this).closest('.line_product').find('.input_id_product').attr('span_value');
        update_session_ral(color_product, id_product);

        show_tooltip();
        magnific_image();

        save_cart_session();
    }

    var send_ajax = "1";
    function calc_price(t) {
        var id_product = $(t).closest('.line_product').find('.input_id_product').attr('span_value');
        var width_product = $(t).closest('.line_product').find('.input_width_product').val();
        var height_product = $(t).closest('.line_product').find('.input_height_product').val();
        var subfamily_product = $(t).closest('.line_product').find('.input_subfamily_product').val();
        var group_color = $(t).closest('.line_product').find('.input_group_color').attr('span_value');
        var unit_product = $(t).closest('.line_product').find('.input_unit_product').val();
        var input_color_product = $(t).closest('.line_product').find('.input_color_product').attr('span_value');
        var total_lacado = $(t).closest('.content_div_linea').find('.total_lacado').val();
        var data = {
            id_product: id_product,
            width_product: width_product,
            height_product: height_product,
            subfamily_product: subfamily_product,
            group_color: group_color,
            unit_product: unit_product,
            input_color_product: input_color_product,
            total_lacado: total_lacado
        }
        if (send_ajax == "1") {
            $.ajax({
                url: "mods/mod_products/ajax/calc_prices.php",
                type: "post",
                data: data,
                beforeSend: function () {
                    send_ajax = "0";
                },
                success: function (response) {
                    $(t).closest('.line_product').find('.product_price').first().html(response);
                    var total = 0;
                    $('.product_price').each(function () {
                        if ($.isNumeric($(this).html())) {
                            total += parseFloat($(this).html());
                        }
                    })
                    $('.product_price_lacado').each(function () {
                        total += parseFloat($(this).html());
                    })
                    if (function_change_color_ral_eject == 0) {
                        var id_product = $(t).closest('.line_product').find('.input_id_product').attr('span_value');
                        update_session_ral(input_color_product, id_product);
                    }
                    total = total.toFixed(2);
                    $('.price_total').val(total);
                    send_ajax = "1";
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }

            });
        }

        if (id_product == "2734") {
            var exist_increments = $(t).closest('.line_product').find('.row_option_product').length;
            if (exist_increments > '0') {
                var incrementos = $(t).closest('.line_product').find('.row_option_product').find('.id_component');
                var medio_punto = "";
                $(incrementos).each(function () {
                    var value_increment = $(this).val();
                    value_increment = value_increment.replace(/\"/g, '');
                    value_increment = $.trim(value_increment);
                    if (value_increment == "279990000004") {
                        medio_punto = $(this).parent().next();
                    }
                });
                if (width_product == height_product * 2) {
                    $(medio_punto).find('.btn-adjuntar-croquis').addClass('hidden');
                    $(medio_punto).find('.name_fichero_upload').addClass('hidden');
                } else {
                    $(medio_punto).find('.btn-adjuntar-croquis').removeClass('hidden');
                    $(medio_punto).find('.name_fichero_upload').removeClass('hidden');
                }
            }
        }
    }


    function count_lacado_color(color) {
        var total_color = 0;
        $('.line_product').each(function () {
            var color_search = $(this).find('.input_color_product').attr('span_value');
            var units = $(this).find('.input_unit_product').val();
            var id_product = $(this).find('.input_id_product').attr('span_value');

            if (color == color_search && units >= "1") {
                var data = {
                    id_product: id_product,
                    color_product: color
                }
                $.ajax({
                    url: "mods/mod_products/ajax/check_is_lacado.php",
                    type: "post",
                    data: data,
                    async: false,
                    success: function (response) {
                        if (response == "1") {
                            total_color = parseInt(total_color) + parseInt(units);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error ajax en submitFormNewDni");
                        console.log(jqXHR, textStatus, errorThrown);
                    }

                });
            }
        })

        $('.producto_corredera').each(function () {
            var color_search = $(this).find('.input_color_product').attr('span_value');
            var units = $(this).find('.input_unidades').val();
            var id_product = '2735';
            var total_hojas = 0;
            if ($(this).hasClass('corredera_completa')) {
                var total_completa = $(this).find('.cantidad_corredera_completa').val();
                if (total_completa > 0) {
                    total_hojas = parseInt($(this).find('.cantidad_hojas').val()) * parseInt(units);
                }
            }

            if (color == color_search && units >= "1") {
                var data = {
                    id_product: id_product,
                    color_product: color
                }
                $.ajax({
                    url: "mods/mod_products/ajax/check_is_lacado.php",
                    type: "post",
                    data: data,
                    async: false,
                    success: function (response) {
                        if (response == "1") {
                            total_color = parseInt(total_color) + parseInt(units) + parseInt(total_hojas);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error ajax en submitFormNewDni");
                        console.log(jqXHR, textStatus, errorThrown);
                    }

                });
            }
        });
        return total_color;
    }

    function calc_price_unit() {
        var t = $(this);
        if ($(this).val() <= 0) {
            $(this).val("1");
            $(this).html("1");
        }

        var id_product = t.closest('.line_product').find('.input_id_product').attr('span_value');
        var width_product = t.closest('.line_product').find('.input_width_product').val();
        var height_product = t.closest('.line_product').find('.input_height_product').val();
        var subfamily_product = t.closest('.line_product').find('.input_subfamily_product').val();
        var group_color = t.closest('.line_product').find('.input_group_color').attr('span_value');
        var unit_product = t.closest('.line_product ').find('.input_unit_product').val();
        var input_color_product = t.closest('.line_product').find('.input_color_product').attr('span_value');
        var total_lacado = t.closest('.content_div_linea').find('.total_lacado').val();

        var data = {
            id_product: id_product,
            width_product: width_product,
            height_product: height_product,
            subfamily_product: subfamily_product,
            group_color: group_color,
            unit_product: unit_product,
            input_color_product: input_color_product,
            total_lacado: total_lacado
        }
        if (send_ajax == "1") {
            $.ajax({
                url: "mods/mod_products/ajax/calc_prices.php",
                type: "post",
                data: data,
                beforeSend: function () {
                    send_ajax = "0";
                },
                success: function (response) {
                    $(t).closest('.row').find('.product_price').first().html(response);
                    var total = 0;
                    $('.product_price').each(function () {
                        if ($.isNumeric($(this).html())) {
                            total += parseFloat($(this).html());
                        }
                    })
                    $('.product_price_lacado').each(function () {
                        total += parseFloat($(this).html());
                    })
                    total = total.toFixed(2);
                    $('.price_total').val(total);
                    save_cart_session();
                    send_ajax = "1";
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }

            });
        }

        var color_product = $(this).closest('.line_product').find('.input_color_product').attr('span_value');
        var id_product = $(this).closest('.line_product').find('.input_id_product').attr('span_value');
        update_session_ral(color_product, id_product);

    }

    function change_width(e) {
        var width_product = $(this).val();
        var min_anc = $(this).closest('.line_product').find('.input_danch').attr('span_value');
        var max_anc = $(this).closest('.line_product').find('.input_hanch').attr('span_value');
        var error_medidas = 0;

        var tecla = (document.all) ? e.keyCode : e.which;

        if ($(this).val().length > 4 && tecla != 8 || tecla == 44 || tecla == 46 || tecla < 48 && tecla != 8 && tecla != 0 || tecla > 57 && tecla != 8 && tecla != 0) {
            e.preventDefault();
        }

        if ($(this).val().length == 1 && tecla != 8) {
            $(this).val($(this).val() + ".");
        }

        if (pad(width_product, 5, 0) < min_anc || pad(width_product, 5, 0) > max_anc || pad(width_product, 5, 0) <= 0) {
            error_medidas = 1;

            $(this).addClass('border-warning').attr('title', "Valor mínimo: " + min_anc + " y máximo: " + max_anc + ". Ejemplo formato: 1.010");
            $(this).removeClass('border-success');
        } else {
            $(this).removeClass('border-warning');
            $(this).addClass('border-success');
        }

        if ($(this).hasClass('border-warning') && $(this).val().length == 5 && tecla == 8) {
            $(this).val('');
        }

        calc_price($(this));

        save_cart_session();
    }

    function change_height(e) {
        var height_product = $(this).val();
        var min_alt = $(this).closest('.line_product').find('.input_dalto').attr('span_value');
        var max_alt = $(this).closest('.line_product').find('.input_halto').attr('span_value');
        var error_medidas = 0;

        var tecla = (document.all) ? e.keyCode : e.which;

        if ($(this).val().length > 4 && tecla != 8 || tecla == 44 || tecla == 46 || tecla < 48 && tecla != 8 && tecla != 0 || tecla > 57 && tecla != 8 && tecla != 0) {
            e.preventDefault();
        }

        if ($(this).val().length == 1 && tecla != 8) {
            $(this).val($(this).val() + ".");
        }

        if (pad(height_product, 5, 0) < min_alt || pad(height_product, 5, 0) > max_alt || pad(height_product, 5, 0) <= 0) {
            error_medidas = 1;
            $(this).addClass('border-warning').attr('title', "Valor mínimo: " + min_alt + " y máximo: " + max_alt + ". Ejemplo formato: 1.010");
            ;
            $(this).removeClass('border-success');
        } else {
            $(this).removeClass('border-warning');
            $(this).addClass('border-success');
        }

        if ($(this).hasClass('border-warning') && $(this).val().length == 5 && tecla == 8) {
            $(this).val('');
        }

        calc_price($(this));

        save_cart_session();
    }

    //function change_img_info(){
    //    $(this).closest('tr').find('td').removeClass('group_select');
    //    $(this).parent().addClass('group_select');
    //    var id_sub = $(this).parent().attr('id_sub');
    //    var id_product = $('.input_id_product').val();
    //    var id_group_color = $('.input_group_color').val();
    //    $('.input_subfamily_product').val(id_sub);
    //    var data = {
    //        subfamily: id_sub,
    //        id_product: id_product,
    //        id_group_color: id_group_color
    //    };
    //    $.ajax({
    //        url: "mods/mod_products/ajax/change_img_info.php",
    //        type: "post",
    //        data: data,
    //        dataType: 'json',
    //        success: function (response) {
    //            $('.input_danch').val(response['ancho_minimo']);
    //            $('.input_hanch').val(response['ancho_maximo']);
    //            $('.input_dalto').val(response['alto_minimo']);
    //            $('.input_halto').val(response['alto_maximo']);
    //        },
    //        error: function (jqXHR, textStatus, errorThrown) {
    //            console.log("Error ajax en submitFormNewDni");
    //            console.log(jqXHR, textStatus, errorThrown);
    //        }
    //
    //    });
    //}

    function focusout() {
        if ($(this).val().length == 2) {
            $($(this).val($(this).val() + "0"));
        }
        if ($(this).val().length >= 2) {
            for (var i = 0; i < 5; i++) {
                if ($(this).val().length < 5) {
                    $($(this).val($(this).val() + "0"));
                }
            }
        }
    }

    function only_numbers(e) {
        var tecla = (document.all) ? e.keyCode : e.which;
        if (tecla > 31 && (tecla < 48 || tecla > 57)) {
            e.preventDefault();
        }
    }

    var bt_option = "";

    function btn_option_product() {
        bt_option = $(this);
        var id_product = $(this).closest('.line_product').find('.input_id_product').attr('span_value');
        var version = $(this).closest('.line_product').find('.change-subfamily').val();
        var id = $(this).closest('.line_product').find('.title_product').html();

        var row_component = $(bt_option).closest('.line_product').find('.row_option_product');
        var array_components = [];
        if ($(row_component).length) {
            $(row_component).each(function (index, element) {
                var id_component = $(element).find('.id_component').val();
                var array_component = {
                    id_component: id_component
                }
                array_components.push(array_component);
            });
        }


        var data = {
            id_product: id_product,
            array_components: array_components,
            version: version
        }

        $.ajax({
            url: "mods/mod_products/ajax/get_options_product.php",
            type: "post",
            data: data,
            success: function (response) {
                $('#modal_option_product').find('.modal-body').html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function checkbox_options_products() {
        var line_product = $(bt_option).closest('.line_product').find('.increments');
        var unit_max = $(bt_option).closest('.line_product').find('.input_unit_product').val();
        var desc = $(this).closest('.row').find('.desc').html();
        var price = $(this).closest('.row').find('.price').html();
        var id_component = $(this).closest('.row').find('.id_component').val();
        price = parseFloat(price).toFixed(2);
        show_button_croquis = "hidden";
        if (id_component == "279990000003" || id_component == "279990000005" || id_component == "279990000004") {
            show_button_croquis = "";
        }
        if (id_component == "279990000004") {
            var ancho_fija = $(bt_option).closest('.line_product').find('.input_width_product').val();
            var alto_fija = $(bt_option).closest('.line_product').find('.input_height_product').val();
            if (ancho_fija == alto_fija * 2) {
                show_button_croquis = "hidden";
            }
        }
        var div_add = "<div class='row_option_product row col-md-12 col-xs-12'><div class='col-xs-1 col-md-1'><span data-tooltip='tooltip' title='Eliminar incremento' data-placement='top' class='fa fa-2x fa-close text-danger delete_increment'></span></div><div class='col-md-1 col-xs-1'>-</div><div class='col-md-5 col-xs-5 text-left'><span class='title_product title_increment_product'>" + desc + "</span></div><div class='col-md-3 col-xs-3'><input required placeholder='0' title='' value='0' min='0' type='number' name='input_unit_product' class='unit_increment border-warning form-control w75'/></div><div class='col-md-2 col-xs-2'><span class='product_price'>0.00</span></div><div class='hidden'><span class='product_price_fixed'>" + price + "</span></div><input type='hidden' class='id_component' value='" + id_component + "'></div><div class='row col-md-12 col-xs-12'><div class='col-xs-1 col-md-1'><span class='hidden " + show_button_croquis + " name_fichero_upload_hidden'></span><span class='" + show_button_croquis + " name_fichero_upload text-danger'>¡Subir archivo!</span></div><div class='col-md-1 col-xs-1'></div><div class='col-md-3 col-xs-3'><button data-toggle='modal' data-target='#modal_add_croquis_incremento' class='btn btn-default btn-grey btn-xs bg-border-info btn-adjuntar-croquis " + show_button_croquis + "'>Adjuntar croquis</button></div><div class='col-md-7 col-xs-7 text-right'><span class='message_units_increment'>*Unidades por mosquitera completa*</span></div><div class='hidden'></div></div>";

        if ($(this).is(':checked')) {
            $(line_product).append(div_add);
            $('.unit_increment').attr('max', unit_max);
            var total = 0;
            $('.product_price').each(function () {
                if ($.isNumeric($(this).html())) {
                    total += parseFloat($(this).html());
                }
            })
            $('.product_price_lacado').each(function () {
                total += parseFloat($(this).html());
            })
            total = total.toFixed(2);
            $('.price_total').val(total);
            save_cart_session();
            $('.delete_increment').confirmation({
                rootSelector: '.delete_increment',
                title: mensaje_estas_seguro,
                btnOkLabel: texto_si,
                btnCancelLabel: texto_no,
                onConfirm: function () {
                    $(this).closest('.row_option_product').next().remove();
                    $(this).closest('.row_option_product').remove();
                    var total = 0;
                    $('.product_price').each(function () {
                        if ($.isNumeric($(this).html())) {
                            total += parseFloat($(this).html());
                        }
                    })
                    $('.product_price_lacado').each(function () {
                        total += parseFloat($(this).html());
                    })
                    total = total.toFixed(2);
                    $('.price_total').val(total);
                    var color_product = $(this).closest('.line_product').find('.input_color_product').attr('span_value');
                    var id_product = $(this).closest('.line_product').find('.input_id_product').attr('span_value');
                    update_session_ral(color_product, id_product);
                    save_cart_session();
                }
            });
            show_tooltip();
        } else {
            var array_com = $(bt_option).closest('.line_product').find('.id_component');
            $(array_com).each(function () {
                if ($(this).val() == id_component) {
                    $(this).closest('.row_option_product').next().remove();
                    $(this).closest('.row_option_product').remove();
                }
            });

        }
    }

    function form_add_new_product(e) {

        e.preventDefault();

        var id_product = $(this).closest('#content_product').find('.input_id_product').val();
        var subfamilia = $(this).closest('#content_product').find('.radio_subfamilia').attr('sub_familia');
        var tipologia = $(this).closest('#content_product').find('.input_tipologia').val();

        if (id_product == "2733") {
            tipologia = "0";
        }

        if (check_add_new_product(id_product, subfamilia, tipologia)) {

            var color_product = $(this).closest('#content_product').find('.input_color_product').val();

            var exist_line_expositor = $('.line_product_expositor').length;

            var exist_line_lacado = $('.content-lacados').length;

            var data = $(this).serializeArray();
            data.push({name: 'exist_line_lacado', value: exist_line_lacado},
                    {name: 'color_product', value: color_product},
                    {name: 'id_product', value: id_product});

            $.ajax({
                url: "mods/mod_products/ajax/add_new_product.php",
                type: "post",
                data: data,

                success: function (response) {

                    if (opcion_expositor == 1 && exist_line_expositor >= 1) {

                        $('.line_product_expositor').append(response);

                    } else if (opcion_expositor == 1 && exist_line_expositor <= 0) {

                        var line_product_expositor = "<div class='effect7 col-md-12 col-xs-12 text-left line_product_expositor'><div class='row col-md-12 col-xs-12'><h4 class='title_product'>Expositores Punto de Venta</h4></div><div class='row col-md-12 col-xs-12 text-center'><div class='col-md-4 col-xs-4 title_line_product'>Producto</div><div class='col-md-4 col-xs-4 title_line_product'>Unidades</div><div class='col-md-3 col-xs-3 title_line_product'>Precio (€)</div><div class='col-md-1 col-xs-1 title_line_product'></div>";

                        $('.content_lineas_pedido').append(line_product_expositor);

                        $('.line_product_expositor').append(response).append('</div>');

                    } else {

                        $('.content_lineas_pedido').append(response);
                    }
                    $('#modal_add_product').modal('hide');
                    delete_product_line();
                    show_tooltip();
                    magnific_image();
                    update_session_ral(color_product, id_product);
                    var total = 0;
                    $('.product_price').each(function () {
                        total += parseFloat($(this).html());
                    })
                    $('.product_price_lacado').each(function () {
                        total += parseFloat($(this).html());
                    })
                    total = total.toFixed(2);
                    $('.price_total').val(total);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }

            });
        } else {
            toastr.options = {
                "positionClass": "toast-top-full-width"
            }
            toastr.error("Revise los datos del producto", "Enviar datos");
            return false;
        }
    }

    function btn_flotante_add(e) {

        e.preventDefault();

        var data = [];

        var filas_componentes = $('.fila_components_products');

        var submit = "true";

        if (type_precio == "fabrica") {

            $('.type_button_hidden').val($(this).val());
            $(filas_componentes).each(function (index, element) {
                var units_components = $(element).find('.linea_medida_componente_check_units').html();
                if (units_components > 0) {
                    var tipo_medida = $(element).find('.type_medida_component').val();
                    if (tipo_medida != 3) {

                        //Metros lineales o metros cuadrados
                        var arsubf = $(element).find('.arsubf').val();
                        var ararti = $(element).find('.ararti').val();
                        var arprpr = $(element).find('.arprpr').val();
                        var ararti_first = $(element).find('.ararti_first').val();
                        var components_color = $(element).find('.components_color').val();
                        var id_component = $(element).find('.id_component').val();
                        var total_component = $(element).find('.total_component').html();
                        var precio_fabrica = $(element).find('.precio_fabrica').val();
                        var is_almacen = $(element).find('.is_almacen').val();
                        var medida_maxima = $(element).find('.max_measure').val();
                        //Metros cuadrados
                        if (value_type_product == 1) {

                            var array_component = [];
                            //var data_array = {
                            //    input_unit_components: units_components,
                            //    total_component: 1,
                            //    arsubf: arsubf,
                            //    ararti: ararti,
                            //    arprpr: arprpr,
                            //    components_color: components_color,
                            //    ararti_first: ararti_first,
                            //    type_medida_component: type_medida_component
                            //}

                            $(this).find('.linea_medida_componente_check').each(function () {
                                var unidades_componente = $(this).find('.linea_medida_componente_check_units').html();
                                var alto_component = $(this).find('.linea_medida_componente_check_alto').html();
                                var ancho_component = $(this).find('.linea_medida_componente_check_ancho').html();
                                var metros_component = $(this).find('.linea_medida_componente_check_metros').html();

                                var medidas = {
                                    unidades: unidades_componente,
                                    alto: alto_component,
                                    ancho: ancho_component,
                                    metros: metros_component,
                                    id_component: id_component,
                                    arsubf: arsubf,
                                    ararti: ararti,
                                    arprpr: arprpr,
                                    total_component: total_component,
                                    tipo_medida: tipo_medida,
                                    components_color: components_color,
                                    precio_fabrica: precio_fabrica,
                                    medida_maxima: medida_maxima,
                                    is_almacen: is_almacen
                                }
                                array_component.push(medidas);
                            })

                            data.push(array_component);


                        } else if (value_type_product == 1) {
                            submit = false;
                            toastr.options = {
                                "positionClass": "toast-top-full-width"
                            }
                            toastr.error("Medidas", "Existen unidades sin medidas. Revise los datos");
                        }
                    } else {
                        //UNIDADES
                        var units_components_3 = $(element).find('.input_unit_components').val();
                        var total_component = $(element).find('.total_component').html();
                        var arsubf = $(element).find('.arsubf').val();
                        var ararti = $(element).find('.ararti').val();
                        var arprpr = $(element).find('.arprpr').val();
                        var ararti_first = $(element).find('.ararti_first').val();
                        var components_color = $(element).find('.components_color').val();
                        var id_component = $(element).find('.id_component').val();
                        var precio_fabrica = $(element).find('.precio_fabrica').val();
                        var is_almacen = $(element).find('.is_almacen').val();
                        var medida_maxima = $(element).find('.max_measure').val();
                        var array_component = [];
                        var data_array = {
                            input_unit_components: units_components_3,
                            total_component: total_component,
                            arsubf: arsubf,
                            ararti: ararti,
                            arprpr: arprpr,
                            id_component: id_component,
                            components_color: components_color,
                            ararti_first: ararti_first,
                            tipo_medida: tipo_medida,
                            precio_fabrica: precio_fabrica,
                            medida_maxima: medida_maxima,
                            is_almacen: is_almacen
                        }
                        array_component.push(data_array);
                        data.push(array_component);
                    }
                } else {
                    $(element).remove();
                }
            })
        } else {

            var check_error = $(filas_componentes).find('.bg-red').length;
            if (check_error <= 0) {
                $(filas_componentes).each(function (index, element) {
                    var units_components = $(element).find('.input_unit_components').val();
                    if (units_components <= 0) {
                        $(element).remove();
                    } else {
                        //UNIDADES
                        var tipo_medida = $(element).find('.type_medida_component').val();
                        var units_components_3 = $(element).find('.input_unit_components').val();
                        var total_component = $(element).find('.total_component').html();
                        var arsubf = $(element).find('.arsubf').val();
                        var ararti = $(element).find('.ararti').val();
                        var arprpr = $(element).find('.arprpr').val();
                        var ararti_first = $(element).find('.ararti_first').val();
                        var components_color = $(element).find('.components_color').val();
                        var id_component = $(element).find('.id_component').val();
                        var precio_almacen = $(element).find('.precio_almacen').val();
                        var is_almacen = $(element).find('.is_almacen').val();
                        var medida_maxima = $(element).find('.max_measure').val();
                        var arumiv = $(element).find('.arumiv').val();
                        var array_component = [];
                        var data_array = {
                            input_unit_components: units_components_3,
                            total_component: total_component,
                            arsubf: arsubf,
                            ararti: ararti,
                            arprpr: arprpr,
                            id_component: id_component,
                            components_color: components_color,
                            ararti_first: ararti_first,
                            tipo_medida: tipo_medida,
                            precio_fabrica: precio_almacen,
                            medida_maxima: medida_maxima,
                            is_almacen: is_almacen,
                            arumiv: arumiv
                        }
                        array_component.push(data_array);
                        data.push(array_component);
                    }
                })
            } else {
                toastr.options = {
                    "positionClass": "toast-top-full-width"
                }
                toastr.warning("Unidades", "Las unidades no son múltiplo del embalaje. Para esta cantidad de producto se debe seleccionar la opción 'Componentes'");
                return false;
            }
        }

        var jsonString = JSON.stringify(data);

        var exist_line_component = $('.content_lineas_pedido').find('.line_product_component_aux').length;

        var lineas_componentes = "";
        $.ajax({
            url: "mods/mod_cart/ajax/get_info_component.php",
            type: "post",
            data: {data: jsonString},
            success: function (response) {
                lineas_componentes = response;

                if (exist_line_component == 1) {

                    $('.line_product_component_aux').append(lineas_componentes);

                } else {

                    var line_product_component_aux = "<div class='line_product effect7 col-md-12 col-xs-12 text-left line_product_component_aux'><div class='row col-md-12 col-xs-12'><h4 class='title_product'>Componentes</h4></div><div class='row col-md-12 col-xs-12'><div class='col-md-2 col-xs-2 title_line_product'>Unidades</div><div class='col-md-2 col-xs-2 title_line_product'>Color</div><div class='col-md-2 col-xs-2 title_line_product'>Referencia</div><div class='col-md-3 col-xs-3 title_line_product'>Descripción</div><div class='col-md-2 col-xs-2 title_line_product'>Precio</div><div class='col-md-2 col-xs-2'></div></div></div>";

                    $('.content_lineas_pedido').append(line_product_component_aux);

                    $('.line_product_component_aux').append(lineas_componentes);

                }

                var total = 0;
                $('.product_price').each(function () {
                    if ($.isNumeric($(this).html())) {
                        total += parseFloat($(this).html());
                    }
                })
                $('.product_price_lacado').each(function () {
                    total += parseFloat($(this).html());
                })
                total = total.toFixed(2);
                $('.price_total').val(total);

                $('#modal_add_product').modal('hide');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });

    }

    function change_subfamily() {
        var element = $(this);
        var id_product = $(element).closest('.line_product').find('.input_id_product').attr('span_value');
        var subfamily_product = $(element).val();
        var group_color = $(element).closest('.line_product').find('.input_group_color').attr('span_value');
        var data = {
            'id_product': id_product,
            'subfamily_product': subfamily_product,
            'group_color': group_color
        }
        $.ajax({
            url: "mods/mod_products/ajax/change_subfamily.php",
            type: "post",
            data: data,
            dataType: 'json',
            success: function (response) {
                $(element).closest('.line_product').find('.input_danch').attr('span_value', pad(response['ancho_minimo'], 5, 0));
                $(element).closest('.line_product').find('.input_hanch').attr('span_value', pad(response['ancho_maximo'], 5, 0));
                $(element).closest('.line_product').find('.input_dalto').attr('span_value', pad(response['alto_minimo'], 5, 0));
                $(element).closest('.line_product').find('.input_halto').attr('span_value', pad(response['alto_maximo'], 5, 0));
                $(element).closest('.line_product').find('.input_width_product').click();
                $(element).closest('.line_product').find('.input_height_product').click();
                $(element).closest('.line_product').find('.input_width_product').focus();

                save_cart_session();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    var select_tr = "";
    var color_product_old = "";
    var units_product_old = "";

    function change_color_product() {
        select_tr = $(this).closest('.line_product');
        var id_product = $(this).closest('.line_product').find('.input_id_product').attr('span_value');
        if (id_product == "2735") {
            select_tr = $(this).closest('.form-horizontal');
        }
        units_product_old = $(this).closest('.line_product').find('.input_unit_product').val();
        color_product_old = $(this).attr('color_ral');
        var data = {
            id_product: id_product
        }
        $.ajax({
            url: "mods/mod_products/ajax/get_colors_product.php",
            type: "post",
            data: data,
            success: function (response) {
                $('#modal_product_color').find('.modal-body').html(response).css('overflow', 'scroll');
                $('#modal_product_color').modal('show').css('overflow', 'scroll');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });

    }

    function change_color_component() {
        $('#modal_product_color').modal('show').css('overflow', 'scroll');
        ;
        select_tr = $(this).closest('.fila_components_products');
        var id_product = $(this).closest('.fila_components_products').find('.input_id_product').val();
        var no_brut = "";
        if ($(this).hasClass('change_color_component_no_brut')) {
            no_brut = "1";
        }
        var data = {
            id_product: id_product,
            no_brut: no_brut
        }
        $.ajax({
            url: "mods/mod_products/ajax/get_colors_components.php",
            type: "post",
            data: data,
            success: function (response) {
                $('#modal_product_color').find('.modal-body').html(response).css('overflow', 'scroll');
                ;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });

    }

    var line_color_resumen = "";

    function change_color_component_resumen() {
        line_color_resumen = $(this);
        $('#modal_product_color').modal('show').css('overflow', 'scroll');
        ;
        select_tr = $(this).closest('.line_product_component_aux');
        var id_product = $(this).closest('.line_product_component_aux').find('.fami_color').val();
        var data = {
            id_product: id_product
        }
        $.ajax({
            url: "mods/mod_products/ajax/get_colors_components.php",
            type: "post",
            data: data,
            success: function (response) {
                $('#modal_product_color').find('.modal-body').html(response).css('overflow', 'scroll');
                ;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function group_colors_components() {
        var group_color = $(this).parent().parent().attr('group_color');
        var arfami = $(select_tr).find('.id_component_product').val();
        var ararti = $(select_tr).find('.ararti').val();
        var arsubf = $(select_tr).find('.arsubf').val();
        var arprpr = $(select_tr).find('.arprpr').val();
        var tipo_medida = $(select_tr).find('.type_medida_component').val();
        var is_resumen_pedido = 0;
        if (!isNaN($(select_tr).find('.type_product').val())) {
            value_type_product = $(select_tr).find('.type_product').val();
            is_resumen_pedido = 1;
            var arfami = $(line_color_resumen).closest('.line_component').find('.id_component_product').val();
            var ararti = $(line_color_resumen).closest('.line_component').find('.ararti').val();
            var arsubf = $(line_color_resumen).closest('.line_component').find('.arsubf').val();
            var arprpr = $(line_color_resumen).closest('.line_component').find('.arprpr').val();
            var tipo_medida = $(line_color_resumen).closest('.line_component').find('.type_medida_component').val();
        }

        if ($(select_tr).find('.linea_medida_componente_check').length > 0) {

            var total_metros_precio = 0.00;

            $(select_tr).find('.linea_medida_componente_check').each(function (index, element) {
                if (tipo_medida == 2) {
                    var metros = $(element).find('.linea_medida_componente_check_metros').html();
                    var unidades = $(element).find('.linea_medida_componente_check_units').html();
                    metros = metros.replace(/,/g, ".");
                    metros = parseFloat(metros);
                    metros = metros.toFixed(2);
                    var precios_metros = (Math.ceil(metros * 20) / 20).toFixed(2);
                    total_metros_precio += parseFloat(precios_metros) * unidades;
                } else {

                }
            })
        }

        var data = {
            'value_type_product': value_type_product,
            'group_color': group_color,
            'arfami': arfami,
            'arsubf': arsubf,
            'ararti': ararti,
            'arprpr': arprpr
        }
        $.ajax({
            url: "mods/mod_products/ajax/change_color_component.php",
            type: "post",
            data: data,
            success: function (response) {

                if (is_resumen_pedido == "1") {
                    if (!isNaN(total_metros_precio)) {
                        $(line_color_resumen).closest('.line_component').find('.total_units_metros').val(total_metros_precio);
                    }
                    var precio_total = "";
                    var precio = parseFloat(response).toFixed(2);
                    var units = $(line_color_resumen).closest('.line_component').find('.total_units_metros').val();
                    if (units > 0) {
                        precio_total = units * parseFloat(precio);
                        precio_total = precio_total.toFixed(2);
                    }
                    $(line_color_resumen).closest('.line_component').find('.precio_component').html(precio);
                    $(line_color_resumen).closest('.line_component').find('.precio_component').next().val(precio);
                    $(line_color_resumen).closest('.line_component').find('.total_component').html(precio_total);
                } else {
                    if (!isNaN(total_metros_precio)) {
                        $(select_tr).find('.total_units_metros').val(total_metros_precio);
                    }
                    var precio_total = "";
                    var precio = parseFloat(response).toFixed(2);
                    var units = $(select_tr).find('.total_units_metros').val();
                    if (units > 0) {
                        precio_total = units * parseFloat(precio);
                        precio_total = precio_total.toFixed(2);
                    }

                    $(select_tr).find('.precio_component').html(precio);
                    $(select_tr).find('.precio_component').next().val(precio);
                    $(select_tr).find('.total_component').html(precio_total);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
        //Aqui desde resumen de pedido
        if (is_resumen_pedido == "1") {
            if ($(this).hasClass('is_image')) {
                var url = $(this).find('.img_color_product').attr('src');
                $(line_color_resumen).find('.img_color_component').removeClass('hidden');
                $(line_color_resumen).find('.img_color_component').attr('src', url);
            } else {
                $(line_color_resumen).find('.img_color_component').addClass('hidden');
            }
            var color = $(this).css('background-color');
            $(line_color_resumen).css('background-color', color);
            var color_product = $(this).parent().attr('color_product');
            $(line_color_resumen).parent().find('.components_color').val(color_product);
            $(line_color_resumen).closest('.line_component').find('.ararti').val(color_product);
            var title_color = $(this).next().html();
            $(line_color_resumen).parent().find('.title_color_component').html(title_color);
            $('#modal_product_color').modal('hide');
            $('#modal_add_product').css('overflow', 'scroll');
        } else {
            if ($(this).hasClass('is_image')) {
                var url = $(this).find('.img_color_product').attr('src');
                $(select_tr).find('.img_color_component').removeClass('hidden');
                $(select_tr).find('.img_color_component').attr('src', url);
            } else {
                $(select_tr).find('.img_color_component').addClass('hidden');
            }
            var color = $(this).css('background-color');
            $(select_tr).find('.change_color_component').css('background-color', color);
            var color_product = $(this).parent().attr('color_product');
            $(select_tr).find('.components_color').val(color_product);
            var title_color = $(this).next().html();
            $(select_tr).find('.title_color_component').html(title_color);
            $('#modal_product_color').modal('hide');
            $('#modal_add_product').css('overflow', 'scroll');
        }
    }

    function select_color_product() {
        if ($(this).hasClass('is_corredera')) {
            $(this).parent().click();
            if ($(this).hasClass('is_image')) {
                var url = $(this).find('.img_color_product').attr('src');
                $(select_tr).find('.img_color_product').removeClass('hidden');
                $(select_tr).find('.img_color_product').attr('src', url);
            } else {
                $(select_tr).find('.img_color_product').addClass('hidden');
            }
            var color = $(this).css('background-color');
            $(select_tr).find('.color_product_line').css('background-color', color);
            var color_product = $(this).parent().attr('color_product');
            $(select_tr).find('.input_color_product').attr('span_value', color_product);
            var group_color = $(this).parent().parent().attr('group_color');
            $(select_tr).find('.input_group_color').attr('span_value', group_color);
            var id_product = $(select_tr).find('.input_id_product').attr('span_value');
            var title_color = $(this).next().html();
            $(select_tr).find('.title_color').html(title_color);
            $(select_tr).find('.color_product_line').attr('color_ral', color_product);
            var cantidad = $(select_tr).find('.input_unidades').val();
            var ancho = $(select_tr).find('.input_ancho').val();
            var alto = $(select_tr).find('.input_alto').val();
            var tipo_producto = $(select_tr).find('.tipo_producto').val();
            calc_prices_corredera(select_tr, cantidad, ancho, alto, group_color, color, tipo_producto, '0');
            //calc_price(select_tr);
            change_color_session_ral(color_product_old, color_product, cantidad, id_product);
            $('#modal_product_color').modal('hide');
            $('#modal_add_product').css('overflow', 'scroll');
            //save_cart_session();
        } else {
            $(this).parent().click();
            if ($(this).hasClass('is_image')) {
                var url = $(this).find('.img_color_product').attr('src');
                $(select_tr).find('.img_color_product').removeClass('hidden');
                $(select_tr).find('.img_color_product').attr('src', url);
            } else {
                $(select_tr).find('.img_color_product').addClass('hidden');
            }
            var color = $(this).css('background-color');
            $(select_tr).find('.color_product_line').css('background-color', color);
            var color_product = $(this).parent().attr('color_product');
            $(select_tr).find('.input_color_product').attr('span_value', color_product);
            var group_color = $(this).parent().parent().attr('group_color');
            $(select_tr).find('.input_group_color').attr('span_value', group_color);
            var id_product = $(select_tr).find('.input_id_product').attr('span_value');
            var title_color = $(this).next().html();
            $(select_tr).find('.title_color').html(title_color);
            $(select_tr).find('.color_product_line').attr('color_ral', color_product);
            calc_price(select_tr);
            change_color_session_ral(color_product_old, color_product, units_product_old, id_product);
            $('#modal_product_color').modal('hide');
            $('#modal_add_product').css('overflow', 'scroll');
            save_cart_session();
        }
    }

    var value_type_product = 0;

    function show_components() {
        value_type_product = $(this).val();
        var id_product = $('.input_id_product').val();
        var data = {
            id_product: id_product
        }
        $('#modal_add_product').css('overflow', 'scroll');
        $(this).closest('.panel-default').find('.panel-heading').addClass('selected');
        if ($(this).val() == 1) {
            type_precio = "fabrica";
            $('.div_submit_product').addClass('hidden');
            $('.title_versiones').html('Componentes');
            $('.producto_acabado').show();
            $('.div_color_acabado').show();
            $('.div_color_acabado').addClass('hidden');
            $.ajax({
                url: "mods/mod_products/ajax/get_components_product.php",
                type: "post",
                data: data,
                beforeSend: function () {
                    $('#modal_loading').modal('show');
                },
                success: function (response) {
                    $('.info_insert').html(response);
                    magnific_image();
                    $('#modal_loading').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                    $('#modal_loading').modal('hide');
                }

            });
        } else if ($(this).val() == 2) {
            $('.title_versiones').html('Versiones');
            var url = path_images + "images_product/" + id_product + "/2.png";
            var url_big = path_images + "images_product/" + id_product + "/2_big.png";
            $('.preview_img').css("background-image", "url(" + url + ")");
            $('.lupa_div_content_preview_img').attr('href', url_big);
            $('.producto_acabado').show();
            $('.div_color_acabado').show();
            $('.div_color_acabado').removeClass('hidden');
            $('.boton_componentes_flotante_add').addClass('hidden');
            $.ajax({
                url: "mods/mod_products/ajax/show_info_product.php",
                type: "post",
                data: data,
                beforeSend: function () {
                    $('#modal_loading').modal('show');
                },
                success: function (response) {
                    $('.info_insert').html(response);
                    $('html, body').animate({
                        scrollTop: $(".info_insert").offset().top
                    }, 1400);
                    $('.div_all_product_modal').find('.icono-lupa').addClass('hidden');
                    $('.div_all_product_modal').find('.text_lupa').addClass('hidden');
                    magnific_image();
                    $('#modal_loading').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                    $('#modal_loading').modal('hide');
                }

            });
        } else {
            type_precio = "almacen";
            $('.div_submit_product').addClass('hidden');
            $('.title_versiones').html('Venta industrial');
            $('.producto_acabado').show();
            $('.div_color_acabado').show();
            $('.div_color_acabado').addClass('hidden');
            $.ajax({
                url: "mods/mod_products/ajax/get_components_product_industrial.php",
                type: "post",
                data: data,
                beforeSend: function () {
                    $('#modal_loading').modal('show');
                },
                success: function (response) {
                    $('.info_insert').html(response);
                    $('html, body').animate({
                        scrollTop: $(".info_insert").offset().top
                    }, 1400);
                    $('.div_all_product_modal').find('.icono-lupa').addClass('hidden');
                    $('.div_all_product_modal').find('.text_lupa').addClass('hidden');
                    magnific_image();
                    $('#modal_loading').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                    $('#modal_loading').modal('hide');
                }

            });
        }
    }

    function update_price_increment() {
        if ($(this).val() > 0) {
            $(this).removeClass('border-warning');
        } else {
            $(this).addClass('border-warning');
        }
        var unit_max_increment = $(this).closest('.line_product').find('.input_unit_product').val();
        $(this).attr('max', unit_max_increment);
        var product_price_fixed = $(this).closest('.row').find('.product_price_fixed').html();
        var total_price = product_price_fixed * $(this).val();
        total_price = parseFloat(total_price).toFixed(2);
        $(this).closest('.row').find('.product_price').html(total_price);
        var total = 0;
        $('.product_price').each(function () {
            if ($.isNumeric($(this).html())) {
                total += parseFloat($(this).html());
            }
        })
        $('.product_price_lacado').each(function () {
            total += parseFloat($(this).html());
        })
        total = total.toFixed(2);
        $('.price_total').val(total);

        var color_product = $(this).closest('.line_product').find('.input_color_product').attr('span_value');
        var id_product = $(this).closest('.line_product').find('.input_id_product').attr('span_value');
        update_session_ral(color_product, id_product);

        save_cart_session();
    }

    $('.delete_increment').confirmation({
        rootSelector: '.delete_increment',
        title: mensaje_estas_seguro,
        btnOkLabel: texto_si,
        btnCancelLabel: texto_no,
        onConfirm: function () {
            $(this).closest('.row_option_product').next().remove();
            $(this).closest('.row_option_product').remove();
            var total = 0;
            $('.product_price').each(function () {
                if ($.isNumeric($(this).html())) {
                    total += parseFloat($(this).html());
                }
            })
            $('.product_price_lacado').each(function () {
                total += parseFloat($(this).html());
            })
            var color_product = $(this).closest('.line_product').find('.input_color_product').attr('span_value');
            var id_product = $(this).closest('.line_product').find('.input_id_product').attr('span_value');
            update_session_ral(color_product, id_product);
            total = total.toFixed(2);
            $('.price_total').val(total);

            save_cart_session();
        }
    });

    function save_cart_session() {
        var number_tr = $('.content_lineas_pedido').find('.line_product');

        var data = [];
        $(number_tr).each(function (index, element) {
            if ($(element).find('.line_product_component').length) {
                $('.line_product_component').each(function (index, element) {
                    var tipo_medida_componente = $(element).find('.tipo_medida_component').val();
                    var unit_product = $(element).find('.unidades_unidades_medidas').val();
                    var arfami = $(element).find('.arfami').val();
                    //ararti = color_product
                    var arsubf = $(element).find('.arsubf').val();
                    var ararti = $(element).find('.ararti').val();
                    var price = $(element).find('.price_component_hidden').val();
                    var arprpr = $(element).find('.arprpr').val();
                    var is_almacen = $(element).find('.is_almacen').val();
                    if (tipo_medida_componente == 3) {
                        var array_product = {
                            id_product: arfami,
                            color_product: ararti,
                            subfamily_product: arsubf,
                            unit_product: unit_product,
                            width_product: '0',
                            height_product: '0',
                            observaciones_product: 'is_component',
                            product_price: price,
                            is_almacen: is_almacen,
                            arprpr: arprpr,
                            tipo_medida_componente: tipo_medida_componente,
                            type_product: 'component'
                        };
                        data.push(array_product);
                    } else {
                        if (tipo_medida_componente == 1) {
                            $(element).find('.segunda_linea_producto').each(function (index, element) {
                                var unidades = $(element).find('.unidades_unidades_medidas').val();
                                var ancho = $(element).find('.ancho_unidades_medidas').val();
                                var alto = $(element).find('.alto_unidades_medidas').val();
                                var precio_unit = $(element).find('.precio_unidades_medidas').val();
                                var is_almacen = $(element).find('.is_almacen').val();
                                var array_product = {
                                    id_product: arfami,
                                    color_product: ararti,
                                    subfamily_product: arsubf,
                                    unit_product: unidades,
                                    width_product: ancho,
                                    height_product: alto,
                                    observaciones_product: 'is_component_unit',
                                    product_price: precio_unit,
                                    is_almacen: is_almacen,
                                    tipo_medida_componente: tipo_medida_componente,
                                    type_product: 'component_unit'
                                };
                                data.push(array_product);
                            });
                        } else {
                            $(element).find('.segunda_linea_producto').each(function (index, element) {
                                var metros_unit = $(element).find('.metros_unidades_medidas').val();
                                var unidades = $(element).find('.unidades_unidades_medidas').val();
                                metros_unit = parseFloat(metros_unit);
                                metros_unit = metros_unit.toFixed(3);
                                var precio_unit = $(element).find('.precio_unidades_medidas').val();
                                var is_almacen = $(element).find('.is_almacen').val();

                                var array_product = {
                                    id_product: arfami,
                                    color_product: ararti,
                                    subfamily_product: arsubf,
                                    unit_product: unidades,
                                    width_product: metros_unit,
                                    height_product: '0',
                                    observaciones_product: 'is_component_meter',
                                    product_price: precio_unit,
                                    is_almacen: is_almacen,
                                    tipo_medida_componente: tipo_medida_componente,
                                    type_product: 'component_meter'
                                };
                                data.push(array_product);
                            });
                        }
                    }
                })
            } else {
                var option_product = $(element).find('.row_option_product');
                var id_product = $(element).find('.input_id_product').attr('span_value');
                if (id_product == "2735") {

                    $(element).find('.form-horizontal').each(function (index_corredera, element_corredera) {
                        var unit_product = $(element_corredera).find('.input_unidades').val();
                        var width_product = $(element_corredera).find('.input_ancho').val();
                        var height_product = $(element_corredera).find('.input_alto').val();
                        var product_price = $(element_corredera).find('.product_price').attr('span_value');
                        var subfamily_product = $(element_corredera).find('.subfamily_product').val();
                        var color_product = $(element_corredera).find('.input_color_product').attr('span_value');
                        var group_color = $(element_corredera).find('.input_group_color').attr('span_value');
                        if ($(element_corredera).hasClass('corredera_completa')) {

                            var array_product_hojas = {
                                type_product: 'product',
                                id_product: id_product,
                                color_product: color_product,
                                group_color: group_color,
                                subfamily_product: '1',
                                tipo_corredera: 'hojas_corredera_completa',
                                unit_product: $(element_corredera).find('.cantidad_hojas').val(),
                                width_product: $(element_corredera).find('.ancho_hojas').val(),
                                height_product: height_product,
                                product_price: $(element_corredera).find('.precio_hoja').val()
                            };

                            var array_product = {
                                type_product: 'product',
                                tipo_corredera: $(element_corredera).find('.tipo_producto').val(),
                                id_product: id_product,
                                color_product: color_product,
                                group_color: group_color,
                                subfamily_product: subfamily_product,
                                unit_product: unit_product,
                                width_product: width_product,
                                height_product: height_product,
                                product_price: $(element_corredera).find('.precio_marco').val()
                            };
                            if (unit_product > 0) {
                                data.push(array_linea_3);
                                data.push(array_product_hojas);
                                data.push(array_product);
                                data.push(array_linea_3);
                            }

                        } else {
                            var array_product = {
                                type_product: 'product',
                                id_product: id_product,
                                color_product: color_product,
                                group_color: group_color,
                                subfamily_product: subfamily_product,
                                unit_product: unit_product,
                                width_product: width_product,
                                height_product: height_product,
                                product_price: product_price,
                                tipo_corredera: $(element_corredera).find('.tipo_producto').val(),

                            };
                            if (unit_product > 0) {
                                data.push(array_product);
                            }
                        }
                    });

                    if ($(option_product).length > 0) {

                        $(option_product).each(function (index_com, element_com) {
                            var input_unit_product_component = $(element_com).find('.unit_increment').val();
                            var product_price_component = $(element_com).find('.product_price').html();
                            var id_component = $(element_com).find('.id_component').val();
                            var title_adjunto = $(element_com).next().find('.name_fichero_upload_hidden').html();
                            var array_component = {
                                id_product: id_product,
                                input_unit_product_component: input_unit_product_component,
                                product_price_component: product_price_component,
                                id_component: id_component,
                                adjunto: title_adjunto
                            };
                            data.push(array_component);
                        });
                    }
                    data.push(array_product);
                    if ($(option_product).length > 0) {
                        data.push(array_linea_3);
                    }

                } else {
                    var color_product = $(element).find('.input_color_product').attr('span_value');
                    var group_color = $(element).find('.input_group_color').attr('span_value');
                    var subfamily_product = $(element).find('.input_subfamily_product').val();
                    if (id_product == "1999") {
                        var unit_product = $(element).find('.change_units_resumen_expositor').val();
                        var group_color = $(element).find('.ararti').val();
                    } else {
                        var unit_product = $(element).find('.input_unit_product').val();
                    }
                    var width_product = $(element).find('.input_width_product').val();
                    var height_product = $(element).find('.input_height_product').val();
                    var observaciones_product = $(element).find('.observaciones_product').val();
                    var product_price = $(element).find('.product_price').html();
                    var is_almacen = $(element).find('.is_almacen').val();

                    var data_increment = [];

                    if ($(option_product).length > 0) {
                        data.push(array_linea_3);
                        $(option_product).each(function (index_com, element_com) {
                            var input_unit_product_component = $(element_com).find('.unit_increment').val();
                            var product_price_component = $(element_com).find('.product_price').html();
                            var product_price_fixed = $(element_com).find('.product_price_fixed').html();
                            var id_component = $(element_com).find('.id_component').val();
                            var title_increment = $(element_com).find('.title_increment_product').html();
                            var title_adjunto = $(element_com).next().find('.name_fichero_upload_hidden').html();
                            var array_component = {
                                id_product: id_product,
                                input_unit_product_component: input_unit_product_component,
                                product_price_component: product_price_component,
                                product_price_fixed: product_price_fixed,
                                id_component: id_component,
                                title_increment: title_increment,
                                type_product: 'increment',
                                adjunto: title_adjunto
                            };
                            data_increment.push(array_component);
                        });
                    }

                    if (data_increment.length) {
                        var array_product = {
                            id_product: id_product,
                            color_product: color_product,
                            group_color: group_color,
                            subfamily_product: subfamily_product,
                            unit_product: unit_product,
                            width_product: width_product,
                            height_product: height_product,
                            observaciones_product: observaciones_product,
                            product_price: product_price,
                            is_almacen: is_almacen,
                            type_product: 'product',
                            array_increment: data_increment
                        };
                    } else {
                        var array_product = {
                            id_product: id_product,
                            color_product: color_product,
                            group_color: group_color,
                            subfamily_product: subfamily_product,
                            unit_product: unit_product,
                            width_product: width_product,
                            height_product: height_product,
                            observaciones_product: observaciones_product,
                            product_price: product_price,
                            is_almacen: is_almacen,
                            type_product: 'product'
                        };
                    }
                }

                data.push(array_product);
                if ($(option_product).length > 0) {
                    data.push(array_linea_3);
                }

                var line_lacado = $('.line_lacado_ral');
                if (line_lacado.length) {
                    $(line_lacado).each(function (index, element) {

                        var is_lacado = "is_lacado";
                        var color_lacado = $(element).find('.color_lacado').html();
                        var unit_lacado = $(element).find('.unit_lacado').html();
                        var product_price_lacado = $(element).find('.product_price_lacado').html();
                        var array_lacado = {
                            is_lacado: is_lacado,
                            color_lacado: color_lacado,
                            unit_lacado: unit_lacado,
                            product_price_lacado: product_price_lacado
                        }
                        data.push(array_lacado);
                    })
                }

            }
        });

        $.ajax({
            url: "mods/mod_products/ajax/save_cart_session.php",
            type: "post",
            data: {data: data},
            success: function (response) {
                $('.icon-carrito').removeClass('hidden');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function modal_login() {
        if (check_login() == 0) {
            $('#modal_ini_login').modal('show');
        }
    }

    function modal_help() {
        $('#modal_help .modal-body').css('height', $(window).height() * 0.7);
        $('#modal_help').modal('show');
    }

    var btn_log = "";

    function modal_login_cliente() {
        btn_log = $(this);
        if (check_login() == 0) {
            $('#modal_ini_login').modal('show');
            $('.option_login').val("cliente");
        }
    }

    function modal_login_cliente_href(e) {
        e.preventDefault();
        btn_log = $(this);
        if (check_login() == 0) {
            $('#modal_ini_login').modal('show');
            $('.option_login').val("cliente");
        } else {
            window.location.href = $(this).attr('path')
        }
    }

    function modal_login_product() {
        $('.input_type_button').val($(this).val());
        $('.type_button_hidden').val($(this).val());
        $('.btn_flotante_generar').val($(this).val());
        $('[name="type_button"]').val($(this).val());
        if (check_login() == 0) {
            $('#modal_ini_login').modal('show');
            $('.option_login').val("product_index");
        } else {
            var id_product = $('.input_id_product').val();
            if (check_datos_submit_product(id_product)) {
                $('.form_product').submit();
            } else {
                toastr.options = {
                    "positionClass": "toast-top-full-width"
                }
                $('#modal_ini_login').modal('hide');
                toastr.error("Revise los datos del producto", "Enviar datos");
                return false;
            }
        }
    }

    function btn_modal_ini_contacto() {
        if (check_login() == 0) {
            $('#modal_ini_login').modal('show');
        }
    }

    function submit_form_login(e) {
        e.preventDefault();
        $.ajax({
            url: "mods/mod_customers/action/ini_log.php",
            type: "post",
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {
                if (response['error'] == 1) {
                    $('.div_error_login').removeClass('hidden');
                }
                if (response['error'] == -1) {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    window.location.reload();
                    //toastr.warning("Login", "Ya has iniciado sesión");
                }
                if (response['error'] == -2) {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.warning("Login", "Error desconocido. Inténtelo más tarde");
                }
                if (response['error'] == 0) {
                    window.location.reload();
                }
                if (response['error'] == 2) {
                    window.location.href = $(btn_log).attr('path');
                }
                if (response['error'] == 3) {
                    var id_product = $('.input_id_product').val();
                    if (check_datos_submit_product(id_product)) {
                        $('.form_product').submit();
                    } else {
                        toastr.options = {
                            "positionClass": "toast-top-full-width"
                        }
                        $('#modal_ini_login').modal('hide');
                        toastr.error("Revise los datos del producto", "Enviar datos");
                        return false;
                    }
                }
                if (response['error'] == 4) {
                    $('#modal_ini_login').modal('hide');
                    $('#modal_set_email').modal('show');
                    $('#modal_set_email').find('.clcodi_cliente').val(response['clcodi']);
                    $('#modal_set_email').find('.clprov_cliente').val(response['clprov']);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function modal_edit_dire() {

        var error_increment = 0;
        var check_increment = $('.content_lineas_pedido').find('.unit_increment');
        var check_adjunto = $('.content_lineas_pedido').find('.name_fichero_upload');
        $(check_increment).each(function () {
            if ($(this).val() <= 0) {
                error_increment++;
            }
        });
        $(check_adjunto).each(function () {
            if ($(this).hasClass('text-danger') && !$(this).hasClass('hidden')) {
                error_increment++;
                $(this).closest('.row').find('.btn-adjuntar-croquis').addClass('bg-red');
            }
        });
        var check_size = $('.content_lineas_pedido').find('.check_size');
        var error_size = 0;
        $(check_size).each(function () {
            if ($(this).hasClass('border-warning')) {
                error_size++;
            }
            if ($(this).val() <= 0) {
                error_size++;
            }
        });

        if (error_size > 0 || error_increment > 0) {
            toastr.options = {
                "positionClass": "toast-top-full-width"
            }
            toastr.error("Medidas", "Revise las medidas o incrementos");
        } else {

            var data = {
                clproc: $('.clproc_cliente').val(),
                clcodi: $('.clcodi_cliente').val(),
                id_ire: $('.id_dire').val()
            }

            $.ajax({
                url: "mods/mod_customers/ajax/get_dire_envio.php",
                type: "post",
                data: data,
                success: function (response) {
                    $('#modal_edit_dire_envio').find('.modal-body').html(response);
                    $('#modal_edit_dire_envio').modal('show');
                    if (typeof render_button_paypal === "function") {
                        render_button_paypal();
                        $('.amount_redsys').val($('.price_total').val());
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }

            });
        }
    }

    function act_dire_entre(e) {
        e.preventDefault();

        var error_form = 0;
        var save_dire = 0;
        save_dire = $(this).find('.is_temporal').val();

        if ($(this).find('.input_change_dir').val().length <= 0) {
            error_form = 1;
        }
        if ($(this).find('.select_change_pais').val() <= 0) {
            error_form = 1;
        }
        if ($(this).find('.select_change_provincias').val() <= 0) {
            error_form = 1;
        }
        if ($(this).find('.select_change_poblacion').val() <= 0) {
            error_form = 1;
        }

        if (error_form == 1) {
            toastr.options = {
                "positionClass": "toast-top-full-width"
            }
            toastr.error("Dirección", "Error. Revise los campos.");
        } else {


            $.ajax({
                url: "mods/mod_customers/ajax/add_dire_envio_pedido.php",
                type: "post",
                data: $(this).serialize(),
                dataType: 'json',
                success: function (response) {
                    var error = response['error'];
                    var message = response['message'];
                    var last_id = "";
                    if (error < 0) {
                        toastr.options = {
                            "positionClass": "toast-top-full-width"
                        }
                        toastr.error("Dirección", message);
                    }
                    if (error == 1) {
                        toastr.options = {
                            "positionClass": "toast-top-full-width"
                        }
                        toastr.warning("Dirección", message);
                    }
                    if (error == 0) {
                        toastr.options = {
                            "positionClass": "toast-top-full-width"
                        }
                        toastr.success("Dirección", message);
                        last_id = response['last_id'];
                        $('.id_dire').val(last_id);

                        var id_dir = $('.select_dir').val();

                        var number_tr = $('.content_lineas_pedido').find('.line_product');
                        var total_price = $('.price_total').val();
                        var referencia = $('.referencia').val();
                        var is_pedido = $('.is_pedido').val();
                        var agencia = $('.agencia').val();

                        var data = [];
                        $(number_tr).each(function (index, element) {
                            if ($(element).find('.line_product_component').length) {

                                $('.line_product_component').each(function (index, element) {
                                    var tipo_medida_componente = $(element).find('.tipo_medida_component').val();
                                    var unit_product = $(element).find('.unidades_unidades_medidas').val();
                                    var arfami = $(element).find('.arfami').val();
                                    //ararti = color_product
                                    var arsubf = $(element).find('.arsubf').val();
                                    var ararti = $(element).find('.ararti').val();
                                    var price = $(element).find('.price_component_hidden').val();
                                    var is_almacen = $(element).find('.is_almacen').val();
                                    if (tipo_medida_componente == 3) {
                                        var array_product = {
                                            id_product: arfami,
                                            color_product: ararti,
                                            subfamily_product: arsubf,
                                            unit_product: unit_product,
                                            width_product: '0',
                                            height_product: '0',
                                            observaciones_product: 'is_component',
                                            product_price: price,
                                            is_almacen: is_almacen
                                        };
                                        data.push(array_product);
                                    } else {
                                        if (tipo_medida_componente == 1) {
                                            $(element).find('.segunda_linea_producto').each(function (index, element) {
                                                var unidades = $(element).find('.unidades_unidades_medidas').val();
                                                var ancho = $(element).find('.ancho_unidades_medidas').val();
                                                var alto = $(element).find('.alto_unidades_medidas').val();
                                                var precio_unit = $(element).find('.precio_unidades_medidas').val();
                                                var is_almacen = $(element).find('.is_almacen').val();
                                                var array_product = {
                                                    id_product: arfami,
                                                    color_product: ararti,
                                                    subfamily_product: arsubf,
                                                    unit_product: unidades,
                                                    width_product: ancho,
                                                    height_product: alto,
                                                    observaciones_product: 'is_component_unit',
                                                    product_price: precio_unit,
                                                    is_almacen: is_almacen
                                                };
                                                data.push(array_product);
                                            });
                                        } else {
                                            $(element).find('.segunda_linea_producto').each(function (index, element) {
                                                var metros_unit = $(element).find('.metros_unidades_medidas').val();
                                                var unidades = $(element).find('.unidades_unidades_medidas').val();
                                                metros_unit = parseFloat(metros_unit);
                                                metros_unit = metros_unit.toFixed(3);
                                                var precio_unit = $(element).find('.precio_unidades_medidas').val();
                                                var is_almacen = $(element).find('.is_almacen').val();

                                                var array_product = {
                                                    id_product: arfami,
                                                    color_product: ararti,
                                                    subfamily_product: arsubf,
                                                    unit_product: unidades,
                                                    width_product: metros_unit,
                                                    height_product: '0',
                                                    observaciones_product: 'is_component_meter',
                                                    product_price: precio_unit,
                                                    is_almacen: is_almacen
                                                };
                                                data.push(array_product);
                                            });
                                        }
                                    }
                                })
                            } else {
                                var option_product = $(element).find('.row_option_product');
                                var id_product = $(element).find('.input_id_product').attr('span_value');
                                if (id_product == "2735") {
                                    $(element).find('.form-horizontal').each(function (index_corredera, element_corredera) {
                                        var unit_product = $(element_corredera).find('.input_unidades').val();
                                        var width_product = $(element_corredera).find('.input_ancho').val();
                                        var height_product = $(element_corredera).find('.input_alto').val();
                                        var product_price = $(element_corredera).find('.product_price').attr('span_value');
                                        var subfamily_product = $(element_corredera).find('.subfamily_product').val();
                                        var color_product = $(element_corredera).find('.input_color_product').attr('span_value');
                                        var group_color = $(element_corredera).find('.input_group_color').attr('span_value');
                                        if ($(element_corredera).hasClass('corredera_completa')) {
                                            var array_product = {
                                                id_product: id_product,
                                                color_product: color_product,
                                                group_color: group_color,
                                                subfamily_product: subfamily_product,
                                                unit_product: unit_product,
                                                width_product: width_product,
                                                height_product: height_product,
                                                product_price: $(element_corredera).find('.precio_marco').val()
                                            };
                                            var array_product_hojas = {
                                                id_product: id_product,
                                                color_product: color_product,
                                                group_color: group_color,
                                                subfamily_product: '1',
                                                unit_product: $(element_corredera).find('.cantidad_hojas').val(),
                                                width_product: $(element_corredera).find('.ancho_hojas').val(),
                                                height_product: height_product,
                                                product_price: $(element_corredera).find('.precio_hoja').val()
                                            };
                                            if (unit_product > 0) {
                                                data.push(array_linea_3);
                                                data.push(array_product_hojas);
                                                data.push(array_product);
                                                data.push(array_linea_3);
                                            }
                                        } else {
                                            var array_product = {
                                                id_product: id_product,
                                                color_product: color_product,
                                                group_color: group_color,
                                                subfamily_product: subfamily_product,
                                                unit_product: unit_product,
                                                width_product: width_product,
                                                height_product: height_product,
                                                product_price: product_price
                                            };
                                            if (unit_product > 0) {
                                                data.push(array_product);
                                            }
                                        }
                                    });

                                    if ($(option_product).length > 0) {

                                        $(option_product).each(function (index_com, element_com) {
                                            var input_unit_product_component = $(element_com).find('.unit_increment').val();
                                            var product_price_component = $(element_com).find('.product_price').html();
                                            var id_component = $(element_com).find('.id_component').val();
                                            var title_adjunto = $(element_com).next().find('.name_fichero_upload_hidden').html();
                                            var array_component = {
                                                id_product: id_product,
                                                input_unit_product_component: input_unit_product_component,
                                                product_price_component: product_price_component,
                                                id_component: id_component,
                                                adjunto: title_adjunto
                                            };
                                            data.push(array_component);
                                        });
                                    }
                                    data.push(array_product);
                                    if ($(option_product).length > 0) {
                                        data.push(array_linea_3);
                                    }

                                } else {
                                    var color_product = $(element).find('.input_color_product').attr('span_value');
                                    var group_color = $(element).find('.input_group_color').attr('span_value');
                                    var option_product = $(element).find('.row_option_product');
                                    var subfamily_product = $(element).find('.input_subfamily_product').val();
                                    var tipologia = $(element).find('.input_tipologia').val();
                                    if (id_product == "1999") {
                                        var unit_product = $(element).find('.change_units_resumen_expositor').val();
                                        var group_color = $(element).find('.ararti').val();
                                    } else {
                                        var unit_product = $(element).find('.input_unit_product').val();
                                    }
                                    var width_product = $(element).find('.input_width_product').val();
                                    var height_product = $(element).find('.input_height_product').val();
                                    var observaciones_product = $(element).find('.observaciones_product').val();
                                    var product_price = $(element).find('.product_price').html();
                                    var is_almacen = $(element).find('.is_almacen').val();
                                    var art_desc = $(element).find('.title_product').text();
                                    var col_desc = $(element).find('.title_color').text();
                                    var array_product = {
                                        id_product: id_product,
                                        color_product: color_product,
                                        group_color: group_color,
                                        subfamily_product: subfamily_product,
                                        unit_product: unit_product,
                                        width_product: width_product,
                                        height_product: height_product,
                                        observaciones_product: observaciones_product,
                                        product_price: product_price,
                                        tipologia: tipologia,
                                        is_almacen: is_almacen,
                                        art_desc: art_desc,
                                        col_desc: col_desc
                                    };


                                    if ($(option_product).length > 0) {

                                        data.push(array_linea_3);
                                        $(option_product).each(function (index_com, element_com) {
                                            var input_unit_product_component = $(element_com).find('.unit_increment').val();
                                            var product_price_component = $(element_com).find('.product_price').html();
                                            var id_component = $(element_com).find('.id_component').val();
                                            var title_adjunto = $(element_com).next().find('.name_fichero_upload_hidden').html();
                                            var array_component = {
                                                id_product: id_product,
                                                input_unit_product_component: input_unit_product_component,
                                                product_price_component: product_price_component,
                                                id_component: id_component,
                                                adjunto: title_adjunto
                                            };
                                            data.push(array_component);
                                        });
                                    }
                                    data.push(array_product);
                                    if ($(option_product).length > 0) {
                                        data.push(array_linea_3);
                                    }
                                }
                            }
                        });


                        var line_lacado = $('.line_lacado_ral');
                        $(line_lacado).each(function (index, element) {

                            var is_lacado = "is_lacado";
                            var color_lacado = $(element).find('.color_lacado').html();
                            var unit_lacado = $(element).find('.unit_lacado').html();
                            var product_price_lacado = $(element).find('.product_price_lacado').html();
                            var array_lacado = {
                                is_lacado: is_lacado,
                                color_lacado: color_lacado,
                                unit_lacado: unit_lacado,
                                product_price_lacado: product_price_lacado
                            }
                            data.push(array_lacado);
                        })

                        var id_dire = {
                            id_dire: $('.id_dire').val()
                        }

                        data.push(id_dire);

                        $.ajax({
                            url: "mods/mod_products/ajax/save_presupuesto.php",
                            type: "post",
                            data: {
                                data: data,
                                price: total_price,
                                referencia: referencia,
                                is_pedido: is_pedido,
                                agencia: agencia,
                                is_dire_fija: $('.select_dir').find('option:selected').attr('is_fija')
                            },
                            dataType: 'json',
                            success: function (response) {
                                response_save_presupuesto(response);

                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log("Error ajax en submitFormNewDni");
                                console.log(jqXHR, textStatus, errorThrown);
                            }

                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }

            });
        }
    }

    function form_check_agencia(e) {

        e.preventDefault();

        var id_dir = $('.select_dir').val();
        $('.id_dire').val(id_dir);

        var number_tr = $('.content_lineas_pedido').find('.line_product');
        var total_price = $('.price_total').val();
        var referencia = $('.referencia').val();
        var is_pedido = $('.is_pedido').val();
        var agencia = $('.agencia').val();

        var data = [];
        $(number_tr).each(function (index, element) {
            if ($(element).find('.line_product_component').length) {

                $('.line_product_component').each(function (index, element) {
                    var tipo_medida_componente = $(element).find('.tipo_medida_component').val();
                    var unit_product = $(element).find('.unidades_unidades_medidas').val();
                    var arfami = $(element).find('.arfami').val();
                    //ararti = color_product
                    var arsubf = $(element).find('.arsubf').val();
                    var ararti = $(element).find('.ararti').val();
                    var price = $(element).find('.price_component_hidden').val();
                    var is_almacen = $(element).find('.is_almacen').val();
                    if (tipo_medida_componente == 3) {
                        var array_product = {
                            id_product: arfami,
                            color_product: ararti,
                            subfamily_product: arsubf,
                            unit_product: unit_product,
                            width_product: '0',
                            height_product: '0',
                            observaciones_product: 'is_component',
                            product_price: price,
                            is_almacen: is_almacen
                        };
                        data.push(array_product);
                    } else {
                        if (tipo_medida_componente == 1) {
                            $(element).find('.segunda_linea_producto').each(function (index, element) {
                                var unidades = $(element).find('.unidades_unidades_medidas').val();
                                var ancho = $(element).find('.ancho_unidades_medidas').val();
                                var alto = $(element).find('.alto_unidades_medidas').val();
                                var precio_unit = $(element).find('.precio_unidades_medidas').val();
                                var is_almacen = $(element).find('.is_almacen').val();
                                var array_product = {
                                    id_product: arfami,
                                    color_product: ararti,
                                    subfamily_product: arsubf,
                                    unit_product: unidades,
                                    width_product: ancho,
                                    height_product: alto,
                                    observaciones_product: 'is_component_unit',
                                    product_price: precio_unit,
                                    is_almacen: is_almacen
                                };
                                data.push(array_product);
                            });
                        } else {
                            $(element).find('.segunda_linea_producto').each(function (index, element) {
                                var metros_unit = $(element).find('.metros_unidades_medidas').val();
                                var unidades = $(element).find('.unidades_unidades_medidas').val();
                                metros_unit = parseFloat(metros_unit);
                                metros_unit = metros_unit.toFixed(3);
                                var precio_unit = $(element).find('.precio_unidades_medidas').val();
                                var is_almacen = $(element).find('.is_almacen').val();

                                var array_product = {
                                    id_product: arfami,
                                    color_product: ararti,
                                    subfamily_product: arsubf,
                                    unit_product: unidades,
                                    width_product: metros_unit,
                                    height_product: '0',
                                    observaciones_product: 'is_component_meter',
                                    product_price: precio_unit,
                                    is_almacen: is_almacen
                                };
                                data.push(array_product);
                            });
                        }
                    }
                })
            } else {
                var option_product = $(element).find('.row_option_product');
                var id_product = $(element).find('.input_id_product').attr('span_value');
                if (id_product == "2735") {
                    $(element).find('.form-horizontal').each(function (index_corredera, element_corredera) {
                        var unit_product = $(element_corredera).find('.input_unidades').val();
                        var width_product = $(element_corredera).find('.input_ancho').val();
                        var height_product = $(element_corredera).find('.input_alto').val();
                        var product_price = $(element_corredera).find('.product_price').attr('span_value');
                        var subfamily_product = $(element_corredera).find('.subfamily_product').val();
                        var color_product = $(element_corredera).find('.input_color_product').attr('span_value');
                        var group_color = $(element_corredera).find('.input_group_color').attr('span_value');
                        if ($(element_corredera).hasClass('corredera_completa')) {

                            var array_product = {
                                id_product: id_product,
                                color_product: color_product,
                                group_color: group_color,
                                subfamily_product: subfamily_product,
                                unit_product: unit_product,
                                width_product: width_product,
                                height_product: height_product,
                                product_price: $(element_corredera).find('.precio_marco').val()
                            };
                            var array_product_hojas = {
                                id_product: id_product,
                                color_product: color_product,
                                group_color: group_color,
                                subfamily_product: '1',
                                unit_product: $(element_corredera).find('.cantidad_hojas').val(),
                                width_product: $(element_corredera).find('.ancho_hojas').val(),
                                height_product: height_product,
                                product_price: $(element_corredera).find('.precio_hoja').val()
                            };
                            if (unit_product > 0) {
                                data.push(array_linea_3);
                                data.push(array_product_hojas);
                                data.push(array_product);
                                data.push(array_linea_3);
                            }
                        } else {
                            var array_product = {
                                id_product: id_product,
                                color_product: color_product,
                                group_color: group_color,
                                subfamily_product: subfamily_product,
                                unit_product: unit_product,
                                width_product: width_product,
                                height_product: height_product,
                                product_price: product_price
                            };
                            if (unit_product > 0) {
                                data.push(array_product);
                            }
                        }

                        if ($(option_product).length > 0) {

                            $(option_product).each(function (index_com, element_com) {
                                var input_unit_product_component = $(element_com).find('.unit_increment').val();
                                var product_price_component = $(element_com).find('.product_price').html();
                                var id_component = $(element_com).find('.id_component').val();
                                var title_adjunto = $(element_com).next().find('.name_fichero_upload_hidden').html();
                                var array_component = {
                                    id_product: id_product,
                                    input_unit_product_component: input_unit_product_component,
                                    product_price_component: product_price_component,
                                    id_component: id_component,
                                    adjunto: title_adjunto
                                };
                                data.push(array_component);
                            });
                        }
                        data.push(array_product);
                        if ($(option_product).length > 0) {
                            data.push(array_linea_3);
                        }
                    });

                } else {
                    var color_product = $(element).find('.input_color_product').attr('span_value');
                    var group_color = $(element).find('.input_group_color').attr('span_value');
                    var option_product = $(element).find('.row_option_product');
                    var color_product = $(element).find('.input_color_product').attr('span_value');
                    var group_color = $(element).find('.input_group_color').attr('span_value');
                    var subfamily_product = $(element).find('.input_subfamily_product').val();
                    var tipologia = $(element).find('.input_tipologia').val();
                    if (id_product == "1999") {
                        var unit_product = $(element).find('.change_units_resumen_expositor').val();
                        var group_color = $(element).find('.ararti').val();
                    } else {
                        var unit_product = $(element).find('.input_unit_product').val();
                    }
                    var width_product = $(element).find('.input_width_product').val();
                    var height_product = $(element).find('.input_height_product').val();
                    var observaciones_product = $(element).find('.observaciones_product').val();
                    var product_price = $(element).find('.product_price').html();
                    var is_almacen = $(element).find('.is_almacen').val();
                    var art_desc = $(element).find('.title_product').text();
                    var col_desc = $(element).find('.title_color').text();
                    var array_product = {
                        id_product: id_product,
                        color_product: color_product,
                        group_color: group_color,
                        subfamily_product: subfamily_product,
                        unit_product: unit_product,
                        width_product: width_product,
                        height_product: height_product,
                        observaciones_product: observaciones_product,
                        product_price: product_price,
                        tipologia: tipologia,
                        is_almacen: is_almacen,
                        art_desc: art_desc,
                        col_desc: col_desc
                    };

                    if ($(option_product).length > 0) {

                        data.push(array_linea_3);
                        $(option_product).each(function (index_com, element_com) {
                            var input_unit_product_component = $(element_com).find('.unit_increment').val();
                            var product_price_component = $(element_com).find('.product_price').html();
                            var id_component = $(element_com).find('.id_component').val();
                            var title_adjunto = $(element_com).next().find('.name_fichero_upload_hidden').html();
                            var array_component = {
                                id_product: id_product,
                                input_unit_product_component: input_unit_product_component,
                                product_price_component: product_price_component,
                                id_component: id_component,
                                title_adjunto: title_adjunto
                            };
                            data.push(array_component);
                        });
                    }
                    data.push(array_product);
                    if ($(option_product).length > 0) {
                        data.push(array_linea_3);
                    }
                }
            }
        });


        var line_lacado = $('.line_lacado_ral');
        $(line_lacado).each(function (index, element) {

            var is_lacado = "is_lacado";
            var color_lacado = $(element).find('.color_lacado').html();
            var unit_lacado = $(element).find('.unit_lacado').html();
            var product_price_lacado = $(element).find('.product_price_lacado').html();
            var array_lacado = {
                is_lacado: is_lacado,
                color_lacado: color_lacado,
                unit_lacado: unit_lacado,
                product_price_lacado: product_price_lacado
            }
            data.push(array_lacado);
        })

        var id_dire = {
            id_dire: $('.id_dire').val()
        }

        data.push(id_dire);

        $.ajax({
            url: "mods/mod_products/ajax/save_presupuesto.php",
            type: "post",
            data: {
                data: data,
                price: total_price,
                referencia: referencia,
                is_pedido: is_pedido,
                agencia: agencia,
                is_dire_fija: $('.select_dir').find('option:selected').attr('is_fija')
            },
            dataType: 'json',
            success: function (response) {
                response_save_presupuesto(response);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    var boton_editar_notas = "";
    var id_linea_nota = "";

    function btn_ver_notas() {
        boton_editar_notas = $(this);
        id_linea_nota = $(this).closest('.file_historial').find('.id_pres').val();
        var data = {
            id_linea_nota: id_linea_nota
        }
        $.ajax({
            url: "mods/mod_products/ajax/get_observaciones.php",
            type: "post",
            data: data,
            success: function (response) {
                $('.editor_text').summernote('code', response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function save_observaciones() {
        var data = {
            obs: $(this).closest('.modal-body').find('.editor_text').summernote('code'),
            id_linea_nota: id_linea_nota
        }

        $.ajax({
            url: "mods/mod_products/ajax/save_observaciones.php",
            type: "post",
            data: data,
            dataType: 'json',
            success: function (response) {
                var message = response['message'];
                var error = response['error'];
                if (error < 0) {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.error("Observaciones", message);
                }
                if (error == 1) {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.warning("Observaciones", message);
                }
                if (error == 0) {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.success("Observaciones", message);
                    $(boton_editar_notas).html('').removeClass('bg-border-success');
                    $(boton_editar_notas).html('<i class="fa fa-eye"></i>&nbsp;Ver').addClass('bg-border-info');
                }
                $('#modal_edit_notas').modal('hide');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function search_presupuestos() {

        var data = {
            text: $(this).val(),
            id_cliente: $('.id_cliente').val()
        }

        if ($(this).val().length > 1) {

            $.ajax({
                url: "mods/mod_customers/ajax/search_presupuestos.php",
                type: "post",
                data: data,
                success: function (response) {
                    $('.content_table_historial_search').html(response).removeClass('hidden');
                    $('.content_table_historial').addClass('hidden');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }

            });
        } else {
            $('.content_table_historial_search').addClass('hidden');
            $('.content_table_historial').removeClass('hidden');
        }
    }

    function select_dir() {
        var data = {
            id_dire: $(this).val(),
            clproc: $('.clproc_cliente').val(),
            clcodi: $('.clcodi_cliente').val(),
            is_fija: $(this).find('option:selected').attr('is_fija')
        }
        var val = $(this).val();

        if (val == "otra_dire") {
            $('.act_dire_entre').addClass('hidden');
            $('.add_dire_entre').removeClass('hidden');
            $('.message_info_add_dire').removeClass('hidden');
        } else {

            $.ajax({
                url: "mods/mod_customers/ajax/select_dire_envio.php",
                type: "post",
                data: data,
                success: function (response) {
                    $('.ver_dire').html(response);
                    $('.id_dire').val(val);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }

            });
        }
    }

    function btn_select_dire() {
        var id_dir = $('.select_dir').val();
        $('.id_dire').val(id_dir);

        var number_tr = $('.content_lineas_pedido').find('.line_product');
        var total_price = $('.price_total').val();
        var referencia = $('.referencia').val();
        var is_pedido = $('.is_pedido').val();
        var agencia = $('.agencia').val();

        var data = [];
        $(number_tr).each(function (index, element) {
            if ($(element).find('.line_product_component').length) {

                $('.line_product_component').each(function (index, element) {
                    var tipo_medida_componente = $(element).find('.tipo_medida_component').val();
                    var unit_product = $(element).find('.unidades_unidades_medidas').val();
                    var arfami = $(element).find('.arfami').val();
                    //ararti = color_product
                    var arsubf = $(element).find('.arsubf').val();
                    var ararti = $(element).find('.ararti').val();
                    var price = $(element).find('.price_component_hidden').val();
                    var is_almacen = $(element).find('.is_almacen').val();
                    if (tipo_medida_componente == 3) {
                        var array_product = {
                            id_product: arfami,
                            color_product: ararti,
                            subfamily_product: arsubf,
                            unit_product: unit_product,
                            width_product: '0',
                            height_product: '0',
                            observaciones_product: 'is_component',
                            product_price: price,
                            is_almacen: is_almacen
                        };
                        data.push(array_product);
                    } else {
                        if (tipo_medida_componente == 1) {
                            $(element).find('.segunda_linea_producto').each(function (index, element) {
                                var unidades = $(element).find('.unidades_unidades_medidas').val();
                                var ancho = $(element).find('.ancho_unidades_medidas').val();
                                var alto = $(element).find('.alto_unidades_medidas').val();
                                var precio_unit = $(element).find('.precio_unidades_medidas').val();
                                var is_almacen = $(element).find('.is_almacen').val();
                                var array_product = {
                                    id_product: arfami,
                                    color_product: ararti,
                                    subfamily_product: arsubf,
                                    unit_product: unidades,
                                    width_product: ancho,
                                    height_product: alto,
                                    observaciones_product: 'is_component_unit',
                                    product_price: precio_unit,
                                    is_almacen: is_almacen
                                };
                                data.push(array_product);
                            });
                        } else {
                            $(element).find('.segunda_linea_producto').each(function (index, element) {
                                var metros_unit = $(element).find('.metros_unidades_medidas').val();
                                var unidades = $(element).find('.unidades_unidades_medidas').val();
                                metros_unit = parseFloat(metros_unit);
                                metros_unit = metros_unit.toFixed(3);
                                var precio_unit = $(element).find('.precio_unidades_medidas').val();
                                var is_almacen = $(element).find('.is_almacen').val();

                                var array_product = {
                                    id_product: arfami,
                                    color_product: ararti,
                                    subfamily_product: arsubf,
                                    unit_product: unidades,
                                    width_product: metros_unit,
                                    height_product: '0',
                                    observaciones_product: 'is_component_meter',
                                    product_price: precio_unit,
                                    is_almacen: is_almacen
                                };
                                data.push(array_product);
                            });
                        }
                    }
                })
            } else {
                var option_product = $(element).find('.row_option_product');
                var id_product = $(element).find('.input_id_product').attr('span_value');
                if (id_product == "2735") {
                    $(element).find('.form-horizontal').each(function (index_corredera, element_corredera) {
                        var unit_product = $(element_corredera).find('.input_unidades').val();
                        var width_product = $(element_corredera).find('.input_ancho').val();
                        var height_product = $(element_corredera).find('.input_alto').val();
                        var product_price = $(element_corredera).find('.product_price').attr('span_value');
                        var subfamily_product = $(element_corredera).find('.subfamily_product').val();
                        var color_product = $(element_corredera).find('.input_color_product').attr('span_value');
                        var group_color = $(element_corredera).find('.input_group_color').attr('span_value');
                        if ($(element_corredera).hasClass('corredera_completa')) {
                            var array_product = {
                                id_product: id_product,
                                color_product: color_product,
                                group_color: group_color,
                                subfamily_product: subfamily_product,
                                unit_product: unit_product,
                                width_product: width_product,
                                height_product: height_product,
                                product_price: $(element_corredera).find('.precio_marco').val()
                            };
                            var array_product_hojas = {
                                id_product: id_product,
                                color_product: color_product,
                                group_color: group_color,
                                subfamily_product: '1',
                                unit_product: $(element_corredera).find('.cantidad_hojas').val(),
                                width_product: $(element_corredera).find('.ancho_hojas').val(),
                                height_product: height_product,
                                product_price: $(element_corredera).find('.precio_hoja').val()
                            };
                            if (unit_product > 0) {
                                data.push(array_linea_3);
                                data.push(array_product_hojas);
                                data.push(array_product);
                                data.push(array_linea_3);
                            }
                        } else {
                            var array_product = {
                                id_product: id_product,
                                color_product: color_product,
                                group_color: group_color,
                                subfamily_product: subfamily_product,
                                unit_product: unit_product,
                                width_product: width_product,
                                height_product: height_product,
                                product_price: product_price
                            };
                            if (unit_product > 0) {
                                data.push(array_product);
                            }
                        }
                    });


                    if ($(option_product).length > 0) {

                        $(option_product).each(function (index_com, element_com) {
                            var input_unit_product_component = $(element_com).find('.unit_increment').val();
                            var product_price_component = $(element_com).find('.product_price').html();
                            var id_component = $(element_com).find('.id_component').val();
                            var title_adjunto = $(element_com).next().find('.name_fichero_upload_hidden').html();
                            var array_component = {
                                id_product: id_product,
                                input_unit_product_component: input_unit_product_component,
                                product_price_component: product_price_component,
                                id_component: id_component,
                                adjunto: title_adjunto
                            };
                            data.push(array_component);
                        });
                    }
                    data.push(array_product);
                    if ($(option_product).length > 0) {
                        data.push(array_linea_3);
                    }

                } else {
                    var color_product = $(element).find('.input_color_product').attr('span_value');
                    var group_color = $(element).find('.input_group_color').attr('span_value');
                    var option_product = $(element).find('.row_option_product');
                    var subfamily_product = $(element).find('.input_subfamily_product').val();
                    var tipologia = $(element).find('.input_tipologia').val();
                    if (id_product == "1999") {
                        var unit_product = $(element).find('.change_units_resumen_expositor').val();
                        var group_color = $(element).find('.ararti').val();
                    } else {
                        var unit_product = $(element).find('.input_unit_product').val();
                    }
                    var width_product = $(element).find('.input_width_product').val();
                    var height_product = $(element).find('.input_height_product').val();
                    var observaciones_product = $(element).find('.observaciones_product').val();
                    var product_price = $(element).find('.product_price').html();
                    var is_almacen = $(element).find('.is_almacen').val();
                    var art_desc = $(element).find('.title_product').text();
                    var col_desc = $(element).find('.title_color').text();
                    var array_product = {
                        id_product: id_product,
                        color_product: color_product,
                        group_color: group_color,
                        subfamily_product: subfamily_product,
                        unit_product: unit_product,
                        width_product: width_product,
                        height_product: height_product,
                        observaciones_product: observaciones_product,
                        product_price: product_price,
                        tipologia: tipologia,
                        is_almacen: is_almacen,
                        art_desc: art_desc,
                        col_desc: col_desc
                    };

                    if ($(option_product).length > 0) {
                        data.push(array_linea_3);
                        $(option_product).each(function (index_com, element_com) {
                            var input_unit_product_component = $(element_com).find('.unit_increment').val();
                            var product_price_component = $(element_com).find('.product_price').html();
                            var id_component = $(element_com).find('.id_component').val();
                            var title_adjunto = $(element_com).next().find('.name_fichero_upload_hidden').html();
                            var array_component = {
                                id_product: id_product,
                                input_unit_product_component: input_unit_product_component,
                                product_price_component: product_price_component,
                                id_component: id_component,
                                adjunto: title_adjunto
                            };
                            data.push(array_component);
                        });
                    }
                    data.push(array_product);
                    if ($(option_product).length > 0) {
                        data.push(array_linea_3);
                    }
                }
            }
        });


        var line_lacado = $('.line_lacado_ral');
        $(line_lacado).each(function (index, element) {

            var is_lacado = "is_lacado";
            var color_lacado = $(element).find('.color_lacado').html();
            var unit_lacado = $(element).find('.unit_lacado').html();
            var product_price_lacado = $(element).find('.product_price_lacado').html();
            var array_lacado = {
                is_lacado: is_lacado,
                color_lacado: color_lacado,
                unit_lacado: unit_lacado,
                product_price_lacado: product_price_lacado
            }
            data.push(array_lacado);
        })

        var id_dire = {
            id_dire: $('.id_dire').val()
        }

        data.push(id_dire)

        $.ajax({
            url: "mods/mod_products/ajax/save_presupuesto.php",
            type: "post",
            data: {
                data: data,
                price: total_price,
                referencia: referencia,
                is_pedido: is_pedido,
                agencia: agencia,
                is_dire_fija: $('.select_dir').find('option:selected').attr('is_fija')
            },
            dataType: 'json',
            success: function (response) {
                response_save_presupuesto(response);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function select_change_pais() {
        var data = {
            id_pais: $(this).val()
        }

        $.ajax({
            url: "mods/mod_customers/ajax/change_select_dire_pais.php",
            type: "post",
            data: data,
            success: function (response) {
                $('.select_change_provincias').html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
        ;
    }

    function select_pais_registro() {
        var data = {
            id_pais: $(this).val()
        }

        $.ajax({
            url: "mods/mod_customers/ajax/change_select_dire_pais.php",
            type: "post",
            data: data,
            success: function (response) {
                $('.select_provincia_registro').html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
        ;
    }

    function select_change_provincias() {
        var data = {
            id_pais: $('.select_change_pais').val(),
            id_prov: $(this).val()
        }

        $.ajax({
            url: "mods/mod_customers/ajax/change_select_dire_prov.php",
            type: "post",
            data: data,
            success: function (response) {
                $('.select_change_poblacion').html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function select_change_poblacion() {
        var data = {
            id_pais: $('.select_change_pais').val(),
            id_prov: $('.select_change_provincias').val(),
            nomb_pobl: $('.select_change_poblacion option:selected').text()
        }

        $.ajax({
            url: "mods/mod_customers/ajax/change_select_dire_poblacion.php",
            type: "post",
            data: data,
            success: function (response) {
                $('.select_change_cp').html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }


    function select_provincia_registro() {
        var data = {
            id_pais: $('.select_pais_registro').val(),
            id_prov: $(this).val()
        }

        $.ajax({
            url: "mods/mod_customers/ajax/change_select_dire_prov.php",
            type: "post",
            data: data,
            success: function (response) {
                $('.select_poblacion_registro').html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    delete_product_line()

    $('.btn_delete_dire_cliente').confirmation({
        rootSelector: '.btn_delete_dire_cliente',
        title: mensaje_estas_seguro,
        btnOkLabel: texto_si,
        btnCancelLabel: texto_no,
        onConfirm: function () {
            var id_dire_cli = $(this).closest('.tab-content').find('.active').attr('id_dire');

            var data = {
                id_dire_cli: id_dire_cli
            }

            $.ajax({
                url: "mods/mod_customers/ajax/delete_dire_clientes.php",
                type: "post",
                data: data,
                dataType: 'json',
                success: function (response) {
                    var message = response['message'];
                    var error = response['error'];
                    if (error < 0) {
                        toastr.options = {
                            "positionClass": "toast-top-full-width"
                        }
                        toastr.error("Direcciones", message);
                    }
                    if (error == 1) {
                        toastr.options = {
                            "positionClass": "toast-top-full-width"
                        }
                        toastr.warning("Direcciones", message);
                    }
                    if (error == 0) {
                        toastr.options = {
                            "positionClass": "toast-top-full-width"
                        }
                        toastr.success("Direcciones", message);
                        setTimeout(function () {
                            window.location.reload()
                        }, 800);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }

            });
        }
    });

    $('.btn_delete_presupuesto').confirmation({
        rootSelector: '.btn_delete_presupuesto',
        title: mensaje_estas_seguro,
        btnOkLabel: texto_si,
        btnCancelLabel: texto_no,
        onConfirm: function () {
            console.log("ELIMINAR");
        }
    });

    function add_dire_entre_cliente(e) {
        e.preventDefault();

        var error_form = 0;

        if ($(this).find('.input_change_dir').val().length <= 0) {
            error_form = 1;
        }
        if ($(this).find('.select_change_pais').val() <= 0) {
            error_form = 1;
        }
        if ($(this).find('.select_change_provincias').val() <= 0) {
            error_form = 1;
        }
        if ($(this).find('.select_change_poblacion').val() <= 0) {
            error_form = 1;
        }
        if ($(this).find('.input_name').val().length <= 0) {
            error_form = 1;
        }

        if (error_form == 1) {
            toastr.options = {
                "positionClass": "toast-top-full-width"
            }
            toastr.error("Dirección", "Error. Revise los campos.");
        } else {


            $.ajax({
                url: "mods/mod_customers/ajax/add_dire_envio_pedido.php",
                type: "post",
                data: $(this).serialize(),
                dataType: 'json',
                success: function (response) {
                    var error = response['error'];
                    var message = response['message'];
                    if (error < 0) {
                        toastr.options = {
                            "positionClass": "toast-top-full-width"
                        }
                        toastr.error("Dirección", message);
                    }
                    if (error == 1) {
                        toastr.options = {
                            "positionClass": "toast-top-full-width"
                        }
                        toastr.warning("Dirección", message);
                    }
                    if (error == 0) {
                        toastr.options = {
                            "positionClass": "toast-top-full-width"
                        }
                        toastr.success("Dirección", message);
                    }
                    $('#modal_edit_dire_envio').modal('hide');
                    setTimeout(function () {
                        window.location.reload()
                    }, 800);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }

            });
        }
    }

    function change_is_temporal() {
        if ($(this).is(':checked')) {
            $(this).val(0);
        } else {
            $(this).val(1);
        }
    }

    function click_prev() {
        $(this).prev().click();
    }

    function check_agencia() {
        var value = $(this).val();
        if ($(this).val() == "seleccionar_dire") {
            $('.select_dire_check').show();
            $('.ver_dire').show();
            $('.div_check_agencia').addClass('hidden');
        } else {
            $('.select_dire_check').hide();
            $('.ver_dire').hide();
            $('.div_check_agencia').removeClass('hidden');
        }
        $('.agencia').val($(this).val());
    }

    function search_historial(e) {
        e.preventDefault();
    }

    function change_color_ral() {
        var $this = $(this);
        if ($(this).val().length == 4) {
            var title_ral = $(this).val();
            var id_product = null;
            id_product = $(select_tr).find('.input_id_product').attr('span_value');
            if (typeof id_product === "undefined" || id_product == "undefined" || isNaN(id_product) || id_product == null) {
                id_product = $(select_tr).find('.input_id_product').val();
            }
            if (typeof id_product === "undefined" || id_product == "undefined" || isNaN(id_product) || id_product == null) {
                id_product = $('.input_id_product').val();
            }
            if (typeof id_product === "undefined" || id_product == "undefined" || isNaN(id_product) || id_product == null) {
                var id_product = $(line_color_resumen).closest('.line_component').find('.fami_color').val();
            }
            var data = {
                'title_ral': title_ral,
                'id_product': id_product
            }
            $.ajax({
                url: "mods/mod_products/ajax/change_color_ral.php",
                type: "post",
                data: data,
                dataType: 'json',
                success: function (response) {
                    if (response['error'] == -2) {
                        toastr.options = {
                            "positionClass": "toast-top-full-width"
                        }
                        toastr.warning('Colores RAL', response['message']);
                        if ($('.group_colors')) {
                            $('.group_colors').each(function () {
                                if ($(this).attr('color_product') == title_ral) {
                                    $(this).children()[0].click();
                                    $(this).children()[1].click();
                                }
                            });
                        }
                        if ($('.group_colors_components')) {
                            $('.group_colors_components').each(function () {
                                if ($(this).attr('color_product') == title_ral) {
                                    $(this).children()[0].click();
                                    $(this).children()[1].click();
                                }
                            });
                        }
                    } else {
                        if (line_color_resumen) {
                            $($this).closest('.group_colors_components').attr('color_product', response['coralti']);
                        }
                        $($this).closest('.group_colors').attr('color_product', response['coralti']);
                        $('.change_color_product_ral').css('background-color', response['coralht']);
                        $('.text_color_ral').html(response['coraldes']);
                        $('.text_color_ral').click();
                        $($this).closest('.group_colors').find('.change_color_product_ral_modal').click();
                        if (response['coraldes'] != "No existe RAL") {
                            $('.change_color_ral').val('');
                            var id_product = $('.input_id_product').val();
                            var url = path_images + "images_product/" + id_product + "/image_ral.png";
                            var url_big = path_images + "images_product/" + id_product + "/image_ral_big.png";
                            $('.preview_img').css("background-image", "url(" + url + ")");
                            $('.lupa_div_content_preview_img').attr('href', url_big);
                        } else {
                            $('.text_no_image_preview').html("No existe imagen previa");
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }

            });
        }
    }

    var function_change_color_ral_eject = 0;

    function change_color_product_ral_modal(e) {
        e.preventDefault();
        var id_product = $(select_tr).find('.input_id_product').attr('span_value');
        var title_color = $(this).closest('.group_colors_modal').find('.title_color_product_modal_ral').html();
        if (title_color != "No existe RAL" && title_color != "") {
            var color = $(this).css('background-color');
            $(select_tr).find('.color_product_line').css('background-color', color);
            var color_product = $(this).closest('.group_colors_modal').attr('color_product');
            $(select_tr).find('.input_color_product').attr('span_value', color_product);
            $(select_tr).find('.img_color_product').addClass('hidden');
            var group_color = 2;
            if (id_product == "2734" || id_product == "2735") {
                group_color = 1
            }
            $(select_tr).find('.input_group_color').attr('span_value', group_color);
            $(select_tr).find('.title_color').html(title_color);
            $(select_tr).find('.color_product_line').attr('color_ral', color_product);
            $('.total_lacado').val(parseInt($(this).val() + 1));
            if (id_product == "2735") {
                units_product_old = $(select_tr).find('.input_unidades').val();
                var ancho = $(select_tr).find('.input_ancho').val();
                var alto = $(select_tr).find('.input_alto').val();
                var tipo_producto = $(select_tr).find('.tipo_producto').val();
                calc_prices_corredera(select_tr, units_product_old, ancho, alto, group_color, color, tipo_producto, '0');
            } else {
                calc_price(select_tr);
            }
            $('#modal_product_color').modal('hide');
            $('#modal_add_product').css('overflow', 'scroll');
            var id_product = $(select_tr).find('.input_id_product').attr('span_value');
            change_color_session_ral(color_product_old, color_product, units_product_old, id_product);
            function_change_color_ral_eject = 1;
            save_cart_session();
        } else {
            toastr.options = {
                "positionClass": "toast-top-full-width"
            }
            toastr.warning('Colores RAL', "Introduce otro valor / Selecciona otro color");
        }

    }

    function change_color_components_ral_modal(e) {
        e.preventDefault();
        var title_color = $(this).closest('.group_colors_modal_components').find('.title_color_product_modal_ral').html();
        if (title_color != "No existe RAL" && title_color != "") {
            var id_product = $(line_color_resumen).closest('.line_component').find('.fami_color').val();
            var color_product_old = $(line_color_resumen).closest('.line_component').find('.ararti').val();
            var color = $(this).css('background-color');
            $(select_tr).find('.change_color_component').css('background-color', color);
            $(select_tr).find('.change_color_component_resumen').css('background-color', color);
            $(select_tr).find('.title_color_component').html(title_color);
            $('#modal_product_color').modal('hide');
            $('#modal_add_product').css('overflow', 'scroll');

            var group_color = 2;
            if (id_product == "2734" || id_product == "2735") {
                group_color = 1
            }
            $('.total_lacado').val(parseInt($(this).val() + 1));
            if (id_product == "2735") {
                units_product_old = $(select_tr).find('.input_unidades').val();
                var ancho = $(select_tr).find('.input_ancho').val();
                var alto = $(select_tr).find('.input_alto').val();
                var tipo_producto = $(select_tr).find('.tipo_producto').val();
                calc_prices_corredera(select_tr, units_product_old, ancho, alto, group_color, color, tipo_producto, '0');
            } else {
                calc_price(select_tr);
            }
            $('#modal_product_color').modal('hide');
            $('#modal_add_product').css('overflow', 'scroll');
            //var id_product = $(select_tr).find('.input_id_product').attr('span_value');
            if (line_color_resumen) {
                var color_product = $(this).closest('.group_colors_components').attr('color_product');
                change_color_session_ral(color_product_old, color_product, 1, id_product);
            }
            function_change_color_ral_eject = 1;
            $('.div_submit_product').addClass('hidden');
        } else {
            toastr.options = {
                "positionClass": "toast-top-full-width"
            }
            toastr.warning('Colores RAL', "Introduce otro valor / Selecciona otro color");
        }
    }

    function btn_search_component(e) {
        var id_component_product = $('.input_id_product').val();
        var type_search_val = "";
        var type_search = $('.type_search_components');
        var change_components = $('.change_components:checked').val();
        $(type_search).each(function () {
            if ($(this).is(':checked')) {
                type_search_val = $(this).val();
            }
        })

        var data = {
            text: $(this).val(),
            type_search: type_search_val,
            id_component_product: id_component_product,
            change_components: change_components
        }

        if (type_search_val == "descripcion") {
            if ($(this).val().length >= 3) {
                $.ajax({
                    url: "mods/mod_products/ajax/search_components.php",
                    type: "get",
                    data: data,
                    beforeSend: function () {
                        $('.body_components').html('<div class="center-block loading_gif"></div>');
                    },
                    success: function (response) {
                        setTimeout(function () {
                            $('.body_components').html('').html(response);
                        }, 2000);
                        magnific_image();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error ajax en submitFormNewDni");
                        console.log(jqXHR, textStatus, errorThrown);
                    }

                });
            } else if ($(this).val().length == 0) {
                $.ajax({
                    url: "mods/mod_products/ajax/search_components.php",
                    type: "get",
                    data: data,
                    beforeSend: function () {
                        $('.body_components').html('<div class="center-block loading_gif"></div>');
                    },
                    success: function (response) {
                        setTimeout(function () {
                            $('.body_components').html('').html(response);
                        }, 2000);
                        magnific_image();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error ajax en submitFormNewDni");
                        console.log(jqXHR, textStatus, errorThrown);
                    }

                });
            } else {
                console.log("NO ENVIAMOS");
            }
        } else {
            if ($(this).val().length == 14) {
                $.ajax({
                    url: "mods/mod_products/ajax/search_components.php",
                    type: "get",
                    data: data,
                    beforeSend: function () {
                        $('.body_components').html('<div class="center-block loading_gif"></div>');
                    },
                    success: function (response) {
                        setTimeout(function () {
                            $('.body_components').html('').html(response);
                        }, 2000);
                        magnific_image();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error ajax en submitFormNewDni");
                        console.log(jqXHR, textStatus, errorThrown);
                    }
                });
            } else if ($(this).val().length == 0) {
                $.ajax({
                    url: "mods/mod_products/ajax/search_components.php",
                    type: "get",
                    data: data,
                    beforeSend: function () {
                        $('.body_components').html('<div class="center-block loading_gif"></div>');
                    },
                    success: function (response) {
                        setTimeout(function () {
                            $('.body_components').html('').html(response);
                        }, 2000);
                        magnific_image();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error ajax en submitFormNewDni");
                        console.log(jqXHR, textStatus, errorThrown);
                    }

                });
            } else {
                console.log("Faltan carácteres...");
            }
        }
    }

    var type_precio = "fabrica";

    function input_unit_components(e) {
        e.preventDefault();
        if ($(this).val() >= 1) {
            $(this).closest('.fila_components_products').find('.total_units_metros').val($(this).val());
            type_precio = "fabrica";
            var precio = $(this).closest('.fila_components_products').find('.precio_fabrica').val();

            if (isNaN(precio)) {
                type_precio = "almacen";
                precio = $(this).closest('.fila_components_products').find('.precio_almacen').val();
            }

            var embalaje = parseInt($(this).closest('.fila_components_products').find('.embalaje').val());

            if (type_precio == "fabrica" && parseInt($(this).val()) >= embalaje) {
                toastr.options = {
                    "positionClass": "toast-top-full-width"
                }
                toastr.warning("Unidades", "Unidades superiores o iguales embalaje. Para esta cantidad de producto se recomienda la opción 'Venta industrial'");
            }

            if (type_precio == "almacen") {
                var resto = parseInt($(this).val()) % parseInt(embalaje);
                if (resto != 0) {
                    $(this).addClass('bg-red');
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.warning("Unidades", "Las unidades no son múltiplo del embalaje. Para esta cantidad de producto se debe seleccionar la opción 'Componentes'");
                } else {
                    $(this).removeClass('bg-red');
                    if ($(this).val() > 0) {
                        $(this).closest('.border_components').addClass('border_units_components');
                    } else {
                        $(this).closest('.border_components').removeClass('border_units_components');
                    }
                }
            }

            var tipo_medida = $(this).closest('.fila_components_products').find('.type_medida_component').val();
            if (tipo_medida == 1 || tipo_medida == 2) {
                if ($(this).val() > 0) {
                    $(this).closest('.fila_components_products').find('.add_medidas_2').removeClass('hidden');
                } else {
                    $(this).closest('.fila_components_products').find('.add_medidas_2').addClass('hidden');
                }

                //Venta industrial
                if ($('.change_components:checked').val() == 3) {
                    var total = $(this).val() * parseFloat(precio);
                    total = total.toFixed(2);
                    $(this).closest('.fila_components_products').find('.total_component').html(total);
                    $(this).closest('.fila_components_products').find('.linea_medida_componente_check_units').html($(this).val());
                }

            } else {
                var arumiv = $(this).closest('.fila_components_products').find('.arumiv').val();
                var total = $(this).val() * parseFloat(precio);
                total = total / arumiv;
                total = total.toFixed(2);
                $(this).closest('.fila_components_products').find('.total_component').html(total);
                $(this).closest('.fila_components_products').find('.linea_medida_componente_check_units').html($(this).val());
            }

            if (is_type_product_modal == 1) {
                var text_type_doc = "presupuesto"
                if (type_doc == "") {
                    text_type_doc = "pedido";
                }
                $('.type_document').html(text_type_doc);
                $('.boton_componentes_flotante_add').removeClass('hidden');
            } else {
                $('.boton_componentes_flotante').removeClass('hidden');
            }
        }

    }

    function input_unit_components_2(e) {
        e.preventDefault();

        var input = $(this);
        var id_component = $(this).closest('.fila_components_products').find('.id_component').val();
        var arsubf = $(this).closest('.fila_components_products').find('.arsubf').val();
        var ararti = $(this).closest('.fila_components_products').find('.ararti').val();
        var arprpr = $(this).closest('.fila_components_products').find('.arprpr').val();

        var data = {
            unit_components: $(this).val(),
            id_component: id_component,
            arsubf: arsubf,
            ararti: ararti,
            arprpr: arprpr
        };

        $.ajax({
            url: "mods/mod_products/ajax/get_price_component.php",
            type: "post",
            data: data,
            dataType: 'json',
            success: function (response) {
                $(input).closest('.fila_components_products').find('.span_almacen').html('');
                $(input).closest('.fila_components_products').find('.span_unitario').html('');
                $(input).closest('.fila_components_products').find('.total_component').html('');
                if (response['Unidades_almacen'] > 0) {
                    $(input).closest('.fila_components_products').find('.span_almacen').html(response['Unidades_almacen'] + " Uds a " + response['Precio_almacen'] + "€");
                }
                if (response['Unidades_unitario'] > 0) {
                    $(input).closest('.fila_components_products').find('.span_unitario').html(response['Unidades_unitario'] + " Uds a " + response['Precio_unitario'] + "€");
                }
                $(input).closest('.fila_components_products').find('.total_component').html(response['Precio_total'] + "€");
                $(input).closest('.fila_components_products').find('.total_component_hidden').val(response['Precio_total']);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });

        $('.boton_componentes_flotante').removeClass('hidden');
    }

    function check_search_components() {
        var $this = $(this).prev().click();
        var val = $($this).val();
        if (val == "codigo") {
            $('.btn_search_component').attr('placeholder', "1791-01-000001");
            $('.btn_search_component').parent().addClass('float-label-control');
            $('.btn_search_component').parent().find('label').html('1791-01-000001');
            $('.btn_search_component').parent().find('label').removeClass('hidden');
            $('.float-label-control').floatLabels();
        } else {
            $('.btn_search_component').attr('placeholder', "Introduce búsqueda");
            $('.btn_search_component').parent().removeClass('float-label-control');
            $('.btn_search_component').parent().find('label').addClass('hidden');
        }
    }

    function btn_flotante_save(e) {
        e.preventDefault();
        var data = [];
        var filas_componentes = $('.fila_components_products');
        $(filas_componentes).each(function (index, element) {
            var units_components = $(element).find('.input_unit_components').val();
            if (units_components > 0) {
                var total_component = $(element).find('.total_component').html();
                var arsubf = $(element).find('.arsubf').val();
                var ararti = $(element).find('.ararti').val();
                var array_component = {
                    units_components: units_components,
                    total_component: total_component,
                    arsubf: arsubf,
                    ararti: ararti
                }
                data.push(array_component);
            }
        })

        $.ajax({
            url: "mods/mod_products/ajax/save_components.php",
            type: "post",
            data: {data_component: data},
            success: function (response) {
                var id_product = $('.input_id_product').val();
                $('.form_product').submit();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function btn_ini_log_flotante(e) {
        e.preventDefault();

        $('#form_login_flotante').submit();

    }

    function btn_flotante_generar_new(e) {
        e.preventDefault();
        if (check_login() == 0) {
            $('#modal_ini_login_flotante').modal('show');
            $('.option_login').val("product_index");
        } else {

            $('.type_button_hidden').val($(this).val());

            var submit = "true";
            var filas_componentes = $('.fila_components_products');
            $(filas_componentes).each(function (index, element) {
                var units_components = $(element).find('.linea_medida_componente_check_units').html();
                if (units_components > 0) {
                    var tipo_medida = $(element).find('.type_medida_component').val();
                    var arfami = $(element).find('.arfami').val();
                    var arsubf = $(element).find('.arsubf').val();
                    var ararti = $(element).find('.ararti').val();
                    var arprpr = $(element).find('.arprpr').val();
                    var ararti_first = "";
                    if (!isNaN($(element).find('.ararti_first').val())) {
                        ararti_first = $(element).find('.ararti_first').val();
                    }
                    var components_color = "";
                    if (!isNaN($(element).find('.components_color').val())) {
                        components_color = $(element).find('.components_color').val()
                    }
                    var total_component = 1;
                    if (!isNaN($(element).find('.total_component').html())) {
                        total_component = $(element).find('.total_component').html();
                    }

                    var array_component = [];

                    var unidades_componente = "";
                    if (!isNaN($(this).find('.linea_medida_componente_check_units').html())) {
                        unidades_componente = $(this).find('.linea_medida_componente_check_units').html();
                    }

                    if (!isNaN(unidades_componente)) {
                        //AQUI COMENTARIO
                        $(this).find('.linea_medida_componente_check').each(function () {
                            var alto_component = $(this).find('.linea_medida_componente_check_alto').html();
                            var ancho_component = $(this).find('.linea_medida_componente_check_ancho').html();
                            var metros_component = $(this).find('.linea_medida_componente_check_metros').html();
                            var medidas = {
                                unidades: unidades_componente,
                                alto: alto_component,
                                ancho: ancho_component,
                                metros: metros_component,
                                arsubf: arsubf,
                                ararti: ararti,
                                arprpr: arprpr
                            }
                            array_component.push(medidas);
                        })
                    }

                    var data_array = {
                        units_components: units_components,
                        tipo_medida: tipo_medida,
                        arfami: arfami,
                        arsubf: arsubf,
                        ararti: ararti,
                        arprpr: arprpr,
                        ararti_first: ararti_first,
                        components_color: components_color,
                        total_component: total_component,
                        array_component: array_component
                    }
                    $('.form_product').find('.datas').val(JSON.stringify(data_array));

                } else {
                    $(element).remove();
                }
            })

            if (submit == "true") {
                setTimeout(function () {
                    var id_product = $('.input_id_product').val();
                    $('.form_product').submit();
                }, 800);
            }
        }
    }


    function btn_flotante_generar(e) {
        e.preventDefault();
        $('.input_type_button').val($(this).val());
        $('.type_button_hidden').val($(this).val());
        $('.btn_flotante_generar').val($(this).val());
        $('[name="type_button"]').val($(this).val());
        if (check_login() == 0) {
            $('#modal_ini_login_flotante').modal('show');
            $('.option_login').val("product_index");
        } else {

            var filas_componentes = $('.fila_components_products');
            var submit = "true";

            if (type_precio == "fabrica") {

                $('.type_button_hidden').val($(this).val());
                $(filas_componentes).each(function (index, element) {
                    var units_components = $(element).find('.linea_medida_componente_check_units').html();
                    if (units_components > 0) {
                        var tipo_medida = $(element).find('.type_medida_component').val();
                        if (tipo_medida != 3) {
                            var arsubf = $(element).find('.arsubf').val();
                            var ararti = $(element).find('.ararti').val();
                            var arprpr = $(element).find('.arprpr').val();
                            var type_medida_component = $(element).find('.type_medida_component').val();
                            var ararti_first = $(element).find('.ararti_first').val();
                            var components_color = $(element).find('.components_color').val();
                            if (value_type_product == 1) {
                                var array_component = [];
                                var data_array = {
                                    units_components: units_components,
                                    total_component: 1,
                                    arsubf: arsubf,
                                    ararti: ararti,
                                    arprpr: arprpr,
                                    components_color: components_color,
                                    ararti_first: ararti_first,
                                    type_medida_component: type_medida_component
                                }

                                $(this).find('.linea_medida_componente_check').each(function () {
                                    var unidades_componente = $(this).find('.linea_medida_componente_check_units').html();
                                    var alto_component = $(this).find('.linea_medida_componente_check_alto').html();
                                    var ancho_component = $(this).find('.linea_medida_componente_check_ancho').html();
                                    var metros_component = $(this).find('.linea_medida_componente_check_metros').html();

                                    var medidas = {
                                        unidades: unidades_componente,
                                        alto: alto_component,
                                        ancho: ancho_component,
                                        metros: metros_component,
                                        arsubf: arsubf,
                                        ararti: ararti,
                                        arprpr: arprpr
                                    }

                                    array_component.push(medidas);

                                })
                                $(element).find('.data_component').val(JSON.stringify(array_component));

                            } else if (value_type_product == 1) {
                                submit = false;
                                toastr.options = {
                                    "positionClass": "toast-top-full-width"
                                }
                                toastr.error("Medidas", "Existen unidades sin medidas. Revise los datos");
                            }
                        } else {
                            var units_components_3 = $(element).find('.input_unit_components').val();
                            var total_component = $(element).find('.total_component').html();
                            var arsubf = $(element).find('.arsubf').val();
                            var ararti = $(element).find('.ararti').val();
                            var arprpr = $(element).find('.arprpr').val();
                            var ararti_first = $(element).find('.ararti_first').val();
                            var components_color = $(element).find('.components_color').val();
                            var type_medida_component = $(element).find('.type_medida_component').val();
                            var array_component = {
                                input_unit_components: units_components_3,
                                total_component: total_component,
                                arsubf: arsubf,
                                ararti: ararti,
                                arprpr: arprpr,
                                components_color: components_color,
                                ararti_first: ararti_first,
                                type_medida_component: type_medida_component
                            }
                            $(element).find('.data_component').val(JSON.stringify(array_component));
                        }
                    } else {
                        $(element).remove();
                    }
                })
            } else {
                var check_error = $(filas_componentes).find('.bg-red').length;
                if (check_error <= 0) {
                    $(filas_componentes).each(function (index, element) {
                        var units_components = $(element).find('.input_unit_components').val();
                        if (units_components <= 0) {
                            $(element).remove();
                        }
                    })
                } else {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.warning("Unidades", "Las unidades no son múltiplo del embalaje. Para esta cantidad de producto se debe seleccionar la opción 'Componentes'");
                    return false;
                }
            }

            if (submit == "true") {
                setTimeout(function () {
                    var id_product = $('.input_id_product').val();
                    $('.form_product').submit();
                }, 800);
            }
        }
    }

    function form_login_flotante(e) {
        e.preventDefault();
        $.ajax({
            url: "mods/mod_customers/action/ini_log.php",
            type: "post",
            dataType: 'json',
            data: $(this).serialize(),
            success: function (response) {
                if (response['error'] == 1) {
                    $('.div_error_login').removeClass('hidden');
                }
                if (response['error'] == -1) {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    window.location.reload();
                    //toastr.warning("Login", "Ya has iniciado sesión");
                }
                if (response['error'] == -2) {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.warning("Login", "Error desconocido. Inténtelo más tarde");
                }
                if (response['error'] == 0 || response['error'] == 2 || response['error'] == 3) {

                    var submit = "true";
                    var filas_componentes = $('.fila_components_products');
                    $(filas_componentes).each(function (index, element) {
                        var units_components = $(element).find('.linea_medida_componente_check_units').html();
                        if (value_type_product == 3) {
                            units_components = $(element).find('.input_unit_components').val();
                        }
                        if (units_components > 0) {
                            var tipo_medida = $(element).find('.type_medida_component').val();
                            if (tipo_medida != 3) {
                                var arsubf = $(element).find('.arsubf').val();
                                var ararti = $(element).find('.ararti').val();
                                var arprpr = $(element).find('.arprpr').val();
                                var ararti_first = $(element).find('.ararti_first').val();
                                var components_color = $(element).find('.components_color').val();
                                var type_medida_component = $(element).find('.type_medida_component').val();
                                if (value_type_product == 1) {
                                    var array_component = [];
                                    var data_array = {
                                        units_components: units_components,
                                        total_component: 1,
                                        arsubf: arsubf,
                                        ararti: ararti,
                                        arprpr: arprpr,
                                        components_color: components_color,
                                        ararti_first: ararti_first,
                                        type_medida_component: type_medida_component
                                    }

                                    $(this).find('.linea_medida_componente_check').each(function () {
                                        var unidades_componente = $(this).find('.linea_medida_componente_check_units').html();
                                        var alto_component = $(this).find('.linea_medida_componente_check_alto').html();
                                        var ancho_component = $(this).find('.linea_medida_componente_check_ancho').html();
                                        var metros_component = $(this).find('.linea_medida_componente_check_metros').html();


                                        var medidas = {
                                            unidades: unidades_componente,
                                            alto: alto_component,
                                            ancho: ancho_component,
                                            metros: metros_component,
                                            arsubf: arsubf,
                                            ararti: ararti,
                                            arprpr: arprpr
                                        }

                                        array_component.push(medidas);

                                    })
                                    $(element).find('.data_component').val(JSON.stringify(array_component));

                                } else if (value_type_product == 1) {
                                    submit = false;
                                    toastr.options = {
                                        "positionClass": "toast-top-full-width"
                                    }
                                    toastr.error("Medidas", "Existen unidades sin medidas. Revise los datos");
                                }
                            } else {
                                var total_component = $(element).find('.total_component').html();
                                var arsubf = $(element).find('.arsubf').val();
                                var ararti = $(element).find('.ararti').val();
                                var arprpr = $(element).find('.arprpr').val();
                                var ararti_first = $(element).find('.ararti_first').val();
                                var components_color = $(element).find('.components_color').val();
                                var type_medida_component = $(element).find('.type_medida_component').val();
                                var array_component = {
                                    units_components: units_components,
                                    total_component: total_component,
                                    arsubf: arsubf,
                                    ararti: ararti,
                                    arprpr: arprpr,
                                    components_color: components_color,
                                    ararti_first: ararti_first,
                                    type_medida_component: type_medida_component
                                }
                                $('.form_product').append(array_component);
                            }
                        } else {
                            $(element).remove();
                        }
                    })

                    if (submit == "true") {
                        setTimeout(function () {
                            var id_product = $('.input_id_product').val();
                            $('.form_product').submit();
                        }, 800);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function change_line_product_component_unit(e) {
        e.preventDefault();
        var tipo_de_medida = $(this).closest('.line_product_component').find('.tipo_medida_component').val();
        if (tipo_de_medida == 3) {
            var precio = $(this).closest('.line_product_component').find('.price_component_hidden').val();
            var total_linea = $(this).val() * parseFloat(precio);
            total_linea = total_linea.toFixed(2);
            $(this).closest('.line_product_component').find('.product_price').html(total_linea);

            var total = 0;
            $('.product_price').each(function () {
                if ($.isNumeric($(this).html())) {
                    total += parseFloat($(this).html());
                }
            })
            $('.product_price_lacado').each(function () {
                total += parseFloat($(this).html());
            })
            total = total.toFixed(2);
            $('.price_total').val(total);
        }
    }

    var line_component = "";
    var tipo_medida = "";
    var exist_lineas_medidas = "";
    var max_measure = "";
    var check_error = 0;

    function add_medidas_2_new(e) {
        e.preventDefault();
        $('#modal_add_medidas_component').find('.modal-body').empty();
        $('#modal_add_medidas_component').find('.btn_save_medidas_componente_resumen').removeClass("btn_save_medidas_componente_resumen").addClass('btn_save_medidas_componente');
        line_component = $(this).closest('.fila_components_products');
        exist_lineas_medidas = $(line_component).find('.content_lineas_medidas_componente').find('.linea_medida_componente_check').length;
        var number_product = $(line_component).find('.input_unit_components').val();
        tipo_medida = $(line_component).find('.type_medida_component').val();
        max_measure = $(line_component).find('.max_measure').val();
        max_measure.replace(/,/g, ".");
        max_measure = parseFloat(max_measure);
        max_measure = max_measure.toFixed(2);
        var line = "";

        if (tipo_medida == 1) {
            if (exist_lineas_medidas <= 0) {
                var content_add_medidas = "<div class='hidden row content_medidas_metros_cuadrados content_medidas_metros'><div class='row col-md-12 col-xs-12'><div class='col-md-2 col-xs-2'>Unidades</div><div class='col-md-3 col-xs-3'>Ancho</div><div class='col-md-3 col-xs-3'>Alto</div><div class='col-md-2 col-xs-2'>Metros<sup>2</sup></div><div class='col-md-1 col-xs-1'>Opciones</div></div><div class='col-md-12 col-xs-12 row line_component_metros line_component mt-10'><div class='col-xs-2 col-md-2'><input type='number' value='0' min='0' class='unidades_medida_componente form-control'></div><div class='col-xs-3 col-md-3'><input type='text' maxlength='5' class='form-control ancho_medida_componente border-warning' placeholder='0.000'></div><div class='col-xs-3 col-md-3'><input type='text' maxlength='5' class='form-control alto_medida_componente border-warning' placeholder='0.000'></div><div class='col-xs-2 col-md-2'><span class='total_metros_componente'></span><input type='hidden' class='total_metros_componente_hidden'></div><div class='col-xs-1 col-md-1 content_options_components'><span class='new_line_size_component text-success fa fa-plus'></span><span class='delete_line_size_component text-danger fa fa-close'></span></div></div></div>";
                $('#modal_add_medidas_component').find('.modal-body').append(content_add_medidas);
            }
            //CARGAR INFO
            else if (exist_lineas_medidas > 0) {
                var header_content = "<div class='hidden row content_medidas_metros_cuadrados content_medidas_metros'><div class='row col-md-12 col-xs-12'><div class='col-md-2 col-xs-2'>Unidades</div><div class='col-md-3 col-xs-3'>Ancho</div><div class='col-md-3 col-xs-3'>Alto</div><div class='col-md-2 col-xs-2'>Metros<sup>2</sup></div><div class='col-md-1 col-xs-1'>Opciones</div></div>";
                $('#modal_add_medidas_component').find('.modal-body').append(header_content);
                $(line_component).find('.linea_medida_componente_check').each(function (index, element) {
                    var unidades_medidas = $(element).find('.linea_medida_componente_check_units').html();
                    var ancho_medidas = $(element).find('.linea_medida_componente_check_ancho').html();
                    var alto_medidas = $(element).find('.linea_medida_componente_check_alto').html();
                    ancho_medidas = ancho_medidas.replace("m", "");
                    ancho_medidas = ancho_medidas.replace(" ", "");
                    alto_medidas = alto_medidas.replace(" ", "");
                    alto_medidas = alto_medidas.replace("m", "");
                    alto_medidas = alto_medidas.replace(/,/g, ".");
                    ancho_medidas = ancho_medidas.replace(/,/g, ".");
                    var metros_medidas = ancho_medidas * alto_medidas;
                    metros_medidas = parseFloat(metros_medidas);
                    metros_medidas = metros_medidas.toFixed(2);
                    line = "<div class='col-md-12 col-xs-12 row line_component_metros line_component mt-10'><div class='col-xs-2 col-md-2'><input type='number' value='" + unidades_medidas + "' min='0' class='unidades_medida_componente form-control'></div><div class='col-xs-3 col-md-3'><input type='text' maxlength='5' class='form-control ancho_medida_componente' placeholder='0.000' value='" + ancho_medidas + "'></div><div class='col-xs-3 col-md-3'><input type='text' maxlength='5' class='form-control alto_medida_componente' placeholder='0.000' value='" + alto_medidas + "'></div><div class='col-xs-2 col-md-2'><span class='total_metros_componente'>" + metros_medidas + "</span><input type='hidden' class='total_metros_componente_hidden' value='" + metros_medidas + "'></div><div class='col-xs-1 col-md-1 content_options_components'><span class='new_line_size_component text-success fa fa-plus'></span><span class='delete_line_size_component text-danger fa fa-close'></span></div></div>";
                    $('#modal_add_medidas_component').find('.modal-body').find('.content_medidas_metros_cuadrados').append(line);
                })
                $('#modal_add_medidas_component').find('.modal-body').append('</div>');
            }
            $('#modal_add_medidas_component').find('.modal-body').find('.content_medidas_metros_cuadrados').removeClass('hidden');
        } else if (tipo_medida == 2) {
            if (exist_lineas_medidas <= 0) {
                var content_add_medidas = "<div class='hidden row content_medidas_metros_lineales content_medidas_metros'><div class='row col-md-12 col-xs-12'><div class='col-md-4 col-xs-4'>Unidades</div><div class='col-md-4 col-xs-4'>Metros</div><div class='col-md-4 col-xs-4'>Opciones</div></div><div class='col-md-12 col-xs-12 row line_component_metros line_component mt-10'><div class='col-xs-4 col-md-4'><input type='number' value='0' min='0' class='unidades_medida_componente form-control'></div><div class='col-xs-4 col-md-4'><input type='text' class='form-control metros_medida_componente' maxlength='5' placeholder='0.000'></div><div class='col-xs-4 col-md-4 content_options_components'><span class='new_line_size_component text-success fa fa-plus'></span><span class='delete_line_size_component text-danger fa fa-close'></span></div></div></div>";
                $('#modal_add_medidas_component').find('.modal-body').append(content_add_medidas);
            }
            //CARGAR INFO
            else if (exist_lineas_medidas > 0) {
                var header_content = "<div class='hidden row content_medidas_metros_lineales content_medidas_metros'><div class='row col-md-12 col-xs-12'><div class='col-md-4 col-xs-4'>Unidades</div><div class='col-md-4 col-xs-4'>Metros</div><div class='col-md-4 col-xs-4'>Opciones</div></div>";
                $('#modal_add_medidas_component').find('.modal-body').append(header_content);
                $(line_component).find('.linea_medida_componente_check').each(function (index, element) {
                    var unidades_medidas = $(element).find('.linea_medida_componente_check_units').html();
                    var metros_medidas = $(element).find('.linea_medida_componente_check_metros').html();
                    line = "<div class='col-md-12 col-xs-12 row line_component_metros line_component mt-10'><div class='col-xs-4 col-md-4'><input type='number' value='" + unidades_medidas + "' min='0' class='unidades_medida_componente form-control'></div><div class='col-xs-4 col-md-4'><input type='text' value='" + metros_medidas + "' maxlength='5' class='form-control metros_medida_componente' placeholder='0.000'></div><div class='col-xs-4 col-md-4 content_options_components'><span class='new_line_size_component text-success fa fa-plus'></span><span class='delete_line_size_component text-danger fa fa-close'></span></div></div>";
                    $('#modal_add_medidas_component').find('.modal-body').find('.content_medidas_metros_lineales').append(line);
                })
                $('#modal_add_medidas_component').find('.modal-body').append('</div>');
            }

            $('#modal_add_medidas_component').find('.modal-body').find('.content_medidas_metros_lineales').removeClass('hidden');

        }

        $(this).val("Editar medidas");

        $('#modal_add_medidas_component').modal('show');

    }

    function btn_save_medidas_componente_new(e) {
        e.preventDefault();
        var modal = $(this).closest('#modal_add_medidas_component');
        var linea_medida = $(modal).find('.line_component_metros');
        var data_line = [];
        var content_lineas = "";
        var num = 0;
        var metros_totales = 0.00;
        var total_metros_precio = 0.00;
        if (tipo_medida == 1) {
            $(linea_medida).each(function (index, element) {
                num = num + 1;
                var unidades = $(this).find('.unidades_medida_componente').val();
                var ancho = $(this).find('.ancho_medida_componente').val();
                var alto = $(this).find('.alto_medida_componente').val();
                var metros = $(this).find('.total_metros_componente_hidden').val();
                metros = metros * unidades;
                metros = parseFloat(metros).toFixed(2);
                metros_totales += parseFloat(metros_totales) + parseFloat(metros);

                $(line_component).find('.ancho').val(ancho);
                $(line_component).find('.alto').val(alto);

                if (metros <= 0 || alto <= 0 || ancho <= 0 || unidades <= 0) {
                    check_error += "1";
                }

                //AQUI COMENTARIO
                var result = "<div class='row linea_medida_componente_check col-xs-12 col-md-12'><div class='col-md-12 col-xs-12'><div class='col-xs-1 col-md-1'> <span class='linea_medida_componente_check_units'>" + unidades + "</span> Uds. </div><div class='col-xs-2 col-md-2'>Ancho: <span class='linea_medida_componente_check_ancho'>" + ancho + " m </span></div><div class='col-xs-2 col-md-2'>Alto: <span class='linea_medida_componente_check_alto'>" + alto + " m </span></div><div class='col-xs-2 col-md-2'>Total: <span class='linea_medida_componente_check_metros'>" + metros + "</span> m<sup>2</sup></div></div><input type='hidden' class='ancho' name='ancho[]' value=''> <input type='hidden' class='alto' name='alto[]' value=''></div>";

                content_lineas += result;

                ancho = ancho.replace(/,/g, '.');
                var precio_ancho = (Math.ceil(ancho * 20) / 20).toFixed(2);
                alto = alto.replace(/,/g, '.');
                var precio_alto = (Math.ceil(alto * 20) / 20).toFixed(2);
                var metros_precio = parseFloat(precio_alto) * parseFloat(precio_ancho);
                metros_precio = metros_precio.toFixed(2);
                total_metros_precio += parseFloat(metros_precio) * unidades;

            })
        } else if (tipo_medida == 2) {
            //metros_medida_componente
            $(linea_medida).each(function (index, element) {
                num = num + 1;
                var unidades = $(this).find('.unidades_medida_componente').val();
                var metros = $(this).find('.metros_medida_componente').val();
                var metros_show = metros;
                metros = metros.replace(/,/g, ".");
                metros = parseFloat(metros);
                metros = metros.toFixed(2);
                metros_totales += parseFloat(metros_totales) + parseFloat(metros);

                var metros_linea = metros * unidades;
                metros_linea = parseFloat(metros_linea);
                metros_linea = metros_linea.toFixed(2);

                var array_line_medida = {
                    'metros': metros
                }

                $(line_component).find('.metros').val(metros);

                if (metros <= 0) {
                    check_error += "1";
                }

                data_line.push(array_line_medida);

                var result = "<div class='row linea_medida_componente_check col-md-12 col-xs-12'><div class='col-md-12 col-xs-12'><div class='col-md-1 col-xs-1'><span class='linea_medida_componente_check_units'>" + unidades + "</span> Uds. </div><div class='col-md-2 col-xs-2'>Medida: <span class='linea_medida_componente_check_metros'>" + metros_show + "</span> m <span></div><div class='col-md-2 col-xs-2'>Total: " + metros_linea + " m</span></div></div><input type='hidden' class='metros' name='metros[]' value=''></div>";
                content_lineas += result;

                metros = metros.replace(/,/g, '.');
                var precios_metros = (Math.ceil(metros * 20) / 20).toFixed(2);
                total_metros_precio += parseFloat(precios_metros) * unidades;

            })
        }

        var precio = $(line_component).find('.precio_fabrica').val();
        if (isNaN(precio)) {
            precio = $(line_component).find('.precio_almacen').val();
        }

        precio = parseFloat(precio);
        metros_totales = parseFloat(metros_totales);

        var precio_total = precio * total_metros_precio;
        precio_total = precio_total.toFixed(2);

        if (check_error == 0) {
            if (is_type_product_modal == 1) {
                //$('#modal_add_product').find('.add_medidas_2').addClass('hidden');
                var text_type_doc = "presupuesto"
                if (type_doc == "") {
                    text_type_doc = "pedido";
                }
                $('.type_document').html(text_type_doc);
                $('.boton_componentes_flotante_add').removeClass('hidden');
            }
            $(line_component).find('.total_component').html(precio_total);

            $(line_component).find('.content_lineas_medidas_componente').removeClass('hidden').html('').append(content_lineas);

            $(line_component).find('.total_units_metros').val(metros_totales);

            var total = 0;
            $('.product_price').each(function () {
                total += parseFloat($(this).html());
            })
            $('.product_price_lacado').each(function () {
                total += parseFloat($(this).html());
            })
            total = total.toFixed(2);
            $('.price_total').val(total);

            var change_color_component = $(line_component).find('.change_color_component').length;

            $('#modal_add_medidas_component').modal('hide');
            if (change_color_component >= 1) {
                $(line_component).find('.change_color_component').click();
            }
            if (is_type_product_modal != 1) {
                $('.boton_componentes_flotante').removeClass('hidden');
            }
            $('#modal_add_product').css('overflow', 'scroll');

        } else {
            toastr.options = {
                "positionClass": "toast-top-full-width"
            }
            toastr.error("Medidas", mensaje_faltan_medidas);
        }
    }

    //En desuso
    function add_medidas_2(e) {
        e.preventDefault();
        line_component = $(this).closest('.fila_components_products');
        exist_lineas_medidas = $(line_component).find('.content_lineas_medidas_componente').find('.linea_medida_componente_check').length;
        var number_product = $(line_component).find('.input_unit_components').val();
        tipo_medida = $(line_component).find('.type_medida_component').val();
        max_measure = $(line_component).find('.max_measure').val();
        max_measure.replace(/,/g, ".");
        max_measure = parseFloat(max_measure);
        max_measure = max_measure.toFixed(2);
        var line = "";
        //No hay que cargar información
        if (exist_lineas_medidas == 0) {
            if (tipo_medida == 1) {
                for (var n = 1; n <= number_product; ++n) {
                    line += "<div class='line_component_metros'>Unidad " + n + ": <br>Ancho: <input type='text' class='ancho_medida_componente'> Alto: <input type='text' class='alto_medida_componente'> <span class='total_metros_componente'></span><input type='hidden' class='total_metros_componente_hidden'></div>";
                }
            } else if (tipo_medida == 2) {
                for (var n = 1; n <= number_product; ++n) {
                    line += "<div class='line_component_metros'>Unidad " + n + ": <br>Metros: <input type='text' class='metros_medida_componente'></div>";
                }
            }

            $(this).val("Editar medidas");
        }
        //Cargamos informacion previa
        else {
            //Editar informacion existentes
            if (exist_lineas_medidas == number_product) {
                $(line_component).find('.linea_medida_componente_check').each(function (index, element) {
                    var n = index + 1;
                    if (tipo_medida == 1) {
                        var ancho_medidas = $(element).find('.linea_medida_componente_check_ancho').html();
                        var alto_medidas = $(element).find('.linea_medida_componente_check_alto').html();
                        ancho_medidas = ancho_medidas.replace("m", "");
                        ancho_medidas = ancho_medidas.replace(" ", "");
                        alto_medidas = alto_medidas.replace(" ", "");
                        alto_medidas = alto_medidas.replace("m", "");
                        alto_medidas = alto_medidas.replace(/,/g, ".");
                        ancho_medidas = ancho_medidas.replace(/,/g, ".");
                        var metros_medidas = ancho_medidas * alto_medidas;
                        metros_medidas = parseFloat(metros_medidas);
                        metros_medidas = metros_medidas.toFixed(2);
                        line += "<div class='line_component_metros'>Unidad " + n + ": <br>Ancho: <input type='text' class='ancho_medida_componente' value='" + ancho_medidas + "'> Alto: <input type='text' class='alto_medida_componente' value='" + alto_medidas + "'> <span class='total_metros_componente'>" + metros_medidas + "</span><input type='hidden' class='total_metros_componente_hidden' value='" + metros_medidas + "'></div>";
                    } else if (tipo_medida == 2) {
                        var metros_medidas_check = $(element).find('.linea_medida_componente_check_metros').html();
                        metros_medidas_check = metros_medidas_check.replace("metros", "");
                        metros_medidas_check = metros_medidas_check.replace(" ", "");
                        line += "<div class='line_component_metros'>Unidad " + n + ": <br>Metros: <input type='text' class='metros_medida_componente' value='" + metros_medidas_check + "'></div>";
                    }
                });
            }
            //Añadimos
            if (exist_lineas_medidas < number_product) {
                var n;
                var line_to_add = number_product - exist_lineas_medidas;
                $(line_component).find('.linea_medida_componente_check').each(function (index, element) {
                    n = index + 1;
                    if (tipo_medida == 1) {
                        var ancho_medidas = $(element).find('.linea_medida_componente_check_ancho').html();
                        var alto_medidas = $(element).find('.linea_medida_componente_check_alto').html();
                        ancho_medidas = ancho_medidas.replace("m", "");
                        ancho_medidas = ancho_medidas.replace(" ", "");
                        alto_medidas = alto_medidas.replace(" ", "");
                        alto_medidas = alto_medidas.replace("m", "");
                        alto_medidas = alto_medidas.replace(/,/g, ".");
                        ancho_medidas = ancho_medidas.replace(/,/g, ".");
                        var metros_medidas = ancho_medidas * alto_medidas;
                        metros_medidas = parseFloat(metros_medidas);
                        metros_medidas = metros_medidas.toFixed(2);
                        line += "<div class='line_component_metros'>Unidad " + n + ": <br>Ancho: <input type='text' class='ancho_medida_componente' value='" + ancho_medidas + "'> Alto: <input type='text' class='alto_medida_componente' value='" + alto_medidas + "'> <span class='total_metros_componente'>" + metros_medidas + "</span><input type='hidden' class='total_metros_componente_hidden' value='" + metros_medidas + "'></div>";
                    } else if (tipo_medida == 2) {
                        var metros_medidas_check = $(element).find('.linea_medida_componente_check_metros').html();
                        metros_medidas_check = metros_medidas_check.replace("metros", "");
                        metros_medidas_check = metros_medidas_check.replace(" ", "");
                        line += "<div class='line_component_metros'>Unidad " + n + ": <br>Metros: <input type='text' class='metros_medida_componente' value='" + metros_medidas_check + "'></div>";
                    }
                });
                if (tipo_medida == 1) {
                    for (var i = 0; i < line_to_add; i++) {
                        line += "<div class='line_component_metros'>Unidad: <br>Ancho: <input type='text' class='ancho_medida_componente'> Alto: <input type='text' class='alto_medida_componente'> <span class='total_metros_componente'></span><input type='hidden' class='total_metros_componente_hidden'></div>";
                    }
                } else if (tipo_medida == 2) {
                    line += "<div class='line_component_metros'>Unidad: <br>Metros: <input type='text' class='metros_medida_componente'></div>";
                }
            }
            //Eliminamos
            if (exist_lineas_medidas > number_product) {
                $(line_component).find('.linea_medida_componente_check').each(function (index, element) {
                    var n = index + 1;
                    if (tipo_medida == 1) {
                        var ancho_medidas = $(element).find('.linea_medida_componente_check_ancho').html();
                        var alto_medidas = $(element).find('.linea_medida_componente_check_alto').html();
                        ancho_medidas = ancho_medidas.replace("m", "");
                        ancho_medidas = ancho_medidas.replace(" ", "");
                        alto_medidas = alto_medidas.replace(" ", "");
                        alto_medidas = alto_medidas.replace("m", "");
                        alto_medidas = alto_medidas.replace(/,/g, ".");
                        ancho_medidas = ancho_medidas.replace(/,/g, ".");
                        var metros_medidas = ancho_medidas * alto_medidas;
                        metros_medidas = parseFloat(metros_medidas);
                        metros_medidas = metros_medidas.toFixed(2);
                        line += "<div class='line_component_metros'>Unidad " + n + ": <br>Ancho: <input type='text' class='ancho_medida_componente' value='" + ancho_medidas + "'> Alto: <input type='text' class='alto_medida_componente' value='" + alto_medidas + "'> <span class='total_metros_componente'>" + metros_medidas + "</span><span><i class='fa fa-times-circle btn_delete_line_component aria-hidden='true'></i></span><input type='hidden' class='total_metros_componente_hidden' value='" + metros_medidas + "'></div>";
                    } else if (tipo_medida == 2) {
                        var metros_medidas_check = $(element).find('.linea_medida_componente_check_metros').html();
                        metros_medidas_check = metros_medidas_check.replace("metros", "");
                        metros_medidas_check = metros_medidas_check.replace(" ", "");
                        line += "<div class='line_component_metros'>Unidad " + n + ": <br>Metros: <input type='text' class='metros_medida_componente' value='" + metros_medidas_check + "'><span><i class='fa fa-times-circle btn_delete_line_component aria-hidden='true'></i></span></div>";
                    }
                });
            }
        }
        $('#modal_add_medidas_component').find('.modal-body').html(line);
        $('#modal_add_medidas_component').modal('show');
    }

    function btn_save_medidas_componente(e) {
        e.preventDefault();
        var modal = $(this).closest('#modal_add_medidas_component');
        var linea_medida = $(modal).find('.line_component_metros');
        var data_line = [];
        var content_lineas = "";
        var num = 0;
        var metros_totales = 0.00;
        if (tipo_medida == 1) {
            $(linea_medida).each(function (index, element) {
                num = num + 1;
                var ancho = $(this).find('.ancho_medida_componente').val();
                var alto = $(this).find('.alto_medida_componente').val();
                var metros = $(this).find('.total_metros_componente_hidden').val();
                metros = parseFloat(metros);
                metros = metros.toFixed(2);
                metros_totales = parseFloat(metros_totales) + parseFloat(metros);

                var array_line_medida = {
                    'ancho': ancho,
                    'alto': alto,
                    'metros': metros
                }

                $(line_component).find('.ancho').val(ancho);
                $(line_component).find('.alto').val(alto);

                if (metros <= 0 || alto <= 0 || ancho <= 0) {
                    check_error += "1";
                }

                data_line.push(array_line_medida);

                var result = "<div class='row linea_medida_componente_check col-xs-12 col-md-12'><div class='col-md-12 col-xs-12'><div class='col-xs-1 col-md-1'>" + num + " - </div><div class='col-xs-2 col-md-2'>Ancho: <span class='linea_medida_componente_check_ancho'>" + ancho + " m </span></div><div class='col-xs-2 col-md-2'> Alto: <span class='linea_medida_componente_check_alto'>" + alto + "m </span></div><div class='col-xs-2 col-md-2'> Total: <span class='linea_medida_componente_check_metros'>" + metros + "</span> m<sup>2</sup></div></div><input type='hidden' class='ancho' name='ancho[]' value=''> <input type='hidden' class='alto' name='alto[]' value=''></div>";
                content_lineas += result;

            })
        } else if (tipo_medida == 2) {
            //metros_medida_componente
            $(linea_medida).each(function (index, element) {
                num = num + 1;
                var metros = $(this).find('.metros_medida_componente').val();
                metros = metros.replace(/,/g, ".");
                metros = parseFloat(metros);
                metros = metros.toFixed(2);
                metros_totales = parseFloat(metros_totales) + parseFloat(metros);

                var array_line_medida = {
                    'metros': metros
                }

                $(line_component).find('.metros').val(metros);

                if (metros <= 0) {
                    check_error += "1";
                }

                data_line.push(array_line_medida);

                var result = "<div class='row linea_medida_componente_check col-md-12 col-xs-12'><div class='col-md-12 col-xs-12'> " + num + " - <span class='linea_medida_componente_check_metros'>" + metros + "metros </span></div><span></span><input type='hidden' class='metros' name='metros[]' value=''></div>";
                content_lineas += result;

            })
        }

        var precio = $(line_component).find('.precio_fabrica').val();
        if (isNaN(precio)) {
            precio = $(line_component).find('.precio_almacen').val();
        }

        precio = parseFloat(precio);
        metros_totales = parseFloat(metros_totales);

        var precio_total = precio * metros_totales;
        precio_total = precio_total.toFixed(2);

        if (check_error == 0) {

            $(line_component).find('.total_component').html(precio_total);

            $(line_component).find('.content_lineas_medidas_componente').removeClass('hidden').html('').append(content_lineas);

            $(line_component).find('.total_units_metros').val(metros_totales);

            var total = 0;
            $('.product_price').each(function () {
                total += parseFloat($(this).html());
            })
            $('.product_price_lacado').each(function () {
                total += parseFloat($(this).html());
            })
            total = total.toFixed(2);
            $('.price_total').val(total);

            $('#modal_add_medidas_component').modal('hide');
        } else {
            toastr.options = {
                "positionClass": "toast-top-full-width"
            }
            toastr.error("Medidas", mensaje_faltan_medidas);
        }

    }

    function calc_medidas_componente(e) {

        var unidades = "";
        var ancho = "";
        var alto = "";

        this.value = this.value.replace(/[^0-9\.,]/g, '');

        if (max_measure_resumen > 0) {
            max_measure = max_measure_resumen;
        }

        if (!isNaN($(this).closest('.line_component_metros').find('.unidades_medida_componente').val())) {
            unidades = $(this).closest('.line_component_metros').find('.unidades_medida_componente').val();
            unidades = unidades.replace(/,/g, ".");
            unidades = parseFloat(unidades);
            unidades = unidades.toFixed(2);
        }

        if (!isNaN($(this).closest('.line_component_metros').find('.ancho_medida_componente').val())) {
            ancho = $(this).closest('.line_component_metros').find('.ancho_medida_componente').val();
            ancho = ancho.replace(/,/g, ".");
            ancho = parseFloat(ancho);
            ancho = ancho.toFixed(2);
        }

        if (!isNaN($(this).closest('.line_component_metros').find('.alto_medida_componente').val())) {
            alto = $(this).closest('.line_component_metros').find('.alto_medida_componente').val();
            alto = alto.replace(/,/g, ".");
            alto = parseFloat(alto);
            alto = alto.toFixed(2);
        }

        //Comprobacion medida máxima
        if (max_measure > 0) {
            if (parseFloat(alto) > parseFloat(max_measure) || parseFloat(ancho) > parseFloat(max_measure)) {
                check_error = 1;
                //toastr.options = {
                //    "positionClass": "toast-top-full-width"
                //}
                //toastr.warning("Medidas", "Medida máxima:" + max_measure);
                $(this).addClass('border-warning');
            } else {
                if (this.value > 0) {
                    $(this).removeClass('border-warning');
                    check_error = 0;
                }
            }
        }

        var metros = alto * ancho * unidades;

        if (!isNaN(metros)) {
            metros = parseFloat(metros);
            metros = metros.toFixed(2);
            $(this).closest('.line_component_metros').find('.total_metros_componente').html(metros + " m<sup>2</sup>");
            $(this).closest('.line_component_metros').find('.total_metros_componente_hidden').val(metros);
        }
    }

    function metros_medida_componente(e) {

        this.value = this.value.replace(/[^0-9\.,]/g, '');

        var metros = $(this).closest('.line_component_metros').find('.metros_medida_componente').val();
        metros = metros.replace(/,/g, ".");
        metros = parseFloat(metros);
        metros = metros.toFixed(2);

        if (max_measure_resumen > 0) {
            max_measure = max_measure_resumen;
        }

        //Comprobacion medida máxima
        if (parseFloat(metros) > parseFloat(max_measure)) {
            check_error = 1;
            //toastr.options = {
            //    "positionClass": "toast-top-full-width"
            //}
            //toastr.warning("Medidas", "Medida máxima:" + max_measure);
            $(this).addClass('border-warning');

        } else {
            check_error = 0;
            if (metros > 0) {
                $(this).removeClass('border-warning');
            }
        }
    }

    var precio_component = "";
    var line_btn_edit = "";
    var tipo_medida_resumen = "";
    var product_price = "";
    var max_measure_resumen = "";

    function btn_edit_units_medidas_resumen(e) {

        max_measure_resumen = $(this).closest('.line_product_component').find('.max_measure').val();
        max_measure_resumen.replace(/,/g, ".");
        max_measure_resumen = parseFloat(max_measure_resumen);
        max_measure_resumen = max_measure_resumen.toFixed(2);
        product_price = $(this).closest('.line_product_component').find('.product_price');
        line_btn_edit = $(this).closest('.line_product_component').find('.content_lines_component');
        tipo_medida_resumen = $(this).closest('.line_product_component').find('.tipo_medida_component').val();
        var numero_lineas = $(this).closest('.line_product_component').find('.change_line_product_component_unit').val();
        var total_lineas_medidas = $(this).closest('.line_product_component').find('.segunda_linea_producto').length;
        var lineas_medidas = $(this).closest('.line_product_component').find('.segunda_linea_producto');
        precio_component = $(this).closest('.line_product_component').find('.price_component_hidden').val();
        $('#modal_add_medidas_component').find('.btn_save_medidas_componente').removeClass("btn_save_medidas_componente").addClass('btn_save_medidas_componente_resumen');

        if (tipo_medida_resumen == 1) {
            var contenido_editar_medidas = "<div class='row content_medidas_metros_cuadrados content_medidas_metros'><div class='col-md-2 col-xs-2'>Unidades</div><div class='col-md-3 col-xs-3'>Ancho</div><div class='col-md-3 col-xs-3'>Alto</div><div class='col-md-3 col-xs-3'>Metros<sup>2</sup></div>";
        } else if (tipo_medida_resumen == 2) {
            var contenido_editar_medidas = "<div class='row content_medidas_metros_cuadrados content_medidas_metros'><div class='col-md-4 col-xs-4'>Unidades</div><div class='col-md-4 col-xs-4'>Metros</div><div class='col-md-4 col-xs-4'>&nbsp;</div>";
        }

        /*
         
         var content_add_medidas = "<div class='hidden row content_medidas_metros_lineales content_medidas_metros'><div class='row col-md-12 col-xs-12'><div class='col-md-4 col-xs-4'>Unidades</div><div class='col-md-4 col-xs-4'>Metros</div><div class='col-md-4 col-xs-4'>Opciones</div></div><div class='col-md-12 col-xs-12 row line_component_metros line_component mt-10'><div class='col-xs-4 col-md-4'><input type='number' value='0' min='0' class='unidades_medida_componente form-control'></div><div class='col-xs-4 col-md-4'><input type='text' class='form-control metros_medida_componente' maxlength='5' placeholder='0.000'></div><div class='col-xs-4 col-md-4 content_options_components'><span class='new_line_size_component text-success fa fa-plus'></span><span class='delete_line_size_component text-danger fa fa-close'></span></div></div></div>";
         
         */

        var lines = "";

        //Mismo numero de unidades (editar lineas)
        $(lineas_medidas).each(function (index, element) {
            var n = $(this).find('.unidades_unidades_medidas').val();
            //medidas metros cuadrados
            if (tipo_medida_resumen == 1) {
                var ancho_medidas = $(element).find('.resumen_medidas_ancho').html();
                var alto_medidas = $(element).find('.resumen_medidas_alto').html();
                var unidades_medidas = $(element).find('.resumen_medidas_unidades').html();
                ancho_medidas = ancho_medidas.replace("m", "");
                ancho_medidas = ancho_medidas.replace(" ", "");
                alto_medidas = alto_medidas.replace(" ", "");
                alto_medidas = alto_medidas.replace("m", "");
                alto_medidas = alto_medidas.replace(/,/g, ".");
                ancho_medidas = ancho_medidas.replace(/,/g, ".");
                var metros_medidas = ancho_medidas * alto_medidas * unidades_medidas;
                metros_medidas = parseFloat(metros_medidas);
                metros_medidas = metros_medidas.toFixed(2);
                lines += "<div class='col-md-12 col-xs-12 row line_component_metros line_component mt-10'><div class='col-md-2 col-xs-2'><input type='text' class='unidades_medida_componente form-control' value='" + unidades_medidas + "'></div><div class='col-md-3 col-xs-3'><input type='text' class='ancho_medida_componente form-control' value='" + ancho_medidas + "'></div><div class='col-md-3 col-xs-3'><input type='text' class='alto_medida_componente form-control' value='" + alto_medidas + "'></div><div class='col-md-3 col-xs-3'><span class='total_metros_componente'>" + metros_medidas + "</span></div><div class='col-md-4 col-xs-4'>&nbsp;</div><input type='hidden' class='total_metros_componente_hidden' value='" + metros_medidas + "'><input type='hidden' class='unidades_unidades_medidas' value='" + n + "'><input type='hidden' class='price_medida' value='" + precio_component + "'></div>";
            } else if (tipo_medida_resumen == 2) {
                var metros_medidas_check = $(element).find('.resumen_medidas_metros').html();
                metros_medidas_check = metros_medidas_check.replace("metros", "");
                metros_medidas_check = metros_medidas_check.replace(" ", "");
                var unidades_medidas = $(element).find('.resumen_medidas_unidades').html();
                lines += "<div class='row content_medidas_metros_lineales content_medidas_metros col-md-12 col-xs-12'><div class='col-md-4 col-xs-4'><input type='text' class='unidades_medida_componente form-control' value='" + unidades_medidas + "'></div>  <div class='col-md-4 col-xs-4'><input type='text' class='metros_medida_componente form-control' value='" + metros_medidas_check + "'></div><input type='hidden' class='unidades_unidades_medidas' value='" + n + "'><input type='hidden' class='price_medida' value='" + precio_component + "'></div>";
            }
        })
        //Añadimos nuevas lineas

        $('#modal_add_medidas_component').find('.modal-body').html(contenido_editar_medidas + lines).append("</div>");
        $('#modal_add_medidas_component').modal('show');
    }

    function change_units_resumen_component() {
        var units = $(this).val();
        var arumiv = $(this).closest('.line_product_component').find('.arumiv').val();
        product_price = $(this).closest('.line_product_component').find('.price_component_hidden').val();
        var total_price = parseFloat(product_price) * units;
        total_price = parseFloat(total_price) / arumiv;
        total_price = total_price.toFixed(2);
        $(this).closest('.line_product_component').find('.total_component').html(total_price);
        $(this).closest('.line_product_component').find('.unidades_unidades_medidas').val(units);
        save_cart_session();
    }

    function btn_save_medidas_componente_resumen(e) {
        e.preventDefault();
        var modal = $(this).closest('#modal_add_medidas_component');
        var linea_medida = $(modal).find('.line_component_metros');
        var data_line = [];
        var content_lineas = "";
        var metros_totales = 0.00;
        var precio_total = 0.00;
        if (tipo_medida_resumen == 1) {
            $(linea_medida).each(function (index, element) {
                var ancho = $(element).find('.ancho_medida_componente').val();
                var alto = $(element).find('.alto_medida_componente').val();
                var metros = $(element).find('.total_metros_componente_hidden').val();
                var num = $(element).find('.unidades_unidades_medidas').val();
                var unidades = $(element).find('.unidades_medida_componente').val();
                var precio_component_line = $(element).find('.price_medida').val();
                alto = alto.replace(/,/g, ".");
                ancho = ancho.replace(/,/g, ".");
                metros = parseFloat(metros);
                metros = metros.toFixed(2);
                metros_totales = parseFloat(metros_totales) + parseFloat(metros);

                var array_line_medida = {
                    'ancho': ancho,
                    'alto': alto,
                    'metros': metros
                }

                $(line_component).find('.ancho').val(ancho);
                $(line_component).find('.alto').val(alto);

                if (metros <= 0 || alto <= 0 || ancho <= 0) {
                    check_error += "1";
                }

                data_line.push(array_line_medida);

                var precio_total_linea = metros * precio_component_line;
                precio_total += precio_total_linea;
                var result = "<div class='segunda_linea_producto mt-10 row col-md-12 col-xs-12 text-left'><div class='col-md-2 col-xs-2'><span class='resumen_medidas_unidades'>" + unidades + "</span> Uds. </div><div class='col-md-2 col-xs-2'>Ancho: <span class='resumen_medidas_ancho'>" + ancho + "</span> m</div><div class='col-md-2 col-xs-2'>Alto: <span class='resumen_medidas_alto'>" + alto + "</span> m</div> <div class='col-md-2 col-xs-2'>Total: " + metros + " m<sup>2</sup></div><input type='hidden' class='ancho_unidades_medidas' value='" + ancho + "'><input type='hidden' class='unidades_unidades_medidas' value='" + unidades + "'><input type='hidden' class='alto_unidades_medidas' value='" + alto + "'><input type='hidden' class='precio_unidades_medidas' value='" + precio_component_line + "'></div>";
                content_lineas += result;

            })
        } else {
            //metros_medida_componente
            $(linea_medida).each(function (index, element) {
                var metros = $(element).find('.metros_medida_componente').val();
                var num = $(element).find('.unidades_unidades_medidas').val();
                var unidades = $(element).find('.unidades_medida_componente').val();
                metros = metros.replace(/,/g, ".");
                metros = parseFloat(metros);
                metros = metros.toFixed(2);
                metros_totales = parseFloat(metros_totales) + parseFloat(metros);

                var array_line_medida = {
                    'metros': metros
                }

                $(line_component).find('.metros').val(metros);

                if (metros <= 0) {
                    check_error += "1";
                }

                data_line.push(array_line_medida);
                var precio_total_linea = metros * precio_component * unidades;
                precio_total += precio_total_linea;

                var result = "<div class='segunda_linea_producto mt-10 row col-md-12 col-xs-12 text-left'><span class='resumen_medidas_unidades'> " + unidades + "</span> Uds. de <span class='resumen_medidas_metros'>" + metros + "</span>metro(s)<input type='hidden' class='metros_unidades_medidas' value='" + metros + "'><input type='hidden' class='precio_unidades_medidas' value='" + precio_component + "'><input type='hidden' class='unidades_unidades_medidas' value='" + num + "'></div>";

                content_lineas += result;
            })
        }

        if (check_error == 0) {
            $(line_btn_edit).html('');
            $(line_btn_edit).html('').append(content_lineas);
            precio_total = parseFloat(precio_total);
            precio_total = precio_total.toFixed(2);
            $(product_price).html(precio_total);

            var total = 0;
            $('.product_price').each(function () {
                total += parseFloat($(this).html());
            })
            $('.product_price_lacado').each(function () {
                total += parseFloat($(this).html());
            })
            total = total.toFixed(2);

            $('.price_total').val(total);

            $('#modal_add_medidas_component').modal('hide');
        } else {
            toastr.options = {
                "positionClass": "toast-top-full-width"
            }
            toastr.error("Medidas", mensaje_faltan_medidas);
        }

    }

    function number_units_components(e) {
        e.preventDefault();
        console.log("Modificamos unidades");
    }

    function new_line_size_component(e) {
        e.preventDefault();
        var new_line = $(this).closest('.line_component').clone();
        var inputs = $(new_line).find('input');
        $(inputs).each(function (index, element) {
            $(element).val(0);
        });
        $(this).closest('.content_medidas_metros').append(new_line);
    }

    function delete_line_size_component(e) {
        e.preventDefault();
        $(this).closest('.line_component_metros').remove();
    }

    function close_modal_medidas(e) {
        e.preventDefault();
        if (confirm(mensaje_cerrar_ventana)) {
            $('#modal_add_medidas_component').modal('hide');
            $('#modal_add_product').css('overflow', 'scroll');
        }
    }

    function close_modal_notas(e) {
        e.preventDefault();
        if (confirm('¿Desea cerrar la ventana sin guardar las notas?')) {
            $('#modal_edit_notas').modal('hide');
        }
    }

    function close_modal_concepto(e) {
        e.preventDefault();
        if (confirm('¿Desea cerrar la ventana sin guardar el concepto?')) {
            $('#modal_add_concepto').modal('hide');
        }
    }

    function focusout_medidas_component(e) {
        e.preventDefault();
        var value = $(this).val();
        value = value.replace(/,/g, ".");
        $(this).val(value);
        var tecla = (document.all) ? e.keyCode : e.which;
        if ($(this).val().length == 1 && tecla != 8) {
            $(this).val($(this).val() + ".");
        }
        if ($(this).val() <= 0) {
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
        $(this).val(pad($(this).val(), 5, 0));
    }

    //Paginado presupuestos / pedidos
    function page_pres_ped(e) {
        e.preventDefault();
        var type_doc = $(this).closest('.pagination').attr('type_doc');
        var page = $(this).attr('page');
        var limit_page = $('.limit_page').val();
        var limit_page_ped = $('.limit_page_ped').val();
        var id_cliente = $('.id_cliente').val();
        var data = {
            'type_doc': type_doc,
            'page': page,
            'limit_page': limit_page,
            'limit_page_ped': limit_page_ped,
            'id_cliente': id_cliente
        }

        $.ajax({
            url: "mods/mod_products/ajax/page_historial.php",
            type: "post",
            data: data,
            success: function (response) {
                $('.container_historial').html('').html(response);
                btn_delete_line_component();
                icon_exchange_presupuestos();
                icon_trash_presupuestos();
                icon_repeat_presupuestos();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }

    //Expositores
    function unidades_expositores(e) {
        var units = $(this).val();
        var precio = $(this).parent().find('.precio_expo').val();
        var total_line = parseInt(units) * parseFloat(precio);
        var total_expo = 0.00;
        var div_content = $(this).closest('.contenido_expositor');

        $(this).parent().find('.total_line').val(total_line);
        $(this).parent().find('.info_submit').val(units);


        $('.total_line').each(function () {
            var $this_val = $(this).val();
            if (!isNaN($this_val) && $this_val > 0) {
                total_expo = parseFloat(total_expo) + parseFloat($this_val);
            }
        });
        total_expo = parseFloat(total_expo).toFixed(2);
        $('.total_expo').html(total_expo);

        if (total_expo > 0) {
            $('.div_buttons_expo').removeClass('invisible');
            $('.div_resumen_expositores').removeClass('hidden');

            $('.content_resumen_expositores').empty();

            $('.contenido_expositor').each(function () {
                var units_expo = $(this).find('.unidades_expositores').val();
                if (!isNaN(units_expo) && units_expo > 0) {
                    var precio_expo = $(this).find('.precio_expo').val();
                    var title_expo = $(this).find('.text').html().toLowerCase();
                    var total_expo = parseInt(units_expo) * parseFloat(precio_expo);

                    var line_info_product = "<div class='line_resumen_expositores text-left'>- Muestra " + title_expo + " " + units_expo + " Uds(s) " + total_expo + "€ </div>";
                    $('.content_resumen_expositores').prepend(line_info_product);
                }
            });

        } else {
            $('.div_buttons_expo').addClass('invisible');
            $('.div_resumen_expositores ').addClass('hidden');
        }

    }


    function change_units_resumen_expositor(e) {
        e.preventDefault();
        var units = $(this).val();
        var div_content = $(this).closest('.line_expositor');
        var precio = $(div_content).find('.price_expositor_hidden').val();
        var precio_total = parseInt(units) * parseFloat(precio);
        precio_total = precio_total.toFixed(2);
        $(div_content).find('.product_price').html(precio_total);

        var total = 0;
        $('.product_price').each(function () {
            total += parseFloat($(this).html());
        })
        $('.product_price_lacado').each(function () {
            total += parseFloat($(this).html());
        })
        total = total.toFixed(2);
        $('.price_total').val(total);

        save_cart_session();
    }

    function unidades_expositores_modal(e) {
        e.preventDefault();
        $('.div_submit_product').removeClass('hidden');
        var units = $(this).val();
        $(this).parent().find('.info_submit').val(units);
    }


    function email_consulta(e) {
        e.preventDefault();
        $.ajax({
            url: "mods/mod_company/ajax/send_email_consulta.php",
            type: "post",
            data: $(this).serializeArray(),
            success: function (response) {
                if (response == 1) {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.success("Consulta", "Consulta enviada correctamente");
                    setTimeout(function () {
                        window.location.reload()
                    }, 800);
                } else {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.error("Consulta", "Error al enviar la consulta");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function remember_pass(e) {
        e.preventDefault();

        toastr.options = {
            "positionClass": "toast-top-full-width"
        }

        $.ajax({
            url: "mods/mod_customers/ajax/send_email_remember.php",
            type: "post",
            data: $(this).serializeArray(),
            dataType: 'json',
            success: function (response) {
                if (response['error'] == 0) {
                    toastr.success(response['message'], "Recordar contraseña");
                } else {
                    toastr.error(response['message'], "Recordar contraseña");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function change_pass(e) {
        e.preventDefault();
        $.ajax({
            url: "mods/mod_customers/ajax/change_pass.php",
            type: "post",
            data: $(this).serializeArray(),
            success: function (response) {
                if (response == 1) {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.success("Consulta", "Contraseña modificada correctamente");
                    setTimeout(function () {
                        window.location.href = 'index.php';
                    }, 800);
                } else {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.error("Consulta", "Error al modificar la consulta");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function set_email_cliente(e) {
        e.preventDefault();
        $.ajax({
            url: "mods/mod_customers/ajax/update_email.php",
            type: "post",
            dataType: 'json',
            data: $(this).serializeArray(),
            success: function (response) {
                toastr.options = {
                    "positionClass": "toast-top-full-width"
                }
                if (response['error'] == "0") {
                    toastr.success(response['message'], "Email cliente");
                    setTimeout(function () {
                        window.location.reload()
                    }, 800);
                } else {
                    toastr.error(response['message'], "Email cliente");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function button_send_form_email(e) {
        e.preventDefault();
        var submit = false;
        if ($('.set_email').val() != $('.set_email_check').val()) {
            $('.check_set_email').html("Datos incorrectos").addClass('text-danger');
        } else {
            if ($(".check_input_email").val().indexOf('@', 0) == -1 || $(".check_input_email").val().indexOf('.', 0) == -1) {
                $('.check_set_email').html("Datos incorrectos").addClass('text-danger');
            } else {
                $('.check_set_email').html("");
                submit = true;
            }
            if (submit) {
                $('.set_email_cliente').submit();
            }
        }
    }

    function check_password_change(e) {
        e.preventDefault();
        var pass1 = $('.change_pass1').val();
        var pass2 = $('.change_pass2').val();
        var cif_check = $('.check_change_pass_cif').val();
        var cif = $('.cif_remember').val();

        if (pass1 == pass2 && cif_check == cif) {
            $('.check_change_pass').addClass('hidden');
            $('.button_send_form_change').removeClass('hidden');
        } else {
            $('.check_change_pass').addClass('text-danger').removeClass('text-success').html("No coinciden las contraseñas").removeClass('hidden');
            $('.button_send_form_change').addClass('hidden');
        }
    }

    //Funciones concepto
    function btn_add_concepto(e) {
        e.preventDefault();
        var is_pedido = $('.is_pedido').val();
        $('.type_document_concepto').html(is_pedido);
    }

    function btn_add_editar_datos_cliente(e) {
        e.preventDefault();
        $('#modal_editar_datos_cliente').find('.editor_text').summernote();
        $('#modal_editar_datos_cliente').modal('show');
    }

    function btn_add_concepto_historial(e) {
        e.preventDefault();
        var id_pres_modal = $('.view_conceptos').find('.id_pres_view').val();
        var data = $('.view_conceptos').serializeArray();
        data.push({name: 'id_pres', value: id_pres_modal});
        $.ajax({
            url: "mods/mod_products/ajax/add_line_concepto.php",
            type: "post",
            data: data,
            dataType: 'json',
            success: function (response) {
                if (response['error'] == 0) {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.success("Concepto", response['message']);
                    $('.content_conceptos_view').append(response['content']);
                } else {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.error("Concepto", response['message']);
                }
                $('.view_conceptos').find("input[type=text], textarea, select").val("");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function btn_flotante_add_concepto(e) {
        e.preventDefault();
        var id_pres_modal = $('.id_pres_modal').val();
        var data = $('.add_concepto').serializeArray();
        data.push({name: 'id_pres', value: id_pres_modal});
        $.ajax({
            url: "mods/mod_products/ajax/add_line_concepto.php",
            type: "post",
            data: data,
            dataType: 'json',
            success: function (response) {
                if (response['error'] == 0) {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.success(response['message'], "Concepto");
                } else {
                    toastr.options = {
                        "positionClass": "toast-top-full-width"
                    }
                    toastr.error(response['message'], "Concepto");
                }
                $('.add_concepto').find("input[type=text], textarea, select").val("");
                $('#modal_add_concepto').modal('hide');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function btn_view_conceptos(e) {
        e.preventDefault();
        var data = {'id_pres': $(this).attr('id_pres')};
        $.ajax({
            url: "mods/mod_customers/ajax/view_conceptos.php",
            type: "post",
            data: data,
            success: function (response) {
                $('#modal_view_conceptos').find('.modal-body').html(response);
                $('#modal_view_conceptos').modal('show');
                delete_concepto();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

//div, cantidad, ancho, alto, group_color, color, 'corredera_completa', una_hoja
    function calc_prices_corredera(div, cantidad, ancho, alto, group_color, color, tipo_producto, una_hoja) {
        var data = {'cantidad': cantidad, 'ancho': ancho, 'alto': alto, 'group_color': group_color, 'color': color, 'tipo_producto': tipo_producto, 'una_hoja': una_hoja};
        if (cantidad > 0 && ancho > 0 && alto > 0) {
            $.ajax({
                url: "mods/mod_products/ajax/calc_prices_corredera.php",
                type: "post",
                data: data,
                dataType: 'json',
                success: function (response) {
                    $(div).find('.product_price').html(response['precio_total']);
                    $(div).find('.product_price').attr('span_value', response['precio_total']);
                    if ($(div).hasClass('corredera_completa')) {
                        $(div).find('.precio_hoja').val(response['precio_hoja']);
                        $(div).find('.cantidad_hojas').val(response['cantidad_hojas']);
                        $(div).find('.precio_marco').val(response['precio_marco']);
                        $(div).find('.ancho_hojas').val(response['ancho_hojas']);
                    }
                    update_session_ral(color, '2735');
                    var total = 0.00;
                    $('.product_price').each(function () {
                        if ($.isNumeric($(this).html())) {
                            total += parseFloat($(this).html());
                        }
                    })
                    $('.product_price_lacado').each(function () {
                        total += parseFloat($(this).html());
                    })
                    total = total.toFixed(2);
                    $('.price_total').val(total);
                    save_cart_session();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }
            });
        } else {
            $(div).find('.product_price').html("0");
            var total = 0.00;
            $('.product_price').each(function () {
                if ($.isNumeric($(this).html())) {
                    total += parseFloat($(this).html());
                }
            })
            $('.product_price_lacado').each(function () {
                total += parseFloat($(this).html());
            })
            total = total.toFixed(2);
            $('.price_total').val(total);
        }
    }

    function completa_una_hoja(e) {
        if ($(this).is(':checked')) {
            $(this).val("1");
        } else {
            $(this).val("0");
        }

        var div = $(this).closest('.form-horizontal');
        var cantidad = $(this).closest('.line_product').find('.cantidad_corredera_completa').val();
        var ancho = $(this).closest('.line_product').find('.ancho_corredera_completa').val();
        var alto = $(this).closest('.line_product').find('.alto_corredera_completa').val();
        var group_color = $(this).closest('.line_product').find('.group_color_corredera_completa').attr('span_value');
        var color = $(this).closest('.line_product').find('.color_corredera_completa').attr('span_value');
        var una_hoja = $(this).closest('.line_product').find('.completa_una_hoja').val();

        calc_prices_corredera(div, cantidad, ancho, alto, group_color, color, 'corredera_completa', una_hoja);

    }

    function corredera_completa(e) {

        var div = $(this).closest('.form-horizontal');
        var error_medidas_mosquiteras = 0;
        var medida = $(this).val();
        var max_anc = $(this).closest('.content_medidas').find('.max').val();
        var min_anc = $(this).closest('.content_medidas').find('.min').val();
        var tecla = (document.all) ? e.keyCode : e.which;

        if (!$(this).hasClass('no_check')) {
            if ($(this).val().length > 4 && tecla != 8 || tecla == 44 || tecla == 46 || tecla < 48 && tecla != 8 && tecla != 0 || tecla > 57 && tecla != 8 && tecla != 0) {
                e.preventDefault();
            }

            if ($(this).val().length == 1 && tecla != 8 && !$(this).hasClass('cantidad_corredera_completa')) {
                $(this).val($(this).val() + ".");
            }

            if (pad(medida, 5, 0) < min_anc || pad(medida, 5, 0) > max_anc || pad(medida, 5, 0) <= 0) {
                error_medidas_mosquiteras = 1;

                $(this).addClass('border-warning').attr('title', "Valor mínimo: " + min_anc + " y máximo: " + max_anc + ". Ejemplo formato: 1.010");
                $(this).removeClass('border-success');
            } else {
                $(this).removeClass('border-warning');
                $(this).addClass('border-success');
            }

            if ($(this).hasClass('border-warning') && $(this).val().length == 5 && tecla == 8) {
                $(this).val('');
            }
        }

        var cantidad = $(this).closest('.line_product').find('.cantidad_corredera_completa').val();
        var ancho = $(this).closest('.line_product').find('.ancho_corredera_completa').val();
        var alto = $(this).closest('.line_product').find('.alto_corredera_completa').val();
        var group_color = $(this).closest('.line_product').find('.group_color_corredera_completa').attr('span_value');
        var color = $(this).closest('.line_product').find('.color_corredera_completa').attr('span_value');
        var una_hoja = $(this).closest('.line_product').find('.completa_una_hoja').val();

        if (error_medidas_mosquiteras <= 0) {
            calc_prices_corredera(div, cantidad, ancho, alto, group_color, color, 'corredera_completa', una_hoja);
        }
    }

    function cantidad_corredera_completa() {

        var div = $(this).closest('.form-horizontal');

        var cantidad = $(this).closest('.line_product').find('.cantidad_corredera_completa').val();
        var ancho = $(this).closest('.line_product').find('.ancho_corredera_completa').val();
        var alto = $(this).closest('.line_product').find('.alto_corredera_completa').val();
        var group_color = $(this).closest('.line_product').find('.group_color_corredera_completa').attr('span_value');
        var color = $(this).closest('.line_product').find('.color_corredera_completa').attr('span_value');
        var una_hoja = $(this).closest('.line_product').find('.completa_una_hoja').val();

        calc_prices_corredera(div, cantidad, ancho, alto, group_color, color, 'corredera_completa', una_hoja);
    }

    function corredera_marco(e) {

        var div = $(this).closest('.form-horizontal');
        var error_medidas_mosquiteras = 0;
        var medida = $(this).val();
        var max_anc = $(this).closest('.content_medidas').find('.max').val();
        var min_anc = $(this).closest('.content_medidas').find('.min').val();
        var tecla = (document.all) ? e.keyCode : e.which;

        if (!$(this).hasClass('no_check')) {
            if ($(this).val().length > 4 && tecla != 8 || tecla == 44 || tecla == 46 || tecla < 48 && tecla != 8 && tecla != 0 || tecla > 57 && tecla != 8 && tecla != 0) {
                e.preventDefault();
            }

            if ($(this).val().length == 1 && tecla != 8) {
                $(this).val($(this).val() + ".");
            }

            if (pad(medida, 5, 0) < min_anc || pad(medida, 5, 0) > max_anc || pad(medida, 5, 0) <= 0) {
                error_medidas_mosquiteras = 1;

                $(this).addClass('border-warning').attr('title', "Valor mínimo: " + min_anc + " y máximo: " + max_anc + ". Ejemplo formato: 1.010");
                $(this).removeClass('border-success');
            } else {
                $(this).removeClass('border-warning');
                $(this).addClass('border-success');
            }

            if ($(this).hasClass('border-warning') && $(this).val().length == 5 && tecla == 8) {
                $(this).val('');
            }
        }

        var cantidad = $(this).closest('.line_product').find('.cantidad_corredera_marco').val();
        var ancho = $(this).closest('.line_product').find('.ancho_corredera_marco').val();
        var alto = $(this).closest('.line_product').find('.alto_corredera_marco').val();
        var group_color = $(this).closest('.line_product').find('.group_color_corredera_marco').attr('span_value');
        var color = $(this).closest('.line_product').find('.color_corredera_marco').attr('span_value');

        if (error_medidas_mosquiteras <= 0) {
            calc_prices_corredera(div, cantidad, ancho, alto, group_color, color, 'corredera_marco', '0');
        }
    }

    function cantidad_corredera_marco() {

        var div = $(this).closest('.form-horizontal');

        var cantidad = $(this).closest('.line_product').find('.cantidad_corredera_marco').val();
        var ancho = $(this).closest('.line_product').find('.ancho_corredera_marco').val();
        var alto = $(this).closest('.line_product').find('.alto_corredera_marco').val();
        var group_color = $(this).closest('.line_product').find('.group_color_corredera_marco').attr('span_value');
        var color = $(this).closest('.line_product').find('.color_corredera_marco').attr('span_value');

        calc_prices_corredera(div, cantidad, ancho, alto, group_color, color, 'corredera_marco', '0');
    }

    function corredera_hoja(e) {

        var div = $(this).closest('.form-horizontal');
        var error_medidas_mosquiteras = 0;
        var medida = $(this).val();
        var max_anc = $(this).closest('.content_medidas').find('.max').val();
        var min_anc = $(this).closest('.content_medidas').find('.min').val();
        var tecla = (document.all) ? e.keyCode : e.which;

        if (!$(this).hasClass('no_check')) {
            if ($(this).val().length > 4 && tecla != 8 || tecla == 44 || tecla == 46 || tecla < 48 && tecla != 8 && tecla != 0 || tecla > 57 && tecla != 8 && tecla != 0) {
                e.preventDefault();
            }

            if ($(this).val().length == 1 && tecla != 8) {
                $(this).val($(this).val() + ".");
            }

            if (pad(medida, 5, 0) < min_anc || pad(medida, 5, 0) > max_anc || pad(medida, 5, 0) <= 0) {
                error_medidas_mosquiteras = 1;

                $(this).addClass('border-warning').attr('title', "Valor mínimo: " + min_anc + " y máximo: " + max_anc + ". Ejemplo formato: 1.010");
                $(this).removeClass('border-success');
            } else {
                $(this).removeClass('border-warning');
                $(this).addClass('border-success');
            }

            if ($(this).hasClass('border-warning') && $(this).val().length == 5 && tecla == 8) {
                $(this).val('');
            }
        }

        var cantidad = $(this).closest('.line_product').find('.cantidad_corredera_hoja').val();
        var ancho = $(this).closest('.line_product').find('.ancho_corredera_hoja').val();
        var alto = $(this).closest('.line_product').find('.alto_corredera_hoja').val();
        var group_color = $(this).closest('.line_product').find('.group_color_corredera_hoja').attr('span_value');
        var color = $(this).closest('.line_product').find('.color_corredera_hoja').attr('span_value');

        if (error_medidas_mosquiteras <= 0) {
            calc_prices_corredera(div, cantidad, ancho, alto, group_color, color, 'corredera_hoja', '0');
        }
    }

    function cantidad_corredera_hoja() {

        var div = $(this).closest('.form-horizontal');
        var cantidad = $(this).closest('.line_product').find('.cantidad_corredera_hoja').val();
        var ancho = $(this).closest('.line_product').find('.ancho_corredera_hoja').val();
        var alto = $(this).closest('.line_product').find('.alto_corredera_hoja').val();
        var group_color = $(this).closest('.line_product').find('.group_color_corredera_hoja').attr('span_value');
        var color = $(this).closest('.line_product').find('.color_corredera_hoja').attr('span_value');

        calc_prices_corredera(div, cantidad, ancho, alto, group_color, color, 'corredera_hoja', '0');

    }

//    function ancho_medidas_corredera(e) {
//        e.preventDefault();
//        var div = $(this).closest('.form-horizontal');
//        var error_medidas_mosquiteras = 0;
//        var width_product = $(this).val();
//        var max_anc = $(this).closest('.content_medidas').find('.max').val();
//        var min_anc = $(this).closest('.content_medidas').find('.min').val();
//        var tecla = (document.all) ? e.keyCode : e.which;
//
//        if ($(this).val().length > 4 && tecla != 8 || tecla == 44 || tecla == 46 || tecla < 48 && tecla != 8 && tecla != 0 || tecla > 57 && tecla != 8 && tecla != 0) {
//            e.preventDefault();
//        }
//
//        if ($(this).val().length == 1 && tecla != 8) {
//            $(this).val($(this).val() + ".");
//        }
//
//        if (pad(width_product, 5, 0) < min_anc || pad(width_product, 5, 0) > max_anc || pad(width_product, 5, 0) <= 0) {
//            error_medidas_mosquiteras = 1;
//
//            $(this).addClass('border-warning').attr('title', "Valor mínimo: " + min_anc + " y máximo: " + max_anc + ". Ejemplo formato: 1.010");
//            $(this).removeClass('border-success');
//        } else {
//            $(this).removeClass('border-warning');
//            $(this).addClass('border-success');
//        }
//
//        if ($(this).hasClass('border-warning') && $(this).val().length == 5 && tecla == 8) {
//            $(this).val('');
//        }
//
//        //cantidad_marco, ancho_marco, alto_marco, cantidad_hoja, ancho_hoja, alto_hoja
//        var cantidad_marco = $(this).closest('.line_product').find('.cantidad_marco_corredera').val();
//        var ancho_marco = $(this).closest('.line_product').find('.ancho_marco_corredera').val();
//        var alto_marco = $(this).closest('.line_product').find('.alto_marco_corredera').val();
//        var cantidad_hoja = $(this).closest('.line_product').find('.cantidad_hoja_corredera').val();
//        var ancho_hoja = $(this).closest('.line_product').find('.ancho_hoja_corredera').val();
//        var alto_hoja = $(this).closest('.line_product').find('.alto_hoja_corredera').val();
//        var group_color_corredera = $(this).closest('.line_product').find('.group_color_corredera').val();
//        var color_product_corredera = $(this).closest('.line_product').find('.color_product_corredera').val();
//
//        if (error_medidas_mosquiteras <= 0) {
//            calc_prices_corredera(div, cantidad_marco, ancho_marco, alto_marco, cantidad_hoja, ancho_hoja, alto_hoja, group_color_corredera, color_product_corredera);
//        } else {
//            console.log("No enviamos");
//        }
//
//    }
//
//    function alto_medidas_corredera(e) {
//        e.preventDefault();
//        var div = $(this).closest('.form-horizontal');
//        var error_medidas_mosquiteras = 0;
//        var alto_product = $(this).val();
//        var max_alt = $(this).closest('.content_medidas').find('.max').val();
//        var min_alt = $(this).closest('.content_medidas').find('.min').val();
//        var tecla = (document.all) ? e.keyCode : e.which;
//
//        if ($(this).val().length > 4 && tecla != 8 || tecla == 44 || tecla == 46 || tecla < 48 && tecla != 8 && tecla != 0 || tecla > 57 && tecla != 8 && tecla != 0) {
//            e.preventDefault();
//        }
//
//        if ($(this).val().length == 1 && tecla != 8) {
//            $(this).val($(this).val() + ".");
//        }
//
//        if (pad(alto_product, 5, 0) < min_alt || pad(alto_product, 5, 0) > max_alt || pad(alto_product, 5, 0) <= 0) {
//            error_medidas_mosquiteras = 1;
//
//            $(this).addClass('border-warning').attr('title', "Valor mínimo: " + min_alt + " y máximo: " + max_alt + ". Ejemplo formato: 1.010");
//            $(this).removeClass('border-success');
//        } else {
//            $(this).removeClass('border-warning');
//            $(this).addClass('border-success');
//        }
//
//        if ($(this).hasClass('border-warning') && $(this).val().length == 5 && tecla == 8) {
//            $(this).val('');
//        }
//
//        //cantidad_marco, ancho_marco, alto_marco, cantidad_hoja, ancho_hoja, alto_hoja
//        var cantidad_marco = $(this).closest('.line_product').find('.cantidad_marco_corredera').val();
//        var ancho_marco = $(this).closest('.line_product').find('.ancho_marco_corredera').val();
//        var alto_marco = $(this).closest('.line_product').find('.alto_marco_corredera').val();
//        var cantidad_hoja = $(this).closest('.line_product').find('.cantidad_hoja_corredera').val();
//        var ancho_hoja = $(this).closest('.line_product').find('.ancho_hoja_corredera').val();
//        var alto_hoja = $(this).closest('.line_product').find('.alto_hoja_corredera').val();
//        var group_color_corredera = $(this).closest('.line_product').find('.group_color_corredera').val();
//        var color_product_corredera = $(this).closest('.line_product').find('.color_product_corredera').val();
//
//        if (error_medidas_mosquiteras <= 0) {
//            calc_prices_corredera(div, cantidad_marco, ancho_marco, alto_marco, cantidad_hoja, ancho_hoja, alto_hoja, group_color_corredera, color_product_corredera);
//        } else {
//            console.log("No enviamos");
//        }
//    }

    function check_size_file(file) {
        var upload_file = 0;
        var size = file[0].files[0].size;
        if (size < 1000000) {
            upload_file = 1;
        }
        return upload_file;
    }

    var btn_upload_croquis = "";
    function upload_croquis_increment(e) {
        e.preventDefault();
        var form = $(this);

        if (check_size_file($(form).find('.file_croquis')) == 1) {

            var form_data = new FormData(form[0]);

            toastr.options = {
                "positionClass": "toast-top-full-width"
            }

            $.ajax({
                url: "mods/mod_products/ajax/upload_croquis_file.php",
                data: form_data,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (response) {
                    if (response['error'] == 0) {
                        toastr.success(response['message'], response['title']);
                        $('#modal_add_croquis_incremento').modal('hide');
                    } else {
                        toastr.error(response['message'], response['title']);
                    }
                    $(btn_upload_croquis).closest('.col-md-12').find('.name_fichero_upload').html(response['file_title']).removeClass('text-danger').addClass('text-success');
                    $(btn_upload_croquis).closest('.col-md-12').find('.name_fichero_upload_hidden').html(response['file_title_hidden']);
                    $(btn_upload_croquis).html("Modificar adjunto");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }
            });
        } else {
            toastr.error("Limite de tamaño de archivo superado", "Croquis");
        }
    }

    function btn_adjuntar_croquis(e) {
        e.preventDefault();
        btn_upload_croquis = $(this);
    }

    function check_is_venta_industrial() {
        var is_almacen = $('.is_almacen');
        var exist_almacen = "-1";
        $(is_almacen).each(function () {
            if ($(this).val() == "1") {
                exist_almacen = "1";
            } else {
                exist_almacen = "0";
            }
        });
        return exist_almacen;
    }

    function form_customer_register(e) {
        e.preventDefault();

        toastr.options = {
            "positionClass": "toast-top-full-width"
        }

        $.ajax({
            url: "mods/mod_customers/ajax/registrar_cliente_final.php",
            type: "post",
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {
                if (response['error'] == 0) {
                    toastr.success(response['message'], "Registro cliente");
                } else {
                    toastr.error(response['message'], "Registro cliente");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function text_forma_pago(e) {
        e.preventDefault();
        $(this).prev().click();
    }

    function check_forma_pago() {
        var value_forma_pago = $(this).val();
        $('.content_forma_pago').addClass('hidden');
        $('.' + value_forma_pago).removeClass('hidden');
    }

    function upload_file_modal(e) {
        e.preventDefault();
        $('#modal_upload_file').modal('show');
    }

    function upload_logo_pres(e) {
        e.preventDefault();
        var form = $(this);

        if (check_size_file($(form).find('#archivo1')) == 1) {

            var form_data = new FormData(form[0]);

            toastr.options = {
                "positionClass": "toast-top-full-width"
            }

            $.ajax({
                url: "mods/mod_customers/ajax/upload_logo_pres.php",
                data: form_data,
                type: 'post',
                processData: false,
                dataType: 'json',
                contentType: false,
                success: function (response) {
                    if (response['error'] == 0) {
                        toastr.success(response['message'], response['title']);
                        $('#modal_add_croquis_incremento').modal('hide');
                    } else {
                        toastr.error(response['message'], response['title']);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax en submitFormNewDni");
                    console.log(jqXHR, textStatus, errorThrown);
                }
            });

        } else {
            toastr.error("Limite de tamaño de archivo superado", "Logo presupuesto");
        }
    }

    function form_editar_datos_cliente(e) {
        e.preventDefault();
        var id_pres_modal = $('.id_pres_modal').val();
        var data = $(this).serializeArray();
        data.push({name: 'id_pres', value: id_pres_modal});
        $.ajax({
            url: "mods/mod_products/ajax/editar_datos_cabecera_cliente.php",
            type: "post",
            data: data,
            //dataType: 'json',
            success: function (response) {
                console.log(response);
                /*if (response['error'] == 0) {
                 toastr.success(response['message'], "Número presupuesto");
                 $('#modal_change_num_pres').modal('hide');
                 } else {
                 toastr.error(response['message'], "Número presupuesto");
                 }*/
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }


    $(document).on('click', '.radio_product', check_product);
    $(document).on('click', '.group_colors', check_color);
    $(document).on('click', '.btn_copy_line', copy_table_line);
    $(document).on('click', '.radio_subfamilia', check_subfamilia);
    $(document).on('change', '.select_tipologia', select_tipologia);
    $(document).on('change click keypress keyup', '.input_width_product', change_width);
    $(document).on('change click keypress keyup', '.input_height_product', change_height);
    $(document).on('change', '.input_unit_product', calc_price_unit);
    $(document).on('focusout', '.input_height_product, .input_width_product', focusout);
    $(document).on('click', '.btn_option_product', btn_option_product);
    $(document).on('click', '.checkbox_options', checkbox_options_products);
    $(document).on('submit', '.form_product_add_product', form_add_new_product);
    $(document).on('change', '.change-subfamily', change_subfamily);
    $(document).on('click', '.color_product_line', change_color_product);
    $(document).on('click', '.change_color_product', select_color_product);
    $(document).on('change', '.change_components', show_components);
    $(document).on('change click keypress keyup', '.unit_increment', update_price_increment);

    //Funciones para iniciar sesión / mostrar el modal del login en cada uno de los casos en los que se tiene que msotrar

    $(document).on('click', '.btn_modal_ini', modal_login);
    $(document).on('click', '.btn_modal_ini_cli', modal_login_cliente);
    $(document).on('click', '.btn_modal_ini_cli_ref', modal_login_cliente_href);
    $(document).on('click', '.btn_modal_ini_pro', modal_login_product);
    $(document).on('click', '.btn_modal_ini_contacto', btn_modal_ini_contacto);
    //$(document).on('click', '.btn-editar-dire', modal_edit_dire);
    $('.btn-editar-dire').confirmation({
        title: '¿Desea añadir más productos?',
        btnOkLabel: texto_si,
        btnCancelLabel: texto_no,
        onCancel: function () {
            modal_edit_dire()
        }
    });
    $(document).on('submit', '.form_login', submit_form_login);
    $(document).on('click', '.button-modal-help', modal_help);

    //Fin funciones login

    $(document).on('focusout', '.observaciones_product', function () {
        save_cart_session();
    });
    $(document).on('submit', '.add_dire_entre', act_dire_entre);
    $(document).on('submit', '.form_check_agencia', form_check_agencia);
    $(document).on('click', '.btn-save-observaciones', save_observaciones);
    $(document).on('change keyup', '.btn_search_presupuesto ', search_presupuestos);
    $(document).on('change', '.select_dir', select_dir);
    $(document).on('click', '.btn_select_dire', btn_select_dire);
    $(document).on('change', '.select_change_pais ', select_change_pais);
    $(document).on('change', '.select_change_provincias ', select_change_provincias);
    $(document).on('change', '.select_change_poblacion ', select_change_poblacion);
    $(document).on('change', '.is_temporal ', change_is_temporal);
    $(document).on('click', '.check_product_component', click_prev);
    $(document).on('click', '.text_agencia', click_prev);
    $(document).on('click', '.check_agencia', check_agencia);
    $(document).on('click', '.text_lupa', click_prev);
    $('#modal_view_pdfs').on('hidden.bs.modal', function () {
        window.location.href = 'clientes.php?opt=historial';
    });
    $('#modal_add_product').on('shown.bs.modal', function () {
        $('#modal_add_product').find('.modal-body').html('');
        $.ajax({
            url: "mods/mod_products/ajax/load_all_products_modal.php",
            type: "post",
            success: function (response) {
                $('#modal_add_product').find('.modal-body').html(response);
                var is_industrial = check_is_venta_industrial();
                if (is_industrial == "1") {
                    $('.button_expositor').addClass('hidden');
                }
                $('.type_doc_modal').html($('.is_pedido').val());
                is_type_product_modal = 1;
                type_doc = $('.is_pedido').val();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    })

    function close_modal_add_product(e) {
        e.preventDefault();
        if (confirm('¿Desea cerrar la ventana sin añadir el producto?')) {
            $('#modal_add_product').modal('hide');
        }
    }

    var fila_indicaciones_producto = "";
    function btn_indicaciones_producto(e) {
        e.preventDefault();
        fila_indicaciones_producto = $(this);
        var textearea = $(this).closest('.row').find('.indicaciones_producto').html().text();
        $('.editor_text').summernote('code', textearea);
        $('#modal_edit_indicaciones').modal('show');
    }

    function btn_save_indicaciones(e) {
        e.preventDefault();
        var texto_indicaciones = $(this).closest('.modal-body').find('.editor_text').summernote('code');
        $(fila_indicaciones_producto).closest('.row').find('.indicaciones_producto').html(texto_indicaciones);
        $('#modal_edit_indicaciones').modal('hide');
        $('#modal_edit_indicaciones').closest().find('.editor_text').summernote('code', '');
    }

//Lacado RAL
    $(document).on('change', '.change_color_ral', change_color_ral);
    $(document).on('click', '.change_color_product_ral_modal', change_color_product_ral_modal);
    $(document).on('click', '.change_color_components_ral_modal', change_color_components_ral_modal);

//Componentes
    $(document).on('click', '.change_color_component', change_color_component);
    $(document).on('click', '.change_color_components', group_colors_components);
    $(document).on('change keyup', '.input_unit_components', input_unit_components);
    $(document).on('keyup change', '.btn_search_component', btn_search_component);
    $(document).on('click', '.check_search_components, .type_search_components', check_search_components);
    $(document).on('click', '.btn_flotante_generar', btn_flotante_generar);
    $(document).on('submit', '#form_login_flotante', form_login_flotante);
    $(document).on('change keyup', '.change_line_product_component_unit', change_line_product_component_unit);
    $(document).on('click', '.add_medidas_2', add_medidas_2_new);
    $(document).on('click', '.btn_save_medidas_componente', btn_save_medidas_componente_new);
    $(document).on('focusout', '.alto_medida_componente', focusout_medidas_component);
    $(document).on('focusout', '.ancho_medida_componente', focusout_medidas_component);
    $(document).on('focusout change keyup click keypress', '.ancho_medida_componente', calc_medidas_componente);
    $(document).on('focusout change keyup click keypress', '.alto_medida_componente', calc_medidas_componente);
    $(document).on('change keyup click keypress', '.unidades_medida_componente', calc_medidas_componente);
    $(document).on('change keyup click keypress', '.change_units_resumen_component', change_units_resumen_component);
    $(document).on('change keyup', '.metros_medida_componente', metros_medida_componente);
    $(document).on('click', '.btn_delete_line_component', btn_delete_line_component);
    $(document).on('click', '.btn_edit_units_medidas_resumen', btn_edit_units_medidas_resumen);
    $(document).on('click', '.btn_save_medidas_componente_resumen', btn_save_medidas_componente_resumen);
    $(document).on('click', '.btn_flotante_add', btn_flotante_add);
    $(document).on('click', '.change_color_component_resumen', change_color_component_resumen);
    $(document).on('change', '.number_units_components', number_units_components);
    $(document).on('click', '.new_line_size_component', new_line_size_component);
    $(document).on('click', '.delete_line_size_component', delete_line_size_component);
    $(document).on('click', '.close_modal_medidas', close_modal_medidas);
    $(document).on('click', '.close_modal_add_product', close_modal_add_product);
    $(document).on('click', '.close_modal_concepto', close_modal_concepto);


//Zona historial clientes
    $(document).on('submit', '.search_historial', search_historial);
    $(document).on('submit', '.add_dire_entre_cliente ', add_dire_entre_cliente);
    $(document).on('click', '.btn_ver_notas ', btn_ver_notas);
    $(document).on('click', '.close_modal_notas ', close_modal_notas);
    $(document).on('click', '.icon_exchange_presupuestos ', icon_exchange_presupuestos);
    $(document).on('click', '.icon_trash_presupuestos ', icon_trash_presupuestos);
    $(document).on('click', '.icon_repeat_presupuestos ', icon_repeat_presupuestos);
    $(document).on('click', '.page-link', page_pres_ped);
    $(window).on('resize', getResolution);
    $(document).on('keyup', '.change_pass1, .change_pass2', check_password_change);
    $(document).on('click', '.btn_view_conceptos', btn_view_conceptos);
    $(document).on('click', '.btn_add_concepto_historial', btn_add_concepto_historial);
    //$(document).on('click', '.delete-concepto', delete_concepto);
    $(document).on('submit', '.form_customer_register', form_customer_register);
    $(document).on('change', '.select_pais_registro', select_pais_registro);
    $(document).on('change', '.select_provincia_registro', select_provincia_registro);

//Expositores
    $(document).on('change', '.unidades_expositores', unidades_expositores);
    $(document).on('change', '.change_units_resumen_expositor', change_units_resumen_expositor);
    $(document).on('change', '.unidades_expositores_modal', unidades_expositores_modal);

    //EMAILS
    $(document).on('submit', '.email_consulta', email_consulta);
    $(document).on('submit', '.remember_pass', remember_pass);
    $(document).on('submit', '.change_pass', change_pass);
    $(document).on('submit', '.set_email_cliente', set_email_cliente);
    $(document).on('click', '.button_send_form_email', button_send_form_email);

    //CONCEPTOS
    $(document).on('click', '.btn_add_concepto ', btn_add_concepto);
    $(document).on('click', '.btn_flotante_add_concepto ', btn_flotante_add_concepto);

    //CORREDERAS
    //$(document).on('change click keyup', '.ancho_marco_corredera, .ancho_hoja_corredera', ancho_medidas_corredera);
    //$(document).on('change click keyup', '.alto_marco_corredera, .alto_hoja_corredera', alto_medidas_corredera);
    $(document).on('change keypress click keyup', '.completa_una_hoja', completa_una_hoja);
    $(document).on('change keypress click keyup', '.ancho_corredera_completa, .alto_corredera_completa', corredera_completa);
    $(document).on('change keypress click keyup', '.ancho_corredera_marco, .alto_corredera_marco', corredera_marco);
    $(document).on('change keypress click keyup', '.ancho_corredera_hoja, .alto_corredera_hoja', corredera_hoja);
    $(document).on('focusout', '.ancho_corredera_completa, .ancho_corredera_marco, .ancho_corredera_hoja, .alto_corredera_completa, .alto_corredera_marco, .alto_corredera_hoja', focusout);
    $(document).on('change keypress click keyup', '.cantidad_corredera_completa, .cantidad_corredera_marco, .cantidad_corredera_hoja', only_numbers);
    $(document).on('change keypress click keyup', '.cantidad_corredera_completa', cantidad_corredera_completa);
    $(document).on('change keypress click keyup', '.cantidad_corredera_marco', cantidad_corredera_marco);
    $(document).on('change keypress click keyup', '.cantidad_corredera_hoja', cantidad_corredera_hoja);
    //CROQUIS
    $(document).on('submit', '.upload_croquis_increment', upload_croquis_increment);
    $(document).on('click', '.btn-adjuntar-croquis', btn_adjuntar_croquis);

    //Formas de pago
    $(document).on('click', '.text_forma_pago', text_forma_pago);
    $(document).on('click', '.check_forma_pago', check_forma_pago);

    //SUBIR LOGO
    $(document).on('click', '.btn_upload_file', upload_file_modal);
    $(document).on('submit', '#formuploadajax', upload_logo_pres);

    $(document).on('click', '.btn_add_editar_datos_cliente', btn_add_editar_datos_cliente);
    $(document).on('submit', '.form_editar_datos_cliente', form_editar_datos_cliente);

    $(document).on('click', '.btn_indicaciones_producto', btn_indicaciones_producto);
    $(document).on('click', '.btn-save-indicaciones', btn_save_indicaciones);

});