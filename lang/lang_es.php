<?php

define("lang_menu_empresa", "EMPRESA");
define("lang_menu_productos", "PRODUCTOS");
define("lang_menu_clientes", "ÁREA CLIENTES");
define("lang_menu_somos", "Quienes Somos");
define("lang_menu_expositores", "Expositores");
define("lang_menu_contacto", "Contacto");
define("lang_menu_estamos", "Dónde Estamos");
define("lang_menu_ventana", "Enrollable Ventana");
define("lang_menu_puerta", "Enrollable Puerta");
define("lang_menu_plisada", "Plisadas");
define("lang_menu_abatible", "Puertas Abatibles");
define("lang_menu_fijas", "Fijas");
define("lang_menu_correderas", "Correderas");
define("lang_menu_descargas", "Descargas/Doc.Técnica");
define("lang_menu_tarifa", "Tarifa");
define("lang_menu_ficha_cliente", "Ficha Cliente");
define("lang_menu_presupuestos", "Presupuestos");
define("lang_menu_presupuestos_pedidos", "Generar Presupuestos / Pedidos");
define("lang_menu_historial", "Historial Presupuestos / Pedidos");
define("lang_menu_pedidos", "Pedidos");
define("lang_menu_compra", "COMPRA ONLINE");
define("lang_search_text", "Introduce tu búsqueda...");
define("lang_texto_somos", "Nace un nuevo proyecto empresarial con vocación de líder.<br> En <div class='logo_mosquiflex'></div> hemos desarrollado una gama de mosquiteras ideal para el mercado europeo y que destaca por la calidad de sus diseños y acabados.<br> Además hemos constituido una organización eficiente y dinámica, capaz de ofrecer <b>EL MEJOR SERVICIO</b>.<br> Le damos la bienvenida y le invitamos a participar en el éxito de nuestro proyecto");
define("lang_nav_iniciar_sesion", "Iniciar sesión");
define("lang_nav_mi_cuenta", "Mi cuenta");
define("lang_nav_cerrar_sesion", "Cerrar sesión");
define("lang_company_llamenos", "Llámenos");
define("lang_company_telefono", "Teléfono");
define("lang_company_email_cliente", "Email del cliente");
define("lang_company_persona_contacto", "Persona contacto");
define("lang_company_consultenos", "Consúltenos");
define("lang_company_cliente_registrado", "Cliente registrado");
define("lang_company_cliente_no_registrado", "Cliente no registrado");
define("lang_company_input_nombre", "Nombre, Razón social");
define("lang_company_horario", "Horario");
define("lang_company_consulta_tecnica", "Consulta Técnica");
define("lang_company_consulta_comercial", "Consulta Comercial");
define("lang_company_consulta_incidencias", "Incidencias");
define("lang_company_consulta_otras", "Otras Consultas");
define("lang_company_enviar", "Enviar");
define("lang_title_ficha_producto", "Ficha técnica producto");
define("lang_title_ficha_prestaciones", "Declaración de prestaciones");
define("lang_title_hoja_pedido", "Hoja de pedido");
define("lang_title_product", "Productos");
define("lang_title_tipo_producto", "Tipo de producto");
define("lang_title_producto_acabado", "Producto acabado");
define("lang_title_componente", "Componentes");
define("lang_title_venta_industrial", "Venta industrial");
define("lang_title_colores", "Colores");
define("lang_title_color", "Color");
define("lang_title_carta_colores", "Carta de colores");
define("lang_title_seleccione_producto_colores", "Seleccione un producto para mostrar los colores");
define("lang_title_vista_previa", "Vista previa");
define("lang_title_continuar_pres", "Continuar presupuesto");
define("lang_title_continuar_ped", "Continuar pedido");
define("lang_title_versiones", "Versiones");
define("lang_title_ver_imagen", "Ver imagen");
define("lang_title_cajon", "Cajón");
define("lang_title_grupo", "GRUPO");
define("lang_title_buscar_descripcion", "Buscar descripción");
define("lang_title_buscar_codigo", "Buscar código");
define("lang_title_expositores", "Expositores");
define("lang_lacados_carta_ral", "LACADOS - Carta RAL");
define("lang_products_introduce_busqueda", "Introduce búsqueda...");
define("lang_title_unidades", "Unidades");
define("lang_title_medidas", "Medidas");
define("lang_title_articulo", "Artículo");
define("lang_title_descripcion", "Descripción");
define("lang_title_referencia", "Referencia");
define("lang_title_precio", "Precio");
define("lang_title_embalaje", "Embalaje");
define("lang_title_estandar", "estándar");
define("lang_title_precio_upper", "PRECIO");
define("lang_title_total", "Total");
define("lang_title_unidades_mobile", "Uds.");
define("lang_title_medidas_mobile", "Med.");
define("lang_title_articulo_mobile", "Art.");
define("lang_title_descripcion_mobile", "Descrip.");
define("lang_title_referencia_mobile", "Ref.");
define("lang_title_precio_mobile", "PVP");
define("lang_title_embalaje_mobile", "Emb.");
define("lang_title_estandar_mobile", "");
define("lang_title_precio_upper_mobile", "PVP");
define("lang_title_total_mobile", "Tot.");
define("lang_boton_add_unidades", "Añadir Uds");
define("lang_boton_add_medidas", "Añadir medidas");
define("lang_title_metros", "metros");
define("lang_title_unidad", "unidad");
define("lang_title_bruto", "bruto");
define("lang_title_color_producto", "Color producto");
define("lang_title_cerrar", "Cerrar");
define("lang_title_editar_medidas", "Editar medidas");
define("lang_title_guardar_medidas", "Guardar medidas");
define("lang_title_abreviatura_unidades", "Uds");
define("lang_title_lacado_blanco", "Lacado blanco");
define("lang_title_ancho", "Ancho");
define("lang_title_alto", "Alto");
define("lang_title_expositores_punto_venta", "Expositores Punto de Venta");
define("lang_title_resumen", "Resumen");
define("lang_title_add_al", "Añadir");
define("lang_title_impuestos_no_incluidos", "Impuestos no incluidos");
define("lang_title_add_nuevo_producto", "Añadir nuevo producto");
define("lang_text_diferir_color", "El color real puede diferir ligeramente del mostrado en la pantalla del ordenador o dispositivo electrónico");
define("lang_footer_aviso_legal", "Aviso legal");
define("lang_footer_proteccion", "Protección de datos");
define("lang_footer_condiciones", "Condiciones Generales de Venta");
define("lang_title_puerta_unica", "Puerta única");
define("lang_title_puerta_doble", "Puerta doble");
define("lang_text_acceso_cliente", "Acceso clientes");
define("lang_text_email_nombre_usuario", "Email / Nombre usuario");
define("lang_text_contraseña", "Contraseña");
define("lang_text_mis_datos", "Mis datos");
define("lang_text_datos_fiscales", "Datos fiscales");
define("lang_text_nombre", "Nombre");
define("lang_text_direccion", "Direccion");
define("lang_text_pais", "País");
define("lang_text_provincia", "Provincia");
define("lang_text_poblacion", "Población");
define("lant_text_dni_cif", "CIF / DNI");
define("lang_text_fax", "Fax");
define("lang_text_mis_direcciones", "Mis direcciones de envío");
define("lang_text_eliminar_direccion", "Eliminar dirección");
define("lang_text_add_direccion", "Añadir dirección");
define("lang_text_historial", "Historial");
define("lang_text_historial_mis_presupuestos", "Mis presupuestos");
define("lang_text_historial_mis_pedidos", "Mis pedidos");
define("lang_text_historial_abreviatura_referencia", "Ref");
define("lang_text_historial_fecha", "Fecha");
define("lang_text_historial_validez", "Validez");
define("lang_text_historial_documentos", "Documentos");
define("lang_text_historial_opciones", "Opciones");
define("lang_text_historial_notas", "Notas");
define("lang_text_historial_pvp", "PVP");
define("lang_text_historial_pvp_pedido", "Ver pedido");
define("lang_text_historial_condiciones", "Cond. Cliente");
define("lang_text_historial_confirmar", "Confirmar");
define("lang_text_historial_eliminar", "Eliminar");
define("lang_text_historial_add", "Añadir");
define("lang_text_historial_ver", "Ver");
define("lang_text_siguiente", "Siguiente");
define("lang_text_abreviatura_siguiente", "Sig.");
define("lang_text_anterior", "Anterior");
define("lang_text_abreviatura_anterior", "Ant.");
define("lang_menu_inico", "Inicio");
define("lang_menu_todos_productos", "Todos los productos");
define("lang_title_tipologia", "Tipología");
define("lang_text_historial_abreviatura_documentos","Doc.");
define("lang_text_historial_abreviatura_opciones", "Opc.");
define("lang_text_mis_direcciones_mobile", "Direcciones envio");
define("lang_title_modal_add_dire_envio","Añadir dirección envio de la mercancia");

define("lang_aviso_legal_1", "En cumplimiento de lo establecido en la Ley 34/2002 de 11 de Julio, de Servicios de la Sociedad de La Información y el Comercio Electrónico (LSSICE), se informa de los siguientes aspectos legales:");
define("lang_aviso_legal_2", "Identificación de la web:");
define("lang_aviso_legal_3", "Responsable de la web:");
define("lang_aviso_legal_4", "Domicilio de Contacto:");
define("lang_aviso_legal_5", "Email de Contacto:");
define("lang_aviso_legal_6", "Datos Fiscales:");
define("lang_aviso_legal_7", "Datos Registrales:");
define("lang_aviso_legal_8", "CONDICIONES GENERALES DE USO DE LA PÁGINA WWW.MOSQUIFLEX.ES");
define("lang_aviso_legal_9", "Consideraciones Previas");
define("lang_aviso_legal_10", "Las presentes condiciones legales regulan el uso legal permitido de esta página web, URL www.mosquiflex.es, cuyo responsable legal es Mosquiteras Técnicas, S.L. Estas condiciones generales regulan el acceso y utilización que la titular de la web pone gratuitamente a disposición de los usuarios de Internet. El acceso a la misma implica su aceptación sin reservas. El solicitar información o emitir una opinión, supone la aceptación plena de las presentes condiciones.
La labor de Mosquiteras Técnicas, S.L. es la de fabricación de mosquiteras.");
define("lang_aviso_legal_11", "La labor de Mosquiteras Técnicas, S.L. es la de fabricación de mosquiteras.");
define("lang_aviso_legal_12", "Uso de la Página Web");
define("lang_aviso_legal_13", "El Usuario se obliga a usar la web, los servicios y los contenidos de forma diligente, correcta y lícita. Mosquiteras Técnicas, S.L podrá responsabilizar a los usuarios que haciendo mal uso de la web, causaren daños o perjuicios a terceras personas, así como de los posibles virus o programas informáticos, que pudiesen introducir, generar, alojar en la web, y deterioren o puedan deteriorar tanto los contenidos como el buen funcionamiento de ésta, así como los equipos, sistemas y programas de los usuarios de la web.");
define("lang_aviso_legal_14", "Propiedad Intelectual e industrial");
define("lang_aviso_legal_15", "Los textos, imágenes, logos, signos distintivos, sonidos, animaciones, vídeos, código fuente y resto de contenidos incluidos en este website son propiedad de Mosquiteras Técnicas, S.L., o dispone en su caso, del derecho de uso y explotación de los mismos, y en tal sentido se erigen como obras protegidas por la legislación de propiedad intelectual e industrial vigentes.");
define("lang_aviso_legal_16", "El Usuario se compromete a abstenerse de: suprimir, eludir o manipular el 'copyright' y demás datos identificativos de los derechos de sus titulares incorporados a los Contenidos, así como los dispositivos técnicos de protección, o cualesquiera mecanismos de información que pudieren contener los Contenidos.");
define("lang_aviso_legal_17", "Cualquier transmisión, distribución, reproducción o almacenamiento, total o parcial, de los contenidos almacenados en esta web, queda expresamente prohibido salvo previo y expreso consentimiento del titular, quedando expresa y terminantemente prohibida la reproducción de elementos o contenidos de este website, realizados con ánimo de lucro o fines comerciales.");
define("lang_aviso_legal_18", "Condiciones de Acceso");
define("lang_aviso_legal_19", "Mosquiteras Técnicas, S.L., se reserva la facultad de efectuar, en cualquier momento y sin necesidad de previo aviso, modificaciones y actualizaciones de la información contenida en la Web, de la configuración y presentación de ésta, de las condiciones de acceso, condiciones de contratación. etc. Por lo que el USUARIO deberá acceder a versiones actualizadas de la página.");
define("lang_aviso_legal_20", "La empresa responsable de la web no garantiza la inexistencia de interrupciones o errores en el acceso a la página o a su contenido, ni que ésta se encuentre actualizada o libre de virus, ni de cualquier otro elemento en la que puedan producir alteraciones en su sistema informático. Mosquiteras Técnicas, S.L., declina cualquier responsabilidad contractual o extracontractual con la persona o empresa que haga uso de ella y tuviera perjuicios de cualquier naturaleza ocasionados por virus informáticos o por elementos informáticos de cualquier índole.");
define("lang_aviso_legal_21", "El titular de la web no responderá de ninguna consecuencia, daño o perjuicio que pudieran derivarse del uso no consentido de la información contenida en la misma, o de los derivados de los servicios, opiniones y consejos incluidos en nuestra página web.");
define("lang_aviso_legal_22", "El hardware y software necesarios para acceder a la web serán por cuenta del Usuario. Mosquiteras Técnicas, S.L no será responsable del mal funcionamiento de estos, ni de los derechos de uso o licencias requeridos para su utilización.");
define("lang_aviso_legal_23", "Responsabilidad por Links");
define("lang_aviso_legal_24", "La empresa responsable de la página web, declina cualquier responsabilidad por los servicios y/o información que se preste en otras páginas enlazadas con esta Web. Mosquiteras Técnicas, S.L. no controla ni ejerce ningún tipo de supervisión cuando incluye el link a las mismas, con la creencia de que estos contenidos acatan la legislación aplicable. No obstante, la utilización de Links a otras páginas no supone en modo alguno responsabilidad ni apropiación del contenido de las mismas, sin que se pueda entender que se produce labor alguna de supervisión ni aprobación de los cambios o informaciones que se realicen en ellas. Aconsejamos a los visitantes de las mismas actuar con prudencia y consultar las eventuales condiciones legales que se expongan en dichas webs.");
define("lang_aviso_legal_25", "Mosquiteras Técnicas, S.L. no responde, ni se hace cargo, de ningún tipo de responsabilidad por los daños y perjuicios que puedan relacionarse con el funcionamiento, disponibilidad y continuidad de los sitios enlazados.");
define("lang_aviso_legal_26", "Contenidos de la web");
define("lang_aviso_legal_27", "El idioma utilizado por la web será el castellano, sin perjuicio de la utilización de otras lenguas, nacionales o autonómicas. Mosquiteras Técnicas, S.L no se responsabiliza de la no comprensión o entendimiento del idioma de la web por el Usuario, ni de sus consecuencias.");
define("lang_aviso_legal_28", "Los contenidos de la web tienen como finalidad informar y dar a conocer las actividades, productos y servicios prestados por Mosquiteras Técnicas, S.L. y tienen un carácter general y orientativo.");
define("lang_aviso_legal_29", "Mosquiteras Técnicas, S.L podrá modificar los contenidos sin previo aviso, así como suprimir y cambiar éstos dentro de la web y la forma en que se accede a éstos, sin justificación alguna y libremente, no responsabilizándose de las consecuencias que los mismos puedan ocasionar a los usuarios.");
define("lang_aviso_legal_30", "Se prohíbe el uso de los contenidos de la web para promocionar, contratar o divulgar publicidad o información propia o de terceras personas sin la autorización de Mosquiteras Técnicas, S.L, ni remitir publicidad o información valiéndose para ello de los servicios o información que se ponen a disposición del Usuario, independientemente de si la utilización es gratuita o no.");
define("lang_aviso_legal_31", "Los enlaces o hiperenlaces que incorporen terceros en sus páginas web, dirigidos a esta web, serán para apertura de la página web completa, no pudiendo manifestar, directa o indirectamente, indicaciones falsas, inexactas o confusas, ni incurrir en acciones desleales o ilícitas en contra de Mosquiteras Técnicas, S.L.");
define("lang_aviso_legal_32", "Régimen legal y jurisdicción aplicable");
define("lang_aviso_legal_33", "Las presentes condiciones de uso de la página web se rigen por la legislación española.
Así mismo, Mosquiteras Técnicas, S.L. y el Usuario reconocen y aceptan que para la resolución de cualquier litigio, discrepancia, controversia, conflicto, cuestión o reclamación resultante de la interpretación, desarrollo, ejecución, incumplimiento, resolución o nulidad de las presentes condiciones de uso serán los Juzgados y Tribunales del partido judicial de la ciudad de Granada (España) los encargados de resolverlas, renunciando las partes
expresamente a cualquier otro fuero que pudiera corresponderles, salvo en los casos en que no esté legalmente permitida dicha renuncia.");

define("lang_proteccion_1", "POLÍTICA DE PROTECCIÓN DE DATOS");
define("lang_proteccion_2", "En cumplimiento de lo establecido en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal y su reglamento de desarrollo, 1720/2007, Mosquiteras Técnicas, S.L., le informa de que los datos de carácter personal proporcionados o recabados a través de nuestra página web o a través de correos electrónicos de contacto, podrán ser incorporados a nuestros ficheros para su posterior tratamiento.");
define("lang_proteccion_3", "Toda la información requerida es obligatoria y su omisión podría conllevar la imposibilidad de atender su solicitud. Los datos recogidos son almacenados bajo la confidencialidad y las medidas de seguridad legalmente establecidas. Le rogamos que si se produce alguna modificación en los mismos nos lo comunique para mantener nuestros ficheros debidamente actualizados.");
define("lang_proteccion_4", "Los datos recogidos son almacenados bajo la confidencialidad y las medidas de seguridad legalmente establecidas. Podrá ejercer los derechos de acceso, cancelación, oposición o rectificación, a través del correo electrónico ventas@mosquiflex.es o mediante comunicación escrita, dirigida a Mosquiteras Técnicas, S.L., Camino de Barrasa - Calle B, Nave 5, 18320 Santa Fe, Granada.");

define("lang_condiciones_1", "Las Condiciones Generales de Venta se consideran aceptadas sin reservas por el cliente al tramitar su pedido.");
define("lang_condiciones_2", "PRECIOS");
define("lang_condiciones_3", "Los precios contenidos en esta Tarifa son PVP. Mosquiteras Técnicas, S.L. se reserva el derecho de modificar esta tarifa de precios en función de la oscilación en los precios del aluminio en LME y por la subida del USD frente al EURO. Los impuestos son a cargo del comprador, según legislación vigente.");
define("lang_condiciones_4", "PRODUTOS");
define("lang_condiciones_5", "Mosquiteras Técnicas, S.L. se reserva el derecho a introducir cambios técnicos en sus productos sin previo aviso.");
define("lang_condiciones_6", "EMBALAJES");
define("lang_condiciones_7", "El responsable de la entrega del residuo de envase o envase usado, para su correcta gestión ambiental será el poseedor final.");
define("lang_condiciones_8", "PEDIDOS");
define("lang_condiciones_9", "Deben cursarse por escrito a");
define("lang_condiciones_10", "En caso de ser cursados telefónicamente será responsabilidad del cliente cualquier error que pudiera producirse.");
define("lang_condiciones_11", "El pedido debe contener como mínimo los siguientes datos: Identificación del cliente, referencia y descripción del producto, características, medidas y cantidades, siendo responsabilidad del cliente cualquier retraso que pudiera producirse por la falta de datos en el pedido.
Una vez recibido el pedido, este es firme y no se admitirá la rectificación o anulación del mismo, salvo previa confirmación escrita entre las partes, en cuyo caso, los gastos derivados de la rectificación o anulación serán siempre a cargo del cliente.");
define("lang_condiciones_12", "PORTES");
define("lang_condiciones_13", "La mercancía se enviará a PORTES PAGADOS, siempre que el envío se realice por la agencia de transportes concertada por MOSQUIFLEX®. Cuando el valor de la mercancía que se envía es inferior a 50€ netos, se facturará la cantidad de 9€ en concepto de gastos de envío.");
define("lang_condiciones_14", "FORMA DE PAGO");
define("lang_condiciones_15", "Recibo domiciliado (SEPA B2B) o transferencia.");
define("lang_condiciones_16", "IMPAGADOS");
define("lang_condiciones_17", "El impago de una factura a su vencimiento, conlleva la cancelación del crédito del cliente así como la paralización de los pedidos pendientes de fabricación, hasta la satisfacción del importe pendiente, y en su caso, de los gastos de devolución generados, que serán siempre a cargo del cliente.");
define("lang_condiciones_18", "RECLAMACIONES");
define("lang_condiciones_19", "El cliente debe verificar la mercancía, su estado y cantidades a la recepción de la misma, haciendo constar cualquier anomalía en el albarán de entrega y poniéndolo de inmediato en conocimiento de Mosquiteras Técnicas, S.L.
No se admitirá ninguna reclamación por daños producidos en el transporte pasadas 48 horas de la fecha de recepción de la mercancía. Le recordamos que la firma del albarán a la Agencia de Transportes implica su conformidad.
No se admitirán reclamaciones por pequeñas variaciones en el tono o textura respecto a los muestrarios.
Los errores en la fabricación de la mercancía o en la gestión de los pedidos comportarán exclusivamente la entrega de una mercancía idéntica en cuanto a cantidad y características a la inicialmente pedida. Se excluyen expresamente otros costes como gastos de desplazamiento, mano de obra o indemnizaciones.");
define("lang_condiciones_20", "GARANTÍA Y DEVOLUCIONES");
define("lang_condiciones_21", "PROTECCIÓN DE DATOS PERSONALES");
define("lang_condiciones_22", "A efectos de lo dispuesto en la Ley Orgánica 15/1999 de Protección de Datos de Carácter Personal, informamos al Cliente que los datos personales que este facilite a Mosquiteras Técnicas, S.L. serán incorporados al fichero de CLIENTES del que es responsable Mosquiteras Técnicas, S.L., con la finalidad de prestar un adecuado servicio. No obstante le recordamos que dispone de sus derechos de acceso, rectificación, cancelación y oposición al tratamiento de sus datos que podrá ejercer en nuestro domicilio: Camino de Barrasa, C/B Nave 5. CP 18320. Santa Fe. Granada.
La información recabada podrá ser usada para comunicar vía email o por correo postal, ofertas, novedades o información de interés para el cliente. El cliente podrá oponerse al envió de dichas comunicaciones en el momento de facilitar sus datos o posteriormente según las instrucciones contenidas en dichas comunicaciones.");
define("lang_condiciones_23", "JURISDICCIÓN");
define("lang_condiciones_24", "Las partes se someten a los Juzgados y Tribunales de Granada, con renuncia al fuero propio, para cuantas gestiones se susciten de las operaciones de compra venta entre ambos.");
define("lang_condiciones_25", "Todos los artículos suministrados por Mosquiteras Técnicas, S.L. están protegidos por una garantía de dos años desde la fecha de entrega del material.  La garantía cubre la sustitución y/o reparación del material que presente desperfecto, quedando excluidos de la garantía aquellos productos que hayan sido manipulados o sometidos a un uso inadecuado, así como aquellos cuyos  daños y/o fallos de funcionamiento tengan su origen en: 1. Productos fabricados a petición del cliente con medidas o características no aconsejables. 2. Uso negligente, impropio o inadecuado. 3. Modificaciones o intentos de reparación. 4. Transporte inadecuado. 5. Golpes o marcas no imputables al proceso de  fabricación. 6. Acciones de terceras partes o cualesquiera otras razones distintas a las condiciones normales de uso y funcionamiento del material, incluidos incendios, inundaciones, terremotos, inclemencias atmosféricas, etc.
Los derechos de garantía podrán ser reclamados durante el periodo en vigencia establecido y de forma inmediata a su detección, salvo que se trate de defectos visibles en cuyo caso la reclamación deberá efectuarse en un plazo de 48 horas a contar desde la fecha de recepción del material.
Para la reclamación de los derechos de Garantía el cliente deberá informar por escrito a Mosquiteras Técnicas, S.L., indicando el número de albarán o de factura de compra, la descripción detallada de la incidencia. No se admitirá ninguna devolución que no haya sido previamente acordado con Mosquiteras Técnicas, S.L. La devolución, una vez autorizada, debe realizarse en un embalaje adecuado que garantice unas condiciones de seguridad en el transporte del producto. Los daños producidos en el transporte por un embalaje deficiente no serán cubiertos por la garantía.
La devolución de mercancía que se consideré defectuosa por el cliente quedará a expensas de la valoración por parte de Mosquiteras Técnicas, S.L. respecto de la procedencia o no de la reclamación de garantía.");
define("lang_recordar_contraseña", "¿Recordar contraseña?");
define("lang_modificar_contraseña", "Modificar contraseña");
define("lang_generar_contraseña", "Recordar / Generar contraseña");
define("lang_cif_contraseña", "CIF");
define("lang_introduce_cif", "Introduce CIF");
define("lang_email_contraseña", "Email");
define("lang_introduce_email", "Introduce email");
define("lang_recordar_contraseña_button", "Recordar contraseña");
define("lang_texto_contraseña", "Contraseña");
define("lang_texto_introduce_contraseña", "Introduce contraseña");
define("lang_texto_repite_contraseña", "Repite contraseña");
define("lang_texto_modificar_contraseña", "Modificar contraseña");
define("lang_texto_error_datos", "No coinciden las contraseñas");
define("lang_no_contraseña", "¿Aún no tienes contraseña?");
define("lang_aqui", "aquí");
define("lang_no_contraseña_1", "para generar contraseña de acceso.");
define("lang_text_detalle", "Detalle");
define("lang_text_version", "Versión");
define("lang_text_no_productos", "No existen productos");
define("lang_text_ver_mas_info", "Ver más información del producto");
define("lang_text_color", "Color");
define("lang_text_unidades", "Unidades");
define("lang_text_ancho", "Ancho");
define("lang_text_alto", "Alto");
define("lang_text_precio", "Precio");
define("lang_text_revisar_medidas", "Revisar medidas mínimas y máximas");
define("lang_text_anadir_incrementos", "Añadir incrementos");
define("lang_text_copiar_linea_pedido", "Copiar linea pedido");
define("lang_text_eliminar_linea_pedido", "Eliminar linea pedido");
define("lang_text_añadir_producto", "Añadir otro producto");
define("lang_text_impuestos_no_incluidos", "Impuestos no incluidos");
define("lang_text_guardar_presupuesto", "Guardar presupuesto");
define("lang_text_guardar_pedido", "Confirmar pedido");
define("lang_text_croquis", "Croquis");
define("lang_text_anadir_producto", "Añadir producto");
define("lang_text_seleccione_version", "Seleccione primero una versión");
define("lang_text_add_concepto", "Añadir concepto");
define("lang_text_select_opcion", "Seleccionar opción");
define("lang_text_select_importe", "Importe");
define("lang_text_select_porcentaje", "Porcentaje");
define("lang_text_select_incremento", "Incremento");
define("lang_text_select_descuento", "Descuento");
define("lang_text_select_introduce_importe", "Introduce importe");
define("lang_text_add_al", "Añadir al");
define("lang_text_destinatario", "Destinatario");