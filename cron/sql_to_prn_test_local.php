#!/usr/bin/php
<?php
ini_set("display_errors", 1);

function hideNumPre($text, $clproc, $clcodi)
{
    $text = str_replace('Ppto. MT. CL ', '', $text);
    $text = str_replace('Ped. MT. CL ', '', $text);
    $text = str_replace($clproc . $clcodi . '-', '', $text);
    $text = ltrim($text, '0');
    return $text;
}

function round_number_to_par($number, $min_value = NULL)
{
    if (substr($number, 3, 1) <= 5):
        $number = $number + '0.05';
    endif;
    $number = round($number, 1, PHP_ROUND_HALF_EVEN);
    $number = str_replace(array(",", "."), '', $number);
    if ($number == "1"):
        $number = "10";
    elseif ($number == "2"):
        $number = "20";
    else:
        if ($number % 2 != 0):
            $number++;
        endif;
    endif;
    if ($number < $min_value): $number = $min_value; endif;
    return $number;
}

$fecha = date('d_m_Y');
if(!is_dir("../as400/get/".$fecha)){
    mkdir("../as400/get/".$fecha, 0755, true);
}
$mosquicab_ruta = "../as400/get/".$fecha."/WCAB.prn";
$mosquidet_ruta = "../as400/get/".$fecha."/WDET.prn";
$mosquidir_ruta = "../as400/get/".$fecha."/WDIR.prn";

// Conectando, seleccionando la base de datos
$link = mysqli_connect("", "root", "root", "mosquiflex") or die('No se pudo conectar: ' . mysqli_error());
$file_mosquicab = fopen($mosquicab_ruta, "w") or die("Unable to open file!");
$file_mosquidet = fopen($mosquidet_ruta, "w") or die("Unable to open file!");
$file_mosquidir = fopen($mosquidir_ruta, "w") or die("Unable to open file!");

$query_presupuestos = mysqli_query($link, "SELECT *, DATE_FORMAT(fecha,'%d/%m/%Y') AS fecha2  FROM presupuestos HAVING fecha2 ='" . date('d/m/Y') . "'");

$query_familia = mysqli_query($link, "SELECT fafami, fadesc, status, min_ran_anc, min_ran_alt FROM familias");
$array_familia = array();
while ($familia = $query_familia->fetch_assoc()):
    $array_familia[$familia['fafami']] = $familia;
endwhile;

while ($presupuesto = $query_presupuestos->fetch_assoc()):

    $id = $presupuesto['id'];

    if (!empty($id)):

        $query_albaranes = mysqli_query($link, "SELECT * FROM albaranes WHERE num_pre = '$id'");
        $num_albaranes = mysqli_affected_rows($link);
        $total_albaranes = str_pad($num_albaranes, 2, "0", STR_PAD_LEFT);

        $fecha = new DateTime($presupuesto['fecha']);
        $fecha = $fecha->format('Ymd');

        $clproc = str_pad(substr($presupuesto['id_cliente'], 0, 2), 2, "0", STR_PAD_LEFT);
        $clcodi = str_pad(substr($presupuesto['id_cliente'], 2, 4), 5, "0", STR_PAD_LEFT);

        if ($presupuesto['dire_envio'] == 'fiscal' OR $presupuesto['dire_envio'] == '0'):
            $abdirv = str_pad(" ", 1);
            $abdire = "   ";
        else:
            $abdirv = str_pad("V", 1);
            $abdire = str_pad($presupuesto['dire_envio'],3," ", STR_PAD_LEFT);
        endif;

        $abaget = "   ";
        if ($presupuesto['agencia'] == "seleccionar_dire"):
            $query_agencia_clientes = mysqli_query($link, "SELECT claget FROM clientes WHERE clproc = '$clproc' AND clcodi = '$clcodi'");
            $result_agencia_clientes = $query_agencia_clientes->fetch_assoc();
            if(!empty($result_agencia_clientes['claget'])):
                $abaget = str_pad(strtoupper($result_agencia_clientes['claget']), 3, " ", STR_PAD_LEFT);
            endif;
        elseif ($presupuesto['agencia'] == "fiscal"):
                $abaget = "   ";
        else:
            if(!is_numeric($presupuesto['agencia'])):
                $abaget = strtoupper($presupuesto['agencia']);
            endif;
        endif;

        $abaget = str_pad($abaget, 3, " ", STR_PAD_LEFT);

        //Referencia
        if (empty($presupuesto['referencia'])):
            $abspeed = "          ";
        else:
            $abspeed = str_pad(substr($presupuesto['referencia'], 0, 10), 10, " ", STR_PAD_RIGHT); //ABSPEED
        endif;
        //$abspeed = substr($abspeed,9,10);

        //Salida
        $abfcsa = str_pad("00000000", 8);

        //Reembolso
        $abimpr = str_pad("000000000", 9);

        $aux_codi = ltrim($clcodi, "0");

        $id_pres = hideNumPre($presupuesto['num_pre'], $clproc, $aux_codi);

        $id_pres = str_pad($id_pres, 7, "0", STR_PAD_LEFT);

        $abtecn = str_pad("220", 3); //Tecnico

        //MOSQUICAB
        $abalma = str_pad("20", 2); //20 Producto final o 10 embalaje
        fwrite($file_mosquicab, $abalma);
        $abtipa = str_pad("1", 1); //SIEMPRE 1
        fwrite($file_mosquicab, $abtipa);
        $abtipp = str_pad("Q", 1); //SIEMPRE Q
        fwrite($file_mosquicab, $abtipp);
        $abfcha = str_pad($fecha, 8); //Fecha presupuesto
        fwrite($file_mosquicab, $abfcha);
        fwrite($file_mosquicab, $clproc); //Pro cliente
        fwrite($file_mosquicab, $clcodi); //Codi cliente
        //TIPO DIRECCION V asociada (direccion cliente en blanco es la asignada generica un espacio y no enviar informacion en mosquidir) 1 caracter
        fwrite($file_mosquicab, $abdirv);
        //ID OTRA DIRECCION 3 ceros
        fwrite($file_mosquicab, $abdire);
        fwrite($file_mosquicab, $abaget); //agencia
        //Referencia
        fwrite($file_mosquicab, $abspeed);
        fwrite($file_mosquicab, $abfcsa);
        fwrite($file_mosquicab, $abimpr);
        //id presupuesto
        fwrite($file_mosquicab, $id_pres);
        fwrite($file_mosquicab, $abtecn);
        fwrite($file_mosquicab, $total_albaranes);
        fwrite($file_mosquicab, "\r\n");

        //ENDMOSQUICAB

        $aenrr = "0";
        $aenord = "0";
        $num_alb = "0";
        while ($albaran = $query_albaranes->fetch_assoc()):

            if (!empty($albaran['albfami'])):

                $aenrr++;
                $aenrr = str_pad($aenrr, 3, "0", STR_PAD_LEFT); //Número linea
                $aenord = $aenrr * 10;
                $aenord = str_pad($aenord, 4, "0", STR_PAD_LEFT); //Orden linea
                $num_alb++;
                $cont_alb = $num_albaranes;

                //LINEA 1 ó 3
                $aetipl = "1";
                if ($albaran['albobs'] == "lacado"):
                    $aetipl = "3";
                    $aearti = str_pad($albaran['albarti'], "6", "0", STR_PAD_LEFT);
                endif;

                //Expositores
                if ($albaran['albfami'] == "1999"):
                    $aearti = str_pad($albaran['albarti'], "6", "0", STR_PAD_LEFT);
                endif;

                $aefami = $albaran['albfami'];
                $aesubf = str_pad($albaran['albsub'], 2, "0", STR_PAD_LEFT); //Añadida subfamilia
                $aecolo = str_pad($albaran['albco'], 6, "0", STR_PAD_LEFT); //Color
                $aerep = str_pad("   ", 3); //Repliegue
                //INCREMENTOS ENR. VENTANA
                if ($aefami == "2731" AND strpos($albaran['albobs'], "is_increment") !== false):
                    $aefami = "2799";
                    $aesubf = "90";
                    $aecolo = str_pad("000100", 6); //Color
                    $aearti = str_replace("is_increment279990", "", $albaran['albobs']); //ararti
                endif;

                if (in_array($aefami, array('2731','2732', '2733', '2734', '2735', '2736'), true) AND empty($albaran['albobs'])):
                    $rango = "";

                    $input_width_product_rango = round_number_to_par($albaran['albanc'], $array_familia[$aefami]['min_ran_anc']);
                    $input_height_product_rango = round_number_to_par($albaran['albalt'], $array_familia[$aefami]['min_ran_alt']);

                    $rango = $input_width_product_rango . $input_height_product_rango;

                    $aearti = str_pad($rango, "6", "0", STR_PAD_LEFT);

                    $aerep = str_pad("ME1", 3); //Repliegue

                endif;

                if (in_array($aefami, array('1791','1792', '1793', '1794', '1795', '1796'), true) AND strpos($albaran['albobs'], "is_component") !== false):
                    $aearti = $aecolo;
                endif;

                $aecapr = str_pad($albaran['albunit'], 5, "0", STR_PAD_LEFT); //Cantidad
                $aeanc = $albaran['albanc'] * 1000;
                $aealt = $albaran['albalt'] * 1000;
                $aeanc = str_replace(".", "", $aeanc);
                $aealt = str_replace(".", "", $aealt);
                $aeanc = str_pad($aeanc, 5, "0", STR_PAD_LEFT); //Ancho
                $aealt = str_pad($aealt, 5, "0", STR_PAD_LEFT); //Alto
                $aealt = str_replace(".", "", $aealt);
                $aeman = str_pad("  ", 2); //Mandos
                $aecc = str_pad("00000", 5); //Largo mandos
                $aealma = $albaran['is_almacen'];
                if ($aealma == 0):
                    $aealma = "20";
                else:
                    $aealma = "10";
                endif;

                fwrite($file_mosquidet, $aealma); //igual que cabecera
                fwrite($file_mosquidet, $aenrr); //numero linea
                fwrite($file_mosquidet, $aenord);
                fwrite($file_mosquidet, $aetipl); //tipo linea
                fwrite($file_mosquidet, $aefami);
                fwrite($file_mosquidet, $aesubf); //subfamilia
                fwrite($file_mosquidet, $aecolo); //color
                fwrite($file_mosquidet, $aearti); //color

                //ENVIAR ARARTI

                $aetmt = str_pad("      ", 6); //$aetmt
                $aever = " "; //Vertical
                $aeinc = " "; //Inclinacion

                $aedesc = "Descripcion articulo";
                //Descripcion
                if (in_array($aefami, array('2731','2732', '2733', '2734', '2735', '2736'), true)):
                    $query_desc = mysqli_query($link, "SELECT fadesc FROM familias WHERE fafami = '$aefami'");

                    $desc = $query_desc->fetch_assoc();

                    $aedesc = str_pad($desc['fadesc'], 30, " ", STR_PAD_RIGHT); //Descripcion Pendiente obtenerla de artículos
                else:
                    $query_desc = mysqli_query($link, "SELECT ardesc FROM articulos WHERE arfami = '" . $aefami . "' AND arsubf = '" . $aesubf . "' AND ararti = '" . $aearti . "'");

                    $desc = $query_desc->fetch_assoc();

                    $aedesc = str_pad($desc['ardesc'], 30, " ", STR_PAD_RIGHT); //Descripcion Pendiente obtenerla de artículos
                endif;
                fwrite($file_mosquidet, $aedesc);
//
                fwrite($file_mosquidet, $aecapr); //Cantidad
                fwrite($file_mosquidet, $aeanc); //Ancho
                fwrite($file_mosquidet, $aealt); //Alto
                fwrite($file_mosquidet, $aeman); //Mando
                fwrite($file_mosquidet, $aerep); //Repliegue
                fwrite($file_mosquidet, $aecc); //Largo de mandos
                fwrite($file_mosquidet, $aetmt); //
                fwrite($file_mosquidet, $aever); //
                fwrite($file_mosquidet, $aeinc); //
                fwrite($file_mosquidet, $clproc); //Igual que en cabecera
                fwrite($file_mosquidet, $clcodi); //Igual que en cabecera
                fwrite($file_mosquidet, $abspeed); //Igual que en cabecera
                fwrite($file_mosquidet, $id_pres); //Igual que en cabecera
                fwrite($file_mosquidet, $abtecn); //Tecnico
                fwrite($file_mosquidet, $cont_alb); //Contador alb
                fwrite($file_mosquidet, "\r\n");

            endif;
        endwhile;


        if ($abdirv == "V"):
            if($abdire <= 0):
                $query_clientes = mysqli_query($link, "SELECT *  FROM clientes WHERE clproc = '$clproc' AND clcodi = '$clcodi'");
            else:
                $query_clientes = mysqli_query($link, "SELECT *  FROM direcciones_envio_pedidos WHERE id = '$abdire' AND clproc = '$clproc' AND clcodi = '$clcodi'");
            endif;
            while ($result_clientes = $query_clientes->fetch_assoc()):

                //MOSQUIDIR
                //UTF Codificacion
                //Mayusculas
                //Rellenar caracteres
                fwrite($file_mosquidir, $abalma);
                $clnomb = utf8_encode($result_clientes['clnomb']);
                $clnomb = strtoupper($clnomb);
                $clnomb = str_pad($clnomb, 40);
                fwrite($file_mosquidir, $clnomb);

                $cldir1 = utf8_encode($result_clientes['cldir1']);
                $cldir1 = strtoupper($cldir1);
                $cldir1 = str_pad($cldir1, 30);
                fwrite($file_mosquidir, $cldir1);

                $cldir2 = utf8_encode($result_clientes['cldir2']);
                $cldir2 = strtoupper($cldir2);
                $cldir2 = str_pad($cldir2, 30);
                fwrite($file_mosquidir, $cldir2);

                $clpais = str_pad($result_clientes['clpais'], 2, "0", STR_PAD_LEFT);
                fwrite($file_mosquidir, $clpais);
                $clppro = str_pad($result_clientes['clppro'], 4, "0", STR_PAD_LEFT);
                fwrite($file_mosquidir, $clppro);
                $clprov = str_pad($result_clientes['clprov'], 4, "0", STR_PAD_LEFT);
                fwrite($file_mosquidir, $clprov);
                $clpobl = str_pad($result_clientes['clpobl'], 3, "0", STR_PAD_LEFT);
                fwrite($file_mosquidir, $clpobl);
                $clcont = str_pad($result_clientes['clcont'], 2);
                fwrite($file_mosquidir, $clcont);

                $clobs1 = utf8_encode($result_clientes['clobs1']);
                $clobs1 = strtoupper($clobs1);
                $clobs1 = str_pad($clobs1, 30);
                fwrite($file_mosquidir, $clobs1);

                fwrite($file_mosquidir, $abspeed);

                $query_poblacion = mysqli_query($link, "SELECT cpnomb FROM poblaciones WHERE cppais = '$clpais' AND cpprov = '$clprov' AND cppobl = '$clpobl' AND cpcont = '$clcont'");
                $result_poblacion = $query_poblacion->fetch_assoc();

                $clnompro = str_pad($result_poblacion['cpnomb'], 30);
                fwrite($file_mosquidir, $clnompro);

                $query_provincia = mysqli_query($link, "SELECT ponomb FROM provincias WHERE popais = '$clpais' AND poprov = '$clprov'");
                $result_provincia = $query_provincia->fetch_assoc();

                $clnompro2 = str_pad($result_provincia['ponomb'], 30);
                fwrite($file_mosquidir, $clnompro2);
                fwrite($file_mosquidir, $id_pres);
                fwrite($file_mosquidir, $abtecn);
                fwrite($file_mosquidir, "\r\n");
            endwhile;
        endif;

    endif;

endwhile;

fclose($file_mosquidir);
fclose($file_mosquidet);
fclose($file_mosquicab);