#!/usr/bin/php
<?php
require '../config.php';

$id_pedido = "";
$conn_odbc = new Conn_odbc();
$conexion = $conn_odbc->connect();
$odbc = new Odbc();
$wbfche = date("Ymd");
$wbhora = date("His");
$hora_actual = date("Y-m-d H:i:s");

function hideNumPre_aux($text, $clproc, $clcodi) {
    $text = str_replace('Ppto. MT. CL ', '', $text);
    $text = str_replace('Ped. MT. CL ', '', $text);
    $text = str_replace($clproc . $clcodi . '-', '', $text);
    $text = ltrim($text, '0');
    return $text;
}

function round_number_to_par_aux($number, $min_value = NULL) {
    if (substr($number, 3, 1) <= 5):
        $number = $number + '0.05';
    endif;
    $number = round($number, 1, PHP_ROUND_HALF_EVEN);
    $number = str_replace(array(",", "."), '', $number);
    if ($number == "1"):
        $number = "10";
    elseif ($number == "2"):
        $number = "20";
    else:
        if ($number % 2 != 0):
            $number++;
        endif;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

$fecha = date('d_m_Y');

//ELEGIR UNA OPCION U OTRA
$link = mysqli_connect("82.98.160.111", "sql_mos17", "t5u8KoB4", "mosquiflex") or die('No se pudo conectar: ' . mysqli_error());

//$link = mysqli_connect("", "root", "", "mosquiflex") or die('No se pudo conectar: ' . mysqli_error());

$query_presupuestos = mysqli_query($link, "SELECT *, DATE_FORMAT(fecha,'%d_%m_%Y') AS fecha2  FROM presupuestos HAVING fecha2 = '" . $fecha . "' AND grabado = '0' AND empresa = '".empresa."' ORDER BY fecha2");

$query_familia = mysqli_query($link, "SELECT fafami, fadesc, status, min_ran_anc, min_ran_alt, empresa FROM familias WHERE empresa = '".empresa."'");
$array_familia = array();
while ($familia = $query_familia->fetch_assoc()):
    $array_familia[$familia['fafami']] = $familia;
endwhile;

while ($presupuesto = $query_presupuestos->fetch_assoc()):

    $id = $presupuesto['id'];

    if (!empty($id)):

        $query_albaranes = mysqli_query($link, "SELECT * FROM albaranes WHERE num_pre = '$id' AND empresa = '".empresa."'");
        $num_albaranes = mysqli_affected_rows($link);
        $total_albaranes = str_pad($num_albaranes, 2, "0", STR_PAD_LEFT);

        $fecha = new DateTime($presupuesto['fecha']);
        $fecha = $fecha->format('Ymd');

        $clproc = str_pad(substr($presupuesto['id_cliente'], 0, 2), 2, "0", STR_PAD_LEFT);
        $clcodi = str_pad(substr($presupuesto['id_cliente'], 2, 4), 5, "0", STR_PAD_LEFT);

        if ($presupuesto['dire_envio'] == 'fiscal' OR $presupuesto['dire_envio'] == '0'):
            $abdirv = str_pad(" ", 1);
            $abdire = "   ";
        else:
            $abdirv = str_pad("V", 1);
            $abdire = str_pad($presupuesto['dire_envio'], 3, " ", STR_PAD_LEFT);
        endif;

        $abaget = "   ";
        if ($presupuesto['agencia'] == "seleccionar_dire"):
            $query_agencia_clientes = mysqli_query($link, "SELECT claget FROM clientes WHERE clproc = '$clproc' AND clcodi = '$clcodi' AND empresa = '".empresa."'");
            $result_agencia_clientes = $query_agencia_clientes->fetch_assoc();
            if (!empty($result_agencia_clientes['claget'])):
                $abaget = str_pad(strtoupper($result_agencia_clientes['claget']), 3, " ", STR_PAD_LEFT);
            endif;
        elseif ($presupuesto['agencia'] == "fiscal"):
            $abaget = "   ";
        else:
            if (!is_numeric($presupuesto['agencia'])):
                $abaget = strtoupper($presupuesto['agencia']);
            endif;
        endif;

        $abaget = str_pad($abaget, 3, " ", STR_PAD_LEFT);

        //Referencia
        if (empty($presupuesto['referencia'])):
            $abspeed = "          ";
        else:
            $abspeed = str_pad(substr($presupuesto['referencia'], 0, 10), 10, " ", STR_PAD_RIGHT); //ABSPEED
        endif;
        //$abspeed = substr($abspeed,9,10);
        //Salida
        $abfcsa = str_pad("00000000", 8);

        //Reembolso
        $abimpr = str_pad("000000000", 9);

        $aux_codi = ltrim($clcodi, "0");

        $id_pres = hideNumPre_aux($presupuesto['num_pre'], $clproc, $aux_codi);

        $id_pres = str_pad($id_pres, 7, "0", STR_PAD_LEFT);

        $abtecn = str_pad("220", 3); //Tecnico
        //MOSQUICAB
        $abalma = str_pad("20", 2); //20 Producto final o 10 embalaje
        $abtipa = str_pad("1", 1); //SIEMPRE 1
        $abtipp = str_pad("Q", 1); //SIEMPRE Q
        $abfcha = str_pad($fecha, 8); //Fecha presupuesto
        //TIPO DIRECCION V asociada (direccion cliente en blanco es la asignada generica un espacio y no enviar informacion en mosquidir) 1 caracter
        //ID OTRA DIRECCION 3 ceros
        //Referencia
        //id presupuesto

        $array_tabla = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWB100");
        $wbnro = count($array_tabla) + 1;
        $wbnro = str_pad($wbnro, 7, "0", STR_PAD_LEFT);
        $wempr = "MT";

        $existe_id_pedido = existe_id_pedido($id_pres, "GCWB100", "WBID", "WBEMPR");

        if (empty($existe_id_pedido)):
            $consulta = "INSERT INTO GCWB100 (WBNRO, WBEMPR, WBALMA, WBTIPA, WBTIPP, WBFCHA, WBPROC, WBCODI, WBDIRV, WBDIRE, WBAGET, WBSPED, WBFCSA, WBIMPR, WBID, WBTECN, WBNPED, WBFCHE, WBHORA) VALUES (" . $wbnro . ", '" . empresa . "', " . $abalma . ", " . $abtipa . ", '" . $abtipp . "', " . $abfcha . ", " . $clproc . ", " . $clcodi . ", 'V', '000', '000', '" . $abspeed . "', " . $abfcsa . ", " . $abimpr . ", " . $id_pres . ", " . $abtecn . ", " . $total_albaranes . ", " . $wbfche . ", " . $wbhora . ")";

            echo "CABECERA->" . $consulta . "<br>";

            $odbc->ejecutar($conexion, $consulta);

            mysqli_query($link, "UPDATE presupuestos SET grabado = '1' WHERE num_pre = '$id' AND empresa = '".empresa."'");


        else:

            echo "<br><span class='mensaje_inicial'>" . $hora_actual . " Existe pedido ID ->" . $id_pres . "</span><br>";

            $array_tabla_log = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWX100");
            $wbnro_log = count($array_tabla_log) + 1;

            $mensaje_error = "Existe pedido ID ->" . $id_pres;

            $consulta_log = "INSERT INTO GCWX100 (WXNRO, WXID, WXTECN, WXFCHE, WXHORA, WXTXT) VALUES (" . $wbnro_log . ", " . $id_pres . ", " . $abtecn . ", " . $wbfche . ", " . $wbhora . ", '" . $mensaje_error . "')";

            $odbc->ejecutar($conexion, $consulta_log);
            $consulta = "<br><span class='mensaje_error'>" . $hora_actual . " Error en cabecera. Existe id de pedido</span>";

        endif;

        //ENDMOSQUICAB

        $aenrr = "0";
        $aenord = "0";
        $num_alb = "0";
        while ($albaran = $query_albaranes->fetch_assoc()):

            if (!empty($albaran['albfami']) || $albaran['albobs'] == "Linea3"):

                $aenrr++;
                $aenrr = str_pad($aenrr, 3, "0", STR_PAD_LEFT); //Número linea
                $aenord = $aenrr * 10;
                $aenord = str_pad($aenord, 4, "0", STR_PAD_LEFT); //Orden linea
                $num_alb++;
                $cont_alb = $num_albaranes;

                //LINEA 1 ó 3
                $aetipl = "1";
                if ($albaran['albobs'] == "lacado" || $albaran['albobs'] == "Linea3"):
                    $aetipl = "3";
                    $aearti = str_pad($albaran['albarti'], "6", "0", STR_PAD_LEFT);
                    if ($albaran['albobs'] == "Linea3"):
                        $aedesc = "----------------------------------------------";
                    endif;
                endif;

                //Expositores
                if ($albaran['albfami'] == "1999"):
                    $aearti = str_pad($albaran['albarti'], "6", "0", STR_PAD_LEFT);
                endif;

                $aefami = $albaran['albfami'];
                $aesubf = str_pad($albaran['albsub'], 2, "0", STR_PAD_LEFT); //Añadida subfamilia
                $aecolo = str_pad($albaran['albco'], 6, "0", STR_PAD_LEFT); //Color
                $aerep = str_pad("   ", 3); //Repliegue
                //INCREMENTOS ENR. VENTANA
                if ($aefami == "2731" AND strpos($albaran['albobs'], "is_increment") !== false):
                    $aefami = "2799";
                    $aesubf = "90";
                    $aecolo = str_pad("000100", 6); //Color
                    $aearti = str_replace("is_increment279990", "", $albaran['albobs']); //ararti
                endif;

                if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true) AND empty($albaran['albobs'])):
                    $rango = "";

                    $input_width_product_rango = round_number_to_par_aux($albaran['albanc'], $array_familia[$aefami]['min_ran_anc']);
                    $input_height_product_rango = round_number_to_par_aux($albaran['albalt'], $array_familia[$aefami]['min_ran_alt']);

                    $rango = $input_width_product_rango . $input_height_product_rango;

                    $aearti = str_pad($rango, "6", "0", STR_PAD_LEFT);

                    $aerep = str_pad("ME1", 3); //Repliegue

                endif;

                if (in_array($aefami, array('1791', '1792', '1793', '1794', '1795', '1796'), true) AND strpos($albaran['albobs'], "is_component") !== false):
                    $aearti = $aecolo;
                endif;

                $aecapr = str_pad($albaran['albunit'], 5, "0", STR_PAD_LEFT); //Cantidad
                $aeanc = $albaran['albanc'];
                $aealt = $albaran['albalt'];
                //$aeanc = str_replace(".", "", $aeanc);
                //$aealt = str_replace(".", "", $aealt);
                $aeanc = str_pad($aeanc, 6, "0", STR_PAD_LEFT); //Ancho
                $aealt = str_pad($aealt, 6, "0", STR_PAD_LEFT); //Alto
                $aeman = str_pad("0", 2); //Mandos
                $aecc = str_pad("00000", 5); //Largo mandos
                $aealma = $albaran['is_almacen'];
                if ($aealma == 0):
                    $aealma = "20";
                else:
                    $aealma = "10";
                endif;

                //ENVIAR ARARTI

                $aetmt = str_pad("0", 6); //$aetmt
                $aever = "0"; //Vertical
                $aeinc = "0"; //Inclinacion

                $aedesc = "Descripcion articulo";
                //Descripcion
                if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true)):
                    $query_desc = mysqli_query($link, "SELECT fadesc FROM familias WHERE fafami = '$aefami'");

                    $desc = $query_desc->fetch_assoc();

                    $aedesc = str_pad($desc['fadesc'], 30, " ", STR_PAD_RIGHT); //Descripcion Pendiente obtenerla de artículos
                else:
                    $query_desc = mysqli_query($link, "SELECT ardesc FROM articulos WHERE arfami = '" . $aefami . "' AND arsubf = '" . $aesubf . "' AND ararti = '" . $aearti . "' AND empresa = '".empresa."'");

                    $desc = $query_desc->fetch_assoc();

                    $aedesc = str_pad($desc['ardesc'], 30, " ", STR_PAD_RIGHT); //Descripcion Pendiente obtenerla de artículos
                endif;

                $array_tabla = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWE100");
                $wbnro = count($array_tabla) + 1;
                $wbnro = str_pad($wbnro, 7, "0", STR_PAD_LEFT);
                $wempr = "MT";

                if ($aefami == '2736'):
                    $aerep = $albaran['albobs'];
                endif;

                //$existe_id_pedido = existe_id_pedido($id_pres, "GCWE100", "WEID");
                //if (empty($existe_id_pedido)):
                $consulta = "INSERT INTO GCWE100 (WENRO, WEEMPR, WEALMA, WENRR, WENORD, WETIPL, WEFAMI, WESUBF, WECOLO, WEARTI, WEDESC, WECAPR, WEANCH, WEALTO, WEMAND, WEREPL, WECC, WETOTM, WEVERT, WEINCL, WEPROC, WECODI, WESPED, WEID, WETECN, WEFCHE, WEHORA) VALUES (" . $wbnro . ", '" . $wempr . "', " . $aealma . ", " . $aenrr . ", " . $aenord . ", " . $aetipl . ", " . $aefami . ", " . $aesubf . ", " . $aecolo . ", " . $aearti . ", '" . $aedesc . "', " . $aecapr . ", " . $aeanc . ", " . $aealt . ", " . $aeman . ", '" . $aerep . "', " . $aecc . ", " . $aetmt . ", " . $aever . ", '" . $aeinc . "', " . $clproc . ", " . $clcodi . ", '" . $abspeed . "', " . $id_pres . ", " . $abtecn . ", " . $wbfche . ", " . $wbhora . ")";

                echo "DETALLE->" . $consulta . "<br>";

                $odbc->ejecutar($conexion, $consulta);

                $id_alb = $albaran['id'];

//                else:
//                    echo "<br><span class='mensaje_inicial'>" . $hora_actual . " Existe pedido ID ->" . $id_pres . "</span><br>";
//
//                    $array_tabla_log = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWX100");
//                    $wbnro_log = count($array_tabla_log) + 1;
//
//                    $mensaje_error = "Existe pedido ID ->" . $id_pres;
//
//                    $consulta_log = "INSERT INTO GCWX100 (WXNRO, WXID, WXTECN, WXFCHE, WXHORA, WXTXT) VALUES (" . $wbnro_log . ", " . $id_pres . ", " . $abtecn . ", " . $wbfche . ", " . $wbhora . ", '" . $mensaje_error . "')";
//
//                    $odbc->ejecutar($conexion, $consulta_log);
//                    $consulta = "<br><span class='mensaje_error'>" . $hora_actual . " Error en detalle. Existe id de pedido</span>";
//                endif;

            endif;
        endwhile;


        //if ($abdirv == "V"):
        if ($abdire <= 0):
            $query_clientes = mysqli_query($link, "SELECT *  FROM clientes WHERE clproc = '$clproc' AND clcodi = '$clcodi' AND empresa = '".empresa."'");
        else:
            $query_clientes = mysqli_query($link, "SELECT *  FROM direcciones_envio_pedidos WHERE id = '$abdire' AND clproc = '$clproc' AND clcodi = '$clcodi' AND empresa = '".empresa."'");
        endif;
        while ($result_clientes = $query_clientes->fetch_assoc()):

            //MOSQUIDIR
            //UTF Codificacion
            //Mayusculas
            //Rellenar caracteres
            $clnomb = utf8_encode($result_clientes['clnomb']);
            $clnomb = strtoupper($clnomb);
            $clnomb = str_pad($clnomb, 40);

            $cldir1 = utf8_encode($result_clientes['cldir1']);
            $cldir1 = strtoupper($cldir1);
            $cldir1 = str_pad($cldir1, 30);

            $cldir2 = utf8_encode($result_clientes['cldir2']);
            $cldir2 = strtoupper($cldir2);
            $cldir2 = str_pad($cldir2, 30);

            $clpais = str_pad($result_clientes['clpais'], 2, "0", STR_PAD_LEFT);
            $clppro = str_pad($result_clientes['clppro'], 4, "0", STR_PAD_LEFT);
            $clprov = str_pad($result_clientes['clprov'], 4, "0", STR_PAD_LEFT);
            $clpobl = str_pad($result_clientes['clpobl'], 3, "0", STR_PAD_LEFT);
            $clcont = str_pad($result_clientes['clcont'], 2);

            $clobs1 = utf8_encode($result_clientes['clobs1']);
            $clobs1 = strtoupper($clobs1);
            $clobs1 = str_pad($clobs1, 30);

            $query_poblacion = mysqli_query($link, "SELECT cpnomb FROM poblaciones WHERE cppais = '$clpais' AND cpprov = '$clprov' AND cppobl = '$clpobl' AND cpcont = '$clcont'");
            $result_poblacion = $query_poblacion->fetch_assoc();

            $clnompro = str_pad($result_poblacion['cpnomb'], 30);

            $query_provincia = mysqli_query($link, "SELECT ponomb FROM provincias WHERE popais = '$clpais' AND poprov = '$clprov' AND empresa = '".empresa."'");
            $result_provincia = $query_provincia->fetch_assoc();

            $clnompro2 = str_pad($result_provincia['ponomb'], 30);

            $array_tabla = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWE100");
            $wgnro = count($array_tabla) + 1;
            $wgnro = str_pad($wgnro, 7, "0", STR_PAD_LEFT);
            $wempr = "MT";

            $existe_id_pedido = existe_id_pedido($id_pres, "GCWG100", "WGID", "WGEMPR");

            if (empty($existe_id_pedido)):
                $consulta = "INSERT INTO GCWG100 (WGNRO, WGEMPR, WGALMA, WGDEST, WGDIR1, WGDIR2, WGPAIS, WGPPRO, WGPROV, WGPOBL, WGCONT, WGOBSE, WGSPED, WGNOPB, WGNOPR, WGID, WGTECN, WGFCHE, WGHORA) VALUES (" . $wgnro . ", '" . $wempr . "', " . $abalma . ", '" . $clnomb . "', '" . $cldir1 . "', '" . $cldir2 . "', " . $clpais . ", " . $clproc . ", " . $clprov . ", " . $clpobl . ", '" . $clcont . "', '" . $clobs1 . "', '" . $abspeed . "', '" . $clnompro . "', '" . $clnompro2 . "', '" . $id_pres . "', " . $abtecn . "," . $wbfche . ", " . $wbhora . ")";

                echo "DIRECCION->" . $consulta . "<br>";

                $odbc->ejecutar($conexion, $consulta);

            else:

                echo "<br><span class='mensaje_inicial'>" . $hora_actual . " Existe pedido ID ->" . $id_pres . "</span><br>";

                $array_tabla_log = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWX100");
                $wbnro_log = count($array_tabla_log) + 1;

                $mensaje_error = "Existe pedido ID ->" . $id_pres;

                $consulta_log = "INSERT INTO GCWX100 (WXNRO, WXID, WXTECN, WXFCHE, WXHORA, WXTXT) VALUES (" . $wbnro_log . ", " . $id_pres . ", " . $abtecn . ", " . $wbfche . ", " . $wbhora . ", '" . $mensaje_error . "')";

                $odbc->ejecutar($conexion, $consulta_log);

                $consulta = "<br><span class='mensaje_error'>" . $hora_actual . " Error en dirección. Existe id de pedido</span>";
            endif;
        endwhile;
    //endif;

    endif;

endwhile;
