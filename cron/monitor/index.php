<?php
require '../../config.php';

$hora_actual = date("Y-m-d H:i:s");
$class_line = "mensaje_correcto";

?>
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <div class="content_monitor">
        <span class="mensaje_inicial"><br><?= $hora_actual ?> Iniciamos monotorización</span><br>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="js/js_cron.js?v=<?= microtime() ?>"></script>