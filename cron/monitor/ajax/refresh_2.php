<?php

require '../../../config.php';

ini_set("display_errors", 0);

$id_pedido = "";
$conn_odbc = new Conn_odbc();
$conexion = $conn_odbc->connect();
$odbc = new Odbc();
$wbfche = date("Ymd");
$wbhora = date("His");
$hora_actual = date("Y-m-d H:i:s");
$numero_albaran = 1;

function hideNumPre_aux($text, $clproc, $clcodi) {
    $text = str_replace('Ppto. EN. CL ', '', $text);
    $text = str_replace('Ped. EN. CL ', '', $text);
    $text = str_replace($clproc . $clcodi . '-', '', $text);
    $text = ltrim($text, '0');
    $text = substr($text, -7);
    return $text;
}

function round_number_to_par_aux($number, $min_value = NULL) {
    if (substr($number, 3, 1) <= 5):
        $number = $number + '0.05';
    endif;
    $number = round($number, 1, PHP_ROUND_HALF_EVEN);
    $number = str_replace(array(",", "."), '', $number);
    if ($number == "1"):
        $number = "10";
    elseif ($number == "2"):
        $number = "20";
    else:
        if ($number % 2 != 0):
            $number++;
        endif;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

$fecha = date('d_m_Y');

//ELEGIR UNA OPCION U OTRA
$link = mysqli_connect("217.18.166.150", "sql_mos17", "t5u8KoB4", "mosquiflex") or die('No se pudo conectar: ' . mysqli_error());
//$link = mysqli_connect("", "root", "", "mosquiflex") or die('No se pudo conectar: ' . mysqli_error());

$query_presupuestos = mysqli_query($link, "SELECT *, DATE_FORMAT(fecha,'%d_%m_%Y') AS fecha2  FROM presupuestos HAVING fecha2 = '" . $fecha . "' AND grabado = '0' AND empresa = '" . empresa . "' ORDER BY fecha2");

$query_familia = mysqli_query($link, "SELECT fafami, fadesc, status, min_ran_anc, min_ran_alt, empresa FROM familias WHERE empresa = 'MT'");
$array_familia = array();
while ($familia = $query_familia->fetch_assoc()):
    $array_familia[$familia['fafami']] = $familia;
endwhile;

//Recorremos los presupuestos
while ($presupuesto = $query_presupuestos->fetch_assoc()):

    $mensaje_error_monitor = "";

    $id = $presupuesto['id'];

    if (!empty($id)):


        $insertar_pedidos = 0;
        $insertar_detalle = 0;
        $insertar_dire = 0;

        $query_albaranes = mysqli_query($link, "SELECT * FROM albaranes WHERE num_pre = '$id' AND empresa = 'EN'");
        $query_albaranes_aux = mysqli_query($link, "SELECT * FROM albaranes WHERE num_pre = '$id' AND empresa = 'EN'");
        //$num_albaranes = mysqli_affected_rows($link);
        //Modificado 20/02/18
        //$total_albaranes = str_pad($num_albaranes, 3, "0", STR_PAD_LEFT);

        $fecha = new DateTime($presupuesto['fecha']);
        $fecha = $fecha->format('Ymd');

        $clproc = $presupuesto['cod_proc'];
        $clcodi = $presupuesto['cod_clie'];


        if ($presupuesto['dire_envio'] == 'fiscal' || $presupuesto['dire_envio'] == '0'):
            $abdirv = str_pad(" ", 1);
            $abdire = "000";
            //Modificado 20/02/18
            $grabar_dire = "0";
        else:
            $abdirv = str_pad("V", 1);
            $abdire = str_pad($presupuesto['dire_envio'], 3, " ", STR_PAD_LEFT);
            //$abdire = "000";
            //Modificado 20/02/18
            $grabar_dire = "1";
        endif;

        $abaget = "   ";
        if ($presupuesto['agencia'] == "seleccionar_dire"):
            $query_agencia_clientes = mysqli_query($link, "SELECT claget, empresa FROM clientes WHERE clproc = '$clproc' AND clcodi = '$clcodi' AND empresa = '" . empresa . "'");
            $result_agencia_clientes = $query_agencia_clientes->fetch_assoc();
            if (!empty($result_agencia_clientes['claget'])):
                $abaget = str_pad(strtoupper($result_agencia_clientes['claget']), 3, " ", STR_PAD_LEFT);
            endif;
        elseif ($presupuesto['agencia'] == "fiscal"):
            $abaget = "";
        else:
            if (!is_numeric($presupuesto['agencia'])):
                $abaget = strtoupper($presupuesto['agencia']);
            endif;
        endif;

        $abaget = str_pad($abaget, 3, " ", STR_PAD_LEFT);

        //Referencia
        if (empty($presupuesto['referencia'])):
            $abspeed = "          ";
        else:
            $abspeed = str_pad(substr($presupuesto['referencia'], 0, 10), 10, " ", STR_PAD_RIGHT); //ABSPEED
        endif;
        //$abspeed = substr($abspeed,9,10);
        //Salida
        $abfcsa = str_pad("00000000", 8);

        //Reembolso
        $abimpr = str_pad("000000000", 9);

        $aux_codi = ltrim($clcodi, "0");

        $id_pres = hideNumPre_aux($presupuesto['num_pre'], $clproc, $aux_codi);

        if (strlen($presupuesto['id_cliente']) == "2"):
            $id_pres = substr($id_pres, 3);
        endif;

        $id_pres = str_pad($id_pres, 7, "0", STR_PAD_LEFT);

        $abtecn = str_pad("221", 3); //Tecnico
        //MOSQUICAB
        $abalma = str_pad("20", 2); //20 Producto final o 10 embalaje
        //Actualizado posteriormente segun lineas
        $abtipa = str_pad("1", 1); //SIEMPRE 1
        $abtipp = str_pad("Q", 1); //SIEMPRE Q
        $abfcha = str_pad($fecha, 8); //Fecha presupuesto
        //TIPO DIRECCION V asociada (direccion cliente en blanco es la asignada generica un espacio y no enviar informacion en mosquidir) 1 caracter
        //ID OTRA DIRECCION 3 ceros
        //Referencia
        //id presupuesto

        $array_tabla = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWB100");
        $wbnro = count($array_tabla) + 1;
        $wbnro = str_pad($wbnro, 7, "0", STR_PAD_LEFT);
        $wempr = "MT";

        $consulta = "INSERT INTO GCWB100 (WBNRO, WBEMPR, WBALMA, WBTIPA, WBTIPP, WBFCHA, WBPROC, WBCODI, WBDIRV, WBDIRE, WBAGET, WBSPED, WBFCSA, WBIMPR, WBID, WBTECN, WBNPED, WBFCHE, WBHORA) VALUES ('" . $wbnro . "', '" . empresa . "', '" . $abalma . "', '" . $abtipa . "', '" . $abtipp . "', '" . $abfcha . "', '" . $clproc . "', '" . $clcodi . "', '" . $abdirv . "', '" . $abdire . "', '" . $abaget . "', '" . $abspeed . "', '" . $abfcsa . "', '" . $abimpr . "', '" . $id_pres . "', '" . $abtecn . "', '1', '" . $wbfche . "', '" . $wbhora . "')";

        $insertar_pedidos = $odbc->numero_filas_afectadas($odbc->ejecutar_new($conexion, $consulta));

        if (($insertar_pedidos) > 0):
        else:

            $array_tabla_log = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWX100");
            $wbnro_log = count($array_tabla_log) + 1;

            $mensaje_error = "Error al insertar cabecera pedido->" . $id_pres;
            $mensaje_error_monitor .= "<br>" . $hora_actual . " Error al insertar cabecera pedido->" . $id_pres;

            $consulta_log = "INSERT INTO GCWX100 (WXNRO, WXID, WXTECN, WXFCHE, WXHORA, WXTXT) VALUES (" . $wbnro_log . ", " . $id_pres . ", " . $abtecn . ", " . $wbfche . ", " . $wbhora . ", '" . $mensaje_error . "')";

            $odbc->ejecutar($conexion, $consulta_log);
        endif;

        //ENDMOSQUICAB

        $aenord = "0";
        $wenped = 1;
        $aenrr = 0;
        $cont = 0;
        $cont_aux = 0;
        $wenped_aux = 1;
        $array_info_albaran = array();
        $array_info_albaran_aux = array();
        $array_info_albaran_lacado_aux = array();

        while ($albaran_aux = $query_albaranes_aux->fetch_assoc()):
            if (!empty($albaran_aux['albfami']) && !empty($albaran_aux['albsub']) && !empty($albaran_aux['albco']) && $albaran_aux['albobs'] != "Linea3" || $albaran_aux['albfami'] == "1999"):
                $array_info_albaran_aux[$cont_aux]['fami'] = $albaran_aux['albfami'];
                if (strpos($albaran['albobs'], "is_increment") !== false):
                    $array_info_albaran_aux[$cont_aux]['is_increment'] = "1";
                else:
                    $array_info_albaran_aux[$cont_aux]['is_increment'] = "0";
                endif;
                if ($cont_aux >= 1):
                    $buscado_aux = $albaran['albfami'] . "-" . $albaran['albsub'] . "-" . $albaran['albco'];
                    $key_array_aux = array_search($buscado_aux, $array_info_albaran_aux);
                    if ($key_array_aux != $cont_aux):
                        $wenped_aux = $key_array + 1;
                        $array_info_albaran_aux[$cont_aux]['albaran'] = $wenped_aux + 1;
                    else:
                        $numero_albaran++;
                        $wenped_aux = $numero_albaran;
                        $array_info_albaran_aux[$cont_aux]['albaran'] = $wenped_aux + 1;
                    endif;
                endif;
                $cont_aux++;
            endif;
        endwhile;
        while ($albaran = $query_albaranes->fetch_assoc()):
            if (!empty($albaran['albfami']) && !empty($albaran['albsub']) && !empty($albaran['albco']) && $albaran['albobs'] != "Linea3" || $albaran['albfami'] == "1999"):
                $array_info_albaran[$cont] = $albaran['albfami'] . "-" . $albaran['albsub'] . "-" . $albaran['albco'];
                if ($albaran['albobs'] != "lacado"):
                    $array_info_albaran_lacado_aux[$cont]['color'] = $albaran['albco'];
                endif;
                if ($cont >= 1):
                    $buscado = $albaran['albfami'] . "-" . $albaran['albsub'] . "-" . $albaran['albco'];
                    $key_array = array_search($buscado, $array_info_albaran);
                    if ($key_array != $cont):
                        $wenped = $key_array + 1;
                    else:
                        $numero_albaran++;
                        $wenped = $numero_albaran;
                    endif;
                endif;
                $cont++;
            endif;

            if (!empty($albaran['albfami']) || $albaran['albobs'] == "Linea3"):

                $aenrr++;
                $aenrr = str_pad($aenrr, 3, "0", STR_PAD_LEFT); //Número linea
                $aenord = $aenrr * 10;
                $aenord = str_pad($aenord, 4, "0", STR_PAD_LEFT); //Orden linea
                //LINEA 1 ó 3
                $aetipl = "1";
                if ($albaran['albobs'] == "lacado" || $albaran['albobs'] == "Linea3"):
                    $aetipl = "3";
                    $aearti = str_pad($albaran['albarti'], "6", "0", STR_PAD_LEFT);
                endif;

                //Expositores
                if ($albaran['albfami'] == "1999"):
                    $aearti = str_pad($albaran['albarti'], "6", "0", STR_PAD_LEFT);
                endif;

                $aefami = $albaran['albfami'];
                $aesubf = str_pad($albaran['albsub'], 2, "0", STR_PAD_LEFT); //Añadida subfamilia
                $aecolo = str_pad($albaran['albco'], 6, "0", STR_PAD_LEFT); //Color
                $aerep = str_pad("   ", 3); //Repliegue
                //INCREMENTOS ENR. VENTANA
                if (in_array($aefami, array('2731', '2734'), true) && strpos($albaran['albobs'], "is_increment") !== false):
                    $aefami = "2799";
                    $aesubf = "90";
                    $aecolo = str_pad("000100", 6); //Color
                    $aearti = str_replace("is_increment279990", "", $albaran['albobs']); //ararti
                endif;

                if ($aealma == "20"):
                    if (strpos($albaran['albobs'], "is_increment") !== false):
                        if (substr($array_info_albaran_aux[$cont + 1]['fami'], 0, 4) == $albaran['albfami']):
                            if (empty($cont)):
                                $wenped = "1";
                            else:
                                $wenped = $array_info_albaran_aux[$cont + 1]['albaran'];
                            endif;
                        else:
                            if (empty($cont)):
                                $wenped = "1";
                            else:
                                $wenped = $wenped + 1;
                                if ($aefami == "2799"):
                                    $wenped = $array_info_albaran_aux[$cont + 1]['albaran'];
                                endif;

                            endif;
                        endif;
                    endif;
                else:
                    $wenped = "1";
                endif;

                if ($albaran['albobs'] == "lacado"):
                    $encontrado_lacado = "0";
                    foreach ($array_info_albaran_lacado_aux as $index => $lacado_albaran):
                        $color_buscado = str_pad($aecolo, "6", "0", STR_PAD_LEFT);
                        $busca_en = str_pad($lacado_albaran['color'], "6", "0", STR_PAD_LEFT);
                        if ($color_buscado == $busca_en && $encontrado_lacado == "0"):
                            $wenped = $index + 1;
                            $encontrado_lacado = "1";
                        endif;
                    endforeach;
                endif;

                if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true) AND empty($albaran['albobs']) || $aefami == "2736"):

                    $rango = "";

                    if ($aefami == 2731):
                        $input_width_product_rango = calc_precio_enrollable_ventana($albaran['albanc'], $array_familia[$aefami]['min_ran_anc']);
                        $input_height_product_rango = calc_precio_enrollable_ventana($albaran['albalt'], $array_familia[$aefami]['min_ran_alt']);
                    endif;

                    if ($aefami == 2732):
                        $input_width_product_rango = calc_precio_ancho_enrollable_puerta($albaran['albanc'], $array_familia[$aefami]['min_ran_anc']);
                        $input_height_product_rango = calc_precio_alto_enrollable_puerta($albaran['albalt'], $array_familia[$aefami]['min_ran_alt']);
                    endif;

                    if ($aefami == 2733):
                        $aesubf_aux = 0;
                        if ($aesubf >= "11"):
                            $aesubf_aux = "1";
                        endif;
                        $input_width_product_rango = calc_precio_ancho_plisada($albaran['albanc'], $array_familia[$aefami]['min_ran_anc'], $aesubf_aux);
                        $input_height_product_rango = calc_precio_alto_plisada($albaran['albalt'], $array_familia[$id_product]['min_ran_alt']);
                    endif;

                    if ($aefami == 2734):
                        $input_width_product_rango = calc_precio_ancho_fija($albaran['albanc'], $array_familia[$aefami]['min_ran_anc']);
                        $input_height_product_rango = calc_precio_alto_fija($albaran['albalt'], $array_familia[$id_product]['min_ran_alt']);
                    endif;

                    if ($aefami == 2735):
                        //MARCOS
                        if ($albaran['albsub'] < '11'):
                            $input_width_product_rango = calc_precio_ancho_marco_corredera($albaran['albanc'], "12");
                            $input_height_product_rango = calc_precio_alto_marco_corredera($albaran['albalt'], "10");
                        //HOJAS
                        else:
                            $input_width_product_rango = calc_precio_ancho_hoja_corredera($albaran['albanc'], "6");
                            $input_height_product_rango = calc_precio_alto_hoja_corredera($albaran['albalt'], "10");
                        endif;
                    endif;

                    if ($aefami == 2736):
                        $input_width_product_rango = calc_precio_ancho_abatible($albaran['albanc'], $array_familia[$aefami]['min_ran_anc'], $aesubf);
                        $input_height_product_rango = calc_precio_alto_abatible($albaran['albalt'], $array_familia[$aefami]['min_ran_alt']);
                    endif;
                    $rango = $input_width_product_rango . $input_height_product_rango;
                    $aearti = str_pad($rango, "6", "0", STR_PAD_LEFT);
                    $aerep = str_pad("   ", 3); //Repliegue

                endif;

                if (in_array($aefami, array('1791', '1792', '1793', '1794', '1795', '1796'), true) && strpos($albaran['albobs'], "is_component") !== false):
                    $aearti = $aecolo;
                    $wenped = "1";
                endif;

                $aecapr = str_pad($albaran['albunit'], 5, "0", STR_PAD_LEFT); //Cantidad
                $aeanc = $albaran['albanc'];
                $aealt = $albaran['albalt'];
                //$aeanc = str_replace(".", "", $aeanc);
                //$aealt = str_replace(".", "", $aealt);
                $aeanc = str_pad($aeanc, 6, "0", STR_PAD_LEFT); //Ancho
                $aealt = str_pad($aealt, 6, "0", STR_PAD_LEFT); //Alto
                $aeman = str_pad(" ", 2); //Mandos
                $aecc = str_pad("00000", 5); //Largo mandos
                $aealma = $albaran['is_almacen'];
                if ($aealma == 0):
                    $aealma = "20";
                else:
                    $aealma = "10";
                endif;
                $update_wgalma = "UPDATE GCWB100 SET WGALMA = '" . $aealma . "' WHERE WBFCHE = '" . $wbfche . "' AND WBHORA = '" . $wbhora . "' AND empresa = '" . empresa . "'";
                $odbc->ejecutar($conexion, $update_wgalma);

                //ENVIAR ARARTI

                $aetmt = str_pad("0", 6); //$aetmt
                $aever = "0"; //Vertical
                $aeinc = "0"; //Inclinacion

                $aedesc = "Descripcion articulo";
                //Descripcion
                if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true)):
                    $query_desc = mysqli_query($link, "SELECT fadesc, empresa FROM familias WHERE fafami = '$aefami' AND empresa = 'MT'");

                    $desc = $query_desc->fetch_assoc();

                    $aedesc = str_pad($desc['fadesc'], 30, " ", STR_PAD_RIGHT); //Descripcion Pendiente obtenerla de artículos
                else:
                    $query_desc = mysqli_query($link, "SELECT ardesc FROM articulos WHERE arfami = '" . $aefami . "' AND arsubf = '" . $aesubf . "' AND ararti = '" . $aearti . "' AND empresa = 'MT'");

                    $desc = $query_desc->fetch_assoc();

                    $aedesc = str_pad($desc['ardesc'], 30, " ", STR_PAD_RIGHT); //Descripcion Pendiente obtenerla de artículos
                endif;

                $array_tabla = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWE100");
                $wbnro = count($array_tabla) + 1;
                $wbnro = str_pad($wbnro, 7, "0", STR_PAD_LEFT);
                $wempr = "MT";

                if ($aefami == '2731'):
                    $aerep = str_pad("ME1", 3); //Repliegue
                endif;

                if ($aefami == '2736'):
                    $aerep = $albaran['albobs'];
                endif;

                //MODIFICADO 20/02/18
                if ($albaran['albobs'] == "Linea3"):
                    $aecapr = 0;
                    $aedesc = str_pad($desc['ardesc'], 30, "-", STR_PAD_RIGHT);
                endif;

                //MODIFCADO 20/02/18
                //Kit instalación frontal (Web con código 2799 90 100002) hay que enviarlo con código 1791 33 5
                if (empresa == "EN" && $aefami == "2799" && $aesubf == "90" && $aearti == "100002"):
                    $aefami = "0000";
                    $aesubf = "00";
                    $aearti = "000000";
                    $aecolo = "000000";
                    $aetipl = "3";
                    $aetipl = "3";
                    $aedesc = "KIT INSTALACIÓN FRONTAL";
                endif;

                if (empresa == "EN" && $aefami == "2799" && $aesubf == "90" && $aearti == "100001"):
                    $aefami = "0000";
                    $aesubf = "00";
                    $aearti = "000000";
                    $aecolo = "000000";
                    $aetipl = "3";
                    $aetipl = "3";
                    $aedesc = "GUIA DOBLE FELPUDO ESTANDAR";
                endif;
                
                $aerep = strtoupper($aerep);

                $consulta = "INSERT INTO GCWE100 (WENRO, WEEMPR, WEALMA, WENRR, WENORD, WETIPL, WEFAMI, WESUBF, WECOLO, WEARTI, WEDESC, WECAPR, WEANCH, WEALTO, WEMAND, WEREPL, WECC, WETOTM, WEVERT, WEINCL, WEPROC, WECODI, WESPED, WEID, WETECN, WEFCHE, WEHORA, WENPED) VALUES ('" . $wbnro . "', '" . empresa . "', '" . $aealma . "', '" . $aenrr . "', '" . $aenord . "', '" . $aetipl . "', '" . $aefami . "', '" . $aesubf . "', '" . $aecolo . "', '" . $aearti . "', '" . $aedesc . "', '" . $aecapr . "', '" . $aeanc . "', '" . $aealt . "', '" . $aeman . "', '" . $aerep . "', '" . $aecc . "', '" . $aetmt . "', '" . $aever . "', '" . $aeinc . "', '" . $clproc . "', '" . $clcodi . "', '" . $abspeed . "', '" . $id_pres . "', '" . $abtecn . "', '" . $wbfche . "', '" . $wbhora . "', '" . $wenped . "')";

                $consulta = utf8_decode($consulta);

                echo "<br>" . $consulta . "<br>";

                $insertar_detalle = $odbc->numero_filas_afectadas($odbc->ejecutar_new($conexion, $consulta));

                if (($insertar_detalle) > 0):
                else:

                    $array_tabla_log = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWX100");
                    $wbnro_log = count($array_tabla_log) + 1;

                    $mensaje_error = "Error al insertar detalle pedido->" . $id_pres;
                    $mensaje_error_monitor .= "<br>" . $hora_actual . " Error al insertar detalle pedido->" . $id_pres;


                    $consulta_log = "INSERT INTO GCWX100 (WXNRO, WXID, WXTECN, WXFCHE, WXHORA, WXTXT) VALUES (" . $wbnro_log . ", " . $id_pres . ", " . $abtecn . ", " . $wbfche . ", " . $wbhora . ", '" . $mensaje_error . "')";

                    $odbc->ejecutar($conexion, $consulta_log);
                endif;

            endif;
        endwhile;


        //if ($abdirv == "V"):
        if ($abdire <= 0):
            $query_clientes = mysqli_query($link, "SELECT *  FROM clientes WHERE clproc = '$clproc' AND clcodi = '$clcodi' AND empresa = '" . empresa . "'");
        else:
            $query_clientes = mysqli_query($link, "SELECT *  FROM direcciones_envio_pedidos WHERE id = '$abdire' AND clproc = '$clproc' AND clcodi = '$clcodi' AND empresa = '" . empresa . "'");
        endif;
        if ($grabar_dire == "1"):
            while ($result_clientes = $query_clientes->fetch_assoc()):

                //MOSQUIDIR
                //UTF Codificacion
                //Mayusculas
                //Rellenar caracteres
                $clnomb = utf8_encode($result_clientes['clnomb']);
                $clnomb = strtoupper($clnomb);
                $clnomb = str_pad($clnomb, 40);

                $cldir1 = utf8_encode($result_clientes['cldir1']);
                $cldir1 = strtoupper($cldir1);
                $cldir1 = str_pad($cldir1, 30);

                $cldir2 = utf8_encode($result_clientes['cldir2']);
                $cldir2 = strtoupper($cldir2);
                $cldir2 = str_pad($cldir2, 30);

                $clpais = str_pad($result_clientes['clpais'], 2, "0", STR_PAD_LEFT);
                $clppro = str_pad($result_clientes['clppro'], 4, "0", STR_PAD_LEFT);
                $clprov = str_pad($result_clientes['clprov'], 4, "0", STR_PAD_LEFT);
                $clpobl = str_pad($result_clientes['clpobl'], 3, "0", STR_PAD_LEFT);
                $clcont = str_pad($result_clientes['clcont'], 2);

                $clobs1 = utf8_encode($result_clientes['clobs1']);
                $clobs1 = strtoupper($clobs1);
                $clobs1 = str_pad($clobs1, 30);

                $query_poblacion = mysqli_query($link, "SELECT cpnomb FROM poblaciones WHERE cppais = '$clpais' AND cpprov = '$clprov' AND cppobl = '$clpobl' AND cpcont = '$clcont' AND empresa = 'MT'");
                $result_poblacion = $query_poblacion->fetch_assoc();

                $clnompro = str_pad($result_poblacion['cpnomb'], 30);

                $query_provincia = mysqli_query($link, "SELECT ponomb FROM provincias WHERE popais = '$clpais' AND poprov = '$clprov' AND empresa = 'MT'");
                $result_provincia = $query_provincia->fetch_assoc();

                $clnompro2 = str_pad($result_provincia['ponomb'], 30);

                $array_tabla = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWE100");
                $wgnro = count($array_tabla) + 1;
                $wgnro = str_pad($wgnro, 7, "0", STR_PAD_LEFT);
                $wempr = "MT";

                $consulta = "INSERT INTO GCWG100 (WGNRO, WGEMPR, WGALMA, WGDEST, WGDIR1, WGDIR2, WGPAIS, WGPPRO, WGPROV, WGPOBL, WGCONT, WGOBSE, WGSPED, WGNOPB, WGNOPR, WGID, WGTECN, WGFCHE, WGHORA) VALUES (" . $wgnro . ", '" . empresa . "', " . $abalma . ", '" . $clnomb . "', '" . $cldir1 . "', '" . $cldir2 . "', " . $clpais . ", " . $clproc . ", " . $clprov . ", " . $clpobl . ", '" . $clcont . "', '" . $clobs1 . "', '" . $abspeed . "', '" . $clnompro . "', '" . $clnompro2 . "', '" . $id_pres . "', " . $abtecn . "," . $wbfche . ", " . $wbhora . ")";
                $consulta = utf8_decode($consulta);

                $insertar_dire = $odbc->numero_filas_afectadas($odbc->ejecutar_new($conexion, $consulta));

                //Modificado 20/02/18
                if (($insertar_dire) > 0 || $grabar_dire == "0"):
                else:

                    $array_tabla_log = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWX100");
                    $wbnro_log = count($array_tabla_log) + 1;

                    $mensaje_error = "Error al insertar direccion pedido->" . $id_pres;
                    $mensaje_error_monitor .= "<br>" . $hora_actual . " Error al insertar direccion pedido->" . $id_pres;

                    $consulta_log = "INSERT INTO GCWX100 (WXNRO, WXID, WXTECN, WXFCHE, WXHORA, WXTXT) VALUES (" . $wbnro_log . ", " . $id_pres . ", " . $abtecn . ", " . $wbfche . ", " . $wbhora . ", '" . $mensaje_error . "')";

                    $odbc->ejecutar($conexion, $consulta_log);
                endif;
            endwhile;

        endif;

        if ($insertar_pedidos > "0" && $insertar_detalle > "0"):
            mysqli_query($link, "UPDATE presupuestos SET grabado = '1' WHERE id = '$id' AND empresa = '" . empresa . "'");
            echo "<br><span class='mensaje_correcto'>" . $hora_actual . " Se ha grabado el pedido ->" . $id_pres . " con total albs->" . $numero_albaran . "</span><br>";
        else:
            echo "<br><span class='mensaje_error'>" . $hora_actual . " Error al grabar el pedido ->" . $id_pres . $mensaje_error_monitor . "</span><br>";
        endif;

    endif;

endwhile;

if (empty($query_presupuestos->fetch_assoc())):
    echo "<br><span class='mensaje_inicial'>" . $hora_actual . " No existen pedidos pendientes de grabación</span><br>";
else:
    echo "<br><span class='mensaje_inicial'>" . $hora_actual . " Existen pedidos</span><br>";
endif;