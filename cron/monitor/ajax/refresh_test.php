<?php

require '../../../config.php';

ini_set("display_errors", 1);

$id_pedido = "";
$wbfche = date("Ymd");
$wbhora = date("His");
$hora_actual = date("Y-m-d H:i:s");

function hideNumPre_aux($text, $clproc, $clcodi) {
    $text = str_replace('Ppto. MT. CL ', '', $text);
    $text = str_replace('Ped. MT. CL ', '', $text);
    $text = str_replace($clproc . $clcodi . '-', '', $text);
    $text = ltrim($text, '0');
    return $text;
}

function round_number_to_par_aux($number, $min_value = NULL) {
    if (substr($number, 3, 1) <= 5):
        $number = $number + '0.05';
    endif;
    $number = round($number, 1, PHP_ROUND_HALF_EVEN);
    $number = str_replace(array(",", "."), '', $number);
    if ($number == "1"):
        $number = "10";
    elseif ($number == "2"):
        $number = "20";
    else:
        if ($number % 2 != 0):
            $number++;
        endif;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

$fecha = date('d_m_Y');

//ELEGIR UNA OPCION U OTRA
$link = mysqli_connect("localhost", "root", "", "mosquiflex") or die('No se pudo conectar: ' . mysqli_error());

$query_presupuestos = mysqli_query($link, "SELECT *, DATE_FORMAT(fecha,'%d_%m_%Y') AS fecha2  FROM presupuestos HAVING fecha2 = '" . $fecha . "' AND grabado = '0' AND empresa = 'MT' ORDER BY fecha2");

$query_familia = mysqli_query($link, "SELECT fafami, fadesc, status, min_ran_anc, min_ran_alt, empresa FROM familias WHERE empresa = 'MT'");
$array_familia = array();
while ($familia = $query_familia->fetch_assoc()):
    $array_familia[$familia['fafami']] = $familia;
endwhile;

$query_albaranes = mysqli_query($link, "SELECT * FROM albaranes WHERE num_pre = '" . $_GET['id_pres'] . "' AND empresa = 'MT'");
$query_albaranes_aux = mysqli_query($link, "SELECT * FROM albaranes WHERE num_pre = '" . $_GET['id_pres'] . "' AND empresa = 'MT'");
$array_info_albaran = array();
$array_info_albaran_aux = array();
$array_info_num_albaran = array();
$aenrr = 0;
$cont = 0;
$cont_aux = 0;
$numero_albaran = 0;
$wenped = 0;
$wenped_aux = 1;
$id_pres = $_GET['id_pres'];
$aealma = "20";
$abtecn = "221";

$array_numero_albaran = array();
$array_codigo_producto = array();

$contador_productos = 0;

//NUMERACION DE ALBARANES
while ($albaran_aux = $query_albaranes_aux->fetch_assoc()):
    $aefami = $albaran_aux['albfami'];
    if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true) AND empty($albaran_aux['albobs']) || $aefami == "2736"):
        $contador_productos++;
        $buscado = $albaran_aux['albfami'] . "-" . $albaran_aux['albsub'] . "-" . $albaran_aux['albco'];
        $array_codigo_producto[$contador_productos] = $buscado;
        $array_numero_albaran[$contador_productos]['linea_producto'] = $albaran_aux['linea_producto'];
        if ($contador_productos >= 1):
            $posicion_buscado = array_search($buscado, $array_codigo_producto);
            if ($posicion_buscado !== $contador_productos):
                $wenped = $numero_albaran;
            else:
                $numero_albaran++;
                $wenped = $numero_albaran;
            endif;
            $array_numero_albaran[$albaran_aux['linea_producto']]['albaran'] = $wenped;
        endif;
    endif;

endwhile;

while ($albaran = $query_albaranes->fetch_assoc()):
    if(isset($array_numero_albaran[$albaran['linea_producto']]['albaran'])):
        $wenped = $array_numero_albaran[$albaran['linea_producto']]['albaran'];
    endif;
    $aefami = $albaran['albfami'];
    if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true) AND empty($albaran['albobs']) || $aefami == "2736"):
        if ($albaran['albobs'] != "lacado"):
            $array_info_albaran_lacado_aux[$cont]['color'] = $albaran['albco'];
        endif;
    endif;

    if (!empty($albaran['albfami']) || $albaran['albobs'] == "Linea3"):

        $aenrr++;
        $aenrr = str_pad($aenrr, 3, "0", STR_PAD_LEFT); //Número linea
        $aenord = $aenrr * 10;
        $aenord = str_pad($aenord, 4, "0", STR_PAD_LEFT); //Orden linea
        //LINEA 1 ó 3
        $aetipl = "1";
        if ($albaran['albobs'] == "lacado" || $albaran['albobs'] == "Linea3"):
            $aetipl = "3";
            $aearti = str_pad($albaran['albarti'], "6", "0", STR_PAD_LEFT);
        endif;

        //Expositores
        if ($albaran['albfami'] == "1999"):
            $aearti = str_pad($albaran['albarti'], "6", "0", STR_PAD_LEFT);
        endif;

        $aefami = $albaran['albfami'];
        $aesubf = str_pad($albaran['albsub'], 2, "0", STR_PAD_LEFT); //Añadida subfamilia
        $aecolo = str_pad($albaran['albco'], 6, "0", STR_PAD_LEFT); //Color
        $aerep = str_pad("   ", 3); //Repliegue
        //INCREMENTOS ENR. VENTANA
        if (in_array($aefami, array('2731', '2734'), true) && strpos($albaran['albobs'], "is_increment") !== false):
            $aefami = "2799";
            $aesubf = "90";
            $aecolo = str_pad("000100", 6); //Color
            $aearti = str_replace("is_increment279990", "", $albaran['albobs']); //ararti
        endif;

        if ($albaran['albobs'] == "lacado"):
            $encontrado_lacado = "0";
            foreach ($array_info_albaran_lacado_aux as $index => $lacado_albaran):
                $color_buscado = str_pad($aecolo, "6", "0", STR_PAD_LEFT);
                $busca_en = str_pad($lacado_albaran['color'], "6", "0", STR_PAD_LEFT);
                if ($color_buscado == $busca_en && $encontrado_lacado == "0"):
                    $wenped = $index + 1;
                    $encontrado_lacado = "1";
                endif;
            endforeach;
        endif;

        if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true) AND empty($albaran['albobs']) || $aefami == "2736"):

            $rango = "";
        
            $width_product_anc = number_format($albaran['albanc'], 3, '.', '');
            $height_product_alt = number_format($albaran['albalt'], 3, '.', '');

            if ($aefami == 2731):
                $input_width_product_rango = calc_precio_enrollable_ventana($width_product_anc, $array_familia[$aefami]['min_ran_anc']);
                $input_height_product_rango = calc_precio_enrollable_ventana($height_product_alt, $array_familia[$aefami]['min_ran_alt']);
            endif;

            if ($aefami == 2732):
                $input_width_product_rango = calc_precio_ancho_enrollable_puerta($width_product_anc, $array_familia[$aefami]['min_ran_anc']);
                $input_height_product_rango = calc_precio_alto_enrollable_puerta($height_product_alt, $array_familia[$aefami]['min_ran_alt']);
            endif;

            if ($aefami == 2733):
                $aesubf_aux = 0;
                if ($aesubf >= "11"):
                    $aesubf_aux = "1";
                endif;
                $input_width_product_rango = calc_precio_ancho_plisada($width_product_anc, $array_familia[$aefami]['min_ran_anc'], $aesubf_aux);
                $input_height_product_rango = calc_precio_alto_plisada($height_product_alt, $array_familia[$aefami]['min_ran_alt']);
            endif;

            if ($aefami == 2734):
                echo "<br>WIDTH PRODUCT->".$albaran['albanc']."<br>";
                $input_width_product_rango = calc_precio_ancho_fija($width_product_anc, $array_familia[$aefami]['min_ran_anc']);
                echo "<br>WIDTH PRODUCT RANGO->".$input_width_product_rango."<br>";
                $input_height_product_rango = calc_precio_alto_fija($height_product_alt, $array_familia[$aefami]['min_ran_alt']);
            endif;

            if ($aefami == 2735):
                //MARCOS
                if ($albaran['albsub'] < '11'):
                    $input_width_product_rango = calc_precio_ancho_marco_corredera($width_product_anc, "12");
                    $input_height_product_rango = calc_precio_alto_marco_corredera($height_product_alt, "10");
                //HOJAS
                else:
                    $input_width_product_rango = calc_precio_ancho_hoja_corredera($width_product_anc, "6");
                    $input_height_product_rango = calc_precio_alto_hoja_corredera($height_product_alt, "10");
                endif;
            endif;

            if ($aefami == 2736):
                $input_width_product_rango = calc_precio_ancho_abatible($width_product_anc, $array_familia[$aefami]['min_ran_anc'], $aesubf);
                $input_height_product_rango = calc_precio_alto_abatible($height_product_alt, $array_familia[$aefami]['min_ran_alt']);
            endif;
            $rango = $input_width_product_rango . $input_height_product_rango;
            echo "<br>RANGO->".$rango."<br>";
            $aearti = str_pad($rango, "6", "0", STR_PAD_LEFT);
            $aerep = str_pad("   ", 3); //Repliegue

        endif;

        if (in_array($aefami, array('1791', '1792', '1793', '1794', '1795', '1796'), true) && strpos($albaran['albobs'], "is_component") !== false):
            $aearti = $aecolo;
            $wenped = "1";
        endif;

        $aecapr = str_pad($albaran['albunit'], 5, "0", STR_PAD_LEFT); //Cantidad
        $aeanc = $albaran['albanc'];
        $aealt = $albaran['albalt'];
        //$aeanc = str_replace(".", "", $aeanc);
        //$aealt = str_replace(".", "", $aealt);
        $aeanc = str_pad($aeanc, 6, "0", STR_PAD_LEFT); //Ancho
        $aealt = str_pad($aealt, 6, "0", STR_PAD_LEFT); //Alto
        $aeman = str_pad(" ", 2); //Mandos
        $aecc = str_pad("00000", 5); //Largo mandos
        $aealma = $albaran['is_almacen'];
        if ($aealma == 0):
            $aealma = "20";
        else:
            $aealma = "10";
        endif;
        $update_wgalma = "UPDATE GCWB100 SET WGALMA = '" . $aealma . "' WHERE WBFCHE = '" . $wbfche . "' AND WBHORA = '" . $wbhora . "' AND empresa = '" . empresa . "'";
        //$odbc->ejecutar($conexion, $update_wgalma);
        //ENVIAR ARARTI

        $aetmt = str_pad("0", 6); //$aetmt
        $aever = "0"; //Vertical
        $aeinc = "0"; //Inclinacion

        $aedesc = "Descripcion articulo";
        //Descripcion
        if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true)):
            $query_desc = mysqli_query($link, "SELECT fadesc, empresa FROM familias WHERE fafami = '$aefami' AND empresa = 'MT'");

            $desc = $query_desc->fetch_assoc();

            $aedesc = str_pad($desc['fadesc'], 30, " ", STR_PAD_RIGHT); //Descripcion Pendiente obtenerla de artículos
        else:
            $query_desc = mysqli_query($link, "SELECT ardesc FROM articulos WHERE arfami = '" . $aefami . "' AND arsubf = '" . $aesubf . "' AND ararti = '" . $aearti . "' AND empresa = 'MT'");

            $desc = $query_desc->fetch_assoc();

            $aedesc = str_pad($desc['ardesc'], 30, " ", STR_PAD_RIGHT); //Descripcion Pendiente obtenerla de artículos
        endif;

        $wbnro = 1;
        //$array_tabla = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWE100");
        //$wbnro = count($array_tabla) + 1;
        //$wbnro = str_pad($wbnro, 7, "0", STR_PAD_LEFT);
        $wempr = "MT";

        if ($aefami == '2731'):
            $aerep = str_pad("ME1", 3); //Repliegue
        endif;

        if ($aefami == '2736'):
            $aerep = $albaran['albobs'];
        endif;

        //MODIFICADO 20/02/18
        if ($albaran['albobs'] == "Linea3"):
            $aecapr = 0;
            $aedesc = str_pad($desc['ardesc'], 30, "-", STR_PAD_RIGHT);
        endif;

        //MODIFCADO 20/02/18
        //Kit instalación frontal (Web con código 2799 90 100002) hay que enviarlo con código 1791 33 5
        if (empresa == "EN" && $aefami == "2799" && $aesubf == "90" && $aearti == "100002"):
            $aefami = "0000";
            $aesubf = "00";
            $aearti = "000000";
            $aecolo = "000000";
            $aetipl = "3";
            $aetipl = "3";
            $aedesc = "KIT INSTALACIÓN FRONTAL";
        endif;

        if (empresa == "EN" && $aefami == "2799" && $aesubf == "90" && $aearti == "100001"):
            $aefami = "0000";
            $aesubf = "00";
            $aearti = "000000";
            $aecolo = "000000";
            $aetipl = "3";
            $aetipl = "3";
            $aedesc = "GUIA DOBLE FELPUDO ESTANDAR";
        endif;

        $aerep = strtoupper($aerep);

        $clproc = "0";
        $clcodi = "0";
        $abspeed = "0";
        
        $consulta = "INSERT INTO GCWE100 (WENRO, WEEMPR, WEALMA, WENRR, WENORD, WETIPL, WEFAMI, WESUBF, WECOLO, WEARTI, WEDESC, WECAPR, WEANCH, WEALTO, WEMAND, WEREPL, WECC, WETOTM, WEVERT, WEINCL, WEPROC, WECODI, WESPED, WEID, WETECN, WEFCHE, WEHORA, WENPED) VALUES ('" . $wbnro . "', '" . empresa . "', '" . $aealma . "', '" . $aenrr . "', '" . $aenord . "', '" . $aetipl . "', '" . $aefami . "', '" . $aesubf . "', '" . $aecolo . "', '" . $aearti . "', '" . $aedesc . "', '" . $aecapr . "', '" . $aeanc . "', '" . $aealt . "', '" . $aeman . "', '" . $aerep . "', '" . $aecc . "', '" . $aetmt . "', '" . $aever . "', '" . $aeinc . "', '" . $clproc . "', '" . $clcodi . "', '" . $abspeed . "', '" . $id_pres . "', '" . $abtecn . "', '" . $wbfche . "', '" . $wbhora . "', '" . $wenped . "')";

        $consulta = utf8_decode($consulta);

        echo "<br>" . $consulta . "<br>";

    endif;
endwhile;

print_r($array_info_num_albaran);
print_r($array_info_albaran);
echo "TOTAL ALBARANES->" . $numero_albaran;
