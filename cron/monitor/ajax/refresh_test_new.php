<?php

require '../../../config.php';

ini_set("display_errors", 1);

function hideNumPre_aux($text, $clproc, $clcodi) {
    $text = str_replace('Ppto. MT. CL ', '', $text);
    $text = str_replace('Ped. MT. CL ', '', $text);
    $text = str_replace($clproc . $clcodi . '-', '', $text);
    $text = ltrim($text, '0');
    return $text;
}

function round_number_to_par_aux($number, $min_value = NULL) {
    if (substr($number, 3, 1) <= 5):
        $number = $number + '0.05';
    endif;
    $number = round($number, 1, PHP_ROUND_HALF_EVEN);
    $number = str_replace(array(",", "."), '', $number);
    if ($number == "1"):
        $number = "10";
    elseif ($number == "2"):
        $number = "20";
    else:
        if ($number % 2 != 0):
            $number++;
        endif;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

$fecha = date('d_m_Y');

//ELEGIR UNA OPCION U OTRA
$link = mysqli_connect("localhost", "root", "", "mosquiflex") or die('No se pudo conectar: ' . mysqli_error());

$query_presupuestos = mysqli_query($link, "SELECT *, DATE_FORMAT(fecha,'%d_%m_%Y') AS fecha2  FROM presupuestos HAVING fecha2 = '" . $fecha . "' AND grabado = '0' AND empresa = 'MT' ORDER BY fecha2");

$query_familia = mysqli_query($link, "SELECT fafami, fadesc, status, min_ran_anc, min_ran_alt, empresa FROM familias WHERE empresa = 'MT'");
$array_familia = array();
while ($familia = $query_familia->fetch_assoc()):
    $array_familia[$familia['fafami']] = $familia;
endwhile;

$query_albaranes = mysqli_query($link, "SELECT * FROM albaranes WHERE num_pre = '" . $_GET['id_pres'] . "' AND empresa = 'MT'");

$query_linea3 = mysqli_query($link, "SELECT * FROM albaranes WHERE num_pre = '" . $_GET['id_pres'] . "' AND empresa = 'MT' AND albobs = 'Linea3'");

$id_pedido = "";
$wbfche = date("Ymd");
$wbhora = date("His");
$hora_actual = date("Y-m-d H:i:s");
$aenrr = 0;
$cont = 0;
$cont_aux = 0;
$numero_albaran = 0;
$wenped = 1;
$wenped_aux = 1;
$id_pres = $_GET['id_pres'];
$aealma = "20";
$abtecn = "221";

$array_numero_albaran = array();
$array_codigo_producto = array();

$contador_productos = 0;

//Recorremos el array para asignar el número de albaran correspondiente al producto y guardar la relación linea producto con numero de albaran.
while ($albaran = $query_albaranes->fetch_assoc()):
    $aefami = $albaran['albfami'];
    if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true) AND empty($albaran['albobs']) || $aefami == "2736"):
        $contador_productos++;
        $buscado = $albaran['albfami'] . "-" . $albaran['albsub'] . "-" . $albaran['albco'];
        $array_codigo_producto[$contador_productos] = $buscado;
        $array_numero_albaran[$contador_productos]['linea_producto'] = $albaran['linea_producto'];
        if ($contador_productos >= 1):
            $posicion_buscado = array_search($buscado, $array_codigo_producto);
            if ($posicion_buscado !== $contador_productos):
                $wenped = $numero_albaran;
            else:
                $numero_albaran++;
                $wenped = $numero_albaran;
            endif;
            $array_numero_albaran[$albaran['linea_producto']]['albaran'] = $wenped;
        endif;
    endif;

endwhile;

print_r($array_numero_albaran);

echo "<br>";
echo "TOTAL PRODUCTO ACABADO->" . $contador_productos;
