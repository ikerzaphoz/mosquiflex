$(document).ready(function () {

    setInterval(function () {
        $.ajax({
            url: "ajax/refresh_2.php",
            success: function (result) {
                $(".content_monitor").append(result);
                $("html, body").animate({scrollTop: $(document).height()});
            },
            error: function (error) {
                var mensaje_error = "<span class='mensaje_error'>Error al ejecutar ajax: " + error + "</span>";
                $(".content_monitor").append(mensaje_error);
                $("html, body").animate({scrollTop: $(document).height()});
            }
        })
                ;
    }, 60000);

});